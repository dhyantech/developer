@include('admin.partials.header')
<body class="page-body  page-fade" data-url="http://neon.dev">
<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
    @include('admin.partials.sidebar')
    <div class="main-content">
        {{-- for top bar--}}
        @include('admin.partials.topbar')
        <hr />
        @yield('content')

        <!-- Footer -->
        @include('admin.partials.footer')
    </div>
</div>
@include('admin.partials.javascripts')
@yield('javascript')
</body>
</html>