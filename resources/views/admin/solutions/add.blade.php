@extends('admin.layout.default')

@section('title', 'Dashboard')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/admin/js/jvectormap/jquery-jvectormap-1.2.2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/js/rickshaw/rickshaw.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/admin/css/neon-forms.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/custom.css') }}">


@endsection
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="index.html"><i class="{{url('/admin')}}"></i>Home</a>
        </li>
        <li>

            <a href="{{url('/admin/solutions')}}">Solutions</a>
        </li>
        <li class="active">

            <strong>Add</strong>
        </li>
    </ol>

    <h2>Solutions</h2>
        <br />
    <div class="row">
        <div class="col-md-12">
            
            <div class="panel panel-primary" data-collapsed="0">
            
                <div class="panel-heading">
                    <div class="panel-title">
                        Add Property
                    </div>
                    
                    <!--<div class="panel-options">
                        <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                        <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                    </div>-->
                </div>
                
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-2">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="panel-body">
                    
                    <form role="form" method="post" action="{{url('admin/solutions/store')}}" class="form-horizontal form-groups-bordered" enctype="multipart/form-data">
                         {{ csrf_field() }}
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Title</label>
                            
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="txtPropertyNsmr" name="name" placeholder="Title" required="">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="field-3" class="col-sm-3 control-label">Caption</label>
                            
                            <div class="col-sm-9">
                                <textarea class="form-control ckeditor" name="description"></textarea>
                            </div>
                        </div>
                        
                        
                        
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                    
                </div>
            
            </div>
        
        </div>
    </div>
   
@endsection

@section('javascript')

   
    <script src="{{ asset('assets/admin/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/admin/js/fileinput.js') }}"></script>
    <script src="{{ asset('assets/admin/js/fileinput.js') }}"></script>
    

    <script src="{{ asset('assets/admin/js/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <!-- Imported scripts on this page -->
    <script src="{{ asset('assets/admin/js/jvectormap/jquery-jvectormap-europe-merc-en.js') }}"></script>
    <script src="{{ asset('assets/admin/js/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/rickshaw/vendor/d3.v3.js') }}"></script>
    <script src="{{ asset('assets/admin/js/rickshaw/rickshaw.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/raphael-min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/morris.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/toastr.js') }}"></script>
    <script src="{{ asset('assets/admin/js/neon-chat.js') }}"></script>


    <!-- JavaScripts initializations and stuff -->
    <script src="{{ asset('assets/admin/js/neon-custom.js') }}"></script>

    <!-- Demo Settings -->
    <script src="{{ asset('assets/admin/js/neon-demo.js') }}"></script>
@endsection
