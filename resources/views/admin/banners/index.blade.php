@extends('admin.layout.default')

@section('title', 'Dashboard')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/admin/js/jvectormap/jquery-jvectormap-1.2.2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/js/rickshaw/rickshaw.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/js/datatables/datatables.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/js/select2/select2.css') }}">

@endsection
@section('content')
    
     <ol class="breadcrumb bc-3">
        <li>
            <a href="index.html"><i class="{{url('/admin')}}"></i>Home</a>
        </li>
       
        <li class="active">

            <strong>Banners</strong>
        </li>
    </ol>

   <h2>Banners</h2>
        
        <br />
        
        <a href="{{url('admin/banners/add')}}" class="btn btn-primary">
            <i class="entypo-plus"></i>
            Add Row
        </a>
        <br />
        <br />
        <table class="table table-bordered datatable" id="table-1">
            <thead>
                <tr>
                    <th data-hide="phone">Name</th>
                    <th>Banner</th>
                   
                    
                    <th>Action</th>
                </tr>
            </thead>
            <tbody><?php
                $intCount = 0;
                ?>
                @foreach($banners as $rowBanner)<?php
                    
                    $intCount++;

                    if($intCount % 2 == 0){
                        $strOddEven = "even";
                    }
                    else{
                        $strOddEven = "odd";
                    }
                    ?><tr class="{{$strOddEven}}">
                        <td>{{$rowBanner['banner_title']}}</td>
                        
                        <td><img width="100" height="100" src="{{url('/')}}/uploads/banners/thumb/{{$rowBanner['banner_path']}}"/></td>
                        
                        <td class="center">
                            <a href="{{url('admin/banners/' . $rowBanner['id'] . '/edit')}}" class="btn btn-default btn-sm btn-icon icon-left">
                                <i class="entypo-pencil"></i>
                                Edit
                            </a>&nbsp;
                            
                            
                            <a href="{{url('admin/banners/' . $rowBanner['id'] . '/delete')}}" onclick="return confirm('Do you want to delete this item?');" class="btn btn-danger btn-sm btn-icon icon-left">
                                <i class="entypo-cancel"></i>
                                Delete
                            </a>

                        </td>
                       
                    </tr>
                @endforeach
                
            </tbody>
            <tfoot>
                <tr>
                    <th data-hide="phone">Name</th>
                    <th>Banner</th>
                   
                    
                    <th>Action</th>
                </tr>
            </tfoot>
        </table>
        <br />
        <a href="{{url('admin/banners/add')}}" class="btn btn-primary">
            <i class="entypo-plus"></i>
            Add Row
        </a>
        <br />
        
        
        
@endsection

@section('javascript')

    <script type="text/javascript">
        jQuery( document ).ready( function( $ ) {
            var $table1 = jQuery( '#table-1' );
            
            // Initialize DataTable
            $table1.DataTable( {
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bStateSave": true
            });
            
            // Initalize Select Dropdown after DataTables is created
            $table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
                minimumResultsForSearch: -1
            });
        } );
    </script>
    <script src="{{ asset('assets/admin/js/datatables/datatables.js') }}"></script>
    <script src="{{ asset('assets/admin/js/select2/select2.min.js') }}"></script>

    <script src="{{ asset('assets/admin/js/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <!-- Imported scripts on this page -->
    <script src="{{ asset('assets/admin/js/jvectormap/jquery-jvectormap-europe-merc-en.js') }}"></script>
    <script src="{{ asset('assets/admin/js/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/rickshaw/vendor/d3.v3.js') }}"></script>
    <script src="{{ asset('assets/admin/js/rickshaw/rickshaw.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/raphael-min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/morris.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/toastr.js') }}"></script>
    <script src="{{ asset('assets/admin/js/neon-chat.js') }}"></script>


    <!-- JavaScripts initializations and stuff -->
    <script src="{{ asset('assets/admin/js/neon-custom.js') }}"></script>

    <!-- Demo Settings -->
    <script src="{{ asset('assets/admin/js/neon-demo.js') }}"></script>
@endsection
