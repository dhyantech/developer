@extends('admin.layout.default')

@section('title', 'Dashboard')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/admin/js/jvectormap/jquery-jvectormap-1.2.2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/js/rickshaw/rickshaw.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/admin/css/neon-forms.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/custom.css') }}">


@endsection
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="index.html"><i class="{{url('/admin')}}"></i>Home</a>
        </li>
        <li>

            <a href="{{url('/admin/clients')}}">Clients</a>
        </li>
        <li class="active">

            <strong>Add</strong>
        </li>
    </ol>

    <h2>Clients</h2>
        <br />
    <div class="row">
        <div class="col-md-12">
            
            <div class="panel panel-primary" data-collapsed="0">
            
                <div class="panel-heading">
                    <div class="panel-title">
                        Add Client
                    </div>
                    
                    <!--<div class="panel-options">
                        <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                        <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                    </div>-->
                </div>
                
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-2">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="panel-body">
                    
                    <form role="form" method="post" action="{{url('admin/clients/store')}}" class="form-horizontal form-groups-bordered" enctype="multipart/form-data">
                         {{ csrf_field() }}
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Client Name</label>
                            
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="txtPropertyNsmr" name="client_title" placeholder="Client Name" required="">
                            </div>
                        </div>
                        
                       
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Image Upload</label>
                            
                            <div class="col-sm-5">
                                
                                <div class="fileinput fileinput-new" data-provides="fileinput"><input type="hidden">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                                        <img src="{{url(asset('assets/admin/images/240150.png'))}}" alt="...">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                                    <div>
                                        <span class="btn btn-white btn-file">
                                            <span class="fileinput-new">Select image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="client_image" accept="image/*">
                                        </span>
                                        <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                        
                        
                        
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                    
                </div>
            
            </div>
        
        </div>
    </div>
   
@endsection

@section('javascript')

   
    <script src="{{ asset('assets/admin/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/admin/js/fileinput.js') }}"></script>
    <script src="{{ asset('assets/admin/js/fileinput.js') }}"></script>
    

    <script src="{{ asset('assets/admin/js/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <!-- Imported scripts on this page -->
    <script src="{{ asset('assets/admin/js/jvectormap/jquery-jvectormap-europe-merc-en.js') }}"></script>
    <script src="{{ asset('assets/admin/js/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/rickshaw/vendor/d3.v3.js') }}"></script>
    <script src="{{ asset('assets/admin/js/rickshaw/rickshaw.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/raphael-min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/morris.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/toastr.js') }}"></script>
    <script src="{{ asset('assets/admin/js/neon-chat.js') }}"></script>


    <!-- JavaScripts initializations and stuff -->
    <script src="{{ asset('assets/admin/js/neon-custom.js') }}"></script>

    <!-- Demo Settings -->
    <script src="{{ asset('assets/admin/js/neon-demo.js') }}"></script>
@endsection
