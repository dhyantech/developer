<div class="sidebar-menu">

    <div class="sidebar-menu-inner">

        <header class="logo-env">

            <!-- logo -->
            <div class="logo">
                <a href="index.html">
                    <img src="{{ asset('assets/admin/images/logo2.png') }}" width="120" alt="" />
                </a>
            </div>

            <!-- logo collapse icon -->
            <div class="sidebar-collapse">
                <a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                    <i class="entypo-menu"></i>
                </a>
            </div>


            <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                    <i class="entypo-menu"></i>
                </a>
            </div>

        </header>


        <ul id="main-menu" class="main-menu">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
           
            <li>
                <a href="{{url('/admin/users')}}">
                    <i class="entypo-monitor"></i>
                    <span class="title">Users</span>
                </a>
            </li>

            <li>
                <a href="{{url('/admin/banners')}}">
                    <i class="entypo-monitor"></i>
                    <span class="title">Banners</span>
                </a>
            </li>

            <li>
                <a href="{{url('/admin/clients')}}">
                    <i class="entypo-monitor"></i>
                    <span class="title">Home Page Clients</span>
                </a>
            </li>

            <li>
                <a href="{{url('/admin/customers_category')}}">
                    <i class="entypo-monitor"></i>
                    <span class="title">Customers Category</span>
                </a>
            </li>

            <li>
                <a href="{{url('/admin/solutions')}}">
                    <i class="entypo-monitor"></i>
                    <span class="title">Solutions</span>
                </a>
            </li>

            <li>
                <a href="{{url('/admin/services')}}">
                    <i class="entypo-monitor"></i>
                    <span class="title">Services</span>
                </a>
            </li>

            <li>
                <a href="{{url('/admin/aboutUs')}}">
                    <i class="entypo-monitor"></i>
                    <span class="title">About Us</span>
                </a>
            </li>

           
            
        </ul>

    </div>

</div>