<!-- Bottom scripts (common) -->
<script src="{{ asset('assets/admin/js/jquery-1.11.3.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/gsap/TweenMax.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/bootstrap.js') }}"></script>
<script src="{{ asset('assets/admin/js/joinable.js') }}"></script>
<script src="{{ asset('assets/admin/js/resizeable.js') }}"></script>
<script src="{{ asset('assets/admin/js/neon-api.js') }}"></script>
