@extends('admin.layout.default')

@section('title', 'Dashboard')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/admin/js/jvectormap/jquery-jvectormap-1.2.2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/js/rickshaw/rickshaw.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/admin/css/neon-forms.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/custom.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/admin/js/dropzone/dropzone.css') }}">
    


@endsection
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="index.html"><i class="{{url('/admin')}}"></i>Home</a>
        </li>
        <li>

            <a href="{{url('/admin/customers_category')}}">Customers Category Gallery</a>
        </li>
        <li class="active">

            <strong>Gallery</strong>
        </li>
    </ol>

    <h2>Customers Category Gallery</h2>
        <br />
    <div class="row">
        <div class="col-md-12">
            
            <div class="panel panel-primary" data-collapsed="0">
            
                <div class="panel-heading">
                    <div class="panel-title">
                        Add Customers Category Gallery For {{$customersCategory[0]->name}}
                    </div>
                    
                    <!--<div class="panel-options">
                        <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                        <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                    </div>-->
                </div>
                
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-2">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="panel-body">
                    
                    
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="40%">Image</th>
                                <th>Delete</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2"></td>
                            </tr>
                            @foreach($customersCategory as $rowProperties)
                                @if(!empty($rowProperties->image_path))
                                    <tr>
                                        <td><img width="100" height="100" src="{{url('/')}}/uploads/customers_category/{{$rowProperties->image_path}}"/></td>
                                        <td>
                                                <a href="{{url('admin/customers_category/' . $rowProperties->id . '/deleteGallery/' . $rowProperties->galId)}}" onclick="return confirm('Do you want to delete this item?');" class="btn btn-danger btn-sm btn-icon icon-left">
                                                <i class="entypo-cancel"></i>
                                                Delete
                                        </a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tfoot>
                    </table>

                    <form action="{{url('admin/customers_category/gallery_store') . '/' . $customersCategory[0]->id}}" class="dropzone" id="dropzone_example">
                        <div class="fallback">
                            <input name="file" type="file" multiple />
                        </div>
                        {{ csrf_field() }}
                    </form>
                    <div id="dze_info" class="hidden">
        
                        <br />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">Dropzone Uploaded Files Info</div>
                            </div>
                            
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="40%">File name</th>
                                        <th width="15%">Size</th>
                                        <th width="15%">Type</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="4"></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    
                </div>
            
            </div>
        
        </div>
    </div>
   
@endsection

@section('javascript')

   
    <script src="{{ asset('assets/admin/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/admin/js/fileinput.js') }}"></script>
    <script src="{{ asset('assets/admin/js/dropzone/dropzone.js') }}"></script>

    

    <script src="{{ asset('assets/admin/js/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <!-- Imported scripts on this page -->
    <script src="{{ asset('assets/admin/js/jvectormap/jquery-jvectormap-europe-merc-en.js') }}"></script>
    <script src="{{ asset('assets/admin/js/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/rickshaw/vendor/d3.v3.js') }}"></script>
    <script src="{{ asset('assets/admin/js/rickshaw/rickshaw.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/raphael-min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/morris.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/toastr.js') }}"></script>
    <script src="{{ asset('assets/admin/js/neon-chat.js') }}"></script>


    <!-- JavaScripts initializations and stuff -->
    <script src="{{ asset('assets/admin/js/neon-custom.js') }}"></script>

    <!-- Demo Settings -->
    <script src="{{ asset('assets/admin/js/neon-demo.js') }}"></script>
@endsection
