@extends('layouts.app')

@section('content')
<style type="text/css">

  .bx-viewport{
    padding-bottom: 15px !important;
    height: auto !important;
  }
</style>
            <!-- Menu Bar End -->
            <!-- Page Content and Right Bar Begin -->
            <div class="tabs"></div>
            <div id="HomeMainContent">

<div id="home" class="content__item main-slider active">


  <div class="home-carousel">
    <div class="left">
      <ul><?php
        $revArrBanner = array();
        ?>@foreach($banners as $rowBanner)<?php

            $revArrBanner[] =  $rowBanner;
            ?><li style="background-image: url({{url('/')}}/uploads/banners/{{$rowBanner['banner_path']}});"></li>
           
        @endforeach


      </ul>
    </div><?php
        if(count($revArrBanner) > 0){
            $revArrBanner = array_reverse($revArrBanner);
        }
    ?><div class="right">
      <ul> 
        @foreach($revArrBanner as $rowBanner)
            <li style="background-image: url({{url('/')}}/uploads/banners/{{$rowBanner['banner_path']}});"></li>           
        @endforeach

      </ul>
    </div>
  </div>



</div>




</div>
  <div class="home-page-description">
    <ul>

      @foreach($banners as $rowBanner)
        <li>{{$rowBanner['description']}}</li>
      @endforeach
      
    </ul>

  </div>
<!-- Page Content and Right Bar End -->
<div id="client_scroll">

  <div class="call-us">
    Call Us <br>
    +91 9876543210

  </div>
  <div id="client_scroll_inner">
    <div id="owl-demo">
    @foreach($clients as $clientVal)

      <div class="slide"><img src="{{url('/')}}/uploads/clients/{{$clientVal['client_path']}}"></div>
    @endforeach


    </div>   </div>

  </div>

    <!--<div class="row welcome-content">
      <div class="col-md-12">
        <h2>Welcome to Italian Planters</h2>

        <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
<br><br>
          Why do we use it?
        <br><br>

          It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).



        </p>
      </div>

    </div>--><br/><br/>
    <div class='footer'>


        <div class="col-md-12 social-link">

                <p>
<i class="fa fa-facebook"></i><i class="fa fa-twitter"></i>
                </p>

                <p style="padding-bottom:10px;">
                  
Italian Planters, 
P.O. Box 113494, Dubai, UAE 
Phone: 04-346 8373 
Fax: 04-346 8323 
Email: sales@italianplanters.com


                </p>
        </div>

    </div>
@endsection

@section('javascript')
    

  <script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.2.min.js') }}"></script> <!-- <script type="text/javascript" src="jquery.bxslider/jquery.bxslider.js"></script>

    <link rel="stylesheet" type="text/css" href="jquery.bxslider/jquery.bxslider.css" /> -->
<script type="text/javascript" src="{{ asset('assets/js/jquery.carouFredSel-6.2.1.js') }}"></script>

    <script type='text/javascript'>

    $(function(){

      $('#owl-demo').carouFredSel({
    circular: true,            // Determines whether the carousel should be circular.
    infinite: true,
    scroll: {
            items: 1,
            duration: 6000,
            timeoutDuration: 0,
            easing: 'linear',
            pauseOnHover: 'immediate'
        }
  });



                      $('.home-page-description ul').bxSlider({
                        auto: true,
                        mode: 'horizontal',
                        controls:false,
                        autoControls:false,
                        speed:1100
                      });


      $("body").addClass("menu-expanded menu-active-0");
      $(".menu-nav-control").click(function() {
        $("body").toggleClass("menu-expanded");
      });
      //Menu Sea


    });


    </script>

    <script src="{{ asset('assets/p/start.js') }}"></script>


    <script type="text/javascript" src="{{ asset('assets/js/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
   <!--  <link rel="stylesheet" type="text/css" href="js/jquery.bxslider/jquery.bxslider.css" /> -->


<!--
    <script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
    <link rel="stylesheet" type="text/css" href="js/owl-carousel/owl.carousel.css" /> -->

    <script type="text/javascript">

    $(document).ready(function() {
  //
  //    $('#owl-demo').bxSlider({
  //
  //   mode:'horizontal',
  //   minSlides: 6,
  // maxSlides: 6,
  // slideWidth: 360,
  // slideMargin: 10,
  //     pager:false,
  //     controls:false,
  //     auto:true,
  //     moveSlides:1,
  //     infiniteLoop:true
  //   });




    });

    </script>
    <div class="contact-us">
      <img src="{{ asset('assets/images/contact-us.png') }}" />

    </div>

    <script >
    $(function(){
        $('.contact-us').on('click',function(){

        });
    });
    </script>

    <script type="text/javascript" src="{{ asset('assets/js/avgrund/jquery.avgrund.min.js') }}"></script>

    <script>
        $(function(){

          $('.contact-us img').avgrund({
            height: 200,
            holderClass: 'custom',
            showClose: true,
            showCloseText: 'Close',
            enableStackAnimation: true,
            onBlurContainer: '.container',
            template: '<h3>Contact Us</h3>'+
          '<form action="" method="post"><label>Name</label>'+
          '<input class="form-control" type="text" name="name" />'+
          '<label>E-mail</label>'+
          '<input class="form-control" type="text" name="email" />'+
          '<label>Mobile No.</label>'+
          '<input class="form-control" type="text" name="mobile_no" />'+
          '<label>Message</label>'+
          '<input class="form-control" type="text" name="message" />'+
          '<input class="btn btn-block btn-primary" type="submit" value="Submit" /></form> '  +

        '</div>'
          });

        });
    </script>
  
@endsection
