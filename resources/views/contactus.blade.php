@extends('layouts.app')

@section('content')
<style type="text/css">
.sub-page-content{
  width: 40%;
}
.container{
  margin-top: 150px;
}

.btn{
  z-index: 40;
}
.contact-us{
  position: fixed;
display: none !important; 
  cursor: pointer;
  right: 0px;
  top: 200px;
  z-index: 0;
}

.sub-page-images{
  
  top: 90px;
  
  right: 0px;
  width: 50%;
  position: fixed;
  height: 100%;
}
#bx-pager img{
  width: 100px;
}

.bx-wrapper,.bx-viewport,.sub-slider,.sub-slider li{
  height: 100% !important;

}
.bx-wrapper img{
  max-width: none;
  width: 100%;
  height: 100%;
}
#bx-pager{
  bottom: 10px;
  width: 100%;
  position: absolute;
  padding:3px;
  text-align: center;

}

#bx-pager img{
  border: 2px solid #fff;
}

.sub-page-images iframe{
  width: 100%;
  height: 100%;
}
#contact-section{
  height: 500px;
}

#location-section{
  
  height: 500px;
}

.show-location{
  display: none;
}

.hidden{
  display: none;
}
.move-nav{
  font-size: 32px;
}
.loc-nav{
  text-align: center;
}
</style>
<script src="https://use.fontawesome.com/2c9660f347.js"></script>
<div class="container" >
<div class="sub-page-content">
  <div id="contact-section" >
<h2>Contact Us</h2>

<form action="" method="post">
  <label>Name</label>
  <input class="form-control" type="text" name="name">  
  <label>E-mail</label>
  <input class="form-control" type="text" name="email">
  <label>Mobile No.</label>
  <input class="form-control" type="text" name="mobile_no">
  <label>Message</label>
  <textarea class="form-control" style="height:150px;" name="message"></textarea>
  <input class="btn  btn-primary" type="submit" value="Submit">
</form>
<p class="loc-nav">
<i class="fa fa-arrow-circle-o-down move-nav"></i>
  </p>
</div>

<div id="location-section"  class="hidden">
  <p class="loc-nav">
<i class="fa fa-arrow-circle-o-up move-nav"></i>
  </p>
  <h2>Location </h2>
<p>
<br>
Italian Planters, <br>
P.O. Box 113494, Dubai, UAE <br>
Phone: 04-346 8373 <br>
Fax: 04-346 8323 <br>
Email: sales@italianplanters.com
</p>
  </div>



</div>

</div>
<div class="sub-page-images">
<!-- Google Map  -->

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d24314.048989995972!2d55.215353246202376!3d25.157118993289227!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f6987e044af13%3A0x2e1bb35c187b101a!2sItalian+Planters!5e0!3m2!1sen!2sin!4v1470207465900" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

<!-- Google map over -->
</div>





  
@endsection

@section('javascript')

  <script type="text/javascript">
    $(function(){

       $(window).bind('mousewheel', function(e){

              if(e.originalEvent.wheelDelta /120 > 0) {
                    e.preventDefault();
              
             $('#contact-section').removeClass("show-location");
      $('#location-section').addClass("hidden");
             //     $.scrollTo($('#contact-section'), 1000);
              }
              else{     e.preventDefault();
             
                $('#contact-section').addClass("show-location");
        $('#location-section').removeClass("hidden");
                //$.scrollTo($('#location-section'), 1000);
               
              }

          });
      

    });
</script>
@endsection
