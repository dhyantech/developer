@include('admin.partials.header')
@section('title', 'Login')
<body class="page-body login-page login-form-fall" data-url="http://neon.dev">

<div class="login-container">

    <div class="login-header login-caret">

        <div class="login-content">

            <a href="{{ url('/') }}" class="logo">
                <img src="{{ asset('assets/admin/images/logo2.png') }}" width="120" alt="" />
            </a>

            <p class="description">Dear user, log in to access the admin area!</p>

            <!-- progress bar indicator -->
            <div class="login-progressbar-indicator">
                <h3>43%</h3>
                <span>logging in...</span>
            </div>
        </div>

    </div>

    <div class="login-progressbar">
        <div></div>
    </div>

    <div class="login-form">

        <div class="login-content">
            
        <div class="form-login-error" >
            <h3>Invalid login</h3>
            <!--<p>Enter <strong>demo</strong>/<strong>demo</strong> as login and password.</p>-->
        </div>
           

            
            

            <form method="post" role="form" action="{{ url('/login') }}" id="form_login">
                {{ csrf_field() }}
                <div class="form-group">

                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-user"></i>
                        </div>

                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" autocomplete="off" />
                    </div>

                </div>

                <div class="form-group">

                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-key"></i>
                        </div>

                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
                    </div>

                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login">
                        <i class="entypo-login"></i>
                        Login In
                    </button>
                </div>

            </form>


            <div class="login-bottom-links">

                <!--<a href="{{ url('/password/reset') }}" class="link">Forgot your password?</a>

                <br />

                <a href="#">ToS</a>  - <a href="#">Privacy Policy</a>-->

            </div>
        </div>
    </div>
</div>
@include('admin.partials.javascripts')
<script>
    baseurl = "{{ url('/login') }}";
</script>
<script src="{{ asset('assets/admin/js/jquery.validate.min.js') }}"></script>

<script src="{{ asset('assets/admin/js/neon-login.js') }}"></script>
<!-- JavaScripts initializations and stuff -->
<script src="{{ asset('assets/admin/js/neon-custom.js') }}"></script>
<!-- Demo Settings -->
<script src="{{ asset('assets/admin/js/neon-demo.js') }}"></script>

@if ($errors->has('email') || $errors->has('password'))
    <script>
        $(".form-login-error").css('display', 'block');
         @foreach ($errors->all() as $error)
                                    alert('{{$error}}');
                                @endforeach
    </script>
@endif
</body>
</html>