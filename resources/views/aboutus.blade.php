@extends('layouts.app')

@section('content')
<style type="text/css">
.sub-page-content{

}
.container{
    margin-top: 150px;
}
.sub-page-images{
    
    top: 0px;
    /*top: 91px;*/
    right: 0px;
    width: 50%;
    position: absolute;
    height: 100%;
}
#bx-pager img{
    width: 100px;
}

.bx-wrapper,.bx-viewport,.sub-slider,.sub-slider li{
    height: 100% !important;

}
.bx-wrapper img{
    max-width: none;
    width: 100%;
    height: 100%;
}
#bx-pager{
    bottom: 10px;
    width: 100%;
    position: absolute;
    padding:3px;
    text-align: center;

}

#bx-pager img{
    border: 2px solid #fff;
}
</style>
           <div class="container" >
<div class="sub-page-content">
<h2>{{$aboutUsRow[0]->name}}</h2>

<br/>
{!!$aboutUsRow[0]->description!!}





</div>

</div>
<div class="sub-page-images">

    <ul class="sub-slider">
        @foreach($aboutUsRow as $aboutUsRowVal)
            @if(!empty($aboutUsRowVal->image_path))
                <li><img src="{{url('/')}}/uploads/aboutus/{{str_replace('/thumb', '', $aboutUsRowVal->image_path)}}" /></li>
            @endif
        @endforeach
     
      
    </ul>

    <div id="bx-pager"><?php
        $intCount = 0;
        ?>@foreach($aboutUsRow as $key => $aboutUsRowVal)
            @if(!empty($aboutUsRowVal->image_path))
                <a data-slide-index="{{$intCount}}" href=""><img src="{{url('/')}}/uploads/aboutus/{{$aboutUsRowVal->image_path}}" /></a><?php
                 $intCount++;
            ?>@endif
        @endforeach
      </div>

</div>
@endsection

@section('javascript')
    

  <script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.2.min.js') }}"></script> <!-- <script type="text/javascript" src="jquery.bxslider/jquery.bxslider.js"></script>

    <link rel="stylesheet" type="text/css" href="jquery.bxslider/jquery.bxslider.css" /> -->
<script type="text/javascript" src="{{ asset('assets/js/jquery.carouFredSel-6.2.1.js') }}"></script>

    <script type='text/javascript'>

    $(function(){

      $('#owl-demo').carouFredSel({
    circular: true,            // Determines whether the carousel should be circular.
    infinite: true,
    scroll: {
            items: 1,
            duration: 8000,
            timeoutDuration: 0,
            easing: 'linear',
            pauseOnHover: 'immediate'
        }
  });



                      $('.home-page-description ul').bxSlider({
                        auto: true,
                        mode: 'horizontal',
                        controls:false,
                        autoControls:false,
                        speed:1100
                      });


      $("body").addClass("menu-expanded menu-active-0");
      $(".menu-nav-control").click(function() {
        $("body").toggleClass("menu-expanded");
      });
      //Menu Sea


    });


    </script>

    <script src="{{ asset('assets/p/start.js') }}"></script>


    <script type="text/javascript" src="{{ asset('assets/js/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
   <!--  <link rel="stylesheet" type="text/css" href="js/jquery.bxslider/jquery.bxslider.css" /> -->


<!--
    <script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
    <link rel="stylesheet" type="text/css" href="js/owl-carousel/owl.carousel.css" /> -->

    <script type="text/javascript">

    $(document).ready(function() {
  //
  //    $('#owl-demo').bxSlider({
  //
  //   mode:'horizontal',
  //   minSlides: 6,
  // maxSlides: 6,
  // slideWidth: 360,
  // slideMargin: 10,
  //     pager:false,
  //     controls:false,
  //     auto:true,
  //     moveSlides:1,
  //     infiniteLoop:true
  //   });




    });

    </script>
    <div class="contact-us">
      <img src="{{ asset('assets/images/contact-us.png') }}" />

    </div>

    <script >
    $(function(){
        $('.contact-us').on('click',function(){

        });
    });
    </script>

    <script type="text/javascript" src="{{ asset('assets/js/avgrund/jquery.avgrund.min.js') }}"></script>

    <script>
        $(function(){

          $('.contact-us img').avgrund({
            height: 200,
            holderClass: 'custom',
            showClose: true,
            showCloseText: 'Close',
            enableStackAnimation: true,
            onBlurContainer: '.container',
            template: '<h3>Contact Us</h3>'+
          '<form action="" method="post"><label>Name</label>'+
          '<input class="form-control" type="text" name="name" />'+
          '<label>E-mail</label>'+
          '<input class="form-control" type="text" name="email" />'+
          '<label>Mobile No.</label>'+
          '<input class="form-control" type="text" name="mobile_no" />'+
          '<label>Message</label>'+
          '<input class="form-control" type="text" name="message" />'+
          '<input class="btn btn-block btn-primary" type="submit" value="Submit" /></form> '  +

        '</div>'
          });

        });
    </script>

    <script type="text/javascript">
    
        $('.sub-slider').bxSlider({
          pagerCustom: '#bx-pager',
          mode:'fade'
        });
</script>
  
@endsection
