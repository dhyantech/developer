<!doctype html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" href="{{ asset('assets//images.logo.jpg') }}" type="image/x-icon" />
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" />

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  <meta name="description" content="" />
  <meta name="keywords" content="middle east landscape planters" />
  <link rel="canonical" href="http://www.italianplanters.com/" />
  <link rel="shortlink" href="http://www.italianplanters.com/" />
  <meta property="og:type" content="website" />
  <meta property="og:site_name" content="Italian Planters" />
  <meta property="og:url" content="http://www.italianplanters.com/" />
  <meta property="og:title" content="Italian Planters | Middle East Landscape Planters" />
  <meta property="og:description" content="Italian Planters provides quality services for Multi National Companies in Middle East" />
  

  <title>  Italian Planters  </title>

  <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css') }}" media="all" />
  <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/css_wb46vVHYHeOx30E5-7fZUBrfkBK3asu1mnKwMYFI6kg.css') }}" media="all" />
  <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/css_MnXiytJtb186Ydycnpwpw34cuUsHaKc80ey5LiQXhSY.css') }}" media="all" />
  <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/css_CUHg1BeyxlWWK_hTimW_J229iuFC8Ua9-efd4jcvXSo.css') }}" media="all" />
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="{{ asset('assets/css/extras.css') }}" type="text/css" />
  <script type="text/javascript" src="{{ asset('assets/js/modernizr.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/js/avgrund/style/avgrund.css') }}" />
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:700,400' rel='stylesheet' type='text/css'>
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/p/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->


  <style type="text/css">
  .caroufredsel_wrapper{
    width: 100% !important; 
  }

.social-link{
  text-align: center;

}
.footer .social-link i{
  font-size: 22px;
  line-height: 50px;
  margin: 10px;
}
  body.avgrund-active{
    webkit-transform: none !important;
  -moz-transform: none !important;
  -ms-transform: none !important;
  -o-transform: none !important;
   transform: none !important;

  }

body{
  overflow-x: hidden !important;
}
  #Header, body.not-front.menu-active-1.menu-expanded #Header{
    z-index: 50;
  }
  .avgrund-active .avgrund-overlay{
    z-index: 99;
  }
  .avgrund-popin{
    padding-top: 25px !important;
    height: 390px !important;
    top:30% !important;
  }
  .btn-primary{
    background: #ccc  !important;
    border: none !important;
    color: #333 !important;
    margin-top: 10px;

  }
  label{
    margin-top: 10px !important;
    color: #333 !important;
    font-weight: 400;
  }
  .avgrund-popin h3{
    font-weight: 300;
  }
  .contact-us{

  }
  body{
    padding-top: 0px !important;
  }


  #HomeMobileBanner ul li .home-banner-container {
    width: 800px;
    /* margin-left: -50%; */
    position: absolute;
    top: 161px;
    background: none;
    left: 50%;
    margin: 0;
    margin-left: -400px;
    padding: 35px;
    z-index: 99;
  }

  #HomeMobileBanner .home-banner-content-wrapper p.home-banner-dec{
    color: #fff;
    text-align: center;
    text-transform: uppercase;
    font-size: 38px !important;
    font-weight: 800;
    font-family: 'Open Sans', sans-serif;


  }
  #HomeMobileBanner .home-banner-content-wrapper p.home-banner-dec-small{





    text-align: center;
    color: #ccc;
  }
  #HomeMobileBanner .home-banner-content-wrapper p.home-banner-dec-small span {
    padding: 5px 40px;
    border: 1px solid #8c8787;
    background: rgba(0,0,0,0.5);
    color: #fff;
    border-radius: 4px;
    text-transform: uppercase;
    font-weight: 300;
    font-family: 'Open Sans', sans-serif;

  }

  #HomeMobileBanner .home-banner-content-wrapper p.home-banner-dec-small span:hover{
    text-decoration: underline;
  }


  #Header .main-menu .menu-block-wrapper ul li
  {
    width: 322px;
  }

  .fd1{
    min-height:224px !important;
  }
  .fd3{
    min-height:120px !important;
  }

  #Header .main-menu ul
  {
    padding-right: 84px;
  }
#Header .main-menu ul li .fulldrop{
  min-height: 110px;
}

.cfd{
  min-height: 268px;
}
 

  .title_logo{
    /*margin-top:50px;*/
    font-family: 'PT Sans', sans-serif;
    text-transform: uppercase;
  }
  .title_logo h3{
    font-weight: 600;
  }

  #client_scroll_inner{

    background:#f1f1f1;
    height: 98px;
  }

.menu li:nth-child(odd){

  border-right: 1px solid #fff;

}

.menu .part-one li{
  border-right:none ;
}

.contact-us{
  position: absolute;
  cursor: pointer;
  right: 0px;
  top: 200px;
  z-index: 46;
}
  /* New Styles */

  .footer ,.footer  h3,.footer p{
    text-align: center;

  }
  .social-link {
    background: #046a79;
    color: #fff;
  }
  .footer i{
    width: 50px ;
    color: #fff;
    border: 2px solid #fff;
    border-radius: 25px;
    height: 50px;
    cursor:pointer;

  }
  .footer i:hover{

      border-color: #b5b5b5;
      color: #b5b5b5 !important;
  }

  .welcome-content{
    text-align: center;
  margin: 40px 0px;
  }
.welcome-content p{
  margin: 0px 100px;
}

.about-drop{
  width: 645px !important;
}
.home-page-description {
  position: absolute;
  z-index: 56;
  background: rgba(4,106,121,0.1);
  
  font-weight: 600;
  text-transform: uppercase;
  font-size: 42px;
  line-height: 45px;
  width: 100%;
  top:60%;
}

.home-page-description ul {
 text-align: center !important;
  }
  .home-page-description li{
  float: left;
  

  }

  .bx-wrapper .bx-viewport{
    left: 0px !important;
    background: none !important;
    border: none !important;
color: #fff !important;


    -moz-box-shadow: 0 0 0px #ccc !important;
    -webkit-box-shadow: 0 0 0px #ccc !important;
    box-shadow: 0 0 0px #ccc !important;

  }

  .bx-wrapper .bx-pager.bx-default-pager a{
    background: #ccc !important;
  }
  .bx-wrapper .bx-pager.bx-default-pager a:hover, .bx-wrapper .bx-pager.bx-default-pager a.active{
    background: #fff !important;
  }
  .bx-wrapper .bx-prev{
    left:50px !important;
  }

  .bx-wrapper .bx-next{
    right:50px !important;
  }

  .second-menu{
    position: absolute  !important;
   z-index: 101;
    margin-top: -20px !important;
    margin-left: 40px !important;
    display: none;
    background: #464546 !important;
    padding-bottom: 15px;
    width: 200px;
  }
li:hover>.second-menu{
  opacity:  1 !important;
    visibility: visible  !important;

    display: block;
}
.serv-full{
  overflow: visible !important;
}
.serv-full .menu>li{
  position: relative;
}
.serv-full .menu>li:hover{
  background: #000 !important;
}
.second-menu li,.second-menu li:hover{
  width: 100% !important;
    border: none !important;
}

.fulldrop{
z-index: 100;
}
header{
  z-index: 99;
}
  </style>

  <link rel="stylesheet" href="{{ asset('assets/p/main.css') }}" media="screen, projection" >
  <link rel="stylesheet" href="{{ asset('assets/p/style.css') }}">

  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,800' rel='stylesheet' type='text/css'>

</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-page" >

  <header id="Header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 logo">
          <div class="region region-logo">
            <div id="block-block-1" class="block block-block">
              <div class="content">
                <a href="/">
                  <img class="ip_logo" src="{{ asset('assets/images/logo.jpg') }}"   />
                  <div  class="title_logo">
                    <h3>      Italian Planters </h3><span>
                        Integrated Solutions for <br>Landscape Services </span>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <a href="javascript:void(0)" class="nav-toggle menu-nav-control">
            <span>&nbsp;</span>
          </a>

          <div class="col-xs-12 col-sm-9 col-md-9 col-lg-10 main-menu">
            <ul>
              <li class="dropdown">
                <a href="javascript:void(0)" class="reg-pointer">Customer Category</a>
                <div class="fulldrop fd1">
                  <div class="region region-industries-menu">
                    <div id="block-menu-block-1" class="block block-menu-block">


                      <div class="content">
                        <div class="menu-block-wrapper menu-block-1 menu-name-menu-industries-menu parent-mlid-0 menu-level-1">
                          <ul class="menu">

                            {!!$strMenue!!}
                           
                          </ul>
                        </div>
                    </div>


                  </div>
                </div>
              </div>
            </li>
                <li class="dropdown">
                  <a href="javascript:void(0)" class="reg-pointer">SERVICES</a>
                  <div class="fulldrop">
                    <div class="region region-services-menu">
                      <div id="block-menu-block-2" class="block block-menu-block">


                        <div class="content">
                          <div class="menu-block-wrapper menu-block-2 menu-name-menu-services-menu parent-mlid-0 menu-level-1">
                            <ul class="menu"><li class="first leaf menu-mlid-786"><a href="coming-soon.html">Design – Concept & Details  </a></li>
                              <li class="leaf menu-mlid-785"><a href="coming-soon.html">Project – Supply & Installation</a></li>
                              <li class="leaf menu-mlid-1273"><a href="coming-soon.html">Annual Maintenance Service – AMC</a></li>
                            </ul></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="dropdown solutions-custom">
                    <a href="products.html">Products & Solutions</a>
                    <div class="fulldrop fd3">
                      <div class="region region-solutions-menu">
                        <div id="block-menu-block-3" class="block block-menu-block">


                          <div class="content">
                            <div class="menu-block-wrapper menu-block-3 menu-name-menu-solutions-menu parent-mlid-0 menu-level-1">
                              <ul class="menu">
                                  {!!$strSolutionsMenue!!}

                              </ul></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="dropdown">
                      <a href="javascript:void(0)" class="reg-pointer">About us</a>
                     
                    </li>
                    <li>
                      <a href="{{url('contactus')}}" class="reg-pointer">Contact Us</a>
                    </li>
                    <!--<li>
                      <a href="coming-soon.html" class="reg-pointer">Blog</a>
                    </li>-->



                    </ul>
                  </div>

                </div>
              </div>
    </header>
    <body>

    @yield('content')




  <script type="text/javascript" src="{{asset('assets/js/jquery-1.11.2.min.js') }}"></script> <!-- <script type="text/javascript" src="jquery.bxslider/jquery.bxslider.js"></script>

    <link rel="stylesheet" type="text/css" href="jquery.bxslider/jquery.bxslider.css" /> -->
<script type="text/javascript" src="{{asset('assets/js/jquery.carouFredSel-6.2.1.js') }}"></script>

    <script type='text/javascript'>

    $(function(){

      $('#owl-demo').carouFredSel({
    circular: true,            // Determines whether the carousel should be circular.
    infinite: true,
    scroll: {
            items: 1,
            duration: 8000,
            timeoutDuration: 0,
            easing: 'linear',
            pauseOnHover: 'immediate'
        }
  });



                      $('.home-page-description ul').bxSlider({
                        auto: true,
                        mode: 'horizontal',
                        controls:false,
                        autoControls:false,
                        speed:1100
                      });


      $("body").addClass("menu-expanded menu-active-0");
      $(".menu-nav-control").click(function() {
        $("body").toggleClass("menu-expanded");
      });
      //Menu Sea


    });


    </script>

    <script src="{{asset('assets/p/start.js') }}"></script>


    <script type="text/javascript" src="{{asset('assets/js/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
   <!--  <link rel="stylesheet" type="text/css" href="js/jquery.bxslider/jquery.bxslider.css" /> -->


<!--
    <script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
    <link rel="stylesheet" type="text/css" href="js/owl-carousel/owl.carousel.css" /> -->

    <script type="text/javascript">

    $(document).ready(function() {
  //
  //    $('#owl-demo').bxSlider({
  //
  //   mode:'horizontal',
  //   minSlides: 6,
  // maxSlides: 6,
  // slideWidth: 360,
  // slideMargin: 10,
  //     pager:false,
  //     controls:false,
  //     auto:true,
  //     moveSlides:1,
  //     infiniteLoop:true
  //   });




    });

    </script>
    <div class="contact-us">
      <img src="{{asset('assets/images/contact-us.png') }}" />

    </div>

    <script >
    $(function(){
        $('.contact-us').on('click',function(){

        });
    });
    </script>

    <script type="text/javascript" src="{{asset('assets/js/avgrund/jquery.avgrund.min.js') }}"></script>

    <script>
        $(function(){

          $('.contact-us img').avgrund({
            height: 200,
            holderClass: 'custom',
            showClose: true,
            showCloseText: 'Close',
            enableStackAnimation: true,
            onBlurContainer: '.container',
            template: '<h3>Contact Us</h3>'+
          '<form action="" method="post"><label>Name</label>'+
          '<input class="form-control" type="text" name="name" />'+
          '<label>E-mail</label>'+
          '<input class="form-control" type="text" name="email" />'+
          '<label>Mobile No.</label>'+
          '<input class="form-control" type="text" name="mobile_no" />'+
          '<label>Message</label>'+
          '<input class="form-control" type="text" name="message" />'+
          '<input class="btn btn-block btn-primary" type="submit" value="Submit" /></form> '  +

        '</div>'
          });

        });
    </script>
  


    @yield('javascript')

    </body>
  </html>