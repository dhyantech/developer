/*!
 * jQuery.scrollTo
 * Copyright (c) 2007-2014 Ariel Flesler - aflesler<a>gmail<d>com | http://flesler.blogspot.com
 * Licensed under MIT
 * http://flesler.blogspot.com/2007/10/jqueryscrollto.html
 * @projectDescription Easy element scrolling using jQuery.
 * @author Ariel Flesler
 * @version 1.4.13
 */
;
(function (define) {
	'use strict';

	define(['jquery'], function ($) {

		var $scrollTo = $.scrollTo = function (target, duration, settings) {
			return $(window).scrollTo(target, duration, settings);
		};

		$scrollTo.defaults = {
			axis: 'xy',
			duration: parseFloat($.fn.jquery) >= 1.3 ? 0 : 1,
			limit: true
		};

		// Returns the element that needs to be animated to scroll the window.
		// Kept for backwards compatibility (specially for localScroll & serialScroll)
		$scrollTo.window = function (scope) {
			return $(window)._scrollable();
		};

		// Hack, hack, hack :)
		// Returns the real elements to scroll (supports window/iframes, documents and regular nodes)
		$.fn._scrollable = function () {
			return this.map(function () {
				var elem = this,
					isWin = !elem.nodeName || $.inArray(elem.nodeName.toLowerCase(), ['iframe', '#document', 'html', 'body']) != -1;

				if (!isWin)
					return elem;

				var doc = (elem.contentWindow || elem).document || elem.ownerDocument || elem;

				return /webkit/i.test(navigator.userAgent) || doc.compatMode == 'BackCompat' ?
					doc.body :
					doc.documentElement;
			});
		};

		$.fn.scrollTo = function (target, duration, settings) {
			if (typeof duration == 'object') {
				settings = duration;
				duration = 0;
			}
			if (typeof settings == 'function')
				settings = { onAfter: settings };

			if (target == 'max')
				target = 9e9;

			settings = $.extend({}, $scrollTo.defaults, settings);
			// Speed is still recognized for backwards compatibility
			duration = duration || settings.duration;
			// Make sure the settings are given right
			settings.queue = settings.queue && settings.axis.length > 1;

			if (settings.queue)
			// Let's keep the overall duration
				duration /= 2;
			settings.offset = both(settings.offset);
			settings.over = both(settings.over);

			return this._scrollable().each(function () {
				// Null target yields nothing, just like jQuery does
				if (target == null) return;

				var elem = this,
					$elem = $(elem),
					targ = target, toff, attr = {},
					win = $elem.is('html,body');

				switch (typeof targ) {
					// A number will pass the regex
					case 'number':
					case 'string':
						if (/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ)) {
							targ = both(targ);
							// We are done
							break;
						}
						// Relative/Absolute selector, no break!
						targ = win ? $(targ) : $(targ, this);
						if (!targ.length) return;
					case 'object':
						// DOMElement / jQuery
						if (targ.is || targ.style)
						// Get the real position of the target
							toff = (targ = $(targ)).offset();
				}

				var offset = $.isFunction(settings.offset) && settings.offset(elem, targ) || settings.offset;

				$.each(settings.axis.split(''), function (i, axis) {
					var Pos = axis == 'x' ? 'Left' : 'Top',
						pos = Pos.toLowerCase(),
						key = 'scroll' + Pos,
						old = elem[key],
						max = $scrollTo.max(elem, axis);

					if (toff) {// jQuery / DOMElement
						attr[key] = toff[pos] + ( win ? 0 : old - $elem.offset()[pos] );

						// If it's a dom element, reduce the margin
						if (settings.margin) {
							attr[key] -= parseInt(targ.css('margin' + Pos)) || 0;
							attr[key] -= parseInt(targ.css('border' + Pos + 'Width')) || 0;
						}

						attr[key] += offset[pos] || 0;

						if (settings.over[pos])
						// Scroll to a fraction of its width/height
							attr[key] += targ[axis == 'x' ? 'width' : 'height']() * settings.over[pos];
					} else {
						var val = targ[pos];
						// Handle percentage values
						attr[key] = val.slice && val.slice(-1) == '%' ?
							parseFloat(val) / 100 * max
							: val;
					}

					// Number or 'number'
					if (settings.limit && /^\d+$/.test(attr[key]))
					// Check the limits
						attr[key] = attr[key] <= 0 ? 0 : Math.min(attr[key], max);

					// Queueing axes
					if (!i && settings.queue) {
						// Don't waste time animating, if there's no need.
						if (old != attr[key])
						// Intermediate animation
							animate(settings.onAfterFirst);
						// Don't animate this axis again in the next iteration.
						delete attr[key];
					}
				});

				animate(settings.onAfter);

				function animate(callback) {
					$elem.animate(attr, duration, settings.easing, callback && function () {
						callback.call(this, targ, settings);
					});
				}
			}).end();
		};

		// Max scrolling position, works on quirks mode
		// It only fails (not too badly) on IE, quirks mode.
		$scrollTo.max = function (elem, axis) {
			var Dim = axis == 'x' ? 'Width' : 'Height',
				scroll = 'scroll' + Dim;

			if (!$(elem).is('html,body'))
				return elem[scroll] - $(elem)[Dim.toLowerCase()]();

			var size = 'client' + Dim,
				html = elem.ownerDocument.documentElement,
				body = elem.ownerDocument.body;

			return Math.max(html[scroll], body[scroll]) - Math.min(html[size], body[size]);
		};

		function both(val) {
			return $.isFunction(val) || typeof val == 'object' ? val : { top: val, left: val };
		}

		// AMD requirement
		return $scrollTo;
	})
}(typeof define === 'function' && define.amd ? define : function (deps, factory) {
		if (typeof module !== 'undefined' && module.exports) {
			// Node
			module.exports = factory(require('jquery'));
		} else {
			factory(jQuery);
		}
	}));


// 2. This code loads the IFrame Player API code asynchronously.

/*var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);*/

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
$(window).load(function () {
	setTimeout(function () {
		$('.preloader').fadeOut();
	}, 1000);
});

$(document).ready(function () {

	$('.img-open').on('click', function (e) {
		e.preventDefault();
		$('body').addClass('drop-box');
		//$('.hidden').fadeIn( "slow" );
	});
	$('.fancybox-close').on('click', function (e) {
		e.preventDefault();
		$('body').removeClass('drop-box');
		//$('.hidden').fadeOut( "slow" );
	});
	$(document).scrollTop(0);
	$(window).resize(function () {
		var wh = $(window).height();
		$('.right-menu').height(wh);
		$('.home-carousel li').height(wh);
		$('.about-main').height(wh - 180);
		$('.main-error').height(wh - 97);
		$('.sub-menu').height(wh);
	}).resize();

	$('.sub-icons').on('click', function (e) {
		e.preventDefault();
		$('body').addClass('menu-open');
	});

	$('.btn-contact').on('click', function (e) {
		e.preventDefault();
		$('body').addClass('contact-open');
	});

	$('.contact-form .btn').on('click', function (e) {
		e.preventDefault();
		/*//form submit
		//$(this).closest('form').submit();
		$('body').addClass('popup-open');
		$('.popup-thx').addClass('active');
		setTimeout(function () {
			$('body').removeClass('popup-open');
			$('.popup-thx').removeClass('active');
		}, 3000);*/
	});






	if ($('.no-touch').length) {
		var s = skrollr.init({
			forceHeight: false
		});
	}







	//layers animation
	var scrollAllowed = true;

	var homepageSlide = function (e) {

		if ($('body.menu-open').length) {
			return;
		}
		if (!scrollAllowed) {
			return;
		}


		scrollAllowed = false;
		var delta = e.deltaY * e.deltaFactor;
		var el;
		var current = $('.content__item.active');
		var horizontalSection = $('.horizon-slide').hasClass('active');
		var horizontalActive;

		if (delta > 0) {
			if (horizontalSection) {
				horizontalActive = $('.horizon-slide.active .item.active:not(.left)');
				if (horizontalActive.prev().length) {
					horizontalActive.removeClass('active');
					horizontalActive.prev().removeClass('left');
					setTimeout(function () {
						scrollAllowed = true;
					}, 1100);
					return;
				}
			}
			if (current.prev().length) {
				el = current.prev();
			} else {
				scrollAllowed = true;
				return;
			}
		} else {
			if (horizontalSection) {
				horizontalActive = $('.horizon-slide.active .item.active:not(.left)');
				if (horizontalActive.next().length) {
					horizontalActive.addClass('left');
					horizontalActive.next().addClass('active');
					setTimeout(function () {
						scrollAllowed = true;
					}, 1100);
					return;
				}
			}
			if (current.next().length) {
				el = current.next();
			} else {
				scrollAllowed = true;
				return;
			}
		}

		if (el.length) {
			el.addClass('active');

			current.not(el).addClass('top');
			setTimeout(function () {
				$('.content__item.top').removeClass('top active');
			}, 1000);
			setTimeout(function () {
				scrollAllowed = true;
			}, 1100);
		} else {
			scrollAllowed = true;
		}
	};

	var homePage = $('.home-page');

	if (homePage.length) {

		homePage.on('mousewheel', homepageSlide);

		$('.horizon-slide .arrow_right').on('click', function (e) {
			e.preventDefault();
			var horizontalActive = $('.horizon-slide .item.active:not(.left)');
			if (horizontalActive.next().length) {
				horizontalActive.addClass('left');
				horizontalActive.next().addClass('active');
				setTimeout(function () {
					scrollAllowed = true;
				}, 1100);
				return;
			}
		});

		$('.horizon-slide .arrow_left').on('click', function (e) {
			e.preventDefault();
			var horizontalActive = $('.horizon-slide .item.active:not(.left)');
			if (horizontalActive.prev().length) {
				horizontalActive.removeClass('active');
				horizontalActive.prev().removeClass('left');
				setTimeout(function () {
					scrollAllowed = true;
				}, 1100);
				return;
			}
		});


	}

	$(window).resize(function () {
		if ($('.slide'))
			$('.slide ul li').height($(window).height() - 97);
	}).resize();

	$(window).load(function () {

		var homeSlider = $('.slide ul');
		if (homeSlider.children().length > 1) {
			homeSlider.on('initialized.owl.carousel', function () {
				homeSlider.css("opacity", 1);
			});

			homeSlider.owlCarousel({
				margin: 0,
				loop: true,
				nav: true,
				autoplay:true,
				mouseDrag: false,
				lazyLoad:true,
				autoplayTimeout: 5000,
				smartSpeed: 1500,
				items: 1
			});
		} else {
			homeSlider.css("opacity", 1);
		}
		var blogSlider = $('.carousel-article_i ul');
		if (blogSlider.children().length > 1) {
			blogSlider.on('initialized.owl.carousel', function () {
				blogSlider.css("opacity", 1);
			});

			blogSlider.owlCarousel({
				margin: 0,
				loop: true,
				nav: true,

				lazyLoad:true,

				smartSpeed: 1500,
				items: 1
			});
		} else {
			blogSlider.css("opacity", 1);
		}


	});

	$(function () {
		/*rotation carousel size fix*/
		var doit;

		function resizedw() {
			$(".slide ul, .carousel-article_i ul, .slider__i ul").each(function () {
				$(this).trigger('configuration', ['debug', false, true]);
			});
		}

		$(window).resize(function () {
			clearTimeout(doit);
			doit = setTimeout(function () {
				resizedw();
			}, 500);
		});
		window.addEventListener("orientationchange", function () {
			clearTimeout(doit);
			doit = setTimeout(function () {
				resizedw();
			}, 500);
		}, false);
	});

	$('.section6 .top a, .section-competition .top a').on('click', function (e) {
		e.preventDefault();
		$.scrollTo(0, 800)
	});

});


var translateXY = function (el, positionX, positionY) {
	positionX || (positionX = 0);
	positionY || (positionY = 0);
	el.css({
		'-webkit-transform': 'translate3d(' + positionX + ', ' + positionY + ', 0) ',
		'-moz-transform': 'translate3d(' + positionX + ', ' + positionY + ', 0) ',
		'-ms-transform': 'translate3d(' + positionX + ', ' + positionY + ', 0)',
		'-o-transform': 'translate3d(' + positionX + ', ' + positionY + ', 0)',
		'transform': 'translate3d(' + positionX + ', ' + positionY + ', 0)'
	});
};
$.fn.translateXY = function (positionX, positionY) {
	return this.each(function () {
		translateXY($(this), positionX, positionY);
	});
};

if ($('.home-carousel').length) {
	$(window).load(function () {
		var wH = $(window).height();
		var carousel = $('.home-carousel');
		var slideLeft = carousel.find('.left ul');
		var slideRight = carousel.find('.right ul');
		var count = 0;
		var down = true;
		var clicked = false;
		var start=1;

		$(window).resize(function () {
			wH = $(window).height();
			
			slideLeft.add(slideRight).height(slideLeft.find('li').length * wH);
		}).resize();

		var homecarouselSlide = function () {

		
			if (clicked) {
				clicked = false;
				setTimeout(homecarouselSlide, 5000);
				return;
			}
			if (down) {
				count++;
			} else {  
				count--;
			}
			if (count < slideRight.find('li').length && down) {
				slideLeft.translateXY(0, -wH * count + 'px');
				slideRight.translateXY('-50%', wH * count + 'px');

				if(count==(slideRight.find('li').length-1) )
				{
					down=false;

				}
			} else {


				slideLeft.translateXY(0, -wH * count + 'px');
				slideRight.translateXY('-50%', wH * count + 'px');
				if(count==0)
				{
					down=true;
				}
			}

			setTimeout(homecarouselSlide, 5000);
		}
		setTimeout(homecarouselSlide, 5000);
	});
}

/*(function (a) {


	function init() {

		var mapOptions = {
			zoom: 15,
			mapTypeControl: false,
			scaleControl: false,
			navigationControl: false,
			streetViewControl: false,
			zoomControl: false,
			center:new google.maps.LatLng(40.805478,-73.96522499999998),
			styles: [
				{"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#000000"}, {"lightness": 17}]},
				{"featureType": "landscape","elementType": "geometry","stylers": [{"color": "#000000"},{"lightness": 20}]},
				{"featureType": "road.highway","elementType": "geometry.fill","stylers": [{"color": "#000000"},{"lightness": 17}]},
				{"featureType": "road.highway","elementType": "geometry.stroke","stylers": [{"color": "#000000"},{"lightness": 29},
				{"weight": 0.2}]},
				{"featureType": "road.arterial","elementType": "geometry","stylers": [{"color": "#000000"},{"lightness": 18}]},
				{"featureType": "road.local","elementType": "geometry","stylers": [{"color": "#000000"},
				{"lightness": 16}]},
				{"featureType": "poi","elementType": "geometry","stylers": [{"color": "#000000"},{"lightness": 21}]},
				{"elementType": "labels.text.stroke","stylers": [{"visibility": "on"},{"color": "#000000"},{"lightness": 16}]},
				{"elementType": "labels.text.fill","stylers": [{"saturation": 36},{"color": "#000000"},{"lightness": 40}]},
				{"elementType": "labels.icon","stylers": [{"visibility": "off"}]},
				{"featureType": "transit","elementType": "geometry","stylers": [{"color": "#000000"},{"lightness": 19}]},
				{"featureType": "administrative","elementType": "geometry.fill","stylers": [{"color": "#000000"},{"lightness": 20}]},
				{"featureType": "administrative","elementType": "geometry.stroke","stylers": [{"color": "#000000"},{"lightness": 17},{"weight": 1.2}]}
			]
		};
		var map = new google.maps.Map(document.getElementById('map'), mapOptions);

		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations.place.position[0], locations.place.position[1]),
			map: map,
			icon: ('images/marker.png')
		});

		var boxText = document.createElement("div");
		boxText.innerHTML = locations.place.text;

		myOptions = {
			center: new google.maps.LatLng(locations.place.position[0], locations.place.position[1]),
			content: boxText,
			alignBottom: true,
			disableAutoPan: false,
			infoBoxClearance: new google.maps.Size(70, 70),
			maxWidth: 0,
			pixelOffset: new google.maps.Size(-130, 200),
			zIndex: null,
			boxStyle: {
				width: "260px",
				padding: "0"
			},
			isHidden: false,
			pane: "floatPane",
			enableEventPropagation: false
		};

		var ib = new InfoBox(myOptions);
		ib.open(map, marker);
		google.maps.event.addListener(marker, "click", function (e) {
			ib.setOptions(myOptions);
			ib.open(map, marker);
		});
	}


	if ($('#map').length) {
		google.maps.event.addDomListener(window, 'load', init);
	}})(jQuery);*/


function onYouTubeIframeAPIReady() {
	$(function () {
		if ($('#video-box').length && $('.no-touch').length) {
			var player;
			player = new YT.Player('video-box', {
				width: '100%',
				height: '445px',
				playerVars: {
					showinfo: 0,
					hd: 1,
					rel: 0,
					egm: 0,
					showsearch: 0,
					wmode: 'opaque'
				},
				videoId: $('#video-box').attr('data-video-id'),
				events: {
					'onReady': function (e) {
						e.target.addEventListener('onStateChange', function (event) {
							if (event.data == 0 || event.data == 2) {
								$('.video-section_i .overlay').fadeIn();
							}
						});
					}
				}
			});
			$('.video-section_i .btn').on('click', function (e) {
				e.preventDefault();
				$('.video-section_i .overlay').fadeOut();
				setTimeout(function () {
					player.playVideo();
				}, 1000);
			});
		} else {
			$('.video-section_i .btn').on('click', function (e) {
				window.location.href = "https://www.youtube.com/watch?v=" + $('#video-box').attr('data-video-id');
			});
		}
	});

	var slideToBlock = function (number) {
		var current = $('.content__item.active');
		var el = $('.content__item').eq(number);
		el.addClass('active');
		current.not(el).addClass('top');
		setTimeout(function () {
			$('.content__item.top').removeClass('top active');
		}, 1000);
	};

	$('.bottom-main .footer-right .item-top').on('click', function (e) {
		e.preventDefault();
		slideToBlock(2)
	});

	$('.main-slider .right-menu .arrow').on('click', function (e) {
		e.preventDefault();
		slideToBlock(1)
	});
	$('.about .next-bottom .arrow-icons').on('click', function (e) {
		e.preventDefault();
		slideToBlock(2)
	});
	$('.horizon-slide .arrow-icons-bottom').on('click', function (e) {
		e.preventDefault();
		slideToBlock(3)
	});
}
