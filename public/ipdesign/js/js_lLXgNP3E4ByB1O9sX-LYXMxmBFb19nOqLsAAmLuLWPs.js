/* Modernizr 2.8.2 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-fontface-backgroundsize-borderradius-boxshadow-opacity-rgba-textshadow-cssanimations-generatedcontent-csstransforms-csstransforms3d-csstransitions-audio-video-inlinesvg-smil-svg-svgclippaths-touch-webgl-shiv-mq-cssclasses-teststyles-testprop-testallprops-prefixes-domprefixes-load
 */
;window.Modernizr=function(a,b,c){function C(a){j.cssText=a}function D(a,b){return C(n.join(a+";")+(b||""))}function E(a,b){return typeof a===b}function F(a,b){return!!~(""+a).indexOf(b)}function G(a,b){for(var d in a){var e=a[d];if(!F(e,"-")&&j[e]!==c)return b=="pfx"?e:!0}return!1}function H(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:E(f,"function")?f.bind(d||b):f}return!1}function I(a,b,c){var d=a.charAt(0).toUpperCase()+a.slice(1),e=(a+" "+p.join(d+" ")+d).split(" ");return E(b,"string")||E(b,"undefined")?G(e,b):(e=(a+" "+q.join(d+" ")+d).split(" "),H(e,b,c))}var d="2.8.2",e={},f=!0,g=b.documentElement,h="modernizr",i=b.createElement(h),j=i.style,k,l=":)",m={}.toString,n=" -webkit- -moz- -o- -ms- ".split(" "),o="Webkit Moz O ms",p=o.split(" "),q=o.toLowerCase().split(" "),r={svg:"http://www.w3.org/2000/svg"},s={},t={},u={},v=[],w=v.slice,x,y=function(a,c,d,e){var f,i,j,k,l=b.createElement("div"),m=b.body,n=m||b.createElement("body");if(parseInt(d,10))while(d--)j=b.createElement("div"),j.id=e?e[d]:h+(d+1),l.appendChild(j);return f=["&#173;",'<style id="s',h,'">',a,"</style>"].join(""),l.id=h,(m?l:n).innerHTML+=f,n.appendChild(l),m||(n.style.background="",n.style.overflow="hidden",k=g.style.overflow,g.style.overflow="hidden",g.appendChild(n)),i=c(l,a),m?l.parentNode.removeChild(l):(n.parentNode.removeChild(n),g.style.overflow=k),!!i},z=function(b){var c=a.matchMedia||a.msMatchMedia;if(c)return c(b)&&c(b).matches||!1;var d;return y("@media "+b+" { #"+h+" { position: absolute; } }",function(b){d=(a.getComputedStyle?getComputedStyle(b,null):b.currentStyle)["position"]=="absolute"}),d},A={}.hasOwnProperty,B;!E(A,"undefined")&&!E(A.call,"undefined")?B=function(a,b){return A.call(a,b)}:B=function(a,b){return b in a&&E(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=w.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(w.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(w.call(arguments)))};return e}),s.webgl=function(){return!!a.WebGLRenderingContext},s.touch=function(){var c;return"ontouchstart"in a||a.DocumentTouch&&b instanceof DocumentTouch?c=!0:y(["@media (",n.join("touch-enabled),("),h,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(a){c=a.offsetTop===9}),c},s.rgba=function(){return C("background-color:rgba(150,255,150,.5)"),F(j.backgroundColor,"rgba")},s.backgroundsize=function(){return I("backgroundSize")},s.borderradius=function(){return I("borderRadius")},s.boxshadow=function(){return I("boxShadow")},s.textshadow=function(){return b.createElement("div").style.textShadow===""},s.opacity=function(){return D("opacity:.55"),/^0.55$/.test(j.opacity)},s.cssanimations=function(){return I("animationName")},s.csstransforms=function(){return!!I("transform")},s.csstransforms3d=function(){var a=!!I("perspective");return a&&"webkitPerspective"in g.style&&y("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}",function(b,c){a=b.offsetLeft===9&&b.offsetHeight===3}),a},s.csstransitions=function(){return I("transition")},s.fontface=function(){var a;return y('@font-face {font-family:"font";src:url("https://")}',function(c,d){var e=b.getElementById("smodernizr"),f=e.sheet||e.styleSheet,g=f?f.cssRules&&f.cssRules[0]?f.cssRules[0].cssText:f.cssText||"":"";a=/src/i.test(g)&&g.indexOf(d.split(" ")[0])===0}),a},s.generatedcontent=function(){var a;return y(["#",h,"{font:0/0 a}#",h,':after{content:"',l,'";visibility:hidden;font:3px/1 a}'].join(""),function(b){a=b.offsetHeight>=3}),a},s.video=function(){var a=b.createElement("video"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/,""),c.h264=a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/,""),c.webm=a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,"")}catch(d){}return c},s.audio=function(){var a=b.createElement("audio"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,""),c.mp3=a.canPlayType("audio/mpeg;").replace(/^no$/,""),c.wav=a.canPlayType('audio/wav; codecs="1"').replace(/^no$/,""),c.m4a=(a.canPlayType("audio/x-m4a;")||a.canPlayType("audio/aac;")).replace(/^no$/,"")}catch(d){}return c},s.svg=function(){return!!b.createElementNS&&!!b.createElementNS(r.svg,"svg").createSVGRect},s.inlinesvg=function(){var a=b.createElement("div");return a.innerHTML="<svg/>",(a.firstChild&&a.firstChild.namespaceURI)==r.svg},s.smil=function(){return!!b.createElementNS&&/SVGAnimate/.test(m.call(b.createElementNS(r.svg,"animate")))},s.svgclippaths=function(){return!!b.createElementNS&&/SVGClipPath/.test(m.call(b.createElementNS(r.svg,"clipPath")))};for(var J in s)B(s,J)&&(x=J.toLowerCase(),e[x]=s[J](),v.push((e[x]?"":"no-")+x));return e.addTest=function(a,b){if(typeof a=="object")for(var d in a)B(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof f!="undefined"&&f&&(g.className+=" "+(b?"":"no-")+a),e[a]=b}return e},C(""),i=k=null,function(a,b){function l(a,b){var c=a.createElement("p"),d=a.getElementsByTagName("head")[0]||a.documentElement;return c.innerHTML="x<style>"+b+"</style>",d.insertBefore(c.lastChild,d.firstChild)}function m(){var a=s.elements;return typeof a=="string"?a.split(" "):a}function n(a){var b=j[a[h]];return b||(b={},i++,a[h]=i,j[i]=b),b}function o(a,c,d){c||(c=b);if(k)return c.createElement(a);d||(d=n(c));var g;return d.cache[a]?g=d.cache[a].cloneNode():f.test(a)?g=(d.cache[a]=d.createElem(a)).cloneNode():g=d.createElem(a),g.canHaveChildren&&!e.test(a)&&!g.tagUrn?d.frag.appendChild(g):g}function p(a,c){a||(a=b);if(k)return a.createDocumentFragment();c=c||n(a);var d=c.frag.cloneNode(),e=0,f=m(),g=f.length;for(;e<g;e++)d.createElement(f[e]);return d}function q(a,b){b.cache||(b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag()),a.createElement=function(c){return s.shivMethods?o(c,a,b):b.createElem(c)},a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+m().join().replace(/[\w\-]+/g,function(a){return b.createElem(a),b.frag.createElement(a),'c("'+a+'")'})+");return n}")(s,b.frag)}function r(a){a||(a=b);var c=n(a);return s.shivCSS&&!g&&!c.hasCSS&&(c.hasCSS=!!l(a,"article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")),k||q(a,c),a}var c="3.7.0",d=a.html5||{},e=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,f=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,g,h="_html5shiv",i=0,j={},k;(function(){try{var a=b.createElement("a");a.innerHTML="<xyz></xyz>",g="hidden"in a,k=a.childNodes.length==1||function(){b.createElement("a");var a=b.createDocumentFragment();return typeof a.cloneNode=="undefined"||typeof a.createDocumentFragment=="undefined"||typeof a.createElement=="undefined"}()}catch(c){g=!0,k=!0}})();var s={elements:d.elements||"abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",version:c,shivCSS:d.shivCSS!==!1,supportsUnknownElements:k,shivMethods:d.shivMethods!==!1,type:"default",shivDocument:r,createElement:o,createDocumentFragment:p};a.html5=s,r(b)}(this,b),e._version=d,e._prefixes=n,e._domPrefixes=q,e._cssomPrefixes=p,e.mq=z,e.testProp=function(a){return G([a])},e.testAllProps=I,e.testStyles=y,g.className=g.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(f?" js "+v.join(" "):""),e}(this,this.document),function(a,b,c){function d(a){return"[object Function]"==o.call(a)}function e(a){return"string"==typeof a}function f(){}function g(a){return!a||"loaded"==a||"complete"==a||"uninitialized"==a}function h(){var a=p.shift();q=1,a?a.t?m(function(){("c"==a.t?B.injectCss:B.injectJs)(a.s,0,a.a,a.x,a.e,1)},0):(a(),h()):q=0}function i(a,c,d,e,f,i,j){function k(b){if(!o&&g(l.readyState)&&(u.r=o=1,!q&&h(),l.onload=l.onreadystatechange=null,b)){"img"!=a&&m(function(){t.removeChild(l)},50);for(var d in y[c])y[c].hasOwnProperty(d)&&y[c][d].onload()}}var j=j||B.errorTimeout,l=b.createElement(a),o=0,r=0,u={t:d,s:c,e:f,a:i,x:j};1===y[c]&&(r=1,y[c]=[]),"object"==a?l.data=c:(l.src=c,l.type=a),l.width=l.height="0",l.onerror=l.onload=l.onreadystatechange=function(){k.call(this,r)},p.splice(e,0,u),"img"!=a&&(r||2===y[c]?(t.insertBefore(l,s?null:n),m(k,j)):y[c].push(l))}function j(a,b,c,d,f){return q=0,b=b||"j",e(a)?i("c"==b?v:u,a,b,this.i++,c,d,f):(p.splice(this.i++,0,a),1==p.length&&h()),this}function k(){var a=B;return a.loader={load:j,i:0},a}var l=b.documentElement,m=a.setTimeout,n=b.getElementsByTagName("script")[0],o={}.toString,p=[],q=0,r="MozAppearance"in l.style,s=r&&!!b.createRange().compareNode,t=s?l:n.parentNode,l=a.opera&&"[object Opera]"==o.call(a.opera),l=!!b.attachEvent&&!l,u=r?"object":l?"script":"img",v=l?"script":u,w=Array.isArray||function(a){return"[object Array]"==o.call(a)},x=[],y={},z={timeout:function(a,b){return b.length&&(a.timeout=b[0]),a}},A,B;B=function(a){function b(a){var a=a.split("!"),b=x.length,c=a.pop(),d=a.length,c={url:c,origUrl:c,prefixes:a},e,f,g;for(f=0;f<d;f++)g=a[f].split("="),(e=z[g.shift()])&&(c=e(c,g));for(f=0;f<b;f++)c=x[f](c);return c}function g(a,e,f,g,h){var i=b(a),j=i.autoCallback;i.url.split(".").pop().split("?").shift(),i.bypass||(e&&(e=d(e)?e:e[a]||e[g]||e[a.split("/").pop().split("?")[0]]),i.instead?i.instead(a,e,f,g,h):(y[i.url]?i.noexec=!0:y[i.url]=1,f.load(i.url,i.forceCSS||!i.forceJS&&"css"==i.url.split(".").pop().split("?").shift()?"c":c,i.noexec,i.attrs,i.timeout),(d(e)||d(j))&&f.load(function(){k(),e&&e(i.origUrl,h,g),j&&j(i.origUrl,h,g),y[i.url]=2})))}function h(a,b){function c(a,c){if(a){if(e(a))c||(j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}),g(a,j,b,0,h);else if(Object(a)===a)for(n in m=function(){var b=0,c;for(c in a)a.hasOwnProperty(c)&&b++;return b}(),a)a.hasOwnProperty(n)&&(!c&&!--m&&(d(j)?j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}:j[n]=function(a){return function(){var b=[].slice.call(arguments);a&&a.apply(this,b),l()}}(k[n])),g(a[n],j,b,n,h))}else!c&&l()}var h=!!a.test,i=a.load||a.both,j=a.callback||f,k=j,l=a.complete||f,m,n;c(h?a.yep:a.nope,!!i),i&&c(i)}var i,j,l=this.yepnope.loader;if(e(a))g(a,0,l,0);else if(w(a))for(i=0;i<a.length;i++)j=a[i],e(j)?g(j,0,l,0):w(j)?B(j):Object(j)===j&&h(j,l);else Object(a)===a&&h(a,l)},B.addPrefix=function(a,b){z[a]=b},B.addFilter=function(a){x.push(a)},B.errorTimeout=1e4,null==b.readyState&&b.addEventListener&&(b.readyState="loading",b.addEventListener("DOMContentLoaded",A=function(){b.removeEventListener("DOMContentLoaded",A,0),b.readyState="complete"},0)),a.yepnope=k(),a.yepnope.executeStack=h,a.yepnope.injectJs=function(a,c,d,e,i,j){var k=b.createElement("script"),l,o,e=e||B.errorTimeout;k.src=a;for(o in d)k.setAttribute(o,d[o]);c=j?h:c||f,k.onreadystatechange=k.onload=function(){!l&&g(k.readyState)&&(l=1,c(),k.onload=k.onreadystatechange=null)},m(function(){l||(l=1,c(1))},e),i?k.onload():n.parentNode.insertBefore(k,n)},a.yepnope.injectCss=function(a,c,d,e,g,i){var e=b.createElement("link"),j,c=i?h:c||f;e.href=a,e.rel="stylesheet",e.type="text/css";for(j in d)e.setAttribute(j,d[j]);g||(n.parentNode.insertBefore(e,n),m(c,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))};;
/* Modernizr 2.6.2 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-cssanimations-csstransitions-touch-shiv-cssclasses-prefixed-teststyles-testprop-testallprops-prefixes-domprefixes-load
 */
;window.Modernizr=function(a,b,c){function z(a){j.cssText=a}function A(a,b){return z(m.join(a+";")+(b||""))}function B(a,b){return typeof a===b}function C(a,b){return!!~(""+a).indexOf(b)}function D(a,b){for(var d in a){var e=a[d];if(!C(e,"-")&&j[e]!==c)return b=="pfx"?e:!0}return!1}function E(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:B(f,"function")?f.bind(d||b):f}return!1}function F(a,b,c){var d=a.charAt(0).toUpperCase()+a.slice(1),e=(a+" "+o.join(d+" ")+d).split(" ");return B(b,"string")||B(b,"undefined")?D(e,b):(e=(a+" "+p.join(d+" ")+d).split(" "),E(e,b,c))}var d="2.6.2",e={},f=!0,g=b.documentElement,h="modernizr",i=b.createElement(h),j=i.style,k,l={}.toString,m=" -webkit- -moz- -o- -ms- ".split(" "),n="Webkit Moz O ms",o=n.split(" "),p=n.toLowerCase().split(" "),q={},r={},s={},t=[],u=t.slice,v,w=function(a,c,d,e){var f,i,j,k,l=b.createElement("div"),m=b.body,n=m||b.createElement("body");if(parseInt(d,10))while(d--)j=b.createElement("div"),j.id=e?e[d]:h+(d+1),l.appendChild(j);return f=["&#173;",'<style id="s',h,'">',a,"</style>"].join(""),l.id=h,(m?l:n).innerHTML+=f,n.appendChild(l),m||(n.style.background="",n.style.overflow="hidden",k=g.style.overflow,g.style.overflow="hidden",g.appendChild(n)),i=c(l,a),m?l.parentNode.removeChild(l):(n.parentNode.removeChild(n),g.style.overflow=k),!!i},x={}.hasOwnProperty,y;!B(x,"undefined")&&!B(x.call,"undefined")?y=function(a,b){return x.call(a,b)}:y=function(a,b){return b in a&&B(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=u.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(u.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(u.call(arguments)))};return e}),q.touch=function(){var c;return"ontouchstart"in a||a.DocumentTouch&&b instanceof DocumentTouch?c=!0:w(["@media (",m.join("touch-enabled),("),h,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(a){c=a.offsetTop===9}),c},q.cssanimations=function(){return F("animationName")},q.csstransitions=function(){return F("transition")};for(var G in q)y(q,G)&&(v=G.toLowerCase(),e[v]=q[G](),t.push((e[v]?"":"no-")+v));return e.addTest=function(a,b){if(typeof a=="object")for(var d in a)y(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof f!="undefined"&&f&&(g.className+=" "+(b?"":"no-")+a),e[a]=b}return e},z(""),i=k=null,function(a,b){function k(a,b){var c=a.createElement("p"),d=a.getElementsByTagName("head")[0]||a.documentElement;return c.innerHTML="x<style>"+b+"</style>",d.insertBefore(c.lastChild,d.firstChild)}function l(){var a=r.elements;return typeof a=="string"?a.split(" "):a}function m(a){var b=i[a[g]];return b||(b={},h++,a[g]=h,i[h]=b),b}function n(a,c,f){c||(c=b);if(j)return c.createElement(a);f||(f=m(c));var g;return f.cache[a]?g=f.cache[a].cloneNode():e.test(a)?g=(f.cache[a]=f.createElem(a)).cloneNode():g=f.createElem(a),g.canHaveChildren&&!d.test(a)?f.frag.appendChild(g):g}function o(a,c){a||(a=b);if(j)return a.createDocumentFragment();c=c||m(a);var d=c.frag.cloneNode(),e=0,f=l(),g=f.length;for(;e<g;e++)d.createElement(f[e]);return d}function p(a,b){b.cache||(b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag()),a.createElement=function(c){return r.shivMethods?n(c,a,b):b.createElem(c)},a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+l().join().replace(/\w+/g,function(a){return b.createElem(a),b.frag.createElement(a),'c("'+a+'")'})+");return n}")(r,b.frag)}function q(a){a||(a=b);var c=m(a);return r.shivCSS&&!f&&!c.hasCSS&&(c.hasCSS=!!k(a,"article,aside,figcaption,figure,footer,header,hgroup,nav,section{display:block}mark{background:#FF0;color:#000}")),j||p(a,c),a}var c=a.html5||{},d=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,e=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,f,g="_html5shiv",h=0,i={},j;(function(){try{var a=b.createElement("a");a.innerHTML="<xyz></xyz>",f="hidden"in a,j=a.childNodes.length==1||function(){b.createElement("a");var a=b.createDocumentFragment();return typeof a.cloneNode=="undefined"||typeof a.createDocumentFragment=="undefined"||typeof a.createElement=="undefined"}()}catch(c){f=!0,j=!0}})();var r={elements:c.elements||"abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video",shivCSS:c.shivCSS!==!1,supportsUnknownElements:j,shivMethods:c.shivMethods!==!1,type:"default",shivDocument:q,createElement:n,createDocumentFragment:o};a.html5=r,q(b)}(this,b),e._version=d,e._prefixes=m,e._domPrefixes=p,e._cssomPrefixes=o,e.testProp=function(a){return D([a])},e.testAllProps=F,e.testStyles=w,e.prefixed=function(a,b,c){return b?F(a,b,c):F(a,"pfx")},g.className=g.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(f?" js "+t.join(" "):""),e}(this,this.document),function(a,b,c){function d(a){return"[object Function]"==o.call(a)}function e(a){return"string"==typeof a}function f(){}function g(a){return!a||"loaded"==a||"complete"==a||"uninitialized"==a}function h(){var a=p.shift();q=1,a?a.t?m(function(){("c"==a.t?B.injectCss:B.injectJs)(a.s,0,a.a,a.x,a.e,1)},0):(a(),h()):q=0}function i(a,c,d,e,f,i,j){function k(b){if(!o&&g(l.readyState)&&(u.r=o=1,!q&&h(),l.onload=l.onreadystatechange=null,b)){"img"!=a&&m(function(){t.removeChild(l)},50);for(var d in y[c])y[c].hasOwnProperty(d)&&y[c][d].onload()}}var j=j||B.errorTimeout,l=b.createElement(a),o=0,r=0,u={t:d,s:c,e:f,a:i,x:j};1===y[c]&&(r=1,y[c]=[]),"object"==a?l.data=c:(l.src=c,l.type=a),l.width=l.height="0",l.onerror=l.onload=l.onreadystatechange=function(){k.call(this,r)},p.splice(e,0,u),"img"!=a&&(r||2===y[c]?(t.insertBefore(l,s?null:n),m(k,j)):y[c].push(l))}function j(a,b,c,d,f){return q=0,b=b||"j",e(a)?i("c"==b?v:u,a,b,this.i++,c,d,f):(p.splice(this.i++,0,a),1==p.length&&h()),this}function k(){var a=B;return a.loader={load:j,i:0},a}var l=b.documentElement,m=a.setTimeout,n=b.getElementsByTagName("script")[0],o={}.toString,p=[],q=0,r="MozAppearance"in l.style,s=r&&!!b.createRange().compareNode,t=s?l:n.parentNode,l=a.opera&&"[object Opera]"==o.call(a.opera),l=!!b.attachEvent&&!l,u=r?"object":l?"script":"img",v=l?"script":u,w=Array.isArray||function(a){return"[object Array]"==o.call(a)},x=[],y={},z={timeout:function(a,b){return b.length&&(a.timeout=b[0]),a}},A,B;B=function(a){function b(a){var a=a.split("!"),b=x.length,c=a.pop(),d=a.length,c={url:c,origUrl:c,prefixes:a},e,f,g;for(f=0;f<d;f++)g=a[f].split("="),(e=z[g.shift()])&&(c=e(c,g));for(f=0;f<b;f++)c=x[f](c);return c}function g(a,e,f,g,h){var i=b(a),j=i.autoCallback;i.url.split(".").pop().split("?").shift(),i.bypass||(e&&(e=d(e)?e:e[a]||e[g]||e[a.split("/").pop().split("?")[0]]),i.instead?i.instead(a,e,f,g,h):(y[i.url]?i.noexec=!0:y[i.url]=1,f.load(i.url,i.forceCSS||!i.forceJS&&"css"==i.url.split(".").pop().split("?").shift()?"c":c,i.noexec,i.attrs,i.timeout),(d(e)||d(j))&&f.load(function(){k(),e&&e(i.origUrl,h,g),j&&j(i.origUrl,h,g),y[i.url]=2})))}function h(a,b){function c(a,c){if(a){if(e(a))c||(j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}),g(a,j,b,0,h);else if(Object(a)===a)for(n in m=function(){var b=0,c;for(c in a)a.hasOwnProperty(c)&&b++;return b}(),a)a.hasOwnProperty(n)&&(!c&&!--m&&(d(j)?j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}:j[n]=function(a){return function(){var b=[].slice.call(arguments);a&&a.apply(this,b),l()}}(k[n])),g(a[n],j,b,n,h))}else!c&&l()}var h=!!a.test,i=a.load||a.both,j=a.callback||f,k=j,l=a.complete||f,m,n;c(h?a.yep:a.nope,!!i),i&&c(i)}var i,j,l=this.yepnope.loader;if(e(a))g(a,0,l,0);else if(w(a))for(i=0;i<a.length;i++)j=a[i],e(j)?g(j,0,l,0):w(j)?B(j):Object(j)===j&&h(j,l);else Object(a)===a&&h(a,l)},B.addPrefix=function(a,b){z[a]=b},B.addFilter=function(a){x.push(a)},B.errorTimeout=1e4,null==b.readyState&&b.addEventListener&&(b.readyState="loading",b.addEventListener("DOMContentLoaded",A=function(){b.removeEventListener("DOMContentLoaded",A,0),b.readyState="complete"},0)),a.yepnope=k(),a.yepnope.executeStack=h,a.yepnope.injectJs=function(a,c,d,e,i,j){var k=b.createElement("script"),l,o,e=e||B.errorTimeout;k.src=a;for(o in d)k.setAttribute(o,d[o]);c=j?h:c||f,k.onreadystatechange=k.onload=function(){!l&&g(k.readyState)&&(l=1,c(),k.onload=k.onreadystatechange=null)},m(function(){l||(l=1,c(1))},e),i?k.onload():n.parentNode.insertBefore(k,n)},a.yepnope.injectCss=function(a,c,d,e,g,i){var e=b.createElement("link"),j,c=i?h:c||f;e.href=a,e.rel="stylesheet",e.type="text/css";for(j in d)e.setAttribute(j,d[j]);g||(n.parentNode.insertBefore(e,n),m(c,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))};
/** * jquery.dlmenu.js v1.0.0 http://www.codrops.com */
!function(n,i,t){"use strict";var e=i.Modernizr,s=n("body");n.DLMenu=function(i,t){this.$el=n(t),this._init(i)},n.DLMenu.defaults={animationClasses:{"in":"dl-animate-in-1",out:"dl-animate-out-1"}},n.DLMenu.prototype={_init:function(i){this.options=n.extend(!0,{},n.DLMenu.defaults,i),this._config();var t={WebkitAnimation:"webkitAnimationEnd",OAnimation:"oAnimationEnd",msAnimation:"MSAnimationEnd",animation:"animationend"},s={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd",msTransition:"MSTransitionEnd",transition:"transitionend"};this.animEndEventName=t[e.prefixed("animation")]+".dlmenu",this.transEndEventName=s[e.prefixed("transition")]+".dlmenu",this.supportAnimations=e.cssanimations,this.supportTransitions=e.csstransitions,this._initEvents()},_config:function(){this.open=!1,this.$trigger=this.$el.children("button"),this.$menu=this.$el.children("ul.dl-menu"),this.$menuitems=this.$menu.find("li:not(.dl-back)"),this.$back=this.$menu.find("li.dl-back")},_initEvents:function(){var i=this;this.$trigger.on("click.dlmenu",function(){return i.open?i._closeMenu():(i._openMenu(),s.off("click").on("click.dlmenu",function(){i._closeMenu()})),!1}),this.$menuitems.on("click.dlmenu",function(t){t.stopPropagation();var e=n(this),s=e.children("ul.dl-submenu");if(s.length>0){var o=s.clone().insertAfter(i.$menu).addClass(i.options.animationClasses["in"]),a=function(){i.$menu.off(i.animEndEventName).removeClass(i.options.animationClasses.out).addClass("dl-subview"),e.addClass("dl-subviewopen").parents(".dl-subviewopen:first").removeClass("dl-subviewopen").addClass("dl-subview"),o.remove()};return i.$menu.addClass(i.options.animationClasses.out),i.supportAnimations?i.$menu.on(i.animEndEventName,a):a.call(),!1}}),this.$back.on("click.dlmenu",function(t){var e=n(this),s=e.parents("ul.dl-submenu:first"),o=s.parent(),a=s.clone().insertAfter(i.$menu).addClass(i.options.animationClasses.out),l=function(){i.$menu.off(i.animEndEventName).removeClass(i.options.animationClasses["in"]),a.remove()};i.$menu.addClass(i.options.animationClasses["in"]),i.supportAnimations?i.$menu.on(i.animEndEventName,l):l.call(),o.removeClass("dl-subviewopen");var u=e.parents(".dl-subview:first");return u.is("li")&&u.addClass("dl-subviewopen"),u.removeClass("dl-subview"),!1})},_closeMenu:function(){var n=this,i=function(){n.$menu.off(n.transEndEventName),n._resetMenu()};this.$menu.removeClass("dl-menuopen"),this.$menu.addClass("dl-menu-toggle"),this.$trigger.removeClass("dl-active"),this.supportTransitions?this.$menu.on(this.transEndEventName,i):i.call(),this.open=!1},_openMenu:function(){this.$menu.addClass("dl-menuopen dl-menu-toggle").on(this.transEndEventName,function(){n(this).removeClass("dl-menu-toggle")}),this.$trigger.addClass("dl-active"),this.open=!0},_resetMenu:function(){this.$menu.removeClass("dl-subview"),this.$menuitems.removeClass("dl-subview dl-subviewopen")}};var o=function(n){i.console&&i.console.error(n)};n.fn.dlmenu=function(i){if("string"==typeof i){var t=Array.prototype.slice.call(arguments,1);this.each(function(){var e=n.data(this,"dlmenu");return e?n.isFunction(e[i])&&"_"!==i.charAt(0)?void e[i].apply(e,t):void o("no such method '"+i+"' for dlmenu instance"):void o("cannot call methods on dlmenu prior to initialization; attempted to call method '"+i+"'")})}else this.each(function(){var t=n.data(this,"dlmenu");t?t._init():t=n.data(this,"dlmenu",new n.DLMenu(i,this))});return this}}(jQuery,window);
/** @preserve jQuery DateTimePicker plugin v2.4.1  @homepage http://xdsoft.net/jqplugins/datetimepicker */

!function(e){"use strict";var t={i18n:{en:{months:["January","February","March","April","May","June","July","August","September","October","November","December"],dayOfWeek:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]}},value:"",lang:"en",format:"Y/m/d H:i",formatTime:"H:i",formatDate:"Y/m/d",startDate:!1,step:60,monthChangeSpinner:!0,closeOnDateSelect:!1,closeOnTimeSelect:!1,closeOnWithoutClick:!0,closeOnInputClick:!0,timepicker:!0,datepicker:!0,weeks:!1,defaultTime:!1,defaultDate:!1,minDate:!1,maxDate:!1,minTime:!1,maxTime:!1,allowTimes:[],opened:!1,initTime:!0,inline:!1,theme:"",onSelectDate:function(){},onSelectTime:function(){},onChangeMonth:function(){},onChangeYear:function(){},onChangeDateTime:function(){},onShow:function(){},onClose:function(){},onGenerate:function(){},withoutCopyright:!0,inverseButton:!1,hours12:!1,next:"xdsoft_next",prev:"xdsoft_prev",dayOfWeekStart:0,parentID:"body",timeHeightInTimePicker:25,timepickerScrollbar:!0,todayButton:!0,prevButton:!0,nextButton:!0,defaultSelect:!0,scrollMonth:!0,scrollTime:!0,scrollInput:!0,lazyInit:!1,mask:!1,validateOnBlur:!0,allowBlank:!0,yearStart:1950,yearEnd:2050,monthStart:0,monthEnd:11,style:"",id:"",fixed:!1,roundTime:"round",className:"",weekends:[],disabledDates:[],yearOffset:0,beforeShowDay:null,enterLikeTab:!0,showApplyButton:!1};Array.prototype.indexOf||(Array.prototype.indexOf=function(e,t){var n,o;for(n=t||0,o=this.length;o>n;n+=1)if(this[n]===e)return n;return-1}),Date.prototype.countDaysInMonth=function(){return new Date(this.getFullYear(),this.getMonth()+1,0).getDate()},e.fn.xdsoftScroller=function(t){return this.each(function(){var n,o,a,i,r,s=e(this),d=function(e){var t,n={x:0,y:0};return"touchstart"===e.type||"touchmove"===e.type||"touchend"===e.type||"touchcancel"===e.type?(t=e.originalEvent.touches[0]||e.originalEvent.changedTouches[0],n.x=t.clientX,n.y=t.clientY):("mousedown"===e.type||"mouseup"===e.type||"mousemove"===e.type||"mouseover"===e.type||"mouseout"===e.type||"mouseenter"===e.type||"mouseleave"===e.type)&&(n.x=e.clientX,n.y=e.clientY),n},l=100,c=!1,f=0,u=0,m=0,g=!1,h=0,p=function(){};return"hide"===t?void s.find(".xdsoft_scrollbar").hide():(e(this).hasClass("xdsoft_scroller_box")||(n=s.children().eq(0),o=s[0].clientHeight,a=n[0].offsetHeight,i=e('<div class="xdsoft_scrollbar"></div>'),r=e('<div class="xdsoft_scroller"></div>'),i.append(r),s.addClass("xdsoft_scroller_box").append(i),p=function(e){var t=d(e).y-f+h;0>t&&(t=0),t+r[0].offsetHeight>m&&(t=m-r[0].offsetHeight),s.trigger("scroll_element.xdsoft_scroller",[l?t/l:0])},r.on("touchstart.xdsoft_scroller mousedown.xdsoft_scroller",function(n){o||s.trigger("resize_scroll.xdsoft_scroller",[t]),f=d(n).y,h=parseInt(r.css("margin-top"),10),m=i[0].offsetHeight,"mousedown"===n.type?(document&&e(document.body).addClass("xdsoft_noselect"),e([document.body,window]).on("mouseup.xdsoft_scroller",function a(){e([document.body,window]).off("mouseup.xdsoft_scroller",a).off("mousemove.xdsoft_scroller",p).removeClass("xdsoft_noselect")}),e(document.body).on("mousemove.xdsoft_scroller",p)):(g=!0,n.stopPropagation(),n.preventDefault())}).on("touchmove",function(e){g&&(e.preventDefault(),p(e))}).on("touchend touchcancel",function(e){g=!1,h=0}),s.on("scroll_element.xdsoft_scroller",function(e,t){o||s.trigger("resize_scroll.xdsoft_scroller",[t,!0]),t=t>1?1:0>t||isNaN(t)?0:t,r.css("margin-top",l*t),setTimeout(function(){n.css("marginTop",-parseInt((n[0].offsetHeight-o)*t,10))},10)}).on("resize_scroll.xdsoft_scroller",function(e,t,d){var c,f;o=s[0].clientHeight,a=n[0].offsetHeight,c=o/a,f=c*i[0].offsetHeight,c>1?r.hide():(r.show(),r.css("height",parseInt(f>10?f:10,10)),l=i[0].offsetHeight-r[0].offsetHeight,d!==!0&&s.trigger("scroll_element.xdsoft_scroller",[t||Math.abs(parseInt(n.css("marginTop"),10))/(a-o)]))}),s.on("mousewheel",function(e){var t=Math.abs(parseInt(n.css("marginTop"),10));return t-=20*e.deltaY,0>t&&(t=0),s.trigger("scroll_element.xdsoft_scroller",[t/(a-o)]),e.stopPropagation(),!1}),s.on("touchstart",function(e){c=d(e),u=Math.abs(parseInt(n.css("marginTop"),10))}),s.on("touchmove",function(e){if(c){e.preventDefault();var t=d(e);s.trigger("scroll_element.xdsoft_scroller",[(u-(t.y-c.y))/(a-o)])}}),s.on("touchend touchcancel",function(e){c=!1,u=0})),void s.trigger("resize_scroll.xdsoft_scroller",[t]))})},e.fn.datetimepicker=function(n){var o,a,i=48,r=57,s=96,d=105,l=17,c=46,f=13,u=27,m=8,g=37,h=38,p=39,x=40,v=9,T=116,_=65,w=67,D=86,y=90,b=89,k=!1,S=e.isPlainObject(n)||!n?e.extend(!0,{},t,n):e.extend(!0,{},t),M=0,C=function(e){e.on("open.xdsoft focusin.xdsoft mousedown.xdsoft",function t(n){e.is(":disabled")||e.data("xdsoft_datetimepicker")||(clearTimeout(M),M=setTimeout(function(){e.data("xdsoft_datetimepicker")||o(e),e.off("open.xdsoft focusin.xdsoft mousedown.xdsoft",t).trigger("open.xdsoft")},100))})};return o=function(t){function o(){var e,n=!1;return S.startDate?n=Y.strToDate(S.startDate):(n=S.value||(t&&t.val&&t.val()?t.val():""),n?n=Y.strToDateTime(n):S.defaultDate&&(n=Y.strToDate(S.defaultDate),S.defaultTime&&(e=Y.strtotime(S.defaultTime),n.setHours(e.getHours()),n.setMinutes(e.getMinutes())))),n&&Y.isValidDate(n)?H.data("changed",!0):n="",n||0}var a,M,C,F,O,Y,H=e("<div "+(S.id?'id="'+S.id+'"':"")+" "+(S.style?'style="'+S.style+'"':"")+' class="xdsoft_datetimepicker xdsoft_'+S.theme+" xdsoft_noselect "+(S.weeks?" xdsoft_showweeks":"")+S.className+'"></div>'),I=e('<div class="xdsoft_copyright"><a target="_blank" href="http://xdsoft.net/jqplugins/datetimepicker/">xdsoft.net</a></div>'),W=e('<div class="xdsoft_datepicker active"></div>'),P=e('<div class="xdsoft_mounthpicker"><button type="button" class="xdsoft_prev"></button><button type="button" class="xdsoft_today_button"></button><div class="xdsoft_label xdsoft_month"><span></span><i></i></div><div class="xdsoft_label xdsoft_year"><span></span><i></i></div><button type="button" class="xdsoft_next"></button></div>'),B=e('<div class="xdsoft_calendar"></div>'),q=e('<div class="xdsoft_timepicker active"><button type="button" class="xdsoft_prev"></button><div class="xdsoft_time_box"></div><button type="button" class="xdsoft_next"></button></div>'),A=q.find(".xdsoft_time_box").eq(0),V=e('<div class="xdsoft_time_variant"></div>'),j=e('<button class="xdsoft_save_selected blue-gradient-button">Save Selected</button>'),z=e('<div class="xdsoft_select xdsoft_monthselect"><div></div></div>'),E=e('<div class="xdsoft_select xdsoft_yearselect"><div></div></div>'),N=!1,R=0,$=0;P.find(".xdsoft_month span").after(z),P.find(".xdsoft_year span").after(E),P.find(".xdsoft_month,.xdsoft_year").on("mousedown.xdsoft",function(t){var n,o,a=e(this).find(".xdsoft_select").eq(0),i=0,r=0,s=a.is(":visible");for(P.find(".xdsoft_select").hide(),Y.currentTime&&(i=Y.currentTime[e(this).hasClass("xdsoft_month")?"getMonth":"getFullYear"]()),a[s?"hide":"show"](),n=a.find("div.xdsoft_option"),o=0;o<n.length&&n.eq(o).data("value")!==i;o+=1)r+=n[0].offsetHeight;return a.xdsoftScroller(r/(a.children()[0].offsetHeight-a[0].clientHeight)),t.stopPropagation(),!1}),P.find(".xdsoft_select").xdsoftScroller().on("mousedown.xdsoft",function(e){e.stopPropagation(),e.preventDefault()}).on("mousedown.xdsoft",".xdsoft_option",function(t){(void 0===Y.currentTime||null===Y.currentTime)&&(Y.currentTime=Y.now());var n=Y.currentTime.getFullYear();Y&&Y.currentTime&&Y.currentTime[e(this).parent().parent().hasClass("xdsoft_monthselect")?"setMonth":"setFullYear"](e(this).data("value")),e(this).parent().parent().hide(),H.trigger("xchange.xdsoft"),S.onChangeMonth&&e.isFunction(S.onChangeMonth)&&S.onChangeMonth.call(H,Y.currentTime,H.data("input")),n!==Y.currentTime.getFullYear()&&e.isFunction(S.onChangeYear)&&S.onChangeYear.call(H,Y.currentTime,H.data("input"))}),H.setOptions=function(n){if(S=e.extend(!0,{},S,n),n.allowTimes&&e.isArray(n.allowTimes)&&n.allowTimes.length&&(S.allowTimes=e.extend(!0,[],n.allowTimes)),n.weekends&&e.isArray(n.weekends)&&n.weekends.length&&(S.weekends=e.extend(!0,[],n.weekends)),n.disabledDates&&e.isArray(n.disabledDates)&&n.disabledDates.length&&(S.disabledDates=e.extend(!0,[],n.disabledDates)),!S.open&&!S.opened||S.inline||t.trigger("open.xdsoft"),S.inline&&(N=!0,H.addClass("xdsoft_inline"),t.after(H).hide()),S.inverseButton&&(S.next="xdsoft_prev",S.prev="xdsoft_next"),S.datepicker?W.addClass("active"):W.removeClass("active"),S.timepicker?q.addClass("active"):q.removeClass("active"),S.value&&(t&&t.val&&t.val(S.value),Y.setCurrentTime(S.value)),isNaN(S.dayOfWeekStart)?S.dayOfWeekStart=0:S.dayOfWeekStart=parseInt(S.dayOfWeekStart,10)%7,S.timepickerScrollbar||A.xdsoftScroller("hide"),S.minDate&&/^-(.*)$/.test(S.minDate)&&(S.minDate=Y.strToDateTime(S.minDate).dateFormat(S.formatDate)),S.maxDate&&/^\+(.*)$/.test(S.maxDate)&&(S.maxDate=Y.strToDateTime(S.maxDate).dateFormat(S.formatDate)),j.toggle(S.showApplyButton),P.find(".xdsoft_today_button").css("visibility",S.todayButton?"visible":"hidden"),P.find("."+S.prev).css("visibility",S.prevButton?"visible":"hidden"),P.find("."+S.next).css("visibility",S.nextButton?"visible":"hidden"),S.mask){var o=function(e){try{if(document.selection&&document.selection.createRange){var t=document.selection.createRange();return t.getBookmark().charCodeAt(2)-2}if(e.setSelectionRange)return e.selectionStart}catch(n){return 0}},a=function(e,t){if(e="string"==typeof e||e instanceof String?document.getElementById(e):e,!e)return!1;if(e.createTextRange){var n=e.createTextRange();return n.collapse(!0),n.moveEnd("character",t),n.moveStart("character",t),n.select(),!0}return e.setSelectionRange?(e.setSelectionRange(t,t),!0):!1},M=function(e,t){var n=e.replace(/([\[\]\/\{\}\(\)\-\.\+]{1})/g,"\\$1").replace(/_/g,"{digit+}").replace(/([0-9]{1})/g,"{digit$1}").replace(/\{digit([0-9]{1})\}/g,"[0-$1_]{1}").replace(/\{digit[\+]\}/g,"[0-9_]{1}");return new RegExp(n).test(t)};t.off("keydown.xdsoft"),S.mask===!0&&(S.mask=S.format.replace(/Y/g,"9999").replace(/F/g,"9999").replace(/m/g,"19").replace(/d/g,"39").replace(/H/g,"29").replace(/i/g,"59").replace(/s/g,"59")),"string"===e.type(S.mask)&&(M(S.mask,t.val())||t.val(S.mask.replace(/[0-9]/g,"_")),t.on("keydown.xdsoft",function(n){var C,F,O=this.value,Y=n.which;if(Y>=i&&r>=Y||Y>=s&&d>=Y||Y===m||Y===c){for(C=o(this),F=Y!==m&&Y!==c?String.fromCharCode(Y>=s&&d>=Y?Y-i:Y):"_",Y!==m&&Y!==c||!C||(C-=1,F="_");/[^0-9_]/.test(S.mask.substr(C,1))&&C<S.mask.length&&C>0;)C+=Y===m||Y===c?-1:1;if(O=O.substr(0,C)+F+O.substr(C+1),""===e.trim(O))O=S.mask.replace(/[0-9]/g,"_");else if(C===S.mask.length)return n.preventDefault(),!1;for(C+=Y===m||Y===c?0:1;/[^0-9_]/.test(S.mask.substr(C,1))&&C<S.mask.length&&C>0;)C+=Y===m||Y===c?-1:1;M(S.mask,O)?(this.value=O,a(this,C)):""===e.trim(O)?this.value=S.mask.replace(/[0-9]/g,"_"):t.trigger("error_input.xdsoft")}else if(-1!==[_,w,D,y,b].indexOf(Y)&&k||-1!==[u,h,x,g,p,T,l,v,f].indexOf(Y))return!0;return n.preventDefault(),!1}))}S.validateOnBlur&&t.off("blur.xdsoft").on("blur.xdsoft",function(){if(S.allowBlank&&!e.trim(e(this).val()).length)e(this).val(null),H.data("xdsoft_datetime").empty();else if(Date.parseDate(e(this).val(),S.format))H.data("xdsoft_datetime").setCurrentTime(e(this).val());else{var t=+[e(this).val()[0],e(this).val()[1]].join(""),n=+[e(this).val()[2],e(this).val()[3]].join("");!S.datepicker&&S.timepicker&&t>=0&&24>t&&n>=0&&60>n?e(this).val([t,n].map(function(e){return e>9?e:"0"+e}).join(":")):e(this).val(Y.now().dateFormat(S.format)),H.data("xdsoft_datetime").setCurrentTime(e(this).val())}H.trigger("changedatetime.xdsoft")}),S.dayOfWeekStartPrev=0===S.dayOfWeekStart?6:S.dayOfWeekStart-1,H.trigger("xchange.xdsoft").trigger("afterOpen.xdsoft")},H.data("options",S).on("mousedown.xdsoft",function(e){return e.stopPropagation(),e.preventDefault(),E.hide(),z.hide(),!1}),A.append(V),A.xdsoftScroller(),H.on("afterOpen.xdsoft",function(){A.xdsoftScroller()}),H.append(W).append(q),S.withoutCopyright!==!0&&H.append(I),W.append(P).append(B).append(j),e(S.parentID).append(H),a=function(){var t=this;t.now=function(e){var n,o,a=new Date;return!e&&S.defaultDate&&(n=t.strToDate(S.defaultDate),a.setFullYear(n.getFullYear()),a.setMonth(n.getMonth()),a.setDate(n.getDate())),S.yearOffset&&a.setFullYear(a.getFullYear()+S.yearOffset),!e&&S.defaultTime&&(o=t.strtotime(S.defaultTime),a.setHours(o.getHours()),a.setMinutes(o.getMinutes())),a},t.isValidDate=function(e){return"[object Date]"!==Object.prototype.toString.call(e)?!1:!isNaN(e.getTime())},t.setCurrentTime=function(e){t.currentTime="string"==typeof e?t.strToDateTime(e):t.isValidDate(e)?e:t.now(),H.trigger("xchange.xdsoft")},t.empty=function(){t.currentTime=null},t.getCurrentTime=function(e){return t.currentTime},t.nextMonth=function(){(void 0===t.currentTime||null===t.currentTime)&&(t.currentTime=t.now());var n,o=t.currentTime.getMonth()+1;return 12===o&&(t.currentTime.setFullYear(t.currentTime.getFullYear()+1),o=0),n=t.currentTime.getFullYear(),t.currentTime.setDate(Math.min(new Date(t.currentTime.getFullYear(),o+1,0).getDate(),t.currentTime.getDate())),t.currentTime.setMonth(o),S.onChangeMonth&&e.isFunction(S.onChangeMonth)&&S.onChangeMonth.call(H,Y.currentTime,H.data("input")),n!==t.currentTime.getFullYear()&&e.isFunction(S.onChangeYear)&&S.onChangeYear.call(H,Y.currentTime,H.data("input")),H.trigger("xchange.xdsoft"),o},t.prevMonth=function(){(void 0===t.currentTime||null===t.currentTime)&&(t.currentTime=t.now());var n=t.currentTime.getMonth()-1;return-1===n&&(t.currentTime.setFullYear(t.currentTime.getFullYear()-1),n=11),t.currentTime.setDate(Math.min(new Date(t.currentTime.getFullYear(),n+1,0).getDate(),t.currentTime.getDate())),t.currentTime.setMonth(n),S.onChangeMonth&&e.isFunction(S.onChangeMonth)&&S.onChangeMonth.call(H,Y.currentTime,H.data("input")),H.trigger("xchange.xdsoft"),n},t.getWeekOfYear=function(e){var t=new Date(e.getFullYear(),0,1);return Math.ceil(((e-t)/864e5+t.getDay()+1)/7)},t.strToDateTime=function(e){var n,o,a=[];return e&&e instanceof Date&&t.isValidDate(e)?e:(a=/^(\+|\-)(.*)$/.exec(e),a&&(a[2]=Date.parseDate(a[2],S.formatDate)),a&&a[2]?(n=a[2].getTime()-6e4*a[2].getTimezoneOffset(),o=new Date(Y.now().getTime()+parseInt(a[1]+"1",10)*n)):o=e?Date.parseDate(e,S.format):t.now(),t.isValidDate(o)||(o=t.now()),o)},t.strToDate=function(e){if(e&&e instanceof Date&&t.isValidDate(e))return e;var n=e?Date.parseDate(e,S.formatDate):t.now(!0);return t.isValidDate(n)||(n=t.now(!0)),n},t.strtotime=function(e){if(e&&e instanceof Date&&t.isValidDate(e))return e;var n=e?Date.parseDate(e,S.formatTime):t.now(!0);return t.isValidDate(n)||(n=t.now(!0)),n},t.str=function(){return t.currentTime.dateFormat(S.format)},t.currentTime=this.now()},Y=new a,j.on("click",function(e){e.preventDefault(),H.data("changed",!0),Y.setCurrentTime(o()),t.val(Y.str()),H.trigger("close.xdsoft")}),P.find(".xdsoft_today_button").on("mousedown.xdsoft",function(){H.data("changed",!0),Y.setCurrentTime(0),H.trigger("afterOpen.xdsoft")}).on("dblclick.xdsoft",function(){t.val(Y.str()),H.trigger("close.xdsoft")}),P.find(".xdsoft_prev,.xdsoft_next").on("mousedown.xdsoft",function(){var t=e(this),n=0,o=!1;!function a(e){t.hasClass(S.next)?Y.nextMonth():t.hasClass(S.prev)&&Y.prevMonth(),S.monthChangeSpinner&&(o||(n=setTimeout(a,e||100)))}(500),e([document.body,window]).on("mouseup.xdsoft",function i(){clearTimeout(n),o=!0,e([document.body,window]).off("mouseup.xdsoft",i)})}),q.find(".xdsoft_prev,.xdsoft_next").on("mousedown.xdsoft",function(){var t=e(this),n=0,o=!1,a=110;!function i(e){var r=A[0].clientHeight,s=V[0].offsetHeight,d=Math.abs(parseInt(V.css("marginTop"),10));t.hasClass(S.next)&&s-r-S.timeHeightInTimePicker>=d?V.css("marginTop","-"+(d+S.timeHeightInTimePicker)+"px"):t.hasClass(S.prev)&&d-S.timeHeightInTimePicker>=0&&V.css("marginTop","-"+(d-S.timeHeightInTimePicker)+"px"),A.trigger("scroll_element.xdsoft_scroller",[Math.abs(parseInt(V.css("marginTop"),10)/(s-r))]),a=a>10?10:a-10,o||(n=setTimeout(i,e||a))}(500),e([document.body,window]).on("mouseup.xdsoft",function r(){clearTimeout(n),o=!0,e([document.body,window]).off("mouseup.xdsoft",r)})}),M=0,H.on("xchange.xdsoft",function(t){clearTimeout(M),M=setTimeout(function(){(void 0===Y.currentTime||null===Y.currentTime)&&(Y.currentTime=Y.now());for(var t,o,a,i,r,s,d,l="",c=new Date(Y.currentTime.getFullYear(),Y.currentTime.getMonth(),1,12,0,0),f=0,u=Y.now(),m=!1,g=!1,h=[],p=!0,x="",v="";c.getDay()!==S.dayOfWeekStart;)c.setDate(c.getDate()-1);for(l+="<table><thead><tr>",S.weeks&&(l+="<th></th>"),t=0;7>t;t+=1)l+="<th>"+S.i18n[S.lang].dayOfWeek[(t+S.dayOfWeekStart)%7]+"</th>";for(l+="</tr></thead>",l+="<tbody>",S.maxDate!==!1&&(m=Y.strToDate(S.maxDate),m=new Date(m.getFullYear(),m.getMonth(),m.getDate(),23,59,59,999)),S.minDate!==!1&&(g=Y.strToDate(S.minDate),g=new Date(g.getFullYear(),g.getMonth(),g.getDate()));f<Y.currentTime.countDaysInMonth()||c.getDay()!==S.dayOfWeekStart||Y.currentTime.getMonth()===c.getMonth();)h=[],f+=1,o=c.getDate(),a=c.getFullYear(),i=c.getMonth(),r=Y.getWeekOfYear(c),h.push("xdsoft_date"),s=S.beforeShowDay&&e.isFunction(S.beforeShowDay.call)?S.beforeShowDay.call(H,c):null,m!==!1&&c>m||g!==!1&&g>c||s&&s[0]===!1?h.push("xdsoft_disabled"):-1!==S.disabledDates.indexOf(c.dateFormat(S.formatDate))&&h.push("xdsoft_disabled"),s&&""!==s[1]&&h.push(s[1]),Y.currentTime.getMonth()!==i&&h.push("xdsoft_other_month"),(S.defaultSelect||H.data("changed"))&&Y.currentTime.dateFormat(S.formatDate)===c.dateFormat(S.formatDate)&&h.push("xdsoft_current"),u.dateFormat(S.formatDate)===c.dateFormat(S.formatDate)&&h.push("xdsoft_today"),(0===c.getDay()||6===c.getDay()||~S.weekends.indexOf(c.dateFormat(S.formatDate)))&&h.push("xdsoft_weekend"),S.beforeShowDay&&e.isFunction(S.beforeShowDay)&&h.push(S.beforeShowDay(c)),p&&(l+="<tr>",p=!1,S.weeks&&(l+="<th>"+r+"</th>")),l+='<td data-date="'+o+'" data-month="'+i+'" data-year="'+a+'" class="xdsoft_date xdsoft_day_of_week'+c.getDay()+" "+h.join(" ")+'"><div>'+o+"</div></td>",c.getDay()===S.dayOfWeekStartPrev&&(l+="</tr>",p=!0),c.setDate(o+1);if(l+="</tbody></table>",B.html(l),P.find(".xdsoft_label span").eq(0).text(S.i18n[S.lang].months[Y.currentTime.getMonth()]),P.find(".xdsoft_label span").eq(1).text(Y.currentTime.getFullYear()),x="",v="",i="",d=function(e,t){var n=Y.now();n.setHours(e),e=parseInt(n.getHours(),10),n.setMinutes(t),t=parseInt(n.getMinutes(),10);var o=new Date(Y.currentTime);o.setHours(e),o.setMinutes(t),h=[],(S.minDateTime!==!1&&S.minDateTime>o||S.maxTime!==!1&&Y.strtotime(S.maxTime).getTime()<n.getTime()||S.minTime!==!1&&Y.strtotime(S.minTime).getTime()>n.getTime())&&h.push("xdsoft_disabled");var a=new Date(Y.currentTime);a.setHours(parseInt(Y.currentTime.getHours(),10)),a.setMinutes(Math[S.roundTime](Y.currentTime.getMinutes()/S.step)*S.step),(S.initTime||S.defaultSelect||H.data("changed"))&&a.getHours()===parseInt(e,10)&&(S.step>59||a.getMinutes()===parseInt(t,10))&&(S.defaultSelect||H.data("changed")?h.push("xdsoft_current"):S.initTime&&h.push("xdsoft_init_time")),parseInt(u.getHours(),10)===parseInt(e,10)&&parseInt(u.getMinutes(),10)===parseInt(t,10)&&h.push("xdsoft_today"),x+='<div class="xdsoft_time '+h.join(" ")+'" data-hour="'+e+'" data-minute="'+t+'">'+n.dateFormat(S.formatTime)+"</div>"},S.allowTimes&&e.isArray(S.allowTimes)&&S.allowTimes.length)for(f=0;f<S.allowTimes.length;f+=1)v=Y.strtotime(S.allowTimes[f]).getHours(),i=Y.strtotime(S.allowTimes[f]).getMinutes(),d(v,i);else for(f=0,t=0;f<(S.hours12?12:24);f+=1)for(t=0;60>t;t+=S.step)v=(10>f?"0":"")+f,i=(10>t?"0":"")+t,d(v,i);for(V.html(x),n="",f=0,f=parseInt(S.yearStart,10)+S.yearOffset;f<=parseInt(S.yearEnd,10)+S.yearOffset;f+=1)n+='<div class="xdsoft_option '+(Y.currentTime.getFullYear()===f?"xdsoft_current":"")+'" data-value="'+f+'">'+f+"</div>";for(E.children().eq(0).html(n),f=parseInt(S.monthStart),n="";f<=parseInt(S.monthEnd);f+=1)n+='<div class="xdsoft_option '+(Y.currentTime.getMonth()===f?"xdsoft_current":"")+'" data-value="'+f+'">'+S.i18n[S.lang].months[f]+"</div>";z.children().eq(0).html(n),e(H).trigger("generate.xdsoft")},10),t.stopPropagation()}).on("afterOpen.xdsoft",function(){if(S.timepicker){var e,t,n,o;V.find(".xdsoft_current").length?e=".xdsoft_current":V.find(".xdsoft_init_time").length&&(e=".xdsoft_init_time"),e?(t=A[0].clientHeight,n=V[0].offsetHeight,o=V.find(e).index()*S.timeHeightInTimePicker+1,o>n-t&&(o=n-t),A.trigger("scroll_element.xdsoft_scroller",[parseInt(o,10)/(n-t)])):A.trigger("scroll_element.xdsoft_scroller",[0])}}),C=0,B.on("click.xdsoft","td",function(n){n.stopPropagation(),C+=1;var o=e(this),a=Y.currentTime;return(void 0===a||null===a)&&(Y.currentTime=Y.now(),a=Y.currentTime),o.hasClass("xdsoft_disabled")?!1:(a.setDate(1),a.setFullYear(o.data("year")),a.setMonth(o.data("month")),a.setDate(o.data("date")),H.trigger("select.xdsoft",[a]),t.val(Y.str()),(C>1||S.closeOnDateSelect===!0||0===S.closeOnDateSelect&&!S.timepicker)&&!S.inline&&H.trigger("close.xdsoft"),S.onSelectDate&&e.isFunction(S.onSelectDate)&&S.onSelectDate.call(H,Y.currentTime,H.data("input"),n),H.data("changed",!0),H.trigger("xchange.xdsoft"),H.trigger("changedatetime.xdsoft"),void setTimeout(function(){C=0},200))}),V.on("click.xdsoft","div",function(t){t.stopPropagation();var n=e(this),o=Y.currentTime;return(void 0===o||null===o)&&(Y.currentTime=Y.now(),o=Y.currentTime),n.hasClass("xdsoft_disabled")?!1:(o.setHours(n.data("hour")),o.setMinutes(n.data("minute")),H.trigger("select.xdsoft",[o]),H.data("input").val(Y.str()),S.inline!==!0&&S.closeOnTimeSelect===!0&&H.trigger("close.xdsoft"),S.onSelectTime&&e.isFunction(S.onSelectTime)&&S.onSelectTime.call(H,Y.currentTime,H.data("input"),t),H.data("changed",!0),H.trigger("xchange.xdsoft"),void H.trigger("changedatetime.xdsoft"))}),W.on("mousewheel.xdsoft",function(e){return S.scrollMonth?(e.deltaY<0?Y.nextMonth():Y.prevMonth(),!1):!0}),t.on("mousewheel.xdsoft",function(e){return S.scrollInput?!S.datepicker&&S.timepicker?(F=V.find(".xdsoft_current").length?V.find(".xdsoft_current").eq(0).index():0,F+e.deltaY>=0&&F+e.deltaY<V.children().length&&(F+=e.deltaY),V.children().eq(F).length&&V.children().eq(F).trigger("mousedown"),!1):S.datepicker&&!S.timepicker?(W.trigger(e,[e.deltaY,e.deltaX,e.deltaY]),t.val&&t.val(Y.str()),H.trigger("changedatetime.xdsoft"),!1):void 0:!0}),H.on("changedatetime.xdsoft",function(t){if(S.onChangeDateTime&&e.isFunction(S.onChangeDateTime)){var n=H.data("input");S.onChangeDateTime.call(H,Y.currentTime,n,t),delete S.value,n.trigger("change")}}).on("generate.xdsoft",function(){S.onGenerate&&e.isFunction(S.onGenerate)&&S.onGenerate.call(H,Y.currentTime,H.data("input")),N&&(H.trigger("afterOpen.xdsoft"),N=!1)}).on("click.xdsoft",function(e){e.stopPropagation()}),F=0,O=function(){var t=H.data("input").offset(),n=t.top+H.data("input")[0].offsetHeight-1,o=t.left,a="absolute";S.fixed?(n-=e(window).scrollTop(),o-=e(window).scrollLeft(),a="fixed"):(n+H[0].offsetHeight>e(window).height()+e(window).scrollTop()&&(n=t.top-H[0].offsetHeight+1),0>n&&(n=0),o+H[0].offsetWidth>e(window).width()&&(o=e(window).width()-H[0].offsetWidth)),H.css({left:o,top:n,position:a})},H.on("open.xdsoft",function(t){var n=!0;S.onShow&&e.isFunction(S.onShow)&&(n=S.onShow.call(H,Y.currentTime,H.data("input"),t)),n!==!1&&(H.show(),O(),e(window).off("resize.xdsoft",O).on("resize.xdsoft",O),S.closeOnWithoutClick&&e([document.body,window]).on("mousedown.xdsoft",function o(){H.trigger("close.xdsoft"),e([document.body,window]).off("mousedown.xdsoft",o)}))}).on("close.xdsoft",function(t){var n=!0;P.find(".xdsoft_month,.xdsoft_year").find(".xdsoft_select").hide(),S.onClose&&e.isFunction(S.onClose)&&(n=S.onClose.call(H,Y.currentTime,H.data("input"),t)),n===!1||S.opened||S.inline||H.hide(),t.stopPropagation()}).on("toggle.xdsoft",function(e){H.is(":visible")?H.trigger("close.xdsoft"):H.trigger("open.xdsoft")}).data("input",t),R=0,$=0,H.data("xdsoft_datetime",Y),H.setOptions(S),Y.setCurrentTime(o()),t.data("xdsoft_datetimepicker",H).on("open.xdsoft focusin.xdsoft mousedown.xdsoft",function(e){t.is(":disabled")||t.data("xdsoft_datetimepicker").is(":visible")&&S.closeOnInputClick||(clearTimeout(R),R=setTimeout(function(){t.is(":disabled")||(N=!0,Y.setCurrentTime(o()),H.trigger("open.xdsoft"))},100))}).on("keydown.xdsoft",function(t){var n,o=(this.value,t.which);return-1!==[f].indexOf(o)&&S.enterLikeTab?(n=e("input:visible,textarea:visible"),H.trigger("close.xdsoft"),n.eq(n.index(this)+1).focus(),!1):-1!==[v].indexOf(o)?(H.trigger("close.xdsoft"),!0):void 0})},a=function(t){var n=t.data("xdsoft_datetimepicker");n&&(n.data("xdsoft_datetime",null),n.remove(),t.data("xdsoft_datetimepicker",null).off(".xdsoft"),e(window).off("resize.xdsoft"),e([window,document.body]).off("mousedown.xdsoft"),t.unmousewheel&&t.unmousewheel())},e(document).off("keydown.xdsoftctrl keyup.xdsoftctrl").on("keydown.xdsoftctrl",function(e){e.keyCode===l&&(k=!0)}).on("keyup.xdsoftctrl",function(e){e.keyCode===l&&(k=!1)}),this.each(function(){var t=e(this).data("xdsoft_datetimepicker");if(t){if("string"===e.type(n))switch(n){case"show":e(this).select().focus(),t.trigger("open.xdsoft");break;case"hide":t.trigger("close.xdsoft");break;case"toggle":t.trigger("toggle.xdsoft");break;case"destroy":a(e(this));break;case"reset":this.value=this.defaultValue,this.value&&t.data("xdsoft_datetime").isValidDate(Date.parseDate(this.value,S.format))||t.data("changed",!1),t.data("xdsoft_datetime").setCurrentTime(this.value);break;case"validate":var i=t.data("input");i.trigger("blur.xdsoft")}else t.setOptions(n);return 0}"string"!==e.type(n)&&(!S.lazyInit||S.open||S.inline?o(e(this)):C(e(this)))})},e.fn.datetimepicker.defaults=t}(jQuery);
(function () {
/*! Copyright (c) 2013 Brandon Aaron (http://brandon.aaron.sh)  Version: 3.1.12 */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof exports?module.exports=a:a(jQuery)}(function(a){function b(b){var g=b||window.event,h=i.call(arguments,1),j=0,l=0,m=0,n=0,o=0,p=0;if(b=a.event.fix(g),b.type="mousewheel","detail"in g&&(m=-1*g.detail),"wheelDelta"in g&&(m=g.wheelDelta),"wheelDeltaY"in g&&(m=g.wheelDeltaY),"wheelDeltaX"in g&&(l=-1*g.wheelDeltaX),"axis"in g&&g.axis===g.HORIZONTAL_AXIS&&(l=-1*m,m=0),j=0===m?l:m,"deltaY"in g&&(m=-1*g.deltaY,j=m),"deltaX"in g&&(l=g.deltaX,0===m&&(j=-1*l)),0!==m||0!==l){if(1===g.deltaMode){var q=a.data(this,"mousewheel-line-height");j*=q,m*=q,l*=q}else if(2===g.deltaMode){var r=a.data(this,"mousewheel-page-height");j*=r,m*=r,l*=r}if(n=Math.max(Math.abs(m),Math.abs(l)),(!f||f>n)&&(f=n,d(g,n)&&(f/=40)),d(g,n)&&(j/=40,l/=40,m/=40),j=Math[j>=1?"floor":"ceil"](j/f),l=Math[l>=1?"floor":"ceil"](l/f),m=Math[m>=1?"floor":"ceil"](m/f),k.settings.normalizeOffset&&this.getBoundingClientRect){var s=this.getBoundingClientRect();o=b.clientX-s.left,p=b.clientY-s.top}return b.deltaX=l,b.deltaY=m,b.deltaFactor=f,b.offsetX=o,b.offsetY=p,b.deltaMode=0,h.unshift(b,j,l,m),e&&clearTimeout(e),e=setTimeout(c,200),(a.event.dispatch||a.event.handle).apply(this,h)}}function c(){f=null}function d(a,b){return k.settings.adjustOldDeltas&&"mousewheel"===a.type&&b%120===0}var e,f,g=["wheel","mousewheel","DOMMouseScroll","MozMousePixelScroll"],h="onwheel"in document||document.documentMode>=9?["wheel"]:["mousewheel","DomMouseScroll","MozMousePixelScroll"],i=Array.prototype.slice;if(a.event.fixHooks)for(var j=g.length;j;)a.event.fixHooks[g[--j]]=a.event.mouseHooks;var k=a.event.special.mousewheel={version:"3.1.12",setup:function(){if(this.addEventListener)for(var c=h.length;c;)this.addEventListener(h[--c],b,!1);else this.onmousewheel=b;a.data(this,"mousewheel-line-height",k.getLineHeight(this)),a.data(this,"mousewheel-page-height",k.getPageHeight(this))},teardown:function(){if(this.removeEventListener)for(var c=h.length;c;)this.removeEventListener(h[--c],b,!1);else this.onmousewheel=null;a.removeData(this,"mousewheel-line-height"),a.removeData(this,"mousewheel-page-height")},getLineHeight:function(b){var c=a(b),d=c["offsetParent"in a.fn?"offsetParent":"parent"]();return d.length||(d=a("body")),parseInt(d.css("fontSize"),10)||parseInt(c.css("fontSize"),10)||16},getPageHeight:function(b){return a(b).height()},settings:{adjustOldDeltas:!0,normalizeOffset:!0}};a.fn.extend({mousewheel:function(a){return a?this.bind("mousewheel",a):this.trigger("mousewheel")},unmousewheel:function(a){return this.unbind("mousewheel",a)}})});
// Parse and Format Library http://www.xaprb.com/blog/2005/12/12/javascript-closures-for-runtime-efficiency/
Date.parseFunctions={count:0};Date.parseRegexes=[];Date.formatFunctions={count:0};Date.prototype.dateFormat=function(b){if(b=="unixtime"){return parseInt(this.getTime()/1000);}if(Date.formatFunctions[b]==null){Date.createNewFormat(b);}var a=Date.formatFunctions[b];return this[a]();};Date.createNewFormat=function(format){var funcName="format"+Date.formatFunctions.count++;Date.formatFunctions[format]=funcName;var codePrefix="Date.prototype."+funcName+" = function() {return ";var code="";var special=false;var ch="";for(var i=0;i<format.length;++i){ch=format.charAt(i);if(!special&&ch=="\\"){special=true;}else{if(special){special=false;code+="'"+String.escape(ch)+"' + ";}else{code+=Date.getFormatCode(ch);}}}if(code.length==0){code="\"\"";}else{code=code.substring(0,code.length-3);}eval(codePrefix+code+";}");};Date.getFormatCode=function(a){switch(a){case"d":return"String.leftPad(this.getDate(), 2, '0') + ";case"D":return"Date.dayNames[this.getDay()].substring(0, 3) + ";case"j":return"this.getDate() + ";case"l":return"Date.dayNames[this.getDay()] + ";case"S":return"this.getSuffix() + ";case"w":return"this.getDay() + ";case"z":return"this.getDayOfYear() + ";case"W":return"this.getWeekOfYear() + ";case"F":return"Date.monthNames[this.getMonth()] + ";case"m":return"String.leftPad(this.getMonth() + 1, 2, '0') + ";case"M":return"Date.monthNames[this.getMonth()].substring(0, 3) + ";case"n":return"(this.getMonth() + 1) + ";case"t":return"this.getDaysInMonth() + ";case"L":return"(this.isLeapYear() ? 1 : 0) + ";case"Y":return"this.getFullYear() + ";case"y":return"('' + this.getFullYear()).substring(2, 4) + ";case"a":return"(this.getHours() < 12 ? 'am' : 'pm') + ";case"A":return"(this.getHours() < 12 ? 'AM' : 'PM') + ";case"g":return"((this.getHours() %12) ? this.getHours() % 12 : 12) + ";case"G":return"this.getHours() + ";case"h":return"String.leftPad((this.getHours() %12) ? this.getHours() % 12 : 12, 2, '0') + ";case"H":return"String.leftPad(this.getHours(), 2, '0') + ";case"i":return"String.leftPad(this.getMinutes(), 2, '0') + ";case"s":return"String.leftPad(this.getSeconds(), 2, '0') + ";case"O":return"this.getGMTOffset() + ";case"T":return"this.getTimezone() + ";case"Z":return"(this.getTimezoneOffset() * -60) + ";default:return"'"+String.escape(a)+"' + ";}};Date.parseDate=function(a,c){if(c=="unixtime"){return new Date(!isNaN(parseInt(a))?parseInt(a)*1000:0);}if(Date.parseFunctions[c]==null){Date.createParser(c);}var b=Date.parseFunctions[c];return Date[b](a);};Date.createParser=function(format){var funcName="parse"+Date.parseFunctions.count++;var regexNum=Date.parseRegexes.length;var currentGroup=1;Date.parseFunctions[format]=funcName;var code="Date."+funcName+" = function(input) {\nvar y = -1, m = -1, d = -1, h = -1, i = -1, s = -1, z = -1;\nvar d = new Date();\ny = d.getFullYear();\nm = d.getMonth();\nd = d.getDate();\nvar results = input.match(Date.parseRegexes["+regexNum+"]);\nif (results && results.length > 0) {";var regex="";var special=false;var ch="";for(var i=0;i<format.length;++i){ch=format.charAt(i);if(!special&&ch=="\\"){special=true;}else{if(special){special=false;regex+=String.escape(ch);}else{obj=Date.formatCodeToRegex(ch,currentGroup);currentGroup+=obj.g;regex+=obj.s;if(obj.g&&obj.c){code+=obj.c;}}}}code+="if (y > 0 && z > 0){\nvar doyDate = new Date(y,0);\ndoyDate.setDate(z);\nm = doyDate.getMonth();\nd = doyDate.getDate();\n}";code+="if (y > 0 && m >= 0 && d > 0 && h >= 0 && i >= 0 && s >= 0)\n{return new Date(y, m, d, h, i, s);}\nelse if (y > 0 && m >= 0 && d > 0 && h >= 0 && i >= 0)\n{return new Date(y, m, d, h, i);}\nelse if (y > 0 && m >= 0 && d > 0 && h >= 0)\n{return new Date(y, m, d, h);}\nelse if (y > 0 && m >= 0 && d > 0)\n{return new Date(y, m, d);}\nelse if (y > 0 && m >= 0)\n{return new Date(y, m);}\nelse if (y > 0)\n{return new Date(y);}\n}return null;}";Date.parseRegexes[regexNum]=new RegExp("^"+regex+"$",'i');eval(code);};Date.formatCodeToRegex=function(b,a){switch(b){case"D":return{g:0,c:null,s:"(?:Sun|Mon|Tue|Wed|Thu|Fri|Sat)"};case"j":case"d":return{g:1,c:"d = parseInt(results["+a+"], 10);\n",s:"(\\d{1,2})"};case"l":return{g:0,c:null,s:"(?:"+Date.dayNames.join("|")+")"};case"S":return{g:0,c:null,s:"(?:st|nd|rd|th)"};case"w":return{g:0,c:null,s:"\\d"};case"z":return{g:1,c:"z = parseInt(results["+a+"], 10);\n",s:"(\\d{1,3})"};case"W":return{g:0,c:null,s:"(?:\\d{2})"};case"F":return{g:1,c:"m = parseInt(Date.monthNumbers[results["+a+"].substring(0, 3)], 10);\n",s:"("+Date.monthNames.join("|")+")"};case"M":return{g:1,c:"m = parseInt(Date.monthNumbers[results["+a+"]], 10);\n",s:"(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)"};case"n":case"m":return{g:1,c:"m = parseInt(results["+a+"], 10) - 1;\n",s:"(\\d{1,2})"};case"t":return{g:0,c:null,s:"\\d{1,2}"};case"L":return{g:0,c:null,s:"(?:1|0)"};case"Y":return{g:1,c:"y = parseInt(results["+a+"], 10);\n",s:"(\\d{4})"};case"y":return{g:1,c:"var ty = parseInt(results["+a+"], 10);\ny = ty > Date.y2kYear ? 1900 + ty : 2000 + ty;\n",s:"(\\d{1,2})"};case"a":return{g:1,c:"if (results["+a+"] == 'am') {\nif (h == 12) { h = 0; }\n} else { if (h < 12) { h += 12; }}",s:"(am|pm)"};case"A":return{g:1,c:"if (results["+a+"] == 'AM') {\nif (h == 12) { h = 0; }\n} else { if (h < 12) { h += 12; }}",s:"(AM|PM)"};case"g":case"G":case"h":case"H":return{g:1,c:"h = parseInt(results["+a+"], 10);\n",s:"(\\d{1,2})"};case"i":return{g:1,c:"i = parseInt(results["+a+"], 10);\n",s:"(\\d{2})"};case"s":return{g:1,c:"s = parseInt(results["+a+"], 10);\n",s:"(\\d{2})"};case"O":return{g:0,c:null,s:"[+-]\\d{4}"};case"T":return{g:0,c:null,s:"[A-Z]{3}"};case"Z":return{g:0,c:null,s:"[+-]\\d{1,5}"};default:return{g:0,c:null,s:String.escape(b)};}};Date.prototype.getTimezone=function(){return this.toString().replace(/^.*? ([A-Z]{3}) [0-9]{4}.*$/,"$1").replace(/^.*?\(([A-Z])[a-z]+ ([A-Z])[a-z]+ ([A-Z])[a-z]+\)$/,"$1$2$3");};Date.prototype.getGMTOffset=function(){return(this.getTimezoneOffset()>0?"-":"+")+String.leftPad(Math.floor(Math.abs(this.getTimezoneOffset())/60),2,"0")+String.leftPad(Math.abs(this.getTimezoneOffset())%60,2,"0");};Date.prototype.getDayOfYear=function(){var a=0;Date.daysInMonth[1]=this.isLeapYear()?29:28;for(var b=0;b<this.getMonth();++b){a+=Date.daysInMonth[b];}return a+this.getDate();};Date.prototype.getWeekOfYear=function(){var b=this.getDayOfYear()+(4-this.getDay());var a=new Date(this.getFullYear(),0,1);var c=(7-a.getDay()+4);return String.leftPad(Math.ceil((b-c)/7)+1,2,"0");};Date.prototype.isLeapYear=function(){var a=this.getFullYear();return((a&3)==0&&(a%100||(a%400==0&&a)));};Date.prototype.getFirstDayOfMonth=function(){var a=(this.getDay()-(this.getDate()-1))%7;return(a<0)?(a+7):a;};Date.prototype.getLastDayOfMonth=function(){var a=(this.getDay()+(Date.daysInMonth[this.getMonth()]-this.getDate()))%7;return(a<0)?(a+7):a;};Date.prototype.getDaysInMonth=function(){Date.daysInMonth[1]=this.isLeapYear()?29:28;return Date.daysInMonth[this.getMonth()];};Date.prototype.getSuffix=function(){switch(this.getDate()){case 1:case 21:case 31:return"st";case 2:case 22:return"nd";case 3:case 23:return"rd";default:return"th";}};String.escape=function(a){return a.replace(/('|\\)/g,"\\$1");};String.leftPad=function(d,b,c){var a=new String(d);if(c==null){c=" ";}while(a.length<b){a=c+a;}return a;};Date.daysInMonth=[31,28,31,30,31,30,31,31,30,31,30,31];Date.monthNames=["January","February","March","April","May","June","July","August","September","October","November","December"];Date.dayNames=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];Date.y2kYear=50;Date.monthNumbers={Jan:0,Feb:1,Mar:2,Apr:3,May:4,Jun:5,Jul:6,Aug:7,Sep:8,Oct:9,Nov:10,Dec:11};Date.patterns={ISO8601LongPattern:"Y-m-d H:i:s",ISO8601ShortPattern:"Y-m-d",ShortDatePattern:"n/j/Y",LongDatePattern:"l, F d, Y",FullDateTimePattern:"l, F d, Y g:i:s A",MonthDayPattern:"F d",ShortTimePattern:"g:i A",LongTimePattern:"g:i:s A",SortableDateTimePattern:"Y-m-d\\TH:i:s",UniversalSortableDateTimePattern:"Y-m-d H:i:sO",YearMonthPattern:"F, Y"};
}());
/* jquery.nicescroll 3.5.4 InuYaksa*2013 MIT http://areaaperta.com/nicescroll */(function(e){"function"===typeof define&&define.amd?define(["jquery"],e):e(jQuery)})(function(e){var y=!1,C=!1,J=5E3,K=2E3,x=0,F=["ms","moz","webkit","o"],s=window.requestAnimationFrame||!1,v=window.cancelAnimationFrame||!1;if(!s)for(var L in F){var D=F[L];s||(s=window[D+"RequestAnimationFrame"]);v||(v=window[D+"CancelAnimationFrame"]||window[D+"CancelRequestAnimationFrame"])}var z=window.MutationObserver||window.WebKitMutationObserver||!1,G={zindex:"auto",cursoropacitymin:0,cursoropacitymax:1,cursorcolor:"#424242",
cursorwidth:"5px",cursorborder:"1px solid #fff",cursorborderradius:"5px",scrollspeed:60,mousescrollstep:24,touchbehavior:!1,hwacceleration:!0,usetransition:!0,boxzoom:!1,dblclickzoom:!0,gesturezoom:!0,grabcursorenabled:!0,autohidemode:!0,background:"",iframeautoresize:!0,cursorminheight:32,preservenativescrolling:!0,railoffset:!1,bouncescroll:!0,spacebarenabled:!0,railpadding:{top:0,right:0,left:0,bottom:0},disableoutline:!0,horizrailenabled:!0,railalign:"right",railvalign:"bottom",enabletranslate3d:!0,
enablemousewheel:!0,enablekeyboard:!0,smoothscroll:!0,sensitiverail:!0,enablemouselockapi:!0,cursorfixedheight:!1,directionlockdeadzone:6,hidecursordelay:400,nativeparentscrolling:!0,enablescrollonselection:!0,overflowx:!0,overflowy:!0,cursordragspeed:0.3,rtlmode:"auto",cursordragontouch:!1,oneaxismousemode:"auto",scriptpath:function(){var e=document.getElementsByTagName("script"),e=e[e.length-1].src.split("?")[0];return 0<e.split("/").length?e.split("/").slice(0,-1).join("/")+"/":""}()},E=!1,M=function(){if(E)return E;
var e=document.createElement("DIV"),b={haspointerlock:"pointerLockElement"in document||"mozPointerLockElement"in document||"webkitPointerLockElement"in document};b.isopera="opera"in window;b.isopera12=b.isopera&&"getUserMedia"in navigator;b.isoperamini="[object OperaMini]"===Object.prototype.toString.call(window.operamini);b.isie="all"in document&&"attachEvent"in e&&!b.isopera;b.isieold=b.isie&&!("msInterpolationMode"in e.style);b.isie7=b.isie&&!b.isieold&&(!("documentMode"in document)||7==document.documentMode);
b.isie8=b.isie&&"documentMode"in document&&8==document.documentMode;b.isie9=b.isie&&"performance"in window&&9<=document.documentMode;b.isie10=b.isie&&"performance"in window&&10<=document.documentMode;b.isie9mobile=/iemobile.9/i.test(navigator.userAgent);b.isie9mobile&&(b.isie9=!1);b.isie7mobile=!b.isie9mobile&&b.isie7&&/iemobile/i.test(navigator.userAgent);b.ismozilla="MozAppearance"in e.style;b.iswebkit="WebkitAppearance"in e.style;b.ischrome="chrome"in window;b.ischrome22=b.ischrome&&b.haspointerlock;
b.ischrome26=b.ischrome&&"transition"in e.style;b.cantouch="ontouchstart"in document.documentElement||"ontouchstart"in window;b.hasmstouch=window.navigator.msPointerEnabled||!1;b.ismac=/^mac$/i.test(navigator.platform);b.isios=b.cantouch&&/iphone|ipad|ipod/i.test(navigator.platform);b.isios4=b.isios&&!("seal"in Object);b.isandroid=/android/i.test(navigator.userAgent);b.trstyle=!1;b.hastransform=!1;b.hastranslate3d=!1;b.transitionstyle=!1;b.hastransition=!1;b.transitionend=!1;for(var h=["transform",
"msTransform","webkitTransform","MozTransform","OTransform"],k=0;k<h.length;k++)if("undefined"!=typeof e.style[h[k]]){b.trstyle=h[k];break}b.hastransform=!1!=b.trstyle;b.hastransform&&(e.style[b.trstyle]="translate3d(1px,2px,3px)",b.hastranslate3d=/translate3d/.test(e.style[b.trstyle]));b.transitionstyle=!1;b.prefixstyle="";b.transitionend=!1;for(var h="transition webkitTransition MozTransition OTransition OTransition msTransition KhtmlTransition".split(" "),l=" -webkit- -moz- -o- -o -ms- -khtml-".split(" "),
q="transitionend webkitTransitionEnd transitionend otransitionend oTransitionEnd msTransitionEnd KhtmlTransitionEnd".split(" "),k=0;k<h.length;k++)if(h[k]in e.style){b.transitionstyle=h[k];b.prefixstyle=l[k];b.transitionend=q[k];break}b.ischrome26&&(b.prefixstyle=l[1]);b.hastransition=b.transitionstyle;a:{h=["-moz-grab","-webkit-grab","grab"];if(b.ischrome&&!b.ischrome22||b.isie)h=[];for(k=0;k<h.length;k++)if(l=h[k],e.style.cursor=l,e.style.cursor==l){h=l;break a}h="url(http://www.google.com/intl/en_ALL/mapfiles/openhand.cur),n-resize"}b.cursorgrabvalue=
h;b.hasmousecapture="setCapture"in e;b.hasMutationObserver=!1!==z;return E=b},N=function(g,b){function h(){var c=a.win;if("zIndex"in c)return c.zIndex();for(;0<c.length&&9!=c[0].nodeType;){var b=c.css("zIndex");if(!isNaN(b)&&0!=b)return parseInt(b);c=c.parent()}return!1}function k(c,b,f){b=c.css(b);c=parseFloat(b);return isNaN(c)?(c=w[b]||0,f=3==c?f?a.win.outerHeight()-a.win.innerHeight():a.win.outerWidth()-a.win.innerWidth():1,a.isie8&&c&&(c+=1),f?c:0):c}function l(c,b,f,e){a._bind(c,b,function(a){a=
a?a:window.event;var e={original:a,target:a.target||a.srcElement,type:"wheel",deltaMode:"MozMousePixelScroll"==a.type?0:1,deltaX:0,deltaZ:0,preventDefault:function(){a.preventDefault?a.preventDefault():a.returnValue=!1;return!1},stopImmediatePropagation:function(){a.stopImmediatePropagation?a.stopImmediatePropagation():a.cancelBubble=!0}};"mousewheel"==b?(e.deltaY=-0.025*a.wheelDelta,a.wheelDeltaX&&(e.deltaX=-0.025*a.wheelDeltaX)):e.deltaY=a.detail;return f.call(c,e)},e)}function q(c,b,f){var e,d;
0==c.deltaMode?(e=-Math.floor(c.deltaX*(a.opt.mousescrollstep/54)),d=-Math.floor(c.deltaY*(a.opt.mousescrollstep/54))):1==c.deltaMode&&(e=-Math.floor(c.deltaX*a.opt.mousescrollstep),d=-Math.floor(c.deltaY*a.opt.mousescrollstep));b&&(a.opt.oneaxismousemode&&0==e&&d)&&(e=d,d=0);e&&(a.scrollmom&&a.scrollmom.stop(),a.lastdeltax+=e,a.debounced("mousewheelx",function(){var c=a.lastdeltax;a.lastdeltax=0;a.rail.drag||a.doScrollLeftBy(c)},15));if(d){if(a.opt.nativeparentscrolling&&f&&!a.ispage&&!a.zoomactive)if(0>
d){if(a.getScrollTop()>=a.page.maxh)return!0}else if(0>=a.getScrollTop())return!0;a.scrollmom&&a.scrollmom.stop();a.lastdeltay+=d;a.debounced("mousewheely",function(){var c=a.lastdeltay;a.lastdeltay=0;a.rail.drag||a.doScrollBy(c)},15)}c.stopImmediatePropagation();return c.preventDefault()}var a=this;this.version="3.5.4";this.name="nicescroll";this.me=b;this.opt={doc:e("body"),win:!1};e.extend(this.opt,G);this.opt.snapbackspeed=80;if(g)for(var p in a.opt)"undefined"!=typeof g[p]&&(a.opt[p]=g[p]);this.iddoc=
(this.doc=a.opt.doc)&&this.doc[0]?this.doc[0].id||"":"";this.ispage=/^BODY|HTML/.test(a.opt.win?a.opt.win[0].nodeName:this.doc[0].nodeName);this.haswrapper=!1!==a.opt.win;this.win=a.opt.win||(this.ispage?e(window):this.doc);this.docscroll=this.ispage&&!this.haswrapper?e(window):this.win;this.body=e("body");this.iframe=this.isfixed=this.viewport=!1;this.isiframe="IFRAME"==this.doc[0].nodeName&&"IFRAME"==this.win[0].nodeName;this.istextarea="TEXTAREA"==this.win[0].nodeName;this.forcescreen=!1;this.canshowonmouseevent=
"scroll"!=a.opt.autohidemode;this.page=this.view=this.onzoomout=this.onzoomin=this.onscrollcancel=this.onscrollend=this.onscrollstart=this.onclick=this.ongesturezoom=this.onkeypress=this.onmousewheel=this.onmousemove=this.onmouseup=this.onmousedown=!1;this.scroll={x:0,y:0};this.scrollratio={x:0,y:0};this.cursorheight=20;this.scrollvaluemax=0;this.observerremover=this.observer=this.scrollmom=this.scrollrunning=this.isrtlmode=!1;do this.id="ascrail"+K++;while(document.getElementById(this.id));this.hasmousefocus=
this.hasfocus=this.zoomactive=this.zoom=this.selectiondrag=this.cursorfreezed=this.cursor=this.rail=!1;this.visibility=!0;this.hidden=this.locked=!1;this.cursoractive=!0;this.wheelprevented=!1;this.overflowx=a.opt.overflowx;this.overflowy=a.opt.overflowy;this.nativescrollingarea=!1;this.checkarea=0;this.events=[];this.saved={};this.delaylist={};this.synclist={};this.lastdeltay=this.lastdeltax=0;this.detected=M();var d=e.extend({},this.detected);this.ishwscroll=(this.canhwscroll=d.hastransform&&a.opt.hwacceleration)&&
a.haswrapper;this.istouchcapable=!1;d.cantouch&&(d.ischrome&&!d.isios&&!d.isandroid)&&(this.istouchcapable=!0,d.cantouch=!1);d.cantouch&&(d.ismozilla&&!d.isios&&!d.isandroid)&&(this.istouchcapable=!0,d.cantouch=!1);a.opt.enablemouselockapi||(d.hasmousecapture=!1,d.haspointerlock=!1);this.delayed=function(c,b,f,e){var d=a.delaylist[c],h=(new Date).getTime();if(!e&&d&&d.tt)return!1;d&&d.tt&&clearTimeout(d.tt);if(d&&d.last+f>h&&!d.tt)a.delaylist[c]={last:h+f,tt:setTimeout(function(){a&&(a.delaylist[c].tt=
0,b.call())},f)};else if(!d||!d.tt)a.delaylist[c]={last:h,tt:0},setTimeout(function(){b.call()},0)};this.debounced=function(c,b,f){var d=a.delaylist[c];(new Date).getTime();a.delaylist[c]=b;d||setTimeout(function(){var b=a.delaylist[c];a.delaylist[c]=!1;b.call()},f)};var r=!1;this.synched=function(c,b){a.synclist[c]=b;(function(){r||(s(function(){r=!1;for(c in a.synclist){var b=a.synclist[c];b&&b.call(a);a.synclist[c]=!1}}),r=!0)})();return c};this.unsynched=function(c){a.synclist[c]&&(a.synclist[c]=
!1)};this.css=function(c,b){for(var f in b)a.saved.css.push([c,f,c.css(f)]),c.css(f,b[f])};this.scrollTop=function(c){return"undefined"==typeof c?a.getScrollTop():a.setScrollTop(c)};this.scrollLeft=function(c){return"undefined"==typeof c?a.getScrollLeft():a.setScrollLeft(c)};BezierClass=function(a,b,f,d,e,h,k){this.st=a;this.ed=b;this.spd=f;this.p1=d||0;this.p2=e||1;this.p3=h||0;this.p4=k||1;this.ts=(new Date).getTime();this.df=this.ed-this.st};BezierClass.prototype={B2:function(a){return 3*a*a*(1-
a)},B3:function(a){return 3*a*(1-a)*(1-a)},B4:function(a){return(1-a)*(1-a)*(1-a)},getNow:function(){var a=1-((new Date).getTime()-this.ts)/this.spd,b=this.B2(a)+this.B3(a)+this.B4(a);return 0>a?this.ed:this.st+Math.round(this.df*b)},update:function(a,b){this.st=this.getNow();this.ed=a;this.spd=b;this.ts=(new Date).getTime();this.df=this.ed-this.st;return this}};if(this.ishwscroll){this.doc.translate={x:0,y:0,tx:"0px",ty:"0px"};d.hastranslate3d&&d.isios&&this.doc.css("-webkit-backface-visibility",
"hidden");var t=function(){var c=a.doc.css(d.trstyle);return c&&"matrix"==c.substr(0,6)?c.replace(/^.*\((.*)\)$/g,"$1").replace(/px/g,"").split(/, +/):!1};this.getScrollTop=function(c){if(!c){if(c=t())return 16==c.length?-c[13]:-c[5];if(a.timerscroll&&a.timerscroll.bz)return a.timerscroll.bz.getNow()}return a.doc.translate.y};this.getScrollLeft=function(c){if(!c){if(c=t())return 16==c.length?-c[12]:-c[4];if(a.timerscroll&&a.timerscroll.bh)return a.timerscroll.bh.getNow()}return a.doc.translate.x};
this.notifyScrollEvent=document.createEvent?function(a){var b=document.createEvent("UIEvents");b.initUIEvent("scroll",!1,!0,window,1);a.dispatchEvent(b)}:document.fireEvent?function(a){var b=document.createEventObject();a.fireEvent("onscroll");b.cancelBubble=!0}:function(a,b){};d.hastranslate3d&&a.opt.enabletranslate3d?(this.setScrollTop=function(c,b){a.doc.translate.y=c;a.doc.translate.ty=-1*c+"px";a.doc.css(d.trstyle,"translate3d("+a.doc.translate.tx+","+a.doc.translate.ty+",0px)");b||a.notifyScrollEvent(a.win[0])},
this.setScrollLeft=function(c,b){a.doc.translate.x=c;a.doc.translate.tx=-1*c+"px";a.doc.css(d.trstyle,"translate3d("+a.doc.translate.tx+","+a.doc.translate.ty+",0px)");b||a.notifyScrollEvent(a.win[0])}):(this.setScrollTop=function(c,b){a.doc.translate.y=c;a.doc.translate.ty=-1*c+"px";a.doc.css(d.trstyle,"translate("+a.doc.translate.tx+","+a.doc.translate.ty+")");b||a.notifyScrollEvent(a.win[0])},this.setScrollLeft=function(c,b){a.doc.translate.x=c;a.doc.translate.tx=-1*c+"px";a.doc.css(d.trstyle,
"translate("+a.doc.translate.tx+","+a.doc.translate.ty+")");b||a.notifyScrollEvent(a.win[0])})}else this.getScrollTop=function(){return a.docscroll.scrollTop()},this.setScrollTop=function(c){return a.docscroll.scrollTop(c)},this.getScrollLeft=function(){return a.docscroll.scrollLeft()},this.setScrollLeft=function(c){return a.docscroll.scrollLeft(c)};this.getTarget=function(a){return!a?!1:a.target?a.target:a.srcElement?a.srcElement:!1};this.hasParent=function(a,b){if(!a)return!1;for(var f=a.target||
a.srcElement||a||!1;f&&f.id!=b;)f=f.parentNode||!1;return!1!==f};var w={thin:1,medium:3,thick:5};this.getOffset=function(){if(a.isfixed)return{top:parseFloat(a.win.css("top")),left:parseFloat(a.win.css("left"))};if(!a.viewport)return a.win.offset();var c=a.win.offset(),b=a.viewport.offset();return{top:c.top-b.top+a.viewport.scrollTop(),left:c.left-b.left+a.viewport.scrollLeft()}};this.updateScrollBar=function(c){if(a.ishwscroll)a.rail.css({height:a.win.innerHeight()}),a.railh&&a.railh.css({width:a.win.innerWidth()});
else{var b=a.getOffset(),f=b.top,d=b.left,f=f+k(a.win,"border-top-width",!0);a.win.outerWidth();a.win.innerWidth();var d=d+(a.rail.align?a.win.outerWidth()-k(a.win,"border-right-width")-a.rail.width:k(a.win,"border-left-width")),e=a.opt.railoffset;e&&(e.top&&(f+=e.top),a.rail.align&&e.left&&(d+=e.left));a.locked||a.rail.css({top:f,left:d,height:c?c.h:a.win.innerHeight()});a.zoom&&a.zoom.css({top:f+1,left:1==a.rail.align?d-20:d+a.rail.width+4});a.railh&&!a.locked&&(f=b.top,d=b.left,c=a.railh.align?
f+k(a.win,"border-top-width",!0)+a.win.innerHeight()-a.railh.height:f+k(a.win,"border-top-width",!0),d+=k(a.win,"border-left-width"),a.railh.css({top:c,left:d,width:a.railh.width}))}};this.doRailClick=function(c,b,f){var d;a.locked||(a.cancelEvent(c),b?(b=f?a.doScrollLeft:a.doScrollTop,d=f?(c.pageX-a.railh.offset().left-a.cursorwidth/2)*a.scrollratio.x:(c.pageY-a.rail.offset().top-a.cursorheight/2)*a.scrollratio.y,b(d)):(b=f?a.doScrollLeftBy:a.doScrollBy,d=f?a.scroll.x:a.scroll.y,c=f?c.pageX-a.railh.offset().left:
c.pageY-a.rail.offset().top,f=f?a.view.w:a.view.h,d>=c?b(f):b(-f)))};a.hasanimationframe=s;a.hascancelanimationframe=v;a.hasanimationframe?a.hascancelanimationframe||(v=function(){a.cancelAnimationFrame=!0}):(s=function(a){return setTimeout(a,15-Math.floor(+new Date/1E3)%16)},v=clearInterval);this.init=function(){a.saved.css=[];if(d.isie7mobile||d.isoperamini)return!0;d.hasmstouch&&a.css(a.ispage?e("html"):a.win,{"-ms-touch-action":"none"});a.zindex="auto";a.zindex=!a.ispage&&"auto"==a.opt.zindex?
h()||"auto":a.opt.zindex;!a.ispage&&"auto"!=a.zindex&&a.zindex>x&&(x=a.zindex);a.isie&&(0==a.zindex&&"auto"==a.opt.zindex)&&(a.zindex="auto");if(!a.ispage||!d.cantouch&&!d.isieold&&!d.isie9mobile){var c=a.docscroll;a.ispage&&(c=a.haswrapper?a.win:a.doc);d.isie9mobile||a.css(c,{"overflow-y":"hidden"});a.ispage&&d.isie7&&("BODY"==a.doc[0].nodeName?a.css(e("html"),{"overflow-y":"hidden"}):"HTML"==a.doc[0].nodeName&&a.css(e("body"),{"overflow-y":"hidden"}));d.isios&&(!a.ispage&&!a.haswrapper)&&a.css(e("body"),
{"-webkit-overflow-scrolling":"touch"});var b=e(document.createElement("div"));b.css({cursor:"pointer",position:"relative",top:0,"float":"right",width:a.opt.cursorwidth,height:"0px","background-color":a.opt.cursorcolor,border:a.opt.cursorborder,"background-clip":"padding-box","-webkit-border-radius":a.opt.cursorborderradius,"-moz-border-radius":a.opt.cursorborderradius,"border-radius":a.opt.cursorborderradius});b.hborder=parseFloat(b.outerHeight()-b.innerHeight());a.cursor=b;var f=e(document.createElement("div"));
f.attr("id",a.id);f.addClass("nicescroll-rails");var u,k,g=["left","right"],l;for(l in g)k=g[l],(u=a.opt.railpadding[k])?f.css("padding-"+k,u+"px"):a.opt.railpadding[k]=0;f.append(b);f.width=Math.max(parseFloat(a.opt.cursorwidth),b.outerWidth())+a.opt.railpadding.left+a.opt.railpadding.right;f.css({width:f.width+"px",zIndex:a.zindex,background:a.opt.background,cursor:"default"});f.visibility=!0;f.scrollable=!0;f.align="left"==a.opt.railalign?0:1;a.rail=f;b=a.rail.drag=!1;a.opt.boxzoom&&(!a.ispage&&
!d.isieold)&&(b=document.createElement("div"),a.bind(b,"click",a.doZoom),a.zoom=e(b),a.zoom.css({cursor:"pointer","z-index":a.zindex,backgroundImage:"url("+a.opt.scriptpath+"zoomico.png)",height:18,width:18,backgroundPosition:"0px 0px"}),a.opt.dblclickzoom&&a.bind(a.win,"dblclick",a.doZoom),d.cantouch&&a.opt.gesturezoom&&(a.ongesturezoom=function(c){1.5<c.scale&&a.doZoomIn(c);0.8>c.scale&&a.doZoomOut(c);return a.cancelEvent(c)},a.bind(a.win,"gestureend",a.ongesturezoom)));a.railh=!1;if(a.opt.horizrailenabled){a.css(c,
{"overflow-x":"hidden"});b=e(document.createElement("div"));b.css({cursor:"pointer",position:"relative",top:0,height:a.opt.cursorwidth,width:"0px","background-color":a.opt.cursorcolor,border:a.opt.cursorborder,"background-clip":"padding-box","-webkit-border-radius":a.opt.cursorborderradius,"-moz-border-radius":a.opt.cursorborderradius,"border-radius":a.opt.cursorborderradius});b.wborder=parseFloat(b.outerWidth()-b.innerWidth());a.cursorh=b;var m=e(document.createElement("div"));m.attr("id",a.id+"-hr");m.addClass("nicescroll-rails");
m.height=Math.max(parseFloat(a.opt.cursorwidth),b.outerHeight());m.css({height:m.height+"px",zIndex:a.zindex,background:a.opt.background});m.append(b);m.visibility=!0;m.scrollable=!0;m.align="top"==a.opt.railvalign?0:1;a.railh=m;a.railh.drag=!1}a.ispage?(f.css({position:"fixed",top:"0px",height:"100%"}),f.align?f.css({right:"0px"}):f.css({left:"0px"}),a.body.append(f),a.railh&&(m.css({position:"fixed",left:"0px",width:"100%"}),m.align?m.css({bottom:"0px"}):m.css({top:"0px"}),a.body.append(m))):(a.ishwscroll?
("static"==a.win.css("position")&&a.css(a.win,{position:"relative"}),c="HTML"==a.win[0].nodeName?a.body:a.win,a.zoom&&(a.zoom.css({position:"absolute",top:1,right:0,"margin-right":f.width+4}),c.append(a.zoom)),f.css({position:"absolute",top:0}),f.align?f.css({right:0}):f.css({left:0}),c.append(f),m&&(m.css({position:"absolute",left:0,bottom:0}),m.align?m.css({bottom:0}):m.css({top:0}),c.append(m))):(a.isfixed="fixed"==a.win.css("position"),c=a.isfixed?"fixed":"absolute",a.isfixed||(a.viewport=a.getViewport(a.win[0])),
a.viewport&&(a.body=a.viewport,!1==/fixed|relative|absolute/.test(a.viewport.css("position"))&&a.css(a.viewport,{position:"relative"})),f.css({position:c}),a.zoom&&a.zoom.css({position:c}),a.updateScrollBar(),a.body.append(f),a.zoom&&a.body.append(a.zoom),a.railh&&(m.css({position:c}),a.body.append(m))),d.isios&&a.css(a.win,{"-webkit-tap-highlight-color":"rgba(0,0,0,0)","-webkit-touch-callout":"none"}),d.isie&&a.opt.disableoutline&&a.win.attr("hideFocus","true"),d.iswebkit&&a.opt.disableoutline&&
a.win.css({outline:"none"}));!1===a.opt.autohidemode?(a.autohidedom=!1,a.rail.css({opacity:a.opt.cursoropacitymax}),a.railh&&a.railh.css({opacity:a.opt.cursoropacitymax})):!0===a.opt.autohidemode||"leave"===a.opt.autohidemode?(a.autohidedom=e().add(a.rail),d.isie8&&(a.autohidedom=a.autohidedom.add(a.cursor)),a.railh&&(a.autohidedom=a.autohidedom.add(a.railh)),a.railh&&d.isie8&&(a.autohidedom=a.autohidedom.add(a.cursorh))):"scroll"==a.opt.autohidemode?(a.autohidedom=e().add(a.rail),a.railh&&(a.autohidedom=
a.autohidedom.add(a.railh))):"cursor"==a.opt.autohidemode?(a.autohidedom=e().add(a.cursor),a.railh&&(a.autohidedom=a.autohidedom.add(a.cursorh))):"hidden"==a.opt.autohidemode&&(a.autohidedom=!1,a.hide(),a.locked=!1);if(d.isie9mobile)a.scrollmom=new H(a),a.onmangotouch=function(c){c=a.getScrollTop();var b=a.getScrollLeft();if(c==a.scrollmom.lastscrolly&&b==a.scrollmom.lastscrollx)return!0;var f=c-a.mangotouch.sy,d=b-a.mangotouch.sx;if(0!=Math.round(Math.sqrt(Math.pow(d,2)+Math.pow(f,2)))){var n=0>
f?-1:1,e=0>d?-1:1,h=+new Date;a.mangotouch.lazy&&clearTimeout(a.mangotouch.lazy);80<h-a.mangotouch.tm||a.mangotouch.dry!=n||a.mangotouch.drx!=e?(a.scrollmom.stop(),a.scrollmom.reset(b,c),a.mangotouch.sy=c,a.mangotouch.ly=c,a.mangotouch.sx=b,a.mangotouch.lx=b,a.mangotouch.dry=n,a.mangotouch.drx=e,a.mangotouch.tm=h):(a.scrollmom.stop(),a.scrollmom.update(a.mangotouch.sx-d,a.mangotouch.sy-f),a.mangotouch.tm=h,f=Math.max(Math.abs(a.mangotouch.ly-c),Math.abs(a.mangotouch.lx-b)),a.mangotouch.ly=c,a.mangotouch.lx=
b,2<f&&(a.mangotouch.lazy=setTimeout(function(){a.mangotouch.lazy=!1;a.mangotouch.dry=0;a.mangotouch.drx=0;a.mangotouch.tm=0;a.scrollmom.doMomentum(30)},100)))}},f=a.getScrollTop(),m=a.getScrollLeft(),a.mangotouch={sy:f,ly:f,dry:0,sx:m,lx:m,drx:0,lazy:!1,tm:0},a.bind(a.docscroll,"scroll",a.onmangotouch);else{if(d.cantouch||a.istouchcapable||a.opt.touchbehavior||d.hasmstouch){a.scrollmom=new H(a);a.ontouchstart=function(c){if(c.pointerType&&2!=c.pointerType)return!1;a.hasmoving=!1;if(!a.locked){if(d.hasmstouch)for(var b=
c.target?c.target:!1;b;){var f=e(b).getNiceScroll();if(0<f.length&&f[0].me==a.me)break;if(0<f.length)return!1;if("DIV"==b.nodeName&&b.id==a.id)break;b=b.parentNode?b.parentNode:!1}a.cancelScroll();if((b=a.getTarget(c))&&/INPUT/i.test(b.nodeName)&&/range/i.test(b.type))return a.stopPropagation(c);!("clientX"in c)&&"changedTouches"in c&&(c.clientX=c.changedTouches[0].clientX,c.clientY=c.changedTouches[0].clientY);a.forcescreen&&(f=c,c={original:c.original?c.original:c},c.clientX=f.screenX,c.clientY=
f.screenY);a.rail.drag={x:c.clientX,y:c.clientY,sx:a.scroll.x,sy:a.scroll.y,st:a.getScrollTop(),sl:a.getScrollLeft(),pt:2,dl:!1};if(a.ispage||!a.opt.directionlockdeadzone)a.rail.drag.dl="f";else{var f=e(window).width(),n=e(window).height(),h=Math.max(document.body.scrollWidth,document.documentElement.scrollWidth),k=Math.max(document.body.scrollHeight,document.documentElement.scrollHeight),n=Math.max(0,k-n),f=Math.max(0,h-f);a.rail.drag.ck=!a.rail.scrollable&&a.railh.scrollable?0<n?"v":!1:a.rail.scrollable&&
!a.railh.scrollable?0<f?"h":!1:!1;a.rail.drag.ck||(a.rail.drag.dl="f")}a.opt.touchbehavior&&(a.isiframe&&d.isie)&&(f=a.win.position(),a.rail.drag.x+=f.left,a.rail.drag.y+=f.top);a.hasmoving=!1;a.lastmouseup=!1;a.scrollmom.reset(c.clientX,c.clientY);if(!d.cantouch&&!this.istouchcapable&&!d.hasmstouch){if(!b||!/INPUT|SELECT|TEXTAREA/i.test(b.nodeName))return!a.ispage&&d.hasmousecapture&&b.setCapture(),a.opt.touchbehavior?(b.onclick&&!b._onclick&&(b._onclick=b.onclick,b.onclick=function(c){if(a.hasmoving)return!1;
b._onclick.call(this,c)}),a.cancelEvent(c)):a.stopPropagation(c);/SUBMIT|CANCEL|BUTTON/i.test(e(b).attr("type"))&&(pc={tg:b,click:!1},a.preventclick=pc)}}};a.ontouchend=function(c){if(c.pointerType&&2!=c.pointerType)return!1;if(a.rail.drag&&2==a.rail.drag.pt&&(a.scrollmom.doMomentum(),a.rail.drag=!1,a.hasmoving&&(a.lastmouseup=!0,a.hideCursor(),d.hasmousecapture&&document.releaseCapture(),!d.cantouch)))return a.cancelEvent(c)};var q=a.opt.touchbehavior&&a.isiframe&&!d.hasmousecapture;a.ontouchmove=
function(c,b){if(c.pointerType&&2!=c.pointerType)return!1;if(a.rail.drag&&2==a.rail.drag.pt){if(d.cantouch&&"undefined"==typeof c.original)return!0;a.hasmoving=!0;a.preventclick&&!a.preventclick.click&&(a.preventclick.click=a.preventclick.tg.onclick||!1,a.preventclick.tg.onclick=a.onpreventclick);c=e.extend({original:c},c);"changedTouches"in c&&(c.clientX=c.changedTouches[0].clientX,c.clientY=c.changedTouches[0].clientY);if(a.forcescreen){var f=c;c={original:c.original?c.original:c};c.clientX=f.screenX;
c.clientY=f.screenY}f=ofy=0;if(q&&!b){var n=a.win.position(),f=-n.left;ofy=-n.top}var h=c.clientY+ofy,n=h-a.rail.drag.y,k=c.clientX+f,u=k-a.rail.drag.x,g=a.rail.drag.st-n;a.ishwscroll&&a.opt.bouncescroll?0>g?g=Math.round(g/2):g>a.page.maxh&&(g=a.page.maxh+Math.round((g-a.page.maxh)/2)):(0>g&&(h=g=0),g>a.page.maxh&&(g=a.page.maxh,h=0));if(a.railh&&a.railh.scrollable){var l=a.rail.drag.sl-u;a.ishwscroll&&a.opt.bouncescroll?0>l?l=Math.round(l/2):l>a.page.maxw&&(l=a.page.maxw+Math.round((l-a.page.maxw)/
2)):(0>l&&(k=l=0),l>a.page.maxw&&(l=a.page.maxw,k=0))}f=!1;if(a.rail.drag.dl)f=!0,"v"==a.rail.drag.dl?l=a.rail.drag.sl:"h"==a.rail.drag.dl&&(g=a.rail.drag.st);else{var n=Math.abs(n),u=Math.abs(u),m=a.opt.directionlockdeadzone;if("v"==a.rail.drag.ck){if(n>m&&u<=0.3*n)return a.rail.drag=!1,!0;u>m&&(a.rail.drag.dl="f",e("body").scrollTop(e("body").scrollTop()))}else if("h"==a.rail.drag.ck){if(u>m&&n<=0.3*u)return a.rail.drag=!1,!0;n>m&&(a.rail.drag.dl="f",e("body").scrollLeft(e("body").scrollLeft()))}}a.synched("touchmove",
function(){a.rail.drag&&2==a.rail.drag.pt&&(a.prepareTransition&&a.prepareTransition(0),a.rail.scrollable&&a.setScrollTop(g),a.scrollmom.update(k,h),a.railh&&a.railh.scrollable?(a.setScrollLeft(l),a.showCursor(g,l)):a.showCursor(g),d.isie10&&document.selection.clear())});d.ischrome&&a.istouchcapable&&(f=!1);if(f)return a.cancelEvent(c)}}}a.onmousedown=function(c,b){if(!(a.rail.drag&&1!=a.rail.drag.pt)){if(a.locked)return a.cancelEvent(c);a.cancelScroll();a.rail.drag={x:c.clientX,y:c.clientY,sx:a.scroll.x,
sy:a.scroll.y,pt:1,hr:!!b};var f=a.getTarget(c);!a.ispage&&d.hasmousecapture&&f.setCapture();a.isiframe&&!d.hasmousecapture&&(a.saved.csspointerevents=a.doc.css("pointer-events"),a.css(a.doc,{"pointer-events":"none"}));a.hasmoving=!1;return a.cancelEvent(c)}};a.onmouseup=function(c){if(a.rail.drag&&(d.hasmousecapture&&document.releaseCapture(),a.isiframe&&!d.hasmousecapture&&a.doc.css("pointer-events",a.saved.csspointerevents),1==a.rail.drag.pt))return a.rail.drag=!1,a.hasmoving&&a.triggerScrollEnd(),
a.cancelEvent(c)};a.onmousemove=function(c){if(a.rail.drag&&1==a.rail.drag.pt){if(d.ischrome&&0==c.which)return a.onmouseup(c);a.cursorfreezed=!0;a.hasmoving=!0;if(a.rail.drag.hr){a.scroll.x=a.rail.drag.sx+(c.clientX-a.rail.drag.x);0>a.scroll.x&&(a.scroll.x=0);var b=a.scrollvaluemaxw;a.scroll.x>b&&(a.scroll.x=b)}else a.scroll.y=a.rail.drag.sy+(c.clientY-a.rail.drag.y),0>a.scroll.y&&(a.scroll.y=0),b=a.scrollvaluemax,a.scroll.y>b&&(a.scroll.y=b);a.synched("mousemove",function(){a.rail.drag&&1==a.rail.drag.pt&&
(a.showCursor(),a.rail.drag.hr?a.doScrollLeft(Math.round(a.scroll.x*a.scrollratio.x),a.opt.cursordragspeed):a.doScrollTop(Math.round(a.scroll.y*a.scrollratio.y),a.opt.cursordragspeed))});return a.cancelEvent(c)}};if(d.cantouch||a.opt.touchbehavior)a.onpreventclick=function(c){if(a.preventclick)return a.preventclick.tg.onclick=a.preventclick.click,a.preventclick=!1,a.cancelEvent(c)},a.bind(a.win,"mousedown",a.ontouchstart),a.onclick=d.isios?!1:function(c){return a.lastmouseup?(a.lastmouseup=!1,a.cancelEvent(c)):
!0},a.opt.grabcursorenabled&&d.cursorgrabvalue&&(a.css(a.ispage?a.doc:a.win,{cursor:d.cursorgrabvalue}),a.css(a.rail,{cursor:d.cursorgrabvalue}));else{var p=function(c){if(a.selectiondrag){if(c){var b=a.win.outerHeight();c=c.pageY-a.selectiondrag.top;0<c&&c<b&&(c=0);c>=b&&(c-=b);a.selectiondrag.df=c}0!=a.selectiondrag.df&&(a.doScrollBy(2*-Math.floor(a.selectiondrag.df/6)),a.debounced("doselectionscroll",function(){p()},50))}};a.hasTextSelected="getSelection"in document?function(){return 0<document.getSelection().rangeCount}:
"selection"in document?function(){return"None"!=document.selection.type}:function(){return!1};a.onselectionstart=function(c){a.ispage||(a.selectiondrag=a.win.offset())};a.onselectionend=function(c){a.selectiondrag=!1};a.onselectiondrag=function(c){a.selectiondrag&&a.hasTextSelected()&&a.debounced("selectionscroll",function(){p(c)},250)}}d.hasmstouch&&(a.css(a.rail,{"-ms-touch-action":"none"}),a.css(a.cursor,{"-ms-touch-action":"none"}),a.bind(a.win,"MSPointerDown",a.ontouchstart),a.bind(document,
"MSPointerUp",a.ontouchend),a.bind(document,"MSPointerMove",a.ontouchmove),a.bind(a.cursor,"MSGestureHold",function(a){a.preventDefault()}),a.bind(a.cursor,"contextmenu",function(a){a.preventDefault()}));this.istouchcapable&&(a.bind(a.win,"touchstart",a.ontouchstart),a.bind(document,"touchend",a.ontouchend),a.bind(document,"touchcancel",a.ontouchend),a.bind(document,"touchmove",a.ontouchmove));a.bind(a.cursor,"mousedown",a.onmousedown);a.bind(a.cursor,"mouseup",a.onmouseup);a.railh&&(a.bind(a.cursorh,
"mousedown",function(c){a.onmousedown(c,!0)}),a.bind(a.cursorh,"mouseup",a.onmouseup));if(a.opt.cursordragontouch||!d.cantouch&&!a.opt.touchbehavior)a.rail.css({cursor:"default"}),a.railh&&a.railh.css({cursor:"default"}),a.jqbind(a.rail,"mouseenter",function(){if(!a.win.is(":visible"))return!1;a.canshowonmouseevent&&a.showCursor();a.rail.active=!0}),a.jqbind(a.rail,"mouseleave",function(){a.rail.active=!1;a.rail.drag||a.hideCursor()}),a.opt.sensitiverail&&(a.bind(a.rail,"click",function(c){a.doRailClick(c,
!1,!1)}),a.bind(a.rail,"dblclick",function(c){a.doRailClick(c,!0,!1)}),a.bind(a.cursor,"click",function(c){a.cancelEvent(c)}),a.bind(a.cursor,"dblclick",function(c){a.cancelEvent(c)})),a.railh&&(a.jqbind(a.railh,"mouseenter",function(){if(!a.win.is(":visible"))return!1;a.canshowonmouseevent&&a.showCursor();a.rail.active=!0}),a.jqbind(a.railh,"mouseleave",function(){a.rail.active=!1;a.rail.drag||a.hideCursor()}),a.opt.sensitiverail&&(a.bind(a.railh,"click",function(c){a.doRailClick(c,!1,!0)}),a.bind(a.railh,
"dblclick",function(c){a.doRailClick(c,!0,!0)}),a.bind(a.cursorh,"click",function(c){a.cancelEvent(c)}),a.bind(a.cursorh,"dblclick",function(c){a.cancelEvent(c)})));!d.cantouch&&!a.opt.touchbehavior?(a.bind(d.hasmousecapture?a.win:document,"mouseup",a.onmouseup),a.bind(document,"mousemove",a.onmousemove),a.onclick&&a.bind(document,"click",a.onclick),!a.ispage&&a.opt.enablescrollonselection&&(a.bind(a.win[0],"mousedown",a.onselectionstart),a.bind(document,"mouseup",a.onselectionend),a.bind(a.cursor,
"mouseup",a.onselectionend),a.cursorh&&a.bind(a.cursorh,"mouseup",a.onselectionend),a.bind(document,"mousemove",a.onselectiondrag)),a.zoom&&(a.jqbind(a.zoom,"mouseenter",function(){a.canshowonmouseevent&&a.showCursor();a.rail.active=!0}),a.jqbind(a.zoom,"mouseleave",function(){a.rail.active=!1;a.rail.drag||a.hideCursor()}))):(a.bind(d.hasmousecapture?a.win:document,"mouseup",a.ontouchend),a.bind(document,"mousemove",a.ontouchmove),a.onclick&&a.bind(document,"click",a.onclick),a.opt.cursordragontouch&&
(a.bind(a.cursor,"mousedown",a.onmousedown),a.bind(a.cursor,"mousemove",a.onmousemove),a.cursorh&&a.bind(a.cursorh,"mousedown",function(c){a.onmousedown(c,!0)}),a.cursorh&&a.bind(a.cursorh,"mousemove",a.onmousemove)));a.opt.enablemousewheel&&(a.isiframe||a.bind(d.isie&&a.ispage?document:a.win,"mousewheel",a.onmousewheel),a.bind(a.rail,"mousewheel",a.onmousewheel),a.railh&&a.bind(a.railh,"mousewheel",a.onmousewheelhr));!a.ispage&&(!d.cantouch&&!/HTML|^BODY/.test(a.win[0].nodeName))&&(a.win.attr("tabindex")||
a.win.attr({tabindex:J++}),a.jqbind(a.win,"focus",function(c){y=a.getTarget(c).id||!0;a.hasfocus=!0;a.canshowonmouseevent&&a.noticeCursor()}),a.jqbind(a.win,"blur",function(c){y=!1;a.hasfocus=!1}),a.jqbind(a.win,"mouseenter",function(c){C=a.getTarget(c).id||!0;a.hasmousefocus=!0;a.canshowonmouseevent&&a.noticeCursor()}),a.jqbind(a.win,"mouseleave",function(){C=!1;a.hasmousefocus=!1;a.rail.drag||a.hideCursor()}))}a.onkeypress=function(c){if(a.locked&&0==a.page.maxh)return!0;c=c?c:window.e;var b=a.getTarget(c);
if(b&&/INPUT|TEXTAREA|SELECT|OPTION/.test(b.nodeName)&&(!b.getAttribute("type")&&!b.type||!/submit|button|cancel/i.tp)||e(b).attr("contenteditable"))return!0;if(a.hasfocus||a.hasmousefocus&&!y||a.ispage&&!y&&!C){b=c.keyCode;if(a.locked&&27!=b)return a.cancelEvent(c);var f=c.ctrlKey||!1,n=c.shiftKey||!1,d=!1;switch(b){case 38:case 63233:a.doScrollBy(72);d=!0;break;case 40:case 63235:a.doScrollBy(-72);d=!0;break;case 37:case 63232:a.railh&&(f?a.doScrollLeft(0):a.doScrollLeftBy(72),d=!0);break;case 39:case 63234:a.railh&&
(f?a.doScrollLeft(a.page.maxw):a.doScrollLeftBy(-72),d=!0);break;case 33:case 63276:a.doScrollBy(a.view.h);d=!0;break;case 34:case 63277:a.doScrollBy(-a.view.h);d=!0;break;case 36:case 63273:a.railh&&f?a.doScrollPos(0,0):a.doScrollTo(0);d=!0;break;case 35:case 63275:a.railh&&f?a.doScrollPos(a.page.maxw,a.page.maxh):a.doScrollTo(a.page.maxh);d=!0;break;case 32:a.opt.spacebarenabled&&(n?a.doScrollBy(a.view.h):a.doScrollBy(-a.view.h),d=!0);break;case 27:a.zoomactive&&(a.doZoom(),d=!0)}if(d)return a.cancelEvent(c)}};
a.opt.enablekeyboard&&a.bind(document,d.isopera&&!d.isopera12?"keypress":"keydown",a.onkeypress);a.bind(document,"keydown",function(c){c.ctrlKey&&(a.wheelprevented=!0)});a.bind(document,"keyup",function(c){c.ctrlKey||(a.wheelprevented=!1)});a.bind(window,"resize",a.lazyResize);a.bind(window,"orientationchange",a.lazyResize);a.bind(window,"load",a.lazyResize);if(d.ischrome&&!a.ispage&&!a.haswrapper){var r=a.win.attr("style"),f=parseFloat(a.win.css("width"))+1;a.win.css("width",f);a.synched("chromefix",
function(){a.win.attr("style",r)})}a.onAttributeChange=function(c){a.lazyResize(250)};!a.ispage&&!a.haswrapper&&(!1!==z?(a.observer=new z(function(c){c.forEach(a.onAttributeChange)}),a.observer.observe(a.win[0],{childList:!0,characterData:!1,attributes:!0,subtree:!1}),a.observerremover=new z(function(c){c.forEach(function(c){if(0<c.removedNodes.length)for(var b in c.removedNodes)if(c.removedNodes[b]==a.win[0])return a.remove()})}),a.observerremover.observe(a.win[0].parentNode,{childList:!0,characterData:!1,
attributes:!1,subtree:!1})):(a.bind(a.win,d.isie&&!d.isie9?"propertychange":"DOMAttrModified",a.onAttributeChange),d.isie9&&a.win[0].attachEvent("onpropertychange",a.onAttributeChange),a.bind(a.win,"DOMNodeRemoved",function(c){c.target==a.win[0]&&a.remove()})));!a.ispage&&a.opt.boxzoom&&a.bind(window,"resize",a.resizeZoom);a.istextarea&&a.bind(a.win,"mouseup",a.lazyResize);a.lazyResize(30)}if("IFRAME"==this.doc[0].nodeName){var I=function(c){a.iframexd=!1;try{var b="contentDocument"in this?this.contentDocument:
this.contentWindow.document}catch(f){a.iframexd=!0,b=!1}if(a.iframexd)return"console"in window&&console.log("NiceScroll error: policy restriced iframe"),!0;a.forcescreen=!0;a.isiframe&&(a.iframe={doc:e(b),html:a.doc.contents().find("html")[0],body:a.doc.contents().find("body")[0]},a.getContentSize=function(){return{w:Math.max(a.iframe.html.scrollWidth,a.iframe.body.scrollWidth),h:Math.max(a.iframe.html.scrollHeight,a.iframe.body.scrollHeight)}},a.docscroll=e(a.iframe.body));!d.isios&&(a.opt.iframeautoresize&&
!a.isiframe)&&(a.win.scrollTop(0),a.doc.height(""),c=Math.max(b.getElementsByTagName("html")[0].scrollHeight,b.body.scrollHeight),a.doc.height(c));a.lazyResize(30);d.isie7&&a.css(e(a.iframe.html),{"overflow-y":"hidden"});a.css(e(a.iframe.body),{"overflow-y":"hidden"});d.isios&&a.haswrapper&&a.css(e(b.body),{"-webkit-transform":"translate3d(0,0,0)"});"contentWindow"in this?a.bind(this.contentWindow,"scroll",a.onscroll):a.bind(b,"scroll",a.onscroll);a.opt.enablemousewheel&&a.bind(b,"mousewheel",a.onmousewheel);
a.opt.enablekeyboard&&a.bind(b,d.isopera?"keypress":"keydown",a.onkeypress);if(d.cantouch||a.opt.touchbehavior)a.bind(b,"mousedown",a.ontouchstart),a.bind(b,"mousemove",function(c){a.ontouchmove(c,!0)}),a.opt.grabcursorenabled&&d.cursorgrabvalue&&a.css(e(b.body),{cursor:d.cursorgrabvalue});a.bind(b,"mouseup",a.ontouchend);a.zoom&&(a.opt.dblclickzoom&&a.bind(b,"dblclick",a.doZoom),a.ongesturezoom&&a.bind(b,"gestureend",a.ongesturezoom))};this.doc[0].readyState&&"complete"==this.doc[0].readyState&&
setTimeout(function(){I.call(a.doc[0],!1)},500);a.bind(this.doc,"load",I)}};this.showCursor=function(c,b){a.cursortimeout&&(clearTimeout(a.cursortimeout),a.cursortimeout=0);if(a.rail){a.autohidedom&&(a.autohidedom.stop().css({opacity:a.opt.cursoropacitymax}),a.cursoractive=!0);if(!a.rail.drag||1!=a.rail.drag.pt)"undefined"!=typeof c&&!1!==c&&(a.scroll.y=Math.round(1*c/a.scrollratio.y)),"undefined"!=typeof b&&(a.scroll.x=Math.round(1*b/a.scrollratio.x));a.cursor.css({height:a.cursorheight,top:a.scroll.y});
a.cursorh&&(!a.rail.align&&a.rail.visibility?a.cursorh.css({width:a.cursorwidth,left:a.scroll.x+a.rail.width}):a.cursorh.css({width:a.cursorwidth,left:a.scroll.x}),a.cursoractive=!0);a.zoom&&a.zoom.stop().css({opacity:a.opt.cursoropacitymax})}};this.hideCursor=function(c){!a.cursortimeout&&(a.rail&&a.autohidedom&&!(a.hasmousefocus&&"leave"==a.opt.autohidemode))&&(a.cursortimeout=setTimeout(function(){if(!a.rail.active||!a.showonmouseevent)a.autohidedom.stop().animate({opacity:a.opt.cursoropacitymin}),
a.zoom&&a.zoom.stop().animate({opacity:a.opt.cursoropacitymin}),a.cursoractive=!1;a.cursortimeout=0},c||a.opt.hidecursordelay))};this.noticeCursor=function(c,b,f){a.showCursor(b,f);a.rail.active||a.hideCursor(c)};this.getContentSize=a.ispage?function(){return{w:Math.max(document.body.scrollWidth,document.documentElement.scrollWidth),h:Math.max(document.body.scrollHeight,document.documentElement.scrollHeight)}}:a.haswrapper?function(){return{w:a.doc.outerWidth()+parseInt(a.win.css("paddingLeft"))+
parseInt(a.win.css("paddingRight")),h:a.doc.outerHeight()+parseInt(a.win.css("paddingTop"))+parseInt(a.win.css("paddingBottom"))}}:function(){return{w:a.docscroll[0].scrollWidth,h:a.docscroll[0].scrollHeight}};this.onResize=function(c,b){if(!a||!a.win)return!1;if(!a.haswrapper&&!a.ispage){if("none"==a.win.css("display"))return a.visibility&&a.hideRail().hideRailHr(),!1;!a.hidden&&!a.visibility&&a.showRail().showRailHr()}var f=a.page.maxh,d=a.page.maxw,e=a.view.w;a.view={w:a.ispage?a.win.width():parseInt(a.win[0].clientWidth),
h:a.ispage?a.win.height():parseInt(a.win[0].clientHeight)};a.page=b?b:a.getContentSize();a.page.maxh=Math.max(0,a.page.h-a.view.h);a.page.maxw=Math.max(0,a.page.w-a.view.w);if(a.page.maxh==f&&a.page.maxw==d&&a.view.w==e){if(a.ispage)return a;f=a.win.offset();if(a.lastposition&&(d=a.lastposition,d.top==f.top&&d.left==f.left))return a;a.lastposition=f}0==a.page.maxh?(a.hideRail(),a.scrollvaluemax=0,a.scroll.y=0,a.scrollratio.y=0,a.cursorheight=0,a.setScrollTop(0),a.rail.scrollable=!1):a.rail.scrollable=
!0;0==a.page.maxw?(a.hideRailHr(),a.scrollvaluemaxw=0,a.scroll.x=0,a.scrollratio.x=0,a.cursorwidth=0,a.setScrollLeft(0),a.railh.scrollable=!1):a.railh.scrollable=!0;a.locked=0==a.page.maxh&&0==a.page.maxw;if(a.locked)return a.ispage||a.updateScrollBar(a.view),!1;!a.hidden&&!a.visibility?a.showRail().showRailHr():!a.hidden&&!a.railh.visibility&&a.showRailHr();a.istextarea&&(a.win.css("resize")&&"none"!=a.win.css("resize"))&&(a.view.h-=20);a.cursorheight=Math.min(a.view.h,Math.round(a.view.h*(a.view.h/
a.page.h)));a.cursorheight=a.opt.cursorfixedheight?a.opt.cursorfixedheight:Math.max(a.opt.cursorminheight,a.cursorheight);a.cursorwidth=Math.min(a.view.w,Math.round(a.view.w*(a.view.w/a.page.w)));a.cursorwidth=a.opt.cursorfixedheight?a.opt.cursorfixedheight:Math.max(a.opt.cursorminheight,a.cursorwidth);a.scrollvaluemax=a.view.h-a.cursorheight-a.cursor.hborder;a.railh&&(a.railh.width=0<a.page.maxh?a.view.w-a.rail.width:a.view.w,a.scrollvaluemaxw=a.railh.width-a.cursorwidth-a.cursorh.wborder);a.ispage||
a.updateScrollBar(a.view);a.scrollratio={x:a.page.maxw/a.scrollvaluemaxw,y:a.page.maxh/a.scrollvaluemax};a.getScrollTop()>a.page.maxh?a.doScrollTop(a.page.maxh):(a.scroll.y=Math.round(a.getScrollTop()*(1/a.scrollratio.y)),a.scroll.x=Math.round(a.getScrollLeft()*(1/a.scrollratio.x)),a.cursoractive&&a.noticeCursor());a.scroll.y&&0==a.getScrollTop()&&a.doScrollTo(Math.floor(a.scroll.y*a.scrollratio.y));return a};this.resize=a.onResize;this.lazyResize=function(c){c=isNaN(c)?30:c;a.delayed("resize",a.resize,
c);return a};this._bind=function(c,b,f,d){a.events.push({e:c,n:b,f:f,b:d,q:!1});c.addEventListener?c.addEventListener(b,f,d||!1):c.attachEvent?c.attachEvent("on"+b,f):c["on"+b]=f};this.jqbind=function(c,b,f){a.events.push({e:c,n:b,f:f,q:!0});e(c).bind(b,f)};this.bind=function(c,b,f,e){var h="jquery"in c?c[0]:c;"mousewheel"==b?"onwheel"in a.win?a._bind(h,"wheel",f,e||!1):(c="undefined"!=typeof document.onmousewheel?"mousewheel":"DOMMouseScroll",l(h,c,f,e||!1),"DOMMouseScroll"==c&&l(h,"MozMousePixelScroll",
f,e||!1)):h.addEventListener?(d.cantouch&&/mouseup|mousedown|mousemove/.test(b)&&a._bind(h,"mousedown"==b?"touchstart":"mouseup"==b?"touchend":"touchmove",function(a){if(a.touches){if(2>a.touches.length){var c=a.touches.length?a.touches[0]:a;c.original=a;f.call(this,c)}}else a.changedTouches&&(c=a.changedTouches[0],c.original=a,f.call(this,c))},e||!1),a._bind(h,b,f,e||!1),d.cantouch&&"mouseup"==b&&a._bind(h,"touchcancel",f,e||!1)):a._bind(h,b,function(c){if((c=c||window.event||!1)&&c.srcElement)c.target=
c.srcElement;"pageY"in c||(c.pageX=c.clientX+document.documentElement.scrollLeft,c.pageY=c.clientY+document.documentElement.scrollTop);return!1===f.call(h,c)||!1===e?a.cancelEvent(c):!0})};this._unbind=function(a,b,f,d){a.removeEventListener?a.removeEventListener(b,f,d):a.detachEvent?a.detachEvent("on"+b,f):a["on"+b]=!1};this.unbindAll=function(){for(var c=0;c<a.events.length;c++){var b=a.events[c];b.q?b.e.unbind(b.n,b.f):a._unbind(b.e,b.n,b.f,b.b)}};this.cancelEvent=function(a){a=a.original?a.original:
a?a:window.event||!1;if(!a)return!1;a.preventDefault&&a.preventDefault();a.stopPropagation&&a.stopPropagation();a.preventManipulation&&a.preventManipulation();a.cancelBubble=!0;a.cancel=!0;return a.returnValue=!1};this.stopPropagation=function(a){a=a.original?a.original:a?a:window.event||!1;if(!a)return!1;if(a.stopPropagation)return a.stopPropagation();a.cancelBubble&&(a.cancelBubble=!0);return!1};this.showRail=function(){if(0!=a.page.maxh&&(a.ispage||"none"!=a.win.css("display")))a.visibility=!0,
a.rail.visibility=!0,a.rail.css("display","block");return a};this.showRailHr=function(){if(!a.railh)return a;if(0!=a.page.maxw&&(a.ispage||"none"!=a.win.css("display")))a.railh.visibility=!0,a.railh.css("display","block");return a};this.hideRail=function(){a.visibility=!1;a.rail.visibility=!1;a.rail.css("display","none");return a};this.hideRailHr=function(){if(!a.railh)return a;a.railh.visibility=!1;a.railh.css("display","none");return a};this.show=function(){a.hidden=!1;a.locked=!1;return a.showRail().showRailHr()};
this.hide=function(){a.hidden=!0;a.locked=!0;return a.hideRail().hideRailHr()};this.toggle=function(){return a.hidden?a.show():a.hide()};this.remove=function(){a.stop();a.cursortimeout&&clearTimeout(a.cursortimeout);a.doZoomOut();a.unbindAll();d.isie9&&a.win[0].detachEvent("onpropertychange",a.onAttributeChange);!1!==a.observer&&a.observer.disconnect();!1!==a.observerremover&&a.observerremover.disconnect();a.events=null;a.cursor&&a.cursor.remove();a.cursorh&&a.cursorh.remove();a.rail&&a.rail.remove();
a.railh&&a.railh.remove();a.zoom&&a.zoom.remove();for(var c=0;c<a.saved.css.length;c++){var b=a.saved.css[c];b[0].css(b[1],"undefined"==typeof b[2]?"":b[2])}a.saved=!1;a.me.data("__nicescroll","");var f=e.nicescroll;f.each(function(c){if(this&&this.id===a.id){delete f[c];for(var b=++c;b<f.length;b++,c++)f[c]=f[b];f.length--;f.length&&delete f[f.length]}});for(var h in a)a[h]=null,delete a[h];a=null};this.scrollstart=function(c){this.onscrollstart=c;return a};this.scrollend=function(c){this.onscrollend=
c;return a};this.scrollcancel=function(c){this.onscrollcancel=c;return a};this.zoomin=function(c){this.onzoomin=c;return a};this.zoomout=function(c){this.onzoomout=c;return a};this.isScrollable=function(a){a=a.target?a.target:a;if("OPTION"==a.nodeName)return!0;for(;a&&1==a.nodeType&&!/^BODY|HTML/.test(a.nodeName);){var b=e(a),b=b.css("overflowY")||b.css("overflowX")||b.css("overflow")||"";if(/scroll|auto/.test(b))return a.clientHeight!=a.scrollHeight;a=a.parentNode?a.parentNode:!1}return!1};this.getViewport=
function(a){for(a=a&&a.parentNode?a.parentNode:!1;a&&1==a.nodeType&&!/^BODY|HTML/.test(a.nodeName);){var b=e(a);if(/fixed|absolute/.test(b.css("position")))return b;var f=b.css("overflowY")||b.css("overflowX")||b.css("overflow")||"";if(/scroll|auto/.test(f)&&a.clientHeight!=a.scrollHeight||0<b.getNiceScroll().length)return b;a=a.parentNode?a.parentNode:!1}return a?e(a):!1};this.triggerScrollEnd=function(){if(a.onscrollend){var c=a.getScrollLeft(),b=a.getScrollTop();a.onscrollend.call(a,{type:"scrollend",
current:{x:c,y:b},end:{x:c,y:b}})}};this.onmousewheel=function(c){if(!a.wheelprevented){if(a.locked)return a.debounced("checkunlock",a.resize,250),!0;if(a.rail.drag)return a.cancelEvent(c);"auto"==a.opt.oneaxismousemode&&0!=c.deltaX&&(a.opt.oneaxismousemode=!1);if(a.opt.oneaxismousemode&&0==c.deltaX&&!a.rail.scrollable)return a.railh&&a.railh.scrollable?a.onmousewheelhr(c):!0;var b=+new Date,f=!1;a.opt.preservenativescrolling&&a.checkarea+600<b&&(a.nativescrollingarea=a.isScrollable(c),f=!0);a.checkarea=
b;if(a.nativescrollingarea)return!0;if(c=q(c,!1,f))a.checkarea=0;return c}};this.onmousewheelhr=function(c){if(!a.wheelprevented){if(a.locked||!a.railh.scrollable)return!0;if(a.rail.drag)return a.cancelEvent(c);var b=+new Date,f=!1;a.opt.preservenativescrolling&&a.checkarea+600<b&&(a.nativescrollingarea=a.isScrollable(c),f=!0);a.checkarea=b;return a.nativescrollingarea?!0:a.locked?a.cancelEvent(c):q(c,!0,f)}};this.stop=function(){a.cancelScroll();a.scrollmon&&a.scrollmon.stop();a.cursorfreezed=!1;
a.scroll.y=Math.round(a.getScrollTop()*(1/a.scrollratio.y));a.noticeCursor();return a};this.getTransitionSpeed=function(b){var d=Math.round(10*a.opt.scrollspeed);b=Math.min(d,Math.round(b/20*a.opt.scrollspeed));return 20<b?b:0};a.opt.smoothscroll?a.ishwscroll&&d.hastransition&&a.opt.usetransition?(this.prepareTransition=function(b,e){var f=e?20<b?b:0:a.getTransitionSpeed(b),h=f?d.prefixstyle+"transform "+f+"ms ease-out":"";if(!a.lasttransitionstyle||a.lasttransitionstyle!=h)a.lasttransitionstyle=
h,a.doc.css(d.transitionstyle,h);return f},this.doScrollLeft=function(b,d){var f=a.scrollrunning?a.newscrolly:a.getScrollTop();a.doScrollPos(b,f,d)},this.doScrollTop=function(b,d){var f=a.scrollrunning?a.newscrollx:a.getScrollLeft();a.doScrollPos(f,b,d)},this.doScrollPos=function(b,e,f){var h=a.getScrollTop(),g=a.getScrollLeft();(0>(a.newscrolly-h)*(e-h)||0>(a.newscrollx-g)*(b-g))&&a.cancelScroll();!1==a.opt.bouncescroll&&(0>e?e=0:e>a.page.maxh&&(e=a.page.maxh),0>b?b=0:b>a.page.maxw&&(b=a.page.maxw));
if(a.scrollrunning&&b==a.newscrollx&&e==a.newscrolly)return!1;a.newscrolly=e;a.newscrollx=b;a.newscrollspeed=f||!1;if(a.timer)return!1;a.timer=setTimeout(function(){var f=a.getScrollTop(),h=a.getScrollLeft(),g,k;g=b-h;k=e-f;g=Math.round(Math.sqrt(Math.pow(g,2)+Math.pow(k,2)));g=a.newscrollspeed&&1<a.newscrollspeed?a.newscrollspeed:a.getTransitionSpeed(g);a.newscrollspeed&&1>=a.newscrollspeed&&(g*=a.newscrollspeed);a.prepareTransition(g,!0);a.timerscroll&&a.timerscroll.tm&&clearInterval(a.timerscroll.tm);
0<g&&(!a.scrollrunning&&a.onscrollstart&&a.onscrollstart.call(a,{type:"scrollstart",current:{x:h,y:f},request:{x:b,y:e},end:{x:a.newscrollx,y:a.newscrolly},speed:g}),d.transitionend?a.scrollendtrapped||(a.scrollendtrapped=!0,a.bind(a.doc,d.transitionend,a.onScrollTransitionEnd,!1)):(a.scrollendtrapped&&clearTimeout(a.scrollendtrapped),a.scrollendtrapped=setTimeout(a.onScrollTransitionEnd,g)),a.timerscroll={bz:new BezierClass(f,a.newscrolly,g,0,0,0.58,1),bh:new BezierClass(h,a.newscrollx,g,0,0,0.58,
1)},a.cursorfreezed||(a.timerscroll.tm=setInterval(function(){a.showCursor(a.getScrollTop(),a.getScrollLeft())},60)));a.synched("doScroll-set",function(){a.timer=0;a.scrollendtrapped&&(a.scrollrunning=!0);a.setScrollTop(a.newscrolly);a.setScrollLeft(a.newscrollx);if(!a.scrollendtrapped)a.onScrollTransitionEnd()})},50)},this.cancelScroll=function(){if(!a.scrollendtrapped)return!0;var b=a.getScrollTop(),e=a.getScrollLeft();a.scrollrunning=!1;d.transitionend||clearTimeout(d.transitionend);a.scrollendtrapped=
!1;a._unbind(a.doc,d.transitionend,a.onScrollTransitionEnd);a.prepareTransition(0);a.setScrollTop(b);a.railh&&a.setScrollLeft(e);a.timerscroll&&a.timerscroll.tm&&clearInterval(a.timerscroll.tm);a.timerscroll=!1;a.cursorfreezed=!1;a.showCursor(b,e);return a},this.onScrollTransitionEnd=function(){a.scrollendtrapped&&a._unbind(a.doc,d.transitionend,a.onScrollTransitionEnd);a.scrollendtrapped=!1;a.prepareTransition(0);a.timerscroll&&a.timerscroll.tm&&clearInterval(a.timerscroll.tm);a.timerscroll=!1;var b=
a.getScrollTop(),e=a.getScrollLeft();a.setScrollTop(b);a.railh&&a.setScrollLeft(e);a.noticeCursor(!1,b,e);a.cursorfreezed=!1;0>b?b=0:b>a.page.maxh&&(b=a.page.maxh);0>e?e=0:e>a.page.maxw&&(e=a.page.maxw);if(b!=a.newscrolly||e!=a.newscrollx)return a.doScrollPos(e,b,a.opt.snapbackspeed);a.onscrollend&&a.scrollrunning&&a.triggerScrollEnd();a.scrollrunning=!1}):(this.doScrollLeft=function(b,d){var f=a.scrollrunning?a.newscrolly:a.getScrollTop();a.doScrollPos(b,f,d)},this.doScrollTop=function(b,d){var f=
a.scrollrunning?a.newscrollx:a.getScrollLeft();a.doScrollPos(f,b,d)},this.doScrollPos=function(b,d,f){function e(){if(a.cancelAnimationFrame)return!0;a.scrollrunning=!0;if(p=1-p)return a.timer=s(e)||1;var b=0,c=sy=a.getScrollTop();if(a.dst.ay){var c=a.bzscroll?a.dst.py+a.bzscroll.getNow()*a.dst.ay:a.newscrolly,f=c-sy;if(0>f&&c<a.newscrolly||0<f&&c>a.newscrolly)c=a.newscrolly;a.setScrollTop(c);c==a.newscrolly&&(b=1)}else b=1;var d=sx=a.getScrollLeft();if(a.dst.ax){d=a.bzscroll?a.dst.px+a.bzscroll.getNow()*
a.dst.ax:a.newscrollx;f=d-sx;if(0>f&&d<a.newscrollx||0<f&&d>a.newscrollx)d=a.newscrollx;a.setScrollLeft(d);d==a.newscrollx&&(b+=1)}else b+=1;2==b?(a.timer=0,a.cursorfreezed=!1,a.bzscroll=!1,a.scrollrunning=!1,0>c?c=0:c>a.page.maxh&&(c=a.page.maxh),0>d?d=0:d>a.page.maxw&&(d=a.page.maxw),d!=a.newscrollx||c!=a.newscrolly?a.doScrollPos(d,c):a.onscrollend&&a.triggerScrollEnd()):a.timer=s(e)||1}d="undefined"==typeof d||!1===d?a.getScrollTop(!0):d;if(a.timer&&a.newscrolly==d&&a.newscrollx==b)return!0;a.timer&&
v(a.timer);a.timer=0;var h=a.getScrollTop(),g=a.getScrollLeft();(0>(a.newscrolly-h)*(d-h)||0>(a.newscrollx-g)*(b-g))&&a.cancelScroll();a.newscrolly=d;a.newscrollx=b;if(!a.bouncescroll||!a.rail.visibility)0>a.newscrolly?a.newscrolly=0:a.newscrolly>a.page.maxh&&(a.newscrolly=a.page.maxh);if(!a.bouncescroll||!a.railh.visibility)0>a.newscrollx?a.newscrollx=0:a.newscrollx>a.page.maxw&&(a.newscrollx=a.page.maxw);a.dst={};a.dst.x=b-g;a.dst.y=d-h;a.dst.px=g;a.dst.py=h;var k=Math.round(Math.sqrt(Math.pow(a.dst.x,
2)+Math.pow(a.dst.y,2)));a.dst.ax=a.dst.x/k;a.dst.ay=a.dst.y/k;var l=0,q=k;0==a.dst.x?(l=h,q=d,a.dst.ay=1,a.dst.py=0):0==a.dst.y&&(l=g,q=b,a.dst.ax=1,a.dst.px=0);k=a.getTransitionSpeed(k);f&&1>=f&&(k*=f);a.bzscroll=0<k?a.bzscroll?a.bzscroll.update(q,k):new BezierClass(l,q,k,0,1,0,1):!1;if(!a.timer){(h==a.page.maxh&&d>=a.page.maxh||g==a.page.maxw&&b>=a.page.maxw)&&a.checkContentSize();var p=1;a.cancelAnimationFrame=!1;a.timer=1;a.onscrollstart&&!a.scrollrunning&&a.onscrollstart.call(a,{type:"scrollstart",
current:{x:g,y:h},request:{x:b,y:d},end:{x:a.newscrollx,y:a.newscrolly},speed:k});e();(h==a.page.maxh&&d>=h||g==a.page.maxw&&b>=g)&&a.checkContentSize();a.noticeCursor()}},this.cancelScroll=function(){a.timer&&v(a.timer);a.timer=0;a.bzscroll=!1;a.scrollrunning=!1;return a}):(this.doScrollLeft=function(b,d){var f=a.getScrollTop();a.doScrollPos(b,f,d)},this.doScrollTop=function(b,d){var f=a.getScrollLeft();a.doScrollPos(f,b,d)},this.doScrollPos=function(b,d,f){var e=b>a.page.maxw?a.page.maxw:b;0>e&&
(e=0);var h=d>a.page.maxh?a.page.maxh:d;0>h&&(h=0);a.synched("scroll",function(){a.setScrollTop(h);a.setScrollLeft(e)})},this.cancelScroll=function(){});this.doScrollBy=function(b,d){var f=0,f=d?Math.floor((a.scroll.y-b)*a.scrollratio.y):(a.timer?a.newscrolly:a.getScrollTop(!0))-b;if(a.bouncescroll){var e=Math.round(a.view.h/2);f<-e?f=-e:f>a.page.maxh+e&&(f=a.page.maxh+e)}a.cursorfreezed=!1;py=a.getScrollTop(!0);if(0>f&&0>=py)return a.noticeCursor();if(f>a.page.maxh&&py>=a.page.maxh)return a.checkContentSize(),
a.noticeCursor();a.doScrollTop(f)};this.doScrollLeftBy=function(b,d){var f=0,f=d?Math.floor((a.scroll.x-b)*a.scrollratio.x):(a.timer?a.newscrollx:a.getScrollLeft(!0))-b;if(a.bouncescroll){var e=Math.round(a.view.w/2);f<-e?f=-e:f>a.page.maxw+e&&(f=a.page.maxw+e)}a.cursorfreezed=!1;px=a.getScrollLeft(!0);if(0>f&&0>=px||f>a.page.maxw&&px>=a.page.maxw)return a.noticeCursor();a.doScrollLeft(f)};this.doScrollTo=function(b,d){d&&Math.round(b*a.scrollratio.y);a.cursorfreezed=!1;a.doScrollTop(b)};this.checkContentSize=
function(){var b=a.getContentSize();(b.h!=a.page.h||b.w!=a.page.w)&&a.resize(!1,b)};a.onscroll=function(b){a.rail.drag||a.cursorfreezed||a.synched("scroll",function(){a.scroll.y=Math.round(a.getScrollTop()*(1/a.scrollratio.y));a.railh&&(a.scroll.x=Math.round(a.getScrollLeft()*(1/a.scrollratio.x)));a.noticeCursor()})};a.bind(a.docscroll,"scroll",a.onscroll);this.doZoomIn=function(b){if(!a.zoomactive){a.zoomactive=!0;a.zoomrestore={style:{}};var h="position top left zIndex backgroundColor marginTop marginBottom marginLeft marginRight".split(" "),
f=a.win[0].style,g;for(g in h){var k=h[g];a.zoomrestore.style[k]="undefined"!=typeof f[k]?f[k]:""}a.zoomrestore.style.width=a.win.css("width");a.zoomrestore.style.height=a.win.css("height");a.zoomrestore.padding={w:a.win.outerWidth()-a.win.width(),h:a.win.outerHeight()-a.win.height()};d.isios4&&(a.zoomrestore.scrollTop=e(window).scrollTop(),e(window).scrollTop(0));a.win.css({position:d.isios4?"absolute":"fixed",top:0,left:0,"z-index":x+100,margin:"0px"});h=a.win.css("backgroundColor");(""==h||/transparent|rgba\(0, 0, 0, 0\)|rgba\(0,0,0,0\)/.test(h))&&
a.win.css("backgroundColor","#fff");a.rail.css({"z-index":x+101});a.zoom.css({"z-index":x+102});a.zoom.css("backgroundPosition","0px -18px");a.resizeZoom();a.onzoomin&&a.onzoomin.call(a);return a.cancelEvent(b)}};this.doZoomOut=function(b){if(a.zoomactive)return a.zoomactive=!1,a.win.css("margin",""),a.win.css(a.zoomrestore.style),d.isios4&&e(window).scrollTop(a.zoomrestore.scrollTop),a.rail.css({"z-index":a.zindex}),a.zoom.css({"z-index":a.zindex}),a.zoomrestore=!1,a.zoom.css("backgroundPosition",
"0px 0px"),a.onResize(),a.onzoomout&&a.onzoomout.call(a),a.cancelEvent(b)};this.doZoom=function(b){return a.zoomactive?a.doZoomOut(b):a.doZoomIn(b)};this.resizeZoom=function(){if(a.zoomactive){var b=a.getScrollTop();a.win.css({width:e(window).width()-a.zoomrestore.padding.w+"px",height:e(window).height()-a.zoomrestore.padding.h+"px"});a.onResize();a.setScrollTop(Math.min(a.page.maxh,b))}};this.init();e.nicescroll.push(this)},H=function(e){var b=this;this.nc=e;this.steptime=this.lasttime=this.speedy=
this.speedx=this.lasty=this.lastx=0;this.snapy=this.snapx=!1;this.demuly=this.demulx=0;this.lastscrolly=this.lastscrollx=-1;this.timer=this.chky=this.chkx=0;this.time=function(){return+new Date};this.reset=function(e,g){b.stop();var l=b.time();b.steptime=0;b.lasttime=l;b.speedx=0;b.speedy=0;b.lastx=e;b.lasty=g;b.lastscrollx=-1;b.lastscrolly=-1};this.update=function(e,g){var l=b.time();b.steptime=l-b.lasttime;b.lasttime=l;var l=g-b.lasty,q=e-b.lastx,a=b.nc.getScrollTop(),p=b.nc.getScrollLeft(),a=a+
l,p=p+q;b.snapx=0>p||p>b.nc.page.maxw;b.snapy=0>a||a>b.nc.page.maxh;b.speedx=q;b.speedy=l;b.lastx=e;b.lasty=g};this.stop=function(){b.nc.unsynched("domomentum2d");b.timer&&clearTimeout(b.timer);b.timer=0;b.lastscrollx=-1;b.lastscrolly=-1};this.doSnapy=function(e,g){var l=!1;0>g?(g=0,l=!0):g>b.nc.page.maxh&&(g=b.nc.page.maxh,l=!0);0>e?(e=0,l=!0):e>b.nc.page.maxw&&(e=b.nc.page.maxw,l=!0);l?b.nc.doScrollPos(e,g,b.nc.opt.snapbackspeed):b.nc.triggerScrollEnd()};this.doMomentum=function(e){var g=b.time(),
l=e?g+e:b.lasttime;e=b.nc.getScrollLeft();var q=b.nc.getScrollTop(),a=b.nc.page.maxh,p=b.nc.page.maxw;b.speedx=0<p?Math.min(60,b.speedx):0;b.speedy=0<a?Math.min(60,b.speedy):0;l=l&&60>=g-l;if(0>q||q>a||0>e||e>p)l=!1;e=b.speedx&&l?b.speedx:!1;if(b.speedy&&l&&b.speedy||e){var d=Math.max(16,b.steptime);50<d&&(e=d/50,b.speedx*=e,b.speedy*=e,d=50);b.demulxy=0;b.lastscrollx=b.nc.getScrollLeft();b.chkx=b.lastscrollx;b.lastscrolly=b.nc.getScrollTop();b.chky=b.lastscrolly;var r=b.lastscrollx,t=b.lastscrolly,
s=function(){var c=600<b.time()-g?0.04:0.02;if(b.speedx&&(r=Math.floor(b.lastscrollx-b.speedx*(1-b.demulxy)),b.lastscrollx=r,0>r||r>p))c=0.1;if(b.speedy&&(t=Math.floor(b.lastscrolly-b.speedy*(1-b.demulxy)),b.lastscrolly=t,0>t||t>a))c=0.1;b.demulxy=Math.min(1,b.demulxy+c);b.nc.synched("domomentum2d",function(){b.speedx&&(b.nc.getScrollLeft()!=b.chkx&&b.stop(),b.chkx=r,b.nc.setScrollLeft(r));b.speedy&&(b.nc.getScrollTop()!=b.chky&&b.stop(),b.chky=t,b.nc.setScrollTop(t));b.timer||(b.nc.hideCursor(),
b.doSnapy(r,t))});1>b.demulxy?b.timer=setTimeout(s,d):(b.stop(),b.nc.hideCursor(),b.doSnapy(r,t))};s()}else b.doSnapy(b.nc.getScrollLeft(),b.nc.getScrollTop())}},w=e.fn.scrollTop;e.cssHooks.pageYOffset={get:function(g,b,h){return(b=e.data(g,"__nicescroll")||!1)&&b.ishwscroll?b.getScrollTop():w.call(g)},set:function(g,b){var h=e.data(g,"__nicescroll")||!1;h&&h.ishwscroll?h.setScrollTop(parseInt(b)):w.call(g,b);return this}};e.fn.scrollTop=function(g){if("undefined"==typeof g){var b=this[0]?e.data(this[0],
"__nicescroll")||!1:!1;return b&&b.ishwscroll?b.getScrollTop():w.call(this)}return this.each(function(){var b=e.data(this,"__nicescroll")||!1;b&&b.ishwscroll?b.setScrollTop(parseInt(g)):w.call(e(this),g)})};var A=e.fn.scrollLeft;e.cssHooks.pageXOffset={get:function(g,b,h){return(b=e.data(g,"__nicescroll")||!1)&&b.ishwscroll?b.getScrollLeft():A.call(g)},set:function(g,b){var h=e.data(g,"__nicescroll")||!1;h&&h.ishwscroll?h.setScrollLeft(parseInt(b)):A.call(g,b);return this}};e.fn.scrollLeft=function(g){if("undefined"==
typeof g){var b=this[0]?e.data(this[0],"__nicescroll")||!1:!1;return b&&b.ishwscroll?b.getScrollLeft():A.call(this)}return this.each(function(){var b=e.data(this,"__nicescroll")||!1;b&&b.ishwscroll?b.setScrollLeft(parseInt(g)):A.call(e(this),g)})};var B=function(g){var b=this;this.length=0;this.name="nicescrollarray";this.each=function(e){for(var g=0,a=0;g<b.length;g++)e.call(b[g],a++);return b};this.push=function(e){b[b.length]=e;b.length++};this.eq=function(e){return b[e]};if(g)for(var h=0;h<g.length;h++){var k=
e.data(g[h],"__nicescroll")||!1;k&&(this[this.length]=k,this.length++)}return this};(function(e,b,h){for(var k=0;k<b.length;k++)h(e,b[k])})(B.prototype,"show hide toggle onResize resize remove stop doScrollPos".split(" "),function(e,b){e[b]=function(){var e=arguments;return this.each(function(){this[b].apply(this,e)})}});e.fn.getNiceScroll=function(g){return"undefined"==typeof g?new B(this):this[g]&&e.data(this[g],"__nicescroll")||!1};e.extend(e.expr[":"],{nicescroll:function(g){return e.data(g,"__nicescroll")?
!0:!1}});e.fn.niceScroll=function(g,b){"undefined"==typeof b&&("object"==typeof g&&!("jquery"in g))&&(b=g,g=!1);var h=new B;"undefined"==typeof b&&(b={});g&&(b.doc=e(g),b.win=e(this));var k=!("doc"in b);!k&&!("win"in b)&&(b.win=e(this));this.each(function(){var g=e(this).data("__nicescroll")||!1;g||(b.doc=k?e(this):b.doc,g=new N(b,e(this)),e(this).data("__nicescroll",g));h.push(g)});return 1==h.length?h[0]:h};window.NiceScroll={getjQuery:function(){return e}};e.nicescroll||(e.nicescroll=new B,e.nicescroll.options=
G)});

/*** fullPage 2.0.9 https://github.com/alvarotrigo/fullPage.js */
!function(e){e.fn.fullpage=function(n){function t(t){var o=t.originalEvent;if(n.autoScrolling&&t.preventDefault(),!i(t.target)){var l,a=e(".section.active");if(!q&&!R){var s=z(o);if(K=s.y,Q=s.x,a.find(".slides").length&&Math.abs(j-Q)>Math.abs(X-K))Math.abs(j-Q)>e(window).width()/100*n.touchSensitivity&&(j>Q?e.fn.fullpage.moveSlideRight():e.fn.fullpage.moveSlideLeft());else if(n.autoScrolling&&(l=a.find(".slides").length?a.find(".slide.active").find(".scrollable"):a.find(".scrollable"),Math.abs(X-K)>e(window).height()/100*n.touchSensitivity))if(X>K)if(l.length>0){if(!h("bottom",l))return!0;e.fn.fullpage.moveSectionDown()}else e.fn.fullpage.moveSectionDown();else if(K>X)if(l.length>0){if(!h("top",l))return!0;e.fn.fullpage.moveSectionUp()}else e.fn.fullpage.moveSectionUp()}}}function i(t,o){o=o||0;var l=e(t).parent();return o<n.normalScrollElementTouchThreshold&&l.is(n.normalScrollElements)?!0:o==n.normalScrollElementTouchThreshold?!1:i(l,++o)}function o(e){var n=e.originalEvent,t=z(n);X=t.y,j=t.x}function l(t){if(n.autoScrolling){t=window.event||t;var i,o=Math.max(-1,Math.min(1,t.wheelDelta||-t.deltaY||-t.detail)),l=e(".section.active");if(!q)if(i=l.find(".slides").length?l.find(".slide.active").find(".scrollable"):l.find(".scrollable"),0>o)if(i.length>0){if(!h("bottom",i))return!0;e.fn.fullpage.moveSectionDown()}else e.fn.fullpage.moveSectionDown();else if(i.length>0){if(!h("top",i))return!0;e.fn.fullpage.moveSectionUp()}else e.fn.fullpage.moveSectionUp();return!1}}function a(t){var i=e(".section.active"),o=i.find(".slides");if(o.length&&!R){var l=o.find(".slide.active"),a=null;if(a="prev"===t?l.prev(".slide"):l.next(".slide"),!a.length){if(!n.loopHorizontal)return;a="prev"===t?l.siblings(":last"):l.siblings(":first")}R=!0,c(o,a,l)}}function s(t,i,o){var l,a={},s=t.position();if("undefined"!=typeof s){var r="footer"==t.attr("data-type")?s.top-(V-t.height()):s.top,c=p(t),d=t.data("anchor"),f=t.index(".section"),h=t.find(".slide.active"),g=e(".section.active"),m=g.index(".section")+1,w=O;if(h.length)var S=h.data("anchor"),x=h.index();if(n.autoScrolling&&n.continuousVertical&&"undefined"!=typeof o&&(!o&&"up"==c||o&&"down"==c)){o?e(".section.active").before(g.nextAll(".section")):e(".section.active").after(g.prevAll(".section").get().reverse()),k(e(".section.active").position().top);var y=g;s=t.position(),r=s.top,c=p(t)}t.addClass("active").siblings().removeClass("active"),q=!0,"undefined"!=typeof d&&T(x,S,d),n.autoScrolling?(a.top=-r,l=I.selector):(a.scrollTop=r,l="html, body");var C=function(){y&&y.length&&(o?e(".section:first").before(y):e(".section:last").after(y),k(e(".section.active").position().top))};if(n.css3&&n.autoScrolling){e.isFunction(n.onLeave)&&!w&&n.onLeave.call(this,m,f+1,c);var L="translate3d(0px, -"+r+"px, 0px)";b(L,!0),setTimeout(function(){C(),e.isFunction(n.afterLoad)&&!w&&n.afterLoad.call(this,d,f+1),setTimeout(function(){q=!1,e.isFunction(i)&&i.call(this)},H)},n.scrollingSpeed)}else e.isFunction(n.onLeave)&&!w&&n.onLeave.call(this,m,f+1,c),e(l).animate(a,n.scrollingSpeed,n.easing,function(){C(),e.isFunction(n.afterLoad)&&!w&&n.afterLoad.call(this,d,f+1),setTimeout(function(){q=!1,e.isFunction(i)&&i.call(this)},H)});B=d,n.autoScrolling&&(v(d),u(d,f))}}function r(){var e=window.location.hash.replace("#","").split("/"),n=e[0],t=e[1];n&&x(n,t)}function c(t,i,o){var l=i.position(),a=t.find(".slidesContainer").parent(),s=i.index(),r=t.closest(".section"),c=r.index(".section"),d=r.data("anchor"),f=r.find(".fullPage-slidesNav"),u=i.data("anchor"),v=O;if(n.onSlideLeave){var h=r.find(".slide.active").index(),p=g(h,s);v||e.isFunction(n.onSlideLeave)&&n.onSlideLeave.call(this,d,c+1,h,p)}if(i.addClass("active").siblings().removeClass("active"),"undefined"==typeof u&&(u=s),r.hasClass("active")&&(n.loopHorizontal||(r.find(".controlArrow.prev").toggle(0!=s),r.find(".controlArrow.next").toggle(!i.is(":last-child"))),T(s,u,d)),n.css3){"translate3d(-"+l.left+"px, 0px, 0px)";"undefined"!=typeof o&&o.css({"z-index":"0"}).animate({opacity:"0"},1e3),i.css({"z-index":"999"}).animate({opacity:"1"},1e3),setTimeout(function(){v||e.isFunction(n.afterSlideLoad)&&n.afterSlideLoad.call(this,d,c+1,u,s),R=!1},n.scrollingSpeed,n.easing)}else a.animate({scrollLeft:l.left},n.scrollingSpeed,n.easing,function(){v||e.isFunction(n.afterSlideLoad)&&n.afterSlideLoad.call(this,d,c+1,u,s),R=!1});f.find(".active").removeClass("active"),f.find("li").eq(s).find("a").addClass("active")}function d(){O=!0;var t=e(window).width();V=e(window).height()-e("#Header").outerHeight(),n.resize&&f(V,t),e(".section").each(function(){V-parseInt(e(this).css("padding-bottom"))-parseInt(e(this).css("padding-top"));if(n.verticalCentered&&e(this).find(".tableCell").css("height",S(e(this))+"px"),"footer"==e(this).attr("data-type")?(e(this).removeAttr("style"),e(this).css("height",e(this).outerHeight()+"px")):e(this).css("height",V+"px"),n.scrollOverflow){var t=e(this).find(".slide");t.length?t.each(function(){m(e(this))}):m(e(this))}var t=e(this).find(".slides");t.length&&c(t,t.find(".slide.active"))});var i=(e(".section.active").position(),e(".section.active"));i.index(".section")&&s(i),O=!1,e.isFunction(n.afterResize)&&n.afterResize.call(this)}function f(n,t){var i=825,o=n;if(825>n||900>t){900>t&&(o=t,i=900);var l=100*o/i,a=l.toFixed(2);e("body").css("font-size",a+"%")}else e("body").css("font-size","100%")}function u(t,i){n.navigation&&(e("#fullPage-nav").find(".active").removeClass("active"),t?e("#fullPage-nav").find('a[href="#'+t+'"]').addClass("active"):e("#fullPage-nav").find("li").eq(i).find("a").addClass("active"))}function v(t){n.menu&&(e(n.menu).find(".active").removeClass("active"),e(n.menu).find('[data-menuanchor="'+t+'"]').addClass("active"))}function h(e,n){return"top"===e?!n.scrollTop():"bottom"===e?n.scrollTop()+1+n.innerHeight()>=n[0].scrollHeight:void 0}function p(n){var t=e(".section.active").index(".section"),i=n.index(".section");return t>i?"up":"down"}function g(e,n){return e==n?"none":e>n?"left":"right"}function m(e){e.css("overflow","hidden");var t=e.closest(".section"),i=e.find(".scrollable");if(i.length)var o=e.find(".scrollable").get(0).scrollHeight;else{var o=e.get(0).scrollHeight;n.verticalCentered&&(o=e.find(".tableCell").get(0).scrollHeight)}var l=V-parseInt(t.css("padding-bottom"))-parseInt(t.css("padding-top"));o>l?i.length?i.css("height",l+"px").parent().css("height",l+"px"):(n.verticalCentered?e.find(".tableCell").wrapInner('<div class="scrollable" />'):e.wrapInner('<div class="scrollable" />'),e.find(".scrollable").slimScroll({height:l+"px",size:"10px",alwaysVisible:!0})):(e.find(".scrollable").children().first().unwrap().unwrap(),e.find(".slimScrollBar").remove(),e.find(".slimScrollRail").remove()),e.css("overflow","")}function w(e){e.addClass("table").wrapInner('<div class="tableCell" style="height:'+S(e)+'px;" />')}function S(e){var t=V;if(n.paddingTop||n.paddingBottom){var i=e;i.hasClass("section")||(i=e.closest(".section"));var o=parseInt(i.css("padding-top"))+parseInt(i.css("padding-bottom"));t=V-o}return t}function b(e,n){I.toggleClass("easing",n),I.css(F(e))}function x(n,t){if("undefined"==typeof t&&(t=0),isNaN(n))var i=e('[data-anchor="'+n+'"]');else var i=e(".section").eq(n-1);n===B||i.hasClass("active")?y(i,t):s(i,function(){y(i,t)})}function y(e,n){if("undefined"!=typeof n){var t=e.find(".slides"),i=t.find('[data-anchor="'+n+'"]');i.length||(i=t.find(".slide").eq(n)),i.length&&(console.log("1455"),c(t,i))}}function C(e,t){e.append('<div class="fullPage-slidesNav"><ul></ul></div>');var i=e.find(".fullPage-slidesNav");i.addClass(n.slidesNavPosition);for(var o=0;t>o;o++)i.find("ul").append('<li><a href="#"><span></span></a></li>');i.css("margin-left","-"+i.width()/2+"px"),i.find("li").first().find("a").addClass("active")}function T(e,t,i){var o="";n.anchors.length&&(e?("undefined"!=typeof i&&(o=i),"undefined"==typeof t&&(t=e),D=t,location.hash=o+"/"+t):"undefined"!=typeof e?(D=t,location.hash=i):location.hash=i)}function L(){var e,n=document.createElement("p"),t={webkitTransform:"-webkit-transform",OTransform:"-o-transform",msTransform:"-ms-transform",MozTransform:"-moz-transform",transform:"transform"};document.body.insertBefore(n,null);for(var i in t)void 0!==n.style[i]&&(n.style[i]="translate3d(1px,1px,1px)",e=window.getComputedStyle(n).getPropertyValue(t[i]));return document.body.removeChild(n),void 0!==e&&e.length>0&&"none"!==e}function P(){document.addEventListener?(document.removeEventListener("mousewheel",l,!1),document.removeEventListener("wheel",l,!1)):document.detachEvent("onmousewheel",l)}function A(){document.addEventListener?(document.addEventListener("mousewheel",l,!1),document.addEventListener("wheel",l,!1)):document.attachEvent("onmousewheel",l)}function E(){N&&(e(document).off("touchstart MSPointerDown").on("touchstart MSPointerDown",o),e(document).off("touchmove MSPointerMove").on("touchmove MSPointerMove",t))}function M(){N&&(e(document).off("touchstart MSPointerDown"),e(document).off("touchmove MSPointerMove"))}function z(e){var n=new Array;return window.navigator.msPointerEnabled?(n.y=e.pageY,n.x=e.pageX):(n.y=e.touches[0].pageY,n.x=e.touches[0].pageX),n}function k(e){if(n.css3){var t="translate3d(0px, -"+e+"px, 0px)";b(t,!1)}else I.css("top",-e)}function F(e){return{"-webkit-transform":e,"-moz-transform":e,"-ms-transform":e,transform:e}}n=e.extend({verticalCentered:!0,resize:!0,slidesColor:[],anchors:[],scrollingSpeed:700,easing:"swing",menu:!1,navigation:!1,navigationPosition:"right",navigationColor:"#000",navigationTooltips:[],slidesNavigation:!1,slidesNavPosition:"bottom",controlArrowColor:"#fff",loopBottom:!1,loopTop:!1,loopHorizontal:!0,autoScrolling:!0,scrollOverflow:!1,css3:!1,paddingTop:0,paddingBottom:0,fixedElements:null,normalScrollElements:null,keyboardScrolling:!0,touchSensitivity:5,continuousVertical:!1,animateAnchor:!0,normalScrollElementTouchThreshold:5,afterLoad:null,onLeave:null,afterRender:null,afterResize:null,afterSlideLoad:null,onSlideLeave:null},n),n.continuousVertical&&(n.loopTop||n.loopBottom)&&(n.continuousVertical=!1,console&&console.log&&console.log("Option loopTop/loopBottom is mutually exclusive with continuousVertical; continuousVertical disabled"));var H=600;e.fn.fullpage.setAutoScrolling=function(t){n.autoScrolling=t;var i=e(".section.active");n.autoScrolling?(e("html, body").css({overflow:"hidden",height:"100%"}),i.length&&k(i.position().top)):(e("html, body").css({overflow:"auto",height:"auto"}),k(0),e("html, body").scrollTop(i.position().top))},e.fn.fullpage.setScrollingSpeed=function(e){n.scrollingSpeed=e},e.fn.fullpage.setMouseWheelScrolling=function(e){e?A():P()},e.fn.fullpage.setAllowScrolling=function(n){n?(e.fn.fullpage.setMouseWheelScrolling(!0),E()):(e.fn.fullpage.setMouseWheelScrolling(!1),M())},e.fn.fullpage.setKeyboardScrolling=function(e){n.keyboardScrolling=e};var B,D,R=!1,N=navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|Windows Phone|Tizen|Bada)/),I=e(this),V=e(window).height()-e("#Header").outerHeight(),q=!1,O=!1;if(e.fn.fullpage.setAllowScrolling(!0),n.css3&&(n.css3=L()),e(this).length?I.css({height:"100%",position:"relative","-ms-touch-action":"none"}):(e("body").wrapInner('<div id="superContainer" />'),I=e("#superContainer")),n.navigation){e("body").append('<div id="fullPage-nav"><ul></ul></div>');var U=e("#fullPage-nav");U.css("color",n.navigationColor),U.addClass(n.navigationPosition)}e(".section").each(function(t){var i=e(this),o=e(this).find(".slide"),l=o.length;if(t||0!==e(".section.active").length||e(this).addClass("active"),"footer"==e(this).attr("data-type")?e(this).css("height",e(this).outerHeight()+"px"):e(this).css("height",V+"px"),(n.paddingTop||n.paddingBottom)&&e(this).css("padding",n.paddingTop+" 0 "+n.paddingBottom+" 0"),"undefined"!=typeof n.slidesColor[t]&&e(this).css("background-color",n.slidesColor[t]),"undefined"!=typeof n.anchors[t]&&e(this).attr("data-anchor",n.anchors[t]),n.navigation){var a="";n.anchors.length&&(a=n.anchors[t]);var s=n.navigationTooltips[t];"undefined"==typeof s&&(s=""),U.find("ul").append('<li data-tooltip="'+s+'"><a href="#'+a+'"><span></span></a></li>')}if(l>1){var r=100*l,c=100/l;o.wrapAll('<div class="slidesContainer" />'),o.parent().wrap('<div class="slides" />'),e(this).find(".slidesContainer").css({width:r+"%",height:V+"px"}),e(this).find(".slides").after('<div class="controlArrow prev"></div><div class="controlArrow next"></div>'),"#fff"!=n.controlArrowColor&&(e(this).find(".controlArrow.next").css("border-color","transparent transparent transparent "+n.controlArrowColor),e(this).find(".controlArrow.prev").css("border-color","transparent "+n.controlArrowColor+" transparent transparent")),n.loopHorizontal||e(this).find(".controlArrow.prev").hide(),n.slidesNavigation&&C(e(this),l),o.each(function(t){t||0!=i.find(".slide.active").length||e(this).addClass("active"),e(this).css("width",c+"%"),n.verticalCentered&&w(e(this))})}else n.verticalCentered&&"footer"!=e(this).attr("data-type")&&w(e(this))}).promise().done(function(){e.fn.fullpage.setAutoScrolling(n.autoScrolling),e(".section .slides .slidesContainer .slide").not(":first").css({opacity:"0",filter:"alpha(opacity=0)"});var t=e(".section.active").find(".slide.active");if(t.length&&(0!=e(".section.active").index(".section")||0==e(".section.active").index(".section")&&0!=t.index())){var i=n.scrollingSpeed;e.fn.fullpage.setScrollingSpeed(0),c(e(".section.active").find(".slides"),t),e.fn.fullpage.setScrollingSpeed(i)}n.fixedElements&&n.css3&&e(n.fixedElements).appendTo("body"),n.navigation&&(U.css("margin-top","-"+U.height()/2+"px"),U.find("li").eq(e(".section.active").index(".section")).find("a").addClass("active")),n.menu&&n.css3&&e(n.menu).appendTo("body"),n.scrollOverflow?e(window).on("load",function(){e(".section").each(function(){var n=e(this).find(".slide");n.length?n.each(function(){m(e(this))}):m(e(this))}),e.isFunction(n.afterRender)&&n.afterRender.call(this)}):e.isFunction(n.afterRender)&&n.afterRender.call(this);var o=window.location.hash.replace("#","").split("/"),l=o[0];if(l.length){var a=e('[data-anchor="'+l+'"]');!n.animateAnchor&&a.length&&(k(a.position().top),e.isFunction(n.afterLoad)&&n.afterLoad.call(this,l,a.index(".section")+1),a.addClass("active").siblings().removeClass("active"))}e(window).on("load",function(){r()})});var W,Y=!1;e(window).scroll(function(t){if(!n.autoScrolling){var i=e(window).scrollTop(),o=e(".section").map(function(){return e(this).offset().top<i+100?e(this):void 0}),l=o[o.length-1];if(!l.hasClass("active")){var a=e(".section.active").index(".section")+1;Y=!0;var s=p(l);l.addClass("active").siblings().removeClass("active");var r=l.data("anchor");e.isFunction(n.onLeave)&&n.onLeave.call(this,a,l.index(".section")+1,s),e.isFunction(n.afterLoad)&&n.afterLoad.call(this,r,l.index(".section")+1),v(r),u(r,0),n.anchors.length&&!q&&(B=r,location.hash=r),clearTimeout(W),W=setTimeout(function(){Y=!1},100)}}});var X=0,j=0,K=0,Q=0;if(e.fn.fullpage.moveSectionUp=function(){var t=e(".section.active").prev(".section");t.length||!n.loopTop&&!n.continuousVertical||(t=e(".section").last()),t.length&&s(t,null,!0)},e.fn.fullpage.moveSectionDown=function(){var t=e(".section.active").next(".section");t.length||!n.loopBottom&&!n.continuousVertical||(t=e(".section").first()),(t.length>0||!t.length&&(n.loopBottom||n.continuousVertical))&&s(t,null,!1)},e.fn.fullpage.moveTo=function(n,t){var i="";i=isNaN(n)?e('[data-anchor="'+n+'"]'):e(".section").eq(n-1),"undefined"!=typeof t?x(n,t):i.length>0&&s(i)},e.fn.fullpage.moveSlideRight=function(){a("next")},e.fn.fullpage.moveSlideLeft=function(){a("prev")},e(window).on("hashchange",function(){if(!Y){var e=window.location.hash.replace("#","").split("/"),n=e[0],t=e[1],i="undefined"==typeof B,o="undefined"==typeof B&&"undefined"==typeof t;(n&&n!==B&&!i||o||!R&&D!=t)&&x(n,t)}}),e(document).keydown(function(t){if(n.keyboardScrolling&&!q)switch(t.which){case 38:case 33:e.fn.fullpage.moveSectionUp();break;case 40:case 34:e.fn.fullpage.moveSectionDown();break;case 36:e.fn.fullpage.moveTo(1);break;case 35:e.fn.fullpage.moveTo(e(".section").length);break;case 37:e.fn.fullpage.moveSlideLeft();break;case 39:e.fn.fullpage.moveSlideRight();break;default:return}}),e(document).on("click","#fullPage-nav a",function(n){n.preventDefault();var t=e(this).parent().index();s(e(".section").eq(t))}),e(document).on({mouseenter:function(){var t=e(this).data("tooltip");e('<div class="fullPage-tooltip '+n.navigationPosition+'">'+t+"</div>").hide().appendTo(e(this)).fadeIn(200)},mouseleave:function(){e(this).find(".fullPage-tooltip").fadeOut().remove()}},"#fullPage-nav li"),n.normalScrollElements&&(e(document).on("mouseover",n.normalScrollElements,function(){e.fn.fullpage.setMouseWheelScrolling(!1)}),e(document).on("mouseout",n.normalScrollElements,function(){e.fn.fullpage.setMouseWheelScrolling(!0)})),e(".section").on("click",".controlArrow",function(){e(this).hasClass("prev")?e.fn.fullpage.moveSlideLeft():e.fn.fullpage.moveSlideRight()}),e(".section").on("click",".toSlide",function(n){n.preventDefault();var t=e(this).closest(".section").find(".slides"),i=(t.find(".slide.active"),null);i=t.find(".slide").eq(e(this).data("index")-1),i.length>0&&(console.log("1049"),c(t,i))}),!N){var G;e(window).resize(function(){clearTimeout(G),G=setTimeout(d,500)})}var J="onorientationchange"in window,Z=J?"orientationchange":"resize";e(window).bind(Z,function(){N&&d()}),e(document).on("click",".fullPage-slidesNav a",function(n){n.preventDefault();var t=e(this).closest(".section").find(".slides"),i=t.find(".slide").eq(e(this).closest("li").index()),o=t.find(".slidesContainer .slide.active");c(t,i,o)})}}(jQuery);

/* scrollReveal.js was inspired by cbpScroller.js (c) 2014 Codrops. http://www.opensource.org/licenses/mit-license.php */
window.scrollReveal=function(t){"use strict";function e(e){this.docElem=t.document.documentElement,this.options=this.extend(this.defaults,e),1==this.options.init&&this.init()}return e.prototype={defaults:{after:"0s",enter:"bottom",move:"24px",over:"0.66s",easing:"ease-in-out",viewportFactor:.33,reset:!1,init:!0},init:function(){this.scrolled=!1;var e=this;this.elems=Array.prototype.slice.call(this.docElem.querySelectorAll("[data-scrollReveal]")),this.elems.forEach(function(t,i){e.update(t)});var i=function(){e.scrolled||(e.scrolled=!0,setTimeout(function(){e._scrollPage()},60))},o=function(){function t(){e._scrollPage(),e.resizeTimeout=null}e.resizeTimeout&&clearTimeout(e.resizeTimeout),e.resizeTimeout=setTimeout(t,200)};t.addEventListener("scroll",i,!1),t.addEventListener("resize",o,!1)},_scrollPage:function(){var t=this;this.elems.forEach(function(e,i){t.update(e)}),this.scrolled=!1},parseLanguage:function(t){function e(t){var e=[],i=["from","the","and","then","but","with"];return t.forEach(function(t,o){i.indexOf(t)>-1||e.push(t)}),e}var i=t.getAttribute("data-scrollreveal").split(/[, ]+/),o={};return i=e(i),i.forEach(function(t,e){switch(t){case"enter":return void(o.enter=i[e+1]);case"after":return void(o.after=i[e+1]);case"wait":return void(o.after=i[e+1]);case"move":return void(o.move=i[e+1]);case"ease":return o.move=i[e+1],void(o.ease="ease");case"ease-in":return o.move=i[e+1],void(o.easing="ease-in");case"ease-in-out":return o.move=i[e+1],void(o.easing="ease-in-out");case"ease-out":return o.move=i[e+1],void(o.easing="ease-out");case"over":return void(o.over=i[e+1]);default:return}}),o},update:function(t){var e=this.genCSS(t),i=this;return $jq(t).bind("fpfocused",function(){t.setAttribute("style",e.target+e.transition),i.options.reset||setTimeout(function(){t.removeAttribute("style"),t.setAttribute("data-scrollReveal-complete",!0)},e.totalDuration)}),t.getAttribute("data-scrollReveal-initialized")||(t.setAttribute("style",e.initial),t.setAttribute("data-scrollReveal-initialized",!0)),this.isElementInViewport(t,this.options.viewportFactor)?t.getAttribute("data-scrollReveal-complete")?void 0:this.isElementInViewport(t,this.options.viewportFactor)?(t.setAttribute("style",e.target+e.transition),void(this.options.reset||setTimeout(function(){t.removeAttribute("style"),t.setAttribute("data-scrollReveal-complete",!0)},e.totalDuration))):void 0:void(this.options.reset&&t.setAttribute("style",e.initial+e.reset))},genCSS:function(t){var e,i,o=this.parseLanguage(t);o.enter?(("top"==o.enter||"bottom"==o.enter)&&(e=o.enter,i="y"),("left"==o.enter||"right"==o.enter)&&(e=o.enter,i="x")):(("top"==this.options.enter||"bottom"==this.options.enter)&&(e=this.options.enter,i="y"),("left"==this.options.enter||"right"==this.options.enter)&&(e=this.options.enter,i="x")),("top"==e||"left"==e)&&(o.move="-"+this.options.move);var r=o.move||this.options.move,n=o.over||this.options.over,s=o.after||this.options.after,a=o.easing||this.options.easing,l="-webkit-transition: all "+n+" "+a+" "+s+";-moz-transition: all "+n+" "+a+" "+s+";-o-transition: all "+n+" "+a+" "+s+";transition: all "+n+" "+a+" "+s+";-webkit-perspective: 1000;-webkit-backface-visibility: hidden;",u="-webkit-transition: all "+n+" "+a+" 0s;-moz-transition: all "+n+" "+a+" 0s;-o-transition: all "+n+" "+a+" 0s;transition: all "+n+" "+a+" 0s;-webkit-perspective: 1000;-webkit-backface-visibility: hidden;",c="-webkit-transform: translate"+i+"("+r+");-moz-transform: translate"+i+"("+r+");transform: translate"+i+"("+r+");opacity: 0;",f="-webkit-transform: translate"+i+"(0);-moz-transform: translate"+i+"(0);transform: translate"+i+"(0);opacity: 1;";return{transition:l,initial:c,target:f,reset:u,totalDuration:1e3*(parseFloat(n)+parseFloat(s))}},getViewportH:function(){var e=this.docElem.clientHeight,i=t.innerHeight;return i>e?i:e},getOffset:function(t){var e=0,i=0;do isNaN(t.offsetTop)||(e+=t.offsetTop),isNaN(t.offsetLeft)||(i+=t.offsetLeft);while(t=t.offsetParent);return{top:e,left:i}},isElementInViewport:function(e,i){var o=t.pageYOffset,r=o+this.getViewportH(),n=e.offsetHeight,s=this.getOffset(e).top,a=s+n,i=i||0;return r>=s+n*i&&a>=o},extend:function(t,e){for(var i in e)e.hasOwnProperty(i)&&(t[i]=e[i]);return t}},e}(window);
/*** BxSlider v4.1.2 - Fully loaded, responsive content slider http://bxslider.com*/
!function(t){var e={},s={mode:"horizontal",slideSelector:"",infiniteLoop:!0,hideControlOnEnd:!1,speed:500,easing:null,slideMargin:0,startSlide:0,randomStart:!1,captions:!1,ticker:!1,tickerHover:!1,adaptiveHeight:!1,adaptiveHeightSpeed:500,video:!1,useCSS:!0,preloadImages:"visible",responsive:!0,slideZIndex:50,touchEnabled:!0,swipeThreshold:50,oneToOneTouch:!0,preventDefaultSwipeX:!0,preventDefaultSwipeY:!1,pager:!0,pagerType:"full",pagerShortSeparator:" / ",pagerSelector:null,buildPager:null,pagerCustom:null,controls:!0,nextText:"Next",prevText:"Prev",nextSelector:null,prevSelector:null,autoControls:!1,startText:"Start",stopText:"Stop",autoControlsCombine:!1,autoControlsSelector:null,auto:!1,pause:4e3,autoStart:!0,autoDirection:"next",autoHover:!1,autoDelay:0,minSlides:1,maxSlides:1,moveSlides:0,slideWidth:0,onSliderLoad:function(){},onSlideBefore:function(){},onSlideAfter:function(){},onSlideNext:function(){},onSlidePrev:function(){},onSliderResize:function(){}};t.fn.bxSlider=function(n){if(0==this.length)return this;if(this.length>1)return this.each(function(){t(this).bxSlider(n)}),this;var o={},r=this;e.el=this;var a=t(window).width(),l=t(window).height(),d=function(){o.settings=t.extend({},s,n),o.settings.slideWidth=parseInt(o.settings.slideWidth),o.children=r.children(o.settings.slideSelector),o.children.length<o.settings.minSlides&&(o.settings.minSlides=o.children.length),o.children.length<o.settings.maxSlides&&(o.settings.maxSlides=o.children.length),o.settings.randomStart&&(o.settings.startSlide=Math.floor(Math.random()*o.children.length)),o.active={index:o.settings.startSlide},o.carousel=o.settings.minSlides>1||o.settings.maxSlides>1,o.carousel&&(o.settings.preloadImages="all"),o.minThreshold=o.settings.minSlides*o.settings.slideWidth+(o.settings.minSlides-1)*o.settings.slideMargin,o.maxThreshold=o.settings.maxSlides*o.settings.slideWidth+(o.settings.maxSlides-1)*o.settings.slideMargin,o.working=!1,o.controls={},o.interval=null,o.animProp="vertical"==o.settings.mode?"top":"left",o.usingCSS=o.settings.useCSS&&"fade"!=o.settings.mode&&function(){var t=document.createElement("div"),e=["WebkitPerspective","MozPerspective","OPerspective","msPerspective"];for(var i in e)if(void 0!==t.style[e[i]])return o.cssPrefix=e[i].replace("Perspective","").toLowerCase(),o.animProp="-"+o.cssPrefix+"-transform",!0;return!1}(),"vertical"==o.settings.mode&&(o.settings.maxSlides=o.settings.minSlides),r.data("origStyle",r.attr("style")),r.children(o.settings.slideSelector).each(function(){t(this).data("origStyle",t(this).attr("style"))}),c()},c=function(){r.wrap('<div class="bx-wrapper"><div class="bx-viewport"></div></div>'),o.viewport=r.parent(),o.loader=t('<div class="bx-loading" />'),o.viewport.prepend(o.loader),r.css({width:"horizontal"==o.settings.mode?100*o.children.length+215+"%":"auto",position:"relative"}),o.usingCSS&&o.settings.easing?r.css("-"+o.cssPrefix+"-transition-timing-function",o.settings.easing):o.settings.easing||(o.settings.easing="swing"),f(),o.viewport.css({width:"100%",overflow:"hidden",position:"relative"}),o.viewport.parent().css({maxWidth:p()}),o.settings.pager||o.viewport.parent().css({margin:"0 auto 0px"}),o.children.css({"float":"horizontal"==o.settings.mode?"left":"none",listStyle:"none",position:"relative"}),o.children.css("width",u()),"horizontal"==o.settings.mode&&o.settings.slideMargin>0&&o.children.css("marginRight",o.settings.slideMargin),"vertical"==o.settings.mode&&o.settings.slideMargin>0&&o.children.css("marginBottom",o.settings.slideMargin),"fade"==o.settings.mode&&(o.children.css({position:"absolute",zIndex:0,display:"none"}),o.children.eq(o.settings.startSlide).css({zIndex:o.settings.slideZIndex,display:"block"})),o.controls.el=t('<div class="bx-controls" />'),o.settings.captions&&P(),o.active.last=o.settings.startSlide==x()-1,o.settings.video&&r.fitVids();var e=o.children.eq(o.settings.startSlide);"all"==o.settings.preloadImages&&(e=o.children),o.settings.ticker?o.settings.pager=!1:(o.settings.pager&&T(),o.settings.controls&&C(),o.settings.auto&&o.settings.autoControls&&E(),(o.settings.controls||o.settings.autoControls||o.settings.pager)&&o.viewport.after(o.controls.el)),g(e,h)},g=function(e,i){var s=e.find("img, iframe").length;if(0==s)return i(),void 0;var n=0;e.find("img, iframe").each(function(){t(this).one("load",function(){++n==s&&i()}).each(function(){this.complete&&t(this).load()})})},h=function(){if(o.settings.infiniteLoop&&"fade"!=o.settings.mode&&!o.settings.ticker){var e="vertical"==o.settings.mode?o.settings.minSlides:o.settings.maxSlides,i=o.children.slice(0,e).clone().addClass("bx-clone"),s=o.children.slice(-e).clone().addClass("bx-clone");r.append(i).prepend(s)}o.loader.remove(),S(),"vertical"==o.settings.mode&&(o.settings.adaptiveHeight=!0),o.viewport.height(v()),r.redrawSlider(),o.settings.onSliderLoad(o.active.index),o.initialized=!0,o.settings.responsive&&t(window).bind("resize",Z),o.settings.auto&&o.settings.autoStart&&H(),o.settings.ticker&&L(),o.settings.pager&&q(o.settings.startSlide),o.settings.controls&&W(),o.settings.touchEnabled&&!o.settings.ticker&&O()},v=function(){var e=0,s=t();if("vertical"==o.settings.mode||o.settings.adaptiveHeight)if(o.carousel){var n=1==o.settings.moveSlides?o.active.index:o.active.index*m();for(s=o.children.eq(n),i=1;i<=o.settings.maxSlides-1;i++)s=n+i>=o.children.length?s.add(o.children.eq(i-1)):s.add(o.children.eq(n+i))}else s=o.children.eq(o.active.index);else s=o.children;return"vertical"==o.settings.mode?(s.each(function(){e+=t(this).outerHeight()}),o.settings.slideMargin>0&&(e+=o.settings.slideMargin*(o.settings.minSlides-1))):e=Math.max.apply(Math,s.map(function(){return t(this).outerHeight(!1)}).get()),e},p=function(){var t="100%";return o.settings.slideWidth>0&&(t="horizontal"==o.settings.mode?o.settings.maxSlides*o.settings.slideWidth+(o.settings.maxSlides-1)*o.settings.slideMargin:o.settings.slideWidth),t},u=function(){var t=o.settings.slideWidth,e=o.viewport.width();return 0==o.settings.slideWidth||o.settings.slideWidth>e&&!o.carousel||"vertical"==o.settings.mode?t=e:o.settings.maxSlides>1&&"horizontal"==o.settings.mode&&(e>o.maxThreshold||e<o.minThreshold&&(t=(e-o.settings.slideMargin*(o.settings.minSlides-1))/o.settings.minSlides)),t},f=function(){var t=1;if("horizontal"==o.settings.mode&&o.settings.slideWidth>0)if(o.viewport.width()<o.minThreshold)t=o.settings.minSlides;else if(o.viewport.width()>o.maxThreshold)t=o.settings.maxSlides;else{var e=o.children.first().width();t=Math.floor(o.viewport.width()/e)}else"vertical"==o.settings.mode&&(t=o.settings.minSlides);return t},x=function(){var t=0;if(o.settings.moveSlides>0)if(o.settings.infiniteLoop)t=o.children.length/m();else for(var e=0,i=0;e<o.children.length;)++t,e=i+f(),i+=o.settings.moveSlides<=f()?o.settings.moveSlides:f();else t=Math.ceil(o.children.length/f());return t},m=function(){return o.settings.moveSlides>0&&o.settings.moveSlides<=f()?o.settings.moveSlides:f()},S=function(){if(o.children.length>o.settings.maxSlides&&o.active.last&&!o.settings.infiniteLoop){if("horizontal"==o.settings.mode){var t=o.children.last(),e=t.position();b(-(e.left-(o.viewport.width()-t.width())),"reset",0)}else if("vertical"==o.settings.mode){var i=o.children.length-o.settings.minSlides,e=o.children.eq(i).position();b(-e.top,"reset",0)}}else{var e=o.children.eq(o.active.index*m()).position();o.active.index==x()-1&&(o.active.last=!0),void 0!=e&&("horizontal"==o.settings.mode?b(-e.left,"reset",0):"vertical"==o.settings.mode&&b(-e.top,"reset",0))}},b=function(t,e,i,s){if(o.usingCSS){var n="vertical"==o.settings.mode?"translate3d(0, "+t+"px, 0)":"translate3d("+t+"px, 0, 0)";r.css("-"+o.cssPrefix+"-transition-duration",i/1e3+"s"),"slide"==e?(r.css(o.animProp,n),r.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(){r.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"),D()})):"reset"==e?r.css(o.animProp,n):"ticker"==e&&(r.css("-"+o.cssPrefix+"-transition-timing-function","linear"),r.css(o.animProp,n),r.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(){r.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"),b(s.resetValue,"reset",0),N()}))}else{var a={};a[o.animProp]=t,"slide"==e?r.animate(a,i,o.settings.easing,function(){D()}):"reset"==e?r.css(o.animProp,t):"ticker"==e&&r.animate(a,speed,"linear",function(){b(s.resetValue,"reset",0),N()})}},w=function(){for(var e="",i=x(),s=0;i>s;s++){var n="";o.settings.buildPager&&t.isFunction(o.settings.buildPager)?(n=o.settings.buildPager(s),o.pagerEl.addClass("bx-custom-pager")):(n=s+1,o.pagerEl.addClass("bx-default-pager")),e+='<div class="bx-pager-item"><a href="" data-slide-index="'+s+'" class="bx-pager-link">'+n+"</a></div>"}o.pagerEl.html(e)},T=function(){o.settings.pagerCustom?o.pagerEl=t(o.settings.pagerCustom):(o.pagerEl=t('<div class="bx-pager" />'),o.settings.pagerSelector?t(o.settings.pagerSelector).html(o.pagerEl):o.controls.el.addClass("bx-has-pager").append(o.pagerEl),w()),o.pagerEl.on("click","a",I)},C=function(){o.controls.next=t('<a class="bx-next" href="">'+o.settings.nextText+"</a>"),o.controls.prev=t('<a class="bx-prev" href="">'+o.settings.prevText+"</a>"),o.controls.next.bind("click",y),o.controls.prev.bind("click",z),o.settings.nextSelector&&t(o.settings.nextSelector).append(o.controls.next),o.settings.prevSelector&&t(o.settings.prevSelector).append(o.controls.prev),o.settings.nextSelector||o.settings.prevSelector||(o.controls.directionEl=t('<div class="bx-controls-direction" />'),o.controls.directionEl.append(o.controls.prev).append(o.controls.next),o.controls.el.addClass("bx-has-controls-direction").append(o.controls.directionEl))},E=function(){o.controls.start=t('<div class="bx-controls-auto-item"><a class="bx-start" href="">'+o.settings.startText+"</a></div>"),o.controls.stop=t('<div class="bx-controls-auto-item"><a class="bx-stop" href="">'+o.settings.stopText+"</a></div>"),o.controls.autoEl=t('<div class="bx-controls-auto" />'),o.controls.autoEl.on("click",".bx-start",k),o.controls.autoEl.on("click",".bx-stop",M),o.settings.autoControlsCombine?o.controls.autoEl.append(o.controls.start):o.controls.autoEl.append(o.controls.start).append(o.controls.stop),o.settings.autoControlsSelector?t(o.settings.autoControlsSelector).html(o.controls.autoEl):o.controls.el.addClass("bx-has-controls-auto").append(o.controls.autoEl),A(o.settings.autoStart?"stop":"start")},P=function(){o.children.each(function(){var e=t(this).find("img:first").attr("title");void 0!=e&&(""+e).length&&t(this).append('<div class="bx-caption"><span>'+e+"</span></div>")})},y=function(t){o.settings.auto&&r.stopAuto(),r.goToNextSlide(),t.preventDefault()},z=function(t){o.settings.auto&&r.stopAuto(),r.goToPrevSlide(),t.preventDefault()},k=function(t){r.startAuto(),t.preventDefault()},M=function(t){r.stopAuto(),t.preventDefault()},I=function(e){o.settings.auto&&r.stopAuto();var i=t(e.currentTarget),s=parseInt(i.attr("data-slide-index"));s!=o.active.index&&r.goToSlide(s),e.preventDefault()},q=function(e){var i=o.children.length;return"short"==o.settings.pagerType?(o.settings.maxSlides>1&&(i=Math.ceil(o.children.length/o.settings.maxSlides)),o.pagerEl.html(e+1+o.settings.pagerShortSeparator+i),void 0):(o.pagerEl.find("a").removeClass("active"),o.pagerEl.each(function(i,s){t(s).find("a").eq(e).addClass("active")}),void 0)},D=function(){if(o.settings.infiniteLoop){var t="";0==o.active.index?t=o.children.eq(0).position():o.active.index==x()-1&&o.carousel?t=o.children.eq((x()-1)*m()).position():o.active.index==o.children.length-1&&(t=o.children.eq(o.children.length-1).position()),t&&("horizontal"==o.settings.mode?b(-t.left,"reset",0):"vertical"==o.settings.mode&&b(-t.top,"reset",0))}o.working=!1,o.settings.onSlideAfter(o.children.eq(o.active.index),o.oldIndex,o.active.index)},A=function(t){o.settings.autoControlsCombine?o.controls.autoEl.html(o.controls[t]):(o.controls.autoEl.find("a").removeClass("active"),o.controls.autoEl.find("a:not(.bx-"+t+")").addClass("active"))},W=function(){1==x()?(o.controls.prev.addClass("disabled"),o.controls.next.addClass("disabled")):!o.settings.infiniteLoop&&o.settings.hideControlOnEnd&&(0==o.active.index?(o.controls.prev.addClass("disabled"),o.controls.next.removeClass("disabled")):o.active.index==x()-1?(o.controls.next.addClass("disabled"),o.controls.prev.removeClass("disabled")):(o.controls.prev.removeClass("disabled"),o.controls.next.removeClass("disabled")))},H=function(){o.settings.autoDelay>0?setTimeout(r.startAuto,o.settings.autoDelay):r.startAuto(),o.settings.autoHover&&r.hover(function(){o.interval&&(r.stopAuto(!0),o.autoPaused=!0)},function(){o.autoPaused&&(r.startAuto(!0),o.autoPaused=null)})},L=function(){var e=0;if("next"==o.settings.autoDirection)r.append(o.children.clone().addClass("bx-clone"));else{r.prepend(o.children.clone().addClass("bx-clone"));var i=o.children.first().position();e="horizontal"==o.settings.mode?-i.left:-i.top}b(e,"reset",0),o.settings.pager=!1,o.settings.controls=!1,o.settings.autoControls=!1,o.settings.tickerHover&&!o.usingCSS&&o.viewport.hover(function(){r.stop()},function(){var e=0;o.children.each(function(){e+="horizontal"==o.settings.mode?t(this).outerWidth(!0):t(this).outerHeight(!0)});var i=o.settings.speed/e,s="horizontal"==o.settings.mode?"left":"top",n=i*(e-Math.abs(parseInt(r.css(s))));N(n)}),N()},N=function(t){speed=t?t:o.settings.speed;var e={left:0,top:0},i={left:0,top:0};"next"==o.settings.autoDirection?e=r.find(".bx-clone").first().position():i=o.children.first().position();var s="horizontal"==o.settings.mode?-e.left:-e.top,n="horizontal"==o.settings.mode?-i.left:-i.top,a={resetValue:n};b(s,"ticker",speed,a)},O=function(){o.touch={start:{x:0,y:0},end:{x:0,y:0}},o.viewport.bind("touchstart",X)},X=function(t){if(o.working)t.preventDefault();else{o.touch.originalPos=r.position();var e=t.originalEvent;o.touch.start.x=e.changedTouches[0].pageX,o.touch.start.y=e.changedTouches[0].pageY,o.viewport.bind("touchmove",Y),o.viewport.bind("touchend",V)}},Y=function(t){var e=t.originalEvent,i=Math.abs(e.changedTouches[0].pageX-o.touch.start.x),s=Math.abs(e.changedTouches[0].pageY-o.touch.start.y);if(3*i>s&&o.settings.preventDefaultSwipeX?t.preventDefault():3*s>i&&o.settings.preventDefaultSwipeY&&t.preventDefault(),"fade"!=o.settings.mode&&o.settings.oneToOneTouch){var n=0;if("horizontal"==o.settings.mode){var r=e.changedTouches[0].pageX-o.touch.start.x;n=o.touch.originalPos.left+r}else{var r=e.changedTouches[0].pageY-o.touch.start.y;n=o.touch.originalPos.top+r}b(n,"reset",0)}},V=function(t){o.viewport.unbind("touchmove",Y);var e=t.originalEvent,i=0;if(o.touch.end.x=e.changedTouches[0].pageX,o.touch.end.y=e.changedTouches[0].pageY,"fade"==o.settings.mode){var s=Math.abs(o.touch.start.x-o.touch.end.x);s>=o.settings.swipeThreshold&&(o.touch.start.x>o.touch.end.x?r.goToNextSlide():r.goToPrevSlide(),r.stopAuto())}else{var s=0;"horizontal"==o.settings.mode?(s=o.touch.end.x-o.touch.start.x,i=o.touch.originalPos.left):(s=o.touch.end.y-o.touch.start.y,i=o.touch.originalPos.top),!o.settings.infiniteLoop&&(0==o.active.index&&s>0||o.active.last&&0>s)?b(i,"reset",200):Math.abs(s)>=o.settings.swipeThreshold?(0>s?r.goToNextSlide():r.goToPrevSlide(),r.stopAuto()):b(i,"reset",200)}o.viewport.unbind("touchend",V)},Z=function(){var e=t(window).width(),i=t(window).height();(a!=e||l!=i)&&(a=e,l=i,r.redrawSlider(),o.settings.onSliderResize.call(r,o.active.index))};return r.goToSlide=function(e,i){if(!o.working&&o.active.index!=e)if(o.working=!0,o.oldIndex=o.active.index,o.active.index=0>e?x()-1:e>=x()?0:e,o.settings.onSlideBefore(o.children.eq(o.active.index),o.oldIndex,o.active.index),"next"==i?o.settings.onSlideNext(o.children.eq(o.active.index),o.oldIndex,o.active.index):"prev"==i&&o.settings.onSlidePrev(o.children.eq(o.active.index),o.oldIndex,o.active.index),o.active.last=o.active.index>=x()-1,o.settings.pager&&q(o.active.index),o.settings.controls&&W(),"fade"==o.settings.mode)o.settings.adaptiveHeight&&o.viewport.height()!=v()&&o.viewport.animate({height:v()},o.settings.adaptiveHeightSpeed),o.children.filter(":visible").fadeOut(o.settings.speed).css({zIndex:0}),o.children.eq(o.active.index).css("zIndex",o.settings.slideZIndex+1).fadeIn(o.settings.speed,function(){t(this).css("zIndex",o.settings.slideZIndex),D()});else{o.settings.adaptiveHeight&&o.viewport.height()!=v()&&o.viewport.animate({height:v()},o.settings.adaptiveHeightSpeed);var s=0,n={left:0,top:0};if(!o.settings.infiniteLoop&&o.carousel&&o.active.last)if("horizontal"==o.settings.mode){var a=o.children.eq(o.children.length-1);n=a.position(),s=o.viewport.width()-a.outerWidth()}else{var l=o.children.length-o.settings.minSlides;n=o.children.eq(l).position()}else if(o.carousel&&o.active.last&&"prev"==i){var d=1==o.settings.moveSlides?o.settings.maxSlides-m():(x()-1)*m()-(o.children.length-o.settings.maxSlides),a=r.children(".bx-clone").eq(d);n=a.position()}else if("next"==i&&0==o.active.index)n=r.find("> .bx-clone").eq(o.settings.maxSlides).position(),o.active.last=!1;else if(e>=0){var c=e*m();n=o.children.eq(c).position()}if("undefined"!=typeof n){var g="horizontal"==o.settings.mode?-(n.left-s):-n.top;b(g,"slide",o.settings.speed)}}},r.goToNextSlide=function(){if(o.settings.infiniteLoop||!o.active.last){var t=parseInt(o.active.index)+1;r.goToSlide(t,"next")}},r.goToPrevSlide=function(){if(o.settings.infiniteLoop||0!=o.active.index){var t=parseInt(o.active.index)-1;r.goToSlide(t,"prev")}},r.startAuto=function(t){o.interval||(o.interval=setInterval(function(){"next"==o.settings.autoDirection?r.goToNextSlide():r.goToPrevSlide()},o.settings.pause),o.settings.autoControls&&1!=t&&A("stop"))},r.stopAuto=function(t){o.interval&&(clearInterval(o.interval),o.interval=null,o.settings.autoControls&&1!=t&&A("start"))},r.getCurrentSlide=function(){return o.active.index},r.getCurrentSlideElement=function(){return o.children.eq(o.active.index)},r.getSlideCount=function(){return o.children.length},r.redrawSlider=function(){o.children.add(r.find(".bx-clone")).outerWidth(u()),o.viewport.css("height",v()),o.settings.ticker||S(),o.active.last&&(o.active.index=x()-1),o.active.index>=x()&&(o.active.last=!0),o.settings.pager&&!o.settings.pagerCustom&&(w(),q(o.active.index))},r.destroySlider=function(){o.initialized&&(o.initialized=!1,t(".bx-clone",this).remove(),o.children.each(function(){void 0!=t(this).data("origStyle")?t(this).attr("style",t(this).data("origStyle")):t(this).removeAttr("style")}),void 0!=t(this).data("origStyle")?this.attr("style",t(this).data("origStyle")):t(this).removeAttr("style"),t(this).unwrap().unwrap(),o.controls.el&&o.controls.el.remove(),o.controls.next&&o.controls.next.remove(),o.controls.prev&&o.controls.prev.remove(),o.pagerEl&&o.settings.controls&&o.pagerEl.remove(),t(".bx-caption",this).remove(),o.controls.autoEl&&o.controls.autoEl.remove(),clearInterval(o.interval),o.settings.responsive&&t(window).unbind("resize",Z))},r.reloadSlider=function(t){void 0!=t&&(n=t),r.destroySlider(),d()},d(),this}}(jQuery);

/*!* Isotope PACKAGED v2.0.0 http://isotope.metafizzy.co */
(function(t){function e(){}function i(t){function i(e){e.prototype.option||(e.prototype.option=function(e){t.isPlainObject(e)&&(this.options=t.extend(!0,this.options,e))})}function n(e,i){t.fn[e]=function(n){if("string"==typeof n){for(var s=o.call(arguments,1),a=0,u=this.length;u>a;a++){var p=this[a],h=t.data(p,e);if(h)if(t.isFunction(h[n])&&"_"!==n.charAt(0)){var f=h[n].apply(h,s);if(void 0!==f)return f}else r("no such method '"+n+"' for "+e+" instance");else r("cannot call methods on "+e+" prior to initialization; "+"attempted to call '"+n+"'")}return this}return this.each(function(){var o=t.data(this,e);o?(o.option(n),o._init()):(o=new i(this,n),t.data(this,e,o))})}}if(t){var r="undefined"==typeof console?e:function(t){console.error(t)};return t.bridget=function(t,e){i(e),n(t,e)},t.bridget}}var o=Array.prototype.slice;"function"==typeof define&&define.amd?define("jquery-bridget/jquery.bridget",["jquery"],i):i(t.jQuery)})(window),function(t){function e(e){var i=t.event;return i.target=i.target||i.srcElement||e,i}var i=document.documentElement,o=function(){};i.addEventListener?o=function(t,e,i){t.addEventListener(e,i,!1)}:i.attachEvent&&(o=function(t,i,o){t[i+o]=o.handleEvent?function(){var i=e(t);o.handleEvent.call(o,i)}:function(){var i=e(t);o.call(t,i)},t.attachEvent("on"+i,t[i+o])});var n=function(){};i.removeEventListener?n=function(t,e,i){t.removeEventListener(e,i,!1)}:i.detachEvent&&(n=function(t,e,i){t.detachEvent("on"+e,t[e+i]);try{delete t[e+i]}catch(o){t[e+i]=void 0}});var r={bind:o,unbind:n};"function"==typeof define&&define.amd?define("eventie/eventie",r):"object"==typeof exports?module.exports=r:t.eventie=r}(this),function(t){function e(t){"function"==typeof t&&(e.isReady?t():r.push(t))}function i(t){var i="readystatechange"===t.type&&"complete"!==n.readyState;if(!e.isReady&&!i){e.isReady=!0;for(var o=0,s=r.length;s>o;o++){var a=r[o];a()}}}function o(o){return o.bind(n,"DOMContentLoaded",i),o.bind(n,"readystatechange",i),o.bind(t,"load",i),e}var n=t.document,r=[];e.isReady=!1,"function"==typeof define&&define.amd?(e.isReady="function"==typeof requirejs,define("doc-ready/doc-ready",["eventie/eventie"],o)):t.docReady=o(t.eventie)}(this),function(){function t(){}function e(t,e){for(var i=t.length;i--;)if(t[i].listener===e)return i;return-1}function i(t){return function(){return this[t].apply(this,arguments)}}var o=t.prototype,n=this,r=n.EventEmitter;o.getListeners=function(t){var e,i,o=this._getEvents();if(t instanceof RegExp){e={};for(i in o)o.hasOwnProperty(i)&&t.test(i)&&(e[i]=o[i])}else e=o[t]||(o[t]=[]);return e},o.flattenListeners=function(t){var e,i=[];for(e=0;t.length>e;e+=1)i.push(t[e].listener);return i},o.getListenersAsObject=function(t){var e,i=this.getListeners(t);return i instanceof Array&&(e={},e[t]=i),e||i},o.addListener=function(t,i){var o,n=this.getListenersAsObject(t),r="object"==typeof i;for(o in n)n.hasOwnProperty(o)&&-1===e(n[o],i)&&n[o].push(r?i:{listener:i,once:!1});return this},o.on=i("addListener"),o.addOnceListener=function(t,e){return this.addListener(t,{listener:e,once:!0})},o.once=i("addOnceListener"),o.defineEvent=function(t){return this.getListeners(t),this},o.defineEvents=function(t){for(var e=0;t.length>e;e+=1)this.defineEvent(t[e]);return this},o.removeListener=function(t,i){var o,n,r=this.getListenersAsObject(t);for(n in r)r.hasOwnProperty(n)&&(o=e(r[n],i),-1!==o&&r[n].splice(o,1));return this},o.off=i("removeListener"),o.addListeners=function(t,e){return this.manipulateListeners(!1,t,e)},o.removeListeners=function(t,e){return this.manipulateListeners(!0,t,e)},o.manipulateListeners=function(t,e,i){var o,n,r=t?this.removeListener:this.addListener,s=t?this.removeListeners:this.addListeners;if("object"!=typeof e||e instanceof RegExp)for(o=i.length;o--;)r.call(this,e,i[o]);else for(o in e)e.hasOwnProperty(o)&&(n=e[o])&&("function"==typeof n?r.call(this,o,n):s.call(this,o,n));return this},o.removeEvent=function(t){var e,i=typeof t,o=this._getEvents();if("string"===i)delete o[t];else if(t instanceof RegExp)for(e in o)o.hasOwnProperty(e)&&t.test(e)&&delete o[e];else delete this._events;return this},o.removeAllListeners=i("removeEvent"),o.emitEvent=function(t,e){var i,o,n,r,s=this.getListenersAsObject(t);for(n in s)if(s.hasOwnProperty(n))for(o=s[n].length;o--;)i=s[n][o],i.once===!0&&this.removeListener(t,i.listener),r=i.listener.apply(this,e||[]),r===this._getOnceReturnValue()&&this.removeListener(t,i.listener);return this},o.trigger=i("emitEvent"),o.emit=function(t){var e=Array.prototype.slice.call(arguments,1);return this.emitEvent(t,e)},o.setOnceReturnValue=function(t){return this._onceReturnValue=t,this},o._getOnceReturnValue=function(){return this.hasOwnProperty("_onceReturnValue")?this._onceReturnValue:!0},o._getEvents=function(){return this._events||(this._events={})},t.noConflict=function(){return n.EventEmitter=r,t},"function"==typeof define&&define.amd?define("eventEmitter/EventEmitter",[],function(){return t}):"object"==typeof module&&module.exports?module.exports=t:this.EventEmitter=t}.call(this),function(t){function e(t){if(t){if("string"==typeof o[t])return t;t=t.charAt(0).toUpperCase()+t.slice(1);for(var e,n=0,r=i.length;r>n;n++)if(e=i[n]+t,"string"==typeof o[e])return e}}var i="Webkit Moz ms Ms O".split(" "),o=document.documentElement.style;"function"==typeof define&&define.amd?define("get-style-property/get-style-property",[],function(){return e}):"object"==typeof exports?module.exports=e:t.getStyleProperty=e}(window),function(t){function e(t){var e=parseFloat(t),i=-1===t.indexOf("%")&&!isNaN(e);return i&&e}function i(){for(var t={width:0,height:0,innerWidth:0,innerHeight:0,outerWidth:0,outerHeight:0},e=0,i=s.length;i>e;e++){var o=s[e];t[o]=0}return t}function o(t){function o(t){if("string"==typeof t&&(t=document.querySelector(t)),t&&"object"==typeof t&&t.nodeType){var o=r(t);if("none"===o.display)return i();var n={};n.width=t.offsetWidth,n.height=t.offsetHeight;for(var h=n.isBorderBox=!(!p||!o[p]||"border-box"!==o[p]),f=0,c=s.length;c>f;f++){var d=s[f],l=o[d];l=a(t,l);var y=parseFloat(l);n[d]=isNaN(y)?0:y}var m=n.paddingLeft+n.paddingRight,g=n.paddingTop+n.paddingBottom,v=n.marginLeft+n.marginRight,_=n.marginTop+n.marginBottom,I=n.borderLeftWidth+n.borderRightWidth,L=n.borderTopWidth+n.borderBottomWidth,z=h&&u,S=e(o.width);S!==!1&&(n.width=S+(z?0:m+I));var b=e(o.height);return b!==!1&&(n.height=b+(z?0:g+L)),n.innerWidth=n.width-(m+I),n.innerHeight=n.height-(g+L),n.outerWidth=n.width+v,n.outerHeight=n.height+_,n}}function a(t,e){if(n||-1===e.indexOf("%"))return e;var i=t.style,o=i.left,r=t.runtimeStyle,s=r&&r.left;return s&&(r.left=t.currentStyle.left),i.left=e,e=i.pixelLeft,i.left=o,s&&(r.left=s),e}var u,p=t("boxSizing");return function(){if(p){var t=document.createElement("div");t.style.width="200px",t.style.padding="1px 2px 3px 4px",t.style.borderStyle="solid",t.style.borderWidth="1px 2px 3px 4px",t.style[p]="border-box";var i=document.body||document.documentElement;i.appendChild(t);var o=r(t);u=200===e(o.width),i.removeChild(t)}}(),o}var n=t.getComputedStyle,r=n?function(t){return n(t,null)}:function(t){return t.currentStyle},s=["paddingLeft","paddingRight","paddingTop","paddingBottom","marginLeft","marginRight","marginTop","marginBottom","borderLeftWidth","borderRightWidth","borderTopWidth","borderBottomWidth"];"function"==typeof define&&define.amd?define("get-size/get-size",["get-style-property/get-style-property"],o):"object"==typeof exports?module.exports=o(require("get-style-property")):t.getSize=o(t.getStyleProperty)}(window),function(t,e){function i(t,e){return t[a](e)}function o(t){if(!t.parentNode){var e=document.createDocumentFragment();e.appendChild(t)}}function n(t,e){o(t);for(var i=t.parentNode.querySelectorAll(e),n=0,r=i.length;r>n;n++)if(i[n]===t)return!0;return!1}function r(t,e){return o(t),i(t,e)}var s,a=function(){if(e.matchesSelector)return"matchesSelector";for(var t=["webkit","moz","ms","o"],i=0,o=t.length;o>i;i++){var n=t[i],r=n+"MatchesSelector";if(e[r])return r}}();if(a){var u=document.createElement("div"),p=i(u,"div");s=p?i:r}else s=n;"function"==typeof define&&define.amd?define("matches-selector/matches-selector",[],function(){return s}):window.matchesSelector=s}(this,Element.prototype),function(t){function e(t,e){for(var i in e)t[i]=e[i];return t}function i(t){for(var e in t)return!1;return e=null,!0}function o(t){return t.replace(/([A-Z])/g,function(t){return"-"+t.toLowerCase()})}function n(t,n,r){function a(t,e){t&&(this.element=t,this.layout=e,this.position={x:0,y:0},this._create())}var u=r("transition"),p=r("transform"),h=u&&p,f=!!r("perspective"),c={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"otransitionend",transition:"transitionend"}[u],d=["transform","transition","transitionDuration","transitionProperty"],l=function(){for(var t={},e=0,i=d.length;i>e;e++){var o=d[e],n=r(o);n&&n!==o&&(t[o]=n)}return t}();e(a.prototype,t.prototype),a.prototype._create=function(){this._transn={ingProperties:{},clean:{},onEnd:{}},this.css({position:"absolute"})},a.prototype.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},a.prototype.getSize=function(){this.size=n(this.element)},a.prototype.css=function(t){var e=this.element.style;for(var i in t){var o=l[i]||i;e[o]=t[i]}},a.prototype.getPosition=function(){var t=s(this.element),e=this.layout.options,i=e.isOriginLeft,o=e.isOriginTop,n=parseInt(t[i?"left":"right"],10),r=parseInt(t[o?"top":"bottom"],10);n=isNaN(n)?0:n,r=isNaN(r)?0:r;var a=this.layout.size;n-=i?a.paddingLeft:a.paddingRight,r-=o?a.paddingTop:a.paddingBottom,this.position.x=n,this.position.y=r},a.prototype.layoutPosition=function(){var t=this.layout.size,e=this.layout.options,i={};e.isOriginLeft?(i.left=this.position.x+t.paddingLeft+"px",i.right=""):(i.right=this.position.x+t.paddingRight+"px",i.left=""),e.isOriginTop?(i.top=this.position.y+t.paddingTop+"px",i.bottom=""):(i.bottom=this.position.y+t.paddingBottom+"px",i.top=""),this.css(i),this.emitEvent("layout",[this])};var y=f?function(t,e){return"translate3d("+t+"px, "+e+"px, 0)"}:function(t,e){return"translate("+t+"px, "+e+"px)"};a.prototype._transitionTo=function(t,e){this.getPosition();var i=this.position.x,o=this.position.y,n=parseInt(t,10),r=parseInt(e,10),s=n===this.position.x&&r===this.position.y;if(this.setPosition(t,e),s&&!this.isTransitioning)return this.layoutPosition(),void 0;var a=t-i,u=e-o,p={},h=this.layout.options;a=h.isOriginLeft?a:-a,u=h.isOriginTop?u:-u,p.transform=y(a,u),this.transition({to:p,onTransitionEnd:{transform:this.layoutPosition},isCleaning:!0})},a.prototype.goTo=function(t,e){this.setPosition(t,e),this.layoutPosition()},a.prototype.moveTo=h?a.prototype._transitionTo:a.prototype.goTo,a.prototype.setPosition=function(t,e){this.position.x=parseInt(t,10),this.position.y=parseInt(e,10)},a.prototype._nonTransition=function(t){this.css(t.to),t.isCleaning&&this._removeStyles(t.to);for(var e in t.onTransitionEnd)t.onTransitionEnd[e].call(this)},a.prototype._transition=function(t){if(!parseFloat(this.layout.options.transitionDuration))return this._nonTransition(t),void 0;var e=this._transn;for(var i in t.onTransitionEnd)e.onEnd[i]=t.onTransitionEnd[i];for(i in t.to)e.ingProperties[i]=!0,t.isCleaning&&(e.clean[i]=!0);if(t.from){this.css(t.from);var o=this.element.offsetHeight;o=null}this.enableTransition(t.to),this.css(t.to),this.isTransitioning=!0};var m=p&&o(p)+",opacity";a.prototype.enableTransition=function(){this.isTransitioning||(this.css({transitionProperty:m,transitionDuration:this.layout.options.transitionDuration}),this.element.addEventListener(c,this,!1))},a.prototype.transition=a.prototype[u?"_transition":"_nonTransition"],a.prototype.onwebkitTransitionEnd=function(t){this.ontransitionend(t)},a.prototype.onotransitionend=function(t){this.ontransitionend(t)};var g={"-webkit-transform":"transform","-moz-transform":"transform","-o-transform":"transform"};a.prototype.ontransitionend=function(t){if(t.target===this.element){var e=this._transn,o=g[t.propertyName]||t.propertyName;if(delete e.ingProperties[o],i(e.ingProperties)&&this.disableTransition(),o in e.clean&&(this.element.style[t.propertyName]="",delete e.clean[o]),o in e.onEnd){var n=e.onEnd[o];n.call(this),delete e.onEnd[o]}this.emitEvent("transitionEnd",[this])}},a.prototype.disableTransition=function(){this.removeTransitionStyles(),this.element.removeEventListener(c,this,!1),this.isTransitioning=!1},a.prototype._removeStyles=function(t){var e={};for(var i in t)e[i]="";this.css(e)};var v={transitionProperty:"",transitionDuration:""};return a.prototype.removeTransitionStyles=function(){this.css(v)},a.prototype.removeElem=function(){this.element.parentNode.removeChild(this.element),this.emitEvent("remove",[this])},a.prototype.remove=function(){if(!u||!parseFloat(this.layout.options.transitionDuration))return this.removeElem(),void 0;var t=this;this.on("transitionEnd",function(){return t.removeElem(),!0}),this.hide()},a.prototype.reveal=function(){delete this.isHidden,this.css({display:""});var t=this.layout.options;this.transition({from:t.hiddenStyle,to:t.visibleStyle,isCleaning:!0})},a.prototype.hide=function(){this.isHidden=!0,this.css({display:""});var t=this.layout.options;this.transition({from:t.visibleStyle,to:t.hiddenStyle,isCleaning:!0,onTransitionEnd:{opacity:function(){this.isHidden&&this.css({display:"none"})}}})},a.prototype.destroy=function(){this.css({position:"",left:"",right:"",top:"",bottom:"",transition:"",transform:""})},a}var r=t.getComputedStyle,s=r?function(t){return r(t,null)}:function(t){return t.currentStyle};"function"==typeof define&&define.amd?define("outlayer/item",["eventEmitter/EventEmitter","get-size/get-size","get-style-property/get-style-property"],n):(t.Outlayer={},t.Outlayer.Item=n(t.EventEmitter,t.getSize,t.getStyleProperty))}(window),function(t){function e(t,e){for(var i in e)t[i]=e[i];return t}function i(t){return"[object Array]"===f.call(t)}function o(t){var e=[];if(i(t))e=t;else if(t&&"number"==typeof t.length)for(var o=0,n=t.length;n>o;o++)e.push(t[o]);else e.push(t);return e}function n(t,e){var i=d(e,t);-1!==i&&e.splice(i,1)}function r(t){return t.replace(/(.)([A-Z])/g,function(t,e,i){return e+"-"+i}).toLowerCase()}function s(i,s,f,d,l,y){function m(t,i){if("string"==typeof t&&(t=a.querySelector(t)),!t||!c(t))return u&&u.error("Bad "+this.constructor.namespace+" element: "+t),void 0;this.element=t,this.options=e({},this.constructor.defaults),this.option(i);var o=++g;this.element.outlayerGUID=o,v[o]=this,this._create(),this.options.isInitLayout&&this.layout()}var g=0,v={};return m.namespace="outlayer",m.Item=y,m.defaults={containerStyle:{position:"relative"},isInitLayout:!0,isOriginLeft:!0,isOriginTop:!0,isResizeBound:!0,isResizingContainer:!0,transitionDuration:"0.4s",hiddenStyle:{opacity:0,transform:"scale(0.001)"},visibleStyle:{opacity:1,transform:"scale(1)"}},e(m.prototype,f.prototype),m.prototype.option=function(t){e(this.options,t)},m.prototype._create=function(){this.reloadItems(),this.stamps=[],this.stamp(this.options.stamp),e(this.element.style,this.options.containerStyle),this.options.isResizeBound&&this.bindResize()},m.prototype.reloadItems=function(){this.items=this._itemize(this.element.children)},m.prototype._itemize=function(t){for(var e=this._filterFindItemElements(t),i=this.constructor.Item,o=[],n=0,r=e.length;r>n;n++){var s=e[n],a=new i(s,this);o.push(a)}return o},m.prototype._filterFindItemElements=function(t){t=o(t);for(var e=this.options.itemSelector,i=[],n=0,r=t.length;r>n;n++){var s=t[n];if(c(s))if(e){l(s,e)&&i.push(s);for(var a=s.querySelectorAll(e),u=0,p=a.length;p>u;u++)i.push(a[u])}else i.push(s)}return i},m.prototype.getItemElements=function(){for(var t=[],e=0,i=this.items.length;i>e;e++)t.push(this.items[e].element);return t},m.prototype.layout=function(){this._resetLayout(),this._manageStamps();var t=void 0!==this.options.isLayoutInstant?this.options.isLayoutInstant:!this._isLayoutInited;this.layoutItems(this.items,t),this._isLayoutInited=!0},m.prototype._init=m.prototype.layout,m.prototype._resetLayout=function(){this.getSize()},m.prototype.getSize=function(){this.size=d(this.element)},m.prototype._getMeasurement=function(t,e){var i,o=this.options[t];o?("string"==typeof o?i=this.element.querySelector(o):c(o)&&(i=o),this[t]=i?d(i)[e]:o):this[t]=0},m.prototype.layoutItems=function(t,e){t=this._getItemsForLayout(t),this._layoutItems(t,e),this._postLayout()},m.prototype._getItemsForLayout=function(t){for(var e=[],i=0,o=t.length;o>i;i++){var n=t[i];n.isIgnored||e.push(n)}return e},m.prototype._layoutItems=function(t,e){function i(){o.emitEvent("layoutComplete",[o,t])}var o=this;if(!t||!t.length)return i(),void 0;this._itemsOn(t,"layout",i);for(var n=[],r=0,s=t.length;s>r;r++){var a=t[r],u=this._getItemLayoutPosition(a);u.item=a,u.isInstant=e||a.isLayoutInstant,n.push(u)}this._processLayoutQueue(n)},m.prototype._getItemLayoutPosition=function(){return{x:0,y:0}},m.prototype._processLayoutQueue=function(t){for(var e=0,i=t.length;i>e;e++){var o=t[e];this._positionItem(o.item,o.x,o.y,o.isInstant)}},m.prototype._positionItem=function(t,e,i,o){o?t.goTo(e,i):t.moveTo(e,i)},m.prototype._postLayout=function(){this.resizeContainer()},m.prototype.resizeContainer=function(){if(this.options.isResizingContainer){var t=this._getContainerSize();t&&(this._setContainerMeasure(t.width,!0),this._setContainerMeasure(t.height,!1))}},m.prototype._getContainerSize=h,m.prototype._setContainerMeasure=function(t,e){if(void 0!==t){var i=this.size;i.isBorderBox&&(t+=e?i.paddingLeft+i.paddingRight+i.borderLeftWidth+i.borderRightWidth:i.paddingBottom+i.paddingTop+i.borderTopWidth+i.borderBottomWidth),t=Math.max(t,0),this.element.style[e?"width":"height"]=t+"px"}},m.prototype._itemsOn=function(t,e,i){function o(){return n++,n===r&&i.call(s),!0}for(var n=0,r=t.length,s=this,a=0,u=t.length;u>a;a++){var p=t[a];p.on(e,o)}},m.prototype.ignore=function(t){var e=this.getItem(t);e&&(e.isIgnored=!0)},m.prototype.unignore=function(t){var e=this.getItem(t);e&&delete e.isIgnored},m.prototype.stamp=function(t){if(t=this._find(t)){this.stamps=this.stamps.concat(t);for(var e=0,i=t.length;i>e;e++){var o=t[e];this.ignore(o)}}},m.prototype.unstamp=function(t){if(t=this._find(t))for(var e=0,i=t.length;i>e;e++){var o=t[e];n(o,this.stamps),this.unignore(o)}},m.prototype._find=function(t){return t?("string"==typeof t&&(t=this.element.querySelectorAll(t)),t=o(t)):void 0},m.prototype._manageStamps=function(){if(this.stamps&&this.stamps.length){this._getBoundingRect();for(var t=0,e=this.stamps.length;e>t;t++){var i=this.stamps[t];this._manageStamp(i)}}},m.prototype._getBoundingRect=function(){var t=this.element.getBoundingClientRect(),e=this.size;this._boundingRect={left:t.left+e.paddingLeft+e.borderLeftWidth,top:t.top+e.paddingTop+e.borderTopWidth,right:t.right-(e.paddingRight+e.borderRightWidth),bottom:t.bottom-(e.paddingBottom+e.borderBottomWidth)}},m.prototype._manageStamp=h,m.prototype._getElementOffset=function(t){var e=t.getBoundingClientRect(),i=this._boundingRect,o=d(t),n={left:e.left-i.left-o.marginLeft,top:e.top-i.top-o.marginTop,right:i.right-e.right-o.marginRight,bottom:i.bottom-e.bottom-o.marginBottom};return n},m.prototype.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},m.prototype.bindResize=function(){this.isResizeBound||(i.bind(t,"resize",this),this.isResizeBound=!0)},m.prototype.unbindResize=function(){this.isResizeBound&&i.unbind(t,"resize",this),this.isResizeBound=!1},m.prototype.onresize=function(){function t(){e.resize(),delete e.resizeTimeout}this.resizeTimeout&&clearTimeout(this.resizeTimeout);var e=this;this.resizeTimeout=setTimeout(t,100)},m.prototype.resize=function(){this.isResizeBound&&this.needsResizeLayout()&&this.layout()},m.prototype.needsResizeLayout=function(){var t=d(this.element),e=this.size&&t;return e&&t.innerWidth!==this.size.innerWidth},m.prototype.addItems=function(t){var e=this._itemize(t);return e.length&&(this.items=this.items.concat(e)),e},m.prototype.appended=function(t){var e=this.addItems(t);e.length&&(this.layoutItems(e,!0),this.reveal(e))},m.prototype.prepended=function(t){var e=this._itemize(t);if(e.length){var i=this.items.slice(0);this.items=e.concat(i),this._resetLayout(),this._manageStamps(),this.layoutItems(e,!0),this.reveal(e),this.layoutItems(i)}},m.prototype.reveal=function(t){var e=t&&t.length;if(e)for(var i=0;e>i;i++){var o=t[i];o.reveal()}},m.prototype.hide=function(t){var e=t&&t.length;if(e)for(var i=0;e>i;i++){var o=t[i];o.hide()}},m.prototype.getItem=function(t){for(var e=0,i=this.items.length;i>e;e++){var o=this.items[e];if(o.element===t)return o}},m.prototype.getItems=function(t){if(t&&t.length){for(var e=[],i=0,o=t.length;o>i;i++){var n=t[i],r=this.getItem(n);r&&e.push(r)}return e}},m.prototype.remove=function(t){t=o(t);var e=this.getItems(t);if(e&&e.length){this._itemsOn(e,"remove",function(){this.emitEvent("removeComplete",[this,e])});for(var i=0,r=e.length;r>i;i++){var s=e[i];s.remove(),n(s,this.items)}}},m.prototype.destroy=function(){var t=this.element.style;t.height="",t.position="",t.width="";for(var e=0,i=this.items.length;i>e;e++){var o=this.items[e];o.destroy()}this.unbindResize(),delete this.element.outlayerGUID,p&&p.removeData(this.element,this.constructor.namespace)},m.data=function(t){var e=t&&t.outlayerGUID;return e&&v[e]},m.create=function(t,i){function o(){m.apply(this,arguments)}return Object.create?o.prototype=Object.create(m.prototype):e(o.prototype,m.prototype),o.prototype.constructor=o,o.defaults=e({},m.defaults),e(o.defaults,i),o.prototype.settings={},o.namespace=t,o.data=m.data,o.Item=function(){y.apply(this,arguments)},o.Item.prototype=new y,s(function(){for(var e=r(t),i=a.querySelectorAll(".js-"+e),n="data-"+e+"-options",s=0,h=i.length;h>s;s++){var f,c=i[s],d=c.getAttribute(n);try{f=d&&JSON.parse(d)}catch(l){u&&u.error("Error parsing "+n+" on "+c.nodeName.toLowerCase()+(c.id?"#"+c.id:"")+": "+l);continue}var y=new o(c,f);p&&p.data(c,t,y)}}),p&&p.bridget&&p.bridget(t,o),o},m.Item=y,m}var a=t.document,u=t.console,p=t.jQuery,h=function(){},f=Object.prototype.toString,c="object"==typeof HTMLElement?function(t){return t instanceof HTMLElement}:function(t){return t&&"object"==typeof t&&1===t.nodeType&&"string"==typeof t.nodeName},d=Array.prototype.indexOf?function(t,e){return t.indexOf(e)}:function(t,e){for(var i=0,o=t.length;o>i;i++)if(t[i]===e)return i;return-1};"function"==typeof define&&define.amd?define("outlayer/outlayer",["eventie/eventie","doc-ready/doc-ready","eventEmitter/EventEmitter","get-size/get-size","matches-selector/matches-selector","./item"],s):t.Outlayer=s(t.eventie,t.docReady,t.EventEmitter,t.getSize,t.matchesSelector,t.Outlayer.Item)}(window),function(t){function e(t){function e(){t.Item.apply(this,arguments)}return e.prototype=new t.Item,e.prototype._create=function(){this.id=this.layout.itemGUID++,t.Item.prototype._create.call(this),this.sortData={}},e.prototype.updateSortData=function(){if(!this.isIgnored){this.sortData.id=this.id,this.sortData["original-order"]=this.id,this.sortData.random=Math.random();var t=this.layout.options.getSortData,e=this.layout._sorters;for(var i in t){var o=e[i];this.sortData[i]=o(this.element,this)}}},e}"function"==typeof define&&define.amd?define("isotope/js/item",["outlayer/outlayer"],e):(t.Isotope=t.Isotope||{},t.Isotope.Item=e(t.Outlayer))}(window),function(t){function e(t,e){function i(t){this.isotope=t,t&&(this.options=t.options[this.namespace],this.element=t.element,this.items=t.filteredItems,this.size=t.size)}return function(){function t(t){return function(){return e.prototype[t].apply(this.isotope,arguments)}}for(var o=["_resetLayout","_getItemLayoutPosition","_manageStamp","_getContainerSize","_getElementOffset","needsResizeLayout"],n=0,r=o.length;r>n;n++){var s=o[n];i.prototype[s]=t(s)}}(),i.prototype.needsVerticalResizeLayout=function(){var e=t(this.isotope.element),i=this.isotope.size&&e;return i&&e.innerHeight!==this.isotope.size.innerHeight},i.prototype._getMeasurement=function(){this.isotope._getMeasurement.apply(this,arguments)},i.prototype.getColumnWidth=function(){this.getSegmentSize("column","Width")},i.prototype.getRowHeight=function(){this.getSegmentSize("row","Height")},i.prototype.getSegmentSize=function(t,e){var i=t+e,o="outer"+e;if(this._getMeasurement(i,o),!this[i]){var n=this.getFirstItemSize();this[i]=n&&n[o]||this.isotope.size["inner"+e]}},i.prototype.getFirstItemSize=function(){var e=this.isotope.filteredItems[0];return e&&e.element&&t(e.element)},i.prototype.layout=function(){this.isotope.layout.apply(this.isotope,arguments)},i.prototype.getSize=function(){this.isotope.getSize(),this.size=this.isotope.size},i.modes={},i.create=function(t,e){function o(){i.apply(this,arguments)}return o.prototype=new i,e&&(o.options=e),o.prototype.namespace=t,i.modes[t]=o,o},i}"function"==typeof define&&define.amd?define("isotope/js/layout-mode",["get-size/get-size","outlayer/outlayer"],e):(t.Isotope=t.Isotope||{},t.Isotope.LayoutMode=e(t.getSize,t.Outlayer))}(window),function(t){function e(t,e){var o=t.create("masonry");return o.prototype._resetLayout=function(){this.getSize(),this._getMeasurement("columnWidth","outerWidth"),this._getMeasurement("gutter","outerWidth"),this.measureColumns();var t=this.cols;for(this.colYs=[];t--;)this.colYs.push(0);this.maxY=0},o.prototype.measureColumns=function(){if(this.getContainerWidth(),!this.columnWidth){var t=this.items[0],i=t&&t.element;this.columnWidth=i&&e(i).outerWidth||this.containerWidth}this.columnWidth+=this.gutter,this.cols=Math.floor((this.containerWidth+this.gutter)/this.columnWidth),this.cols=Math.max(this.cols,1)},o.prototype.getContainerWidth=function(){var t=this.options.isFitWidth?this.element.parentNode:this.element,i=e(t);this.containerWidth=i&&i.innerWidth},o.prototype._getItemLayoutPosition=function(t){t.getSize();var e=t.size.outerWidth%this.columnWidth,o=e&&1>e?"round":"ceil",n=Math[o](t.size.outerWidth/this.columnWidth);n=Math.min(n,this.cols);for(var r=this._getColGroup(n),s=Math.min.apply(Math,r),a=i(r,s),u={x:this.columnWidth*a,y:s},p=s+t.size.outerHeight,h=this.cols+1-r.length,f=0;h>f;f++)this.colYs[a+f]=p;return u},o.prototype._getColGroup=function(t){if(2>t)return this.colYs;for(var e=[],i=this.cols+1-t,o=0;i>o;o++){var n=this.colYs.slice(o,o+t);e[o]=Math.max.apply(Math,n)}return e},o.prototype._manageStamp=function(t){var i=e(t),o=this._getElementOffset(t),n=this.options.isOriginLeft?o.left:o.right,r=n+i.outerWidth,s=Math.floor(n/this.columnWidth);s=Math.max(0,s);var a=Math.floor(r/this.columnWidth);a-=r%this.columnWidth?0:1,a=Math.min(this.cols-1,a);for(var u=(this.options.isOriginTop?o.top:o.bottom)+i.outerHeight,p=s;a>=p;p++)this.colYs[p]=Math.max(u,this.colYs[p])},o.prototype._getContainerSize=function(){this.maxY=Math.max.apply(Math,this.colYs);var t={height:this.maxY};return this.options.isFitWidth&&(t.width=this._getContainerFitWidth()),t},o.prototype._getContainerFitWidth=function(){for(var t=0,e=this.cols;--e&&0===this.colYs[e];)t++;return(this.cols-t)*this.columnWidth-this.gutter},o.prototype.needsResizeLayout=function(){var t=this.containerWidth;return this.getContainerWidth(),t!==this.containerWidth},o}var i=Array.prototype.indexOf?function(t,e){return t.indexOf(e)}:function(t,e){for(var i=0,o=t.length;o>i;i++){var n=t[i];if(n===e)return i}return-1};"function"==typeof define&&define.amd?define("masonry/masonry",["outlayer/outlayer","get-size/get-size"],e):t.Masonry=e(t.Outlayer,t.getSize)}(window),function(t){function e(t,e){for(var i in e)t[i]=e[i];return t}function i(t,i){var o=t.create("masonry"),n=o.prototype._getElementOffset,r=o.prototype.layout,s=o.prototype._getMeasurement;e(o.prototype,i.prototype),o.prototype._getElementOffset=n,o.prototype.layout=r,o.prototype._getMeasurement=s;var a=o.prototype.measureColumns;o.prototype.measureColumns=function(){this.items=this.isotope.filteredItems,a.call(this)};var u=o.prototype._manageStamp;return o.prototype._manageStamp=function(){this.options.isOriginLeft=this.isotope.options.isOriginLeft,this.options.isOriginTop=this.isotope.options.isOriginTop,u.apply(this,arguments)},o}"function"==typeof define&&define.amd?define("isotope/js/layout-modes/masonry",["../layout-mode","masonry/masonry"],i):i(t.Isotope.LayoutMode,t.Masonry)}(window),function(t){function e(t){var e=t.create("fitRows");return e.prototype._resetLayout=function(){this.x=0,this.y=0,this.maxY=0},e.prototype._getItemLayoutPosition=function(t){t.getSize(),0!==this.x&&t.size.outerWidth+this.x>this.isotope.size.innerWidth&&(this.x=0,this.y=this.maxY);var e={x:this.x,y:this.y};return this.maxY=Math.max(this.maxY,this.y+t.size.outerHeight),this.x+=t.size.outerWidth,e},e.prototype._getContainerSize=function(){return{height:this.maxY}},e}"function"==typeof define&&define.amd?define("isotope/js/layout-modes/fit-rows",["../layout-mode"],e):e(t.Isotope.LayoutMode)}(window),function(t){function e(t){var e=t.create("vertical",{horizontalAlignment:0});return e.prototype._resetLayout=function(){this.y=0},e.prototype._getItemLayoutPosition=function(t){t.getSize();var e=(this.isotope.size.innerWidth-t.size.outerWidth)*this.options.horizontalAlignment,i=this.y;return this.y+=t.size.outerHeight,{x:e,y:i}},e.prototype._getContainerSize=function(){return{height:this.y}},e}"function"==typeof define&&define.amd?define("isotope/js/layout-modes/vertical",["../layout-mode"],e):e(t.Isotope.LayoutMode)}(window),function(t){function e(t,e){for(var i in e)t[i]=e[i];return t}function i(t){return"[object Array]"===h.call(t)}function o(t){var e=[];if(i(t))e=t;else if(t&&"number"==typeof t.length)for(var o=0,n=t.length;n>o;o++)e.push(t[o]);else e.push(t);return e}function n(t,e){var i=f(e,t);-1!==i&&e.splice(i,1)}function r(t,i,r,u,h){function f(t,e){return function(i,o){for(var n=0,r=t.length;r>n;n++){var s=t[n],a=i.sortData[s],u=o.sortData[s];if(a>u||u>a){var p=void 0!==e[s]?e[s]:e,h=p?1:-1;return(a>u?1:-1)*h}}return 0}}var c=t.create("isotope",{layoutMode:"masonry",isJQueryFiltering:!0,sortAscending:!0});c.Item=u,c.LayoutMode=h,c.prototype._create=function(){this.itemGUID=0,this._sorters={},this._getSorters(),t.prototype._create.call(this),this.modes={},this.filteredItems=this.items,this.sortHistory=["original-order"];for(var e in h.modes)this._initLayoutMode(e)},c.prototype.reloadItems=function(){this.itemGUID=0,t.prototype.reloadItems.call(this)},c.prototype._itemize=function(){for(var e=t.prototype._itemize.apply(this,arguments),i=0,o=e.length;o>i;i++){var n=e[i];n.id=this.itemGUID++}return this._updateItemsSortData(e),e},c.prototype._initLayoutMode=function(t){var i=h.modes[t],o=this.options[t]||{};this.options[t]=i.options?e(i.options,o):o,this.modes[t]=new i(this)},c.prototype.layout=function(){return!this._isLayoutInited&&this.options.isInitLayout?(this.arrange(),void 0):(this._layout(),void 0)},c.prototype._layout=function(){var t=this._getIsInstant();this._resetLayout(),this._manageStamps(),this.layoutItems(this.filteredItems,t),this._isLayoutInited=!0},c.prototype.arrange=function(t){this.option(t),this._getIsInstant(),this.filteredItems=this._filter(this.items),this._sort(),this._layout()},c.prototype._init=c.prototype.arrange,c.prototype._getIsInstant=function(){var t=void 0!==this.options.isLayoutInstant?this.options.isLayoutInstant:!this._isLayoutInited;return this._isInstant=t,t},c.prototype._filter=function(t){function e(){f.reveal(n),f.hide(r)}var i=this.options.filter;i=i||"*";for(var o=[],n=[],r=[],s=this._getFilterTest(i),a=0,u=t.length;u>a;a++){var p=t[a];if(!p.isIgnored){var h=s(p);h&&o.push(p),h&&p.isHidden?n.push(p):h||p.isHidden||r.push(p)}}var f=this;return this._isInstant?this._noTransition(e):e(),o},c.prototype._getFilterTest=function(t){return s&&this.options.isJQueryFiltering?function(e){return s(e.element).is(t)}:"function"==typeof t?function(e){return t(e.element)}:function(e){return r(e.element,t)}},c.prototype.updateSortData=function(t){this._getSorters(),t=o(t);var e=this.getItems(t);e=e.length?e:this.items,this._updateItemsSortData(e)
},c.prototype._getSorters=function(){var t=this.options.getSortData;for(var e in t){var i=t[e];this._sorters[e]=d(i)}},c.prototype._updateItemsSortData=function(t){for(var e=0,i=t.length;i>e;e++){var o=t[e];o.updateSortData()}};var d=function(){function t(t){if("string"!=typeof t)return t;var i=a(t).split(" "),o=i[0],n=o.match(/^\[(.+)\]$/),r=n&&n[1],s=e(r,o),u=c.sortDataParsers[i[1]];return t=u?function(t){return t&&u(s(t))}:function(t){return t&&s(t)}}function e(t,e){var i;return i=t?function(e){return e.getAttribute(t)}:function(t){var i=t.querySelector(e);return i&&p(i)}}return t}();c.sortDataParsers={parseInt:function(t){return parseInt(t,10)},parseFloat:function(t){return parseFloat(t)}},c.prototype._sort=function(){var t=this.options.sortBy;if(t){var e=[].concat.apply(t,this.sortHistory),i=f(e,this.options.sortAscending);this.filteredItems.sort(i),t!==this.sortHistory[0]&&this.sortHistory.unshift(t)}},c.prototype._mode=function(){var t=this.options.layoutMode,e=this.modes[t];if(!e)throw Error("No layout mode: "+t);return e.options=this.options[t],e},c.prototype._resetLayout=function(){t.prototype._resetLayout.call(this),this._mode()._resetLayout()},c.prototype._getItemLayoutPosition=function(t){return this._mode()._getItemLayoutPosition(t)},c.prototype._manageStamp=function(t){this._mode()._manageStamp(t)},c.prototype._getContainerSize=function(){return this._mode()._getContainerSize()},c.prototype.needsResizeLayout=function(){return this._mode().needsResizeLayout()},c.prototype.appended=function(t){var e=this.addItems(t);if(e.length){var i=this._filterRevealAdded(e);this.filteredItems=this.filteredItems.concat(i)}},c.prototype.prepended=function(t){var e=this._itemize(t);if(e.length){var i=this.items.slice(0);this.items=e.concat(i),this._resetLayout(),this._manageStamps();var o=this._filterRevealAdded(e);this.layoutItems(i),this.filteredItems=o.concat(this.filteredItems)}},c.prototype._filterRevealAdded=function(t){var e=this._noTransition(function(){return this._filter(t)});return this.layoutItems(e,!0),this.reveal(e),t},c.prototype.insert=function(t){var e=this.addItems(t);if(e.length){var i,o,n=e.length;for(i=0;n>i;i++)o=e[i],this.element.appendChild(o.element);var r=this._filter(e);for(this._noTransition(function(){this.hide(r)}),i=0;n>i;i++)e[i].isLayoutInstant=!0;for(this.arrange(),i=0;n>i;i++)delete e[i].isLayoutInstant;this.reveal(r)}};var l=c.prototype.remove;return c.prototype.remove=function(t){t=o(t);var e=this.getItems(t);if(l.call(this,t),e&&e.length)for(var i=0,r=e.length;r>i;i++){var s=e[i];n(s,this.filteredItems)}},c.prototype._noTransition=function(t){var e=this.options.transitionDuration;this.options.transitionDuration=0;var i=t.call(this);return this.options.transitionDuration=e,i},c}var s=t.jQuery,a=String.prototype.trim?function(t){return t.trim()}:function(t){return t.replace(/^\s+|\s+$/g,"")},u=document.documentElement,p=u.textContent?function(t){return t.textContent}:function(t){return t.innerText},h=Object.prototype.toString,f=Array.prototype.indexOf?function(t,e){return t.indexOf(e)}:function(t,e){for(var i=0,o=t.length;o>i;i++)if(t[i]===e)return i;return-1};"function"==typeof define&&define.amd?define(["outlayer/outlayer","get-size/get-size","matches-selector/matches-selector","isotope/js/item","isotope/js/layout-mode","isotope/js/layout-modes/masonry","isotope/js/layout-modes/fit-rows","isotope/js/layout-modes/vertical"],r):t.Isotope=r(t.Outlayer,t.getSize,t.matchesSelector,t.Isotope.Item,t.Isotope.LayoutMode)}(window);
/*Isotopes END*/
/*! * jQuery Form Plugin(ajax uplaod) version: 3.51.0-2014.06.20  Examples and documentation at: http://malsup.com/jquery/form/ */
!function(e){"use strict";"function"==typeof define&&define.amd?define(["jquery"],e):e("undefined"!=typeof jQuery?jQuery:window.Zepto)}(function(e){"use strict";function t(t){var r=t.data;t.isDefaultPrevented()||(t.preventDefault(),e(t.target).ajaxSubmit(r))}function r(t){var r=t.target,a=e(r);if(!a.is("[type=submit],[type=image]")){var n=a.closest("[type=submit]");if(0===n.length)return;r=n[0]}var i=this;if(i.clk=r,"image"==r.type)if(void 0!==t.offsetX)i.clk_x=t.offsetX,i.clk_y=t.offsetY;else if("function"==typeof e.fn.offset){var o=a.offset();i.clk_x=t.pageX-o.left,i.clk_y=t.pageY-o.top}else i.clk_x=t.pageX-r.offsetLeft,i.clk_y=t.pageY-r.offsetTop;setTimeout(function(){i.clk=i.clk_x=i.clk_y=null},100)}function a(){if(e.fn.ajaxSubmit.debug){var t="[jquery.form] "+Array.prototype.join.call(arguments,"");window.console&&window.console.log?window.console.log(t):window.opera&&window.opera.postError&&window.opera.postError(t)}}var n={};n.fileapi=void 0!==e("<input type='file'/>").get(0).files,n.formdata=void 0!==window.FormData;var i=!!e.fn.prop;e.fn.attr2=function(){if(!i)return this.attr.apply(this,arguments);var e=this.prop.apply(this,arguments);return e&&e.jquery||"string"==typeof e?e:this.attr.apply(this,arguments)},e.fn.ajaxSubmit=function(t){function r(r){var a,n,i=e.param(r,t.traditional).split("&"),o=i.length,s=[];for(a=0;o>a;a++)i[a]=i[a].replace(/\+/g," "),n=i[a].split("="),s.push([decodeURIComponent(n[0]),decodeURIComponent(n[1])]);return s}function o(a){for(var n=new FormData,i=0;i<a.length;i++)n.append(a[i].name,a[i].value);if(t.extraData){var o=r(t.extraData);for(i=0;i<o.length;i++)o[i]&&n.append(o[i][0],o[i][1])}t.data=null;var s=e.extend(!0,{},e.ajaxSettings,t,{contentType:!1,processData:!1,cache:!1,type:u||"POST"});t.uploadProgress&&(s.xhr=function(){var r=e.ajaxSettings.xhr();return r.upload&&r.upload.addEventListener("progress",function(e){var r=0,a=e.loaded||e.position,n=e.total;e.lengthComputable&&(r=Math.ceil(a/n*100)),t.uploadProgress(e,a,n,r)},!1),r}),s.data=null;var c=s.beforeSend;return s.beforeSend=function(e,r){r.data=t.formData?t.formData:n,c&&c.call(this,e,r)},e.ajax(s)}function s(r){function n(e){var t=null;try{e.contentWindow&&(t=e.contentWindow.document)}catch(r){a("cannot get iframe.contentWindow document: "+r)}if(t)return t;try{t=e.contentDocument?e.contentDocument:e.document}catch(r){a("cannot get iframe.contentDocument: "+r),t=e.document}return t}function o(){function t(){try{var e=n(g).readyState;a("state = "+e),e&&"uninitialized"==e.toLowerCase()&&setTimeout(t,50)}catch(r){a("Server abort: ",r," (",r.name,")"),s(k),j&&clearTimeout(j),j=void 0}}var r=f.attr2("target"),i=f.attr2("action"),o="multipart/form-data",c=f.attr("enctype")||f.attr("encoding")||o;w.setAttribute("target",p),(!u||/post/i.test(u))&&w.setAttribute("method","POST"),i!=m.url&&w.setAttribute("action",m.url),m.skipEncodingOverride||u&&!/post/i.test(u)||f.attr({encoding:"multipart/form-data",enctype:"multipart/form-data"}),m.timeout&&(j=setTimeout(function(){T=!0,s(D)},m.timeout));var l=[];try{if(m.extraData)for(var d in m.extraData)m.extraData.hasOwnProperty(d)&&l.push(e.isPlainObject(m.extraData[d])&&m.extraData[d].hasOwnProperty("name")&&m.extraData[d].hasOwnProperty("value")?e('<input type="hidden" name="'+m.extraData[d].name+'">').val(m.extraData[d].value).appendTo(w)[0]:e('<input type="hidden" name="'+d+'">').val(m.extraData[d]).appendTo(w)[0]);m.iframeTarget||v.appendTo("body"),g.attachEvent?g.attachEvent("onload",s):g.addEventListener("load",s,!1),setTimeout(t,15);try{w.submit()}catch(h){var x=document.createElement("form").submit;x.apply(w)}}finally{w.setAttribute("action",i),w.setAttribute("enctype",c),r?w.setAttribute("target",r):f.removeAttr("target"),e(l).remove()}}function s(t){if(!x.aborted&&!F){if(M=n(g),M||(a("cannot access response document"),t=k),t===D&&x)return x.abort("timeout"),void S.reject(x,"timeout");if(t==k&&x)return x.abort("server abort"),void S.reject(x,"error","server abort");if(M&&M.location.href!=m.iframeSrc||T){g.detachEvent?g.detachEvent("onload",s):g.removeEventListener("load",s,!1);var r,i="success";try{if(T)throw"timeout";var o="xml"==m.dataType||M.XMLDocument||e.isXMLDoc(M);if(a("isXml="+o),!o&&window.opera&&(null===M.body||!M.body.innerHTML)&&--O)return a("requeing onLoad callback, DOM not available"),void setTimeout(s,250);var u=M.body?M.body:M.documentElement;x.responseText=u?u.innerHTML:null,x.responseXML=M.XMLDocument?M.XMLDocument:M,o&&(m.dataType="xml"),x.getResponseHeader=function(e){var t={"content-type":m.dataType};return t[e.toLowerCase()]},u&&(x.status=Number(u.getAttribute("status"))||x.status,x.statusText=u.getAttribute("statusText")||x.statusText);var c=(m.dataType||"").toLowerCase(),l=/(json|script|text)/.test(c);if(l||m.textarea){var f=M.getElementsByTagName("textarea")[0];if(f)x.responseText=f.value,x.status=Number(f.getAttribute("status"))||x.status,x.statusText=f.getAttribute("statusText")||x.statusText;else if(l){var p=M.getElementsByTagName("pre")[0],h=M.getElementsByTagName("body")[0];p?x.responseText=p.textContent?p.textContent:p.innerText:h&&(x.responseText=h.textContent?h.textContent:h.innerText)}}else"xml"==c&&!x.responseXML&&x.responseText&&(x.responseXML=X(x.responseText));try{E=_(x,c,m)}catch(y){i="parsererror",x.error=r=y||i}}catch(y){a("error caught: ",y),i="error",x.error=r=y||i}x.aborted&&(a("upload aborted"),i=null),x.status&&(i=x.status>=200&&x.status<300||304===x.status?"success":"error"),"success"===i?(m.success&&m.success.call(m.context,E,"success",x),S.resolve(x.responseText,"success",x),d&&e.event.trigger("ajaxSuccess",[x,m])):i&&(void 0===r&&(r=x.statusText),m.error&&m.error.call(m.context,x,i,r),S.reject(x,"error",r),d&&e.event.trigger("ajaxError",[x,m,r])),d&&e.event.trigger("ajaxComplete",[x,m]),d&&!--e.active&&e.event.trigger("ajaxStop"),m.complete&&m.complete.call(m.context,x,i),F=!0,m.timeout&&clearTimeout(j),setTimeout(function(){m.iframeTarget?v.attr("src",m.iframeSrc):v.remove(),x.responseXML=null},100)}}}var c,l,m,d,p,v,g,x,y,b,T,j,w=f[0],S=e.Deferred();if(S.abort=function(e){x.abort(e)},r)for(l=0;l<h.length;l++)c=e(h[l]),i?c.prop("disabled",!1):c.removeAttr("disabled");if(m=e.extend(!0,{},e.ajaxSettings,t),m.context=m.context||m,p="jqFormIO"+(new Date).getTime(),m.iframeTarget?(v=e(m.iframeTarget),b=v.attr2("name"),b?p=b:v.attr2("name",p)):(v=e('<iframe name="'+p+'" src="'+m.iframeSrc+'" />'),v.css({position:"absolute",top:"-1000px",left:"-1000px"})),g=v[0],x={aborted:0,responseText:null,responseXML:null,status:0,statusText:"n/a",getAllResponseHeaders:function(){},getResponseHeader:function(){},setRequestHeader:function(){},abort:function(t){var r="timeout"===t?"timeout":"aborted";a("aborting upload... "+r),this.aborted=1;try{g.contentWindow.document.execCommand&&g.contentWindow.document.execCommand("Stop")}catch(n){}v.attr("src",m.iframeSrc),x.error=r,m.error&&m.error.call(m.context,x,r,t),d&&e.event.trigger("ajaxError",[x,m,r]),m.complete&&m.complete.call(m.context,x,r)}},d=m.global,d&&0===e.active++&&e.event.trigger("ajaxStart"),d&&e.event.trigger("ajaxSend",[x,m]),m.beforeSend&&m.beforeSend.call(m.context,x,m)===!1)return m.global&&e.active--,S.reject(),S;if(x.aborted)return S.reject(),S;y=w.clk,y&&(b=y.name,b&&!y.disabled&&(m.extraData=m.extraData||{},m.extraData[b]=y.value,"image"==y.type&&(m.extraData[b+".x"]=w.clk_x,m.extraData[b+".y"]=w.clk_y)));var D=1,k=2,A=e("meta[name=csrf-token]").attr("content"),L=e("meta[name=csrf-param]").attr("content");L&&A&&(m.extraData=m.extraData||{},m.extraData[L]=A),m.forceSync?o():setTimeout(o,10);var E,M,F,O=50,X=e.parseXML||function(e,t){return window.ActiveXObject?(t=new ActiveXObject("Microsoft.XMLDOM"),t.async="false",t.loadXML(e)):t=(new DOMParser).parseFromString(e,"text/xml"),t&&t.documentElement&&"parsererror"!=t.documentElement.nodeName?t:null},C=e.parseJSON||function(e){return window.eval("("+e+")")},_=function(t,r,a){var n=t.getResponseHeader("content-type")||"",i="xml"===r||!r&&n.indexOf("xml")>=0,o=i?t.responseXML:t.responseText;return i&&"parsererror"===o.documentElement.nodeName&&e.error&&e.error("parsererror"),a&&a.dataFilter&&(o=a.dataFilter(o,r)),"string"==typeof o&&("json"===r||!r&&n.indexOf("json")>=0?o=C(o):("script"===r||!r&&n.indexOf("javascript")>=0)&&e.globalEval(o)),o};return S}if(!this.length)return a("ajaxSubmit: skipping submit process - no element selected"),this;var u,c,l,f=this;"function"==typeof t?t={success:t}:void 0===t&&(t={}),u=t.type||this.attr2("method"),c=t.url||this.attr2("action"),l="string"==typeof c?e.trim(c):"",l=l||window.location.href||"",l&&(l=(l.match(/^([^#]+)/)||[])[1]),t=e.extend(!0,{url:l,success:e.ajaxSettings.success,type:u||e.ajaxSettings.type,iframeSrc:/^https/i.test(window.location.href||"")?"javascript:false":"about:blank"},t);var m={};if(this.trigger("form-pre-serialize",[this,t,m]),m.veto)return a("ajaxSubmit: submit vetoed via form-pre-serialize trigger"),this;if(t.beforeSerialize&&t.beforeSerialize(this,t)===!1)return a("ajaxSubmit: submit aborted via beforeSerialize callback"),this;var d=t.traditional;void 0===d&&(d=e.ajaxSettings.traditional);var p,h=[],v=this.formToArray(t.semantic,h);if(t.data&&(t.extraData=t.data,p=e.param(t.data,d)),t.beforeSubmit&&t.beforeSubmit(v,this,t)===!1)return a("ajaxSubmit: submit aborted via beforeSubmit callback"),this;if(this.trigger("form-submit-validate",[v,this,t,m]),m.veto)return a("ajaxSubmit: submit vetoed via form-submit-validate trigger"),this;var g=e.param(v,d);p&&(g=g?g+"&"+p:p),"GET"==t.type.toUpperCase()?(t.url+=(t.url.indexOf("?")>=0?"&":"?")+g,t.data=null):t.data=g;var x=[];if(t.resetForm&&x.push(function(){f.resetForm()}),t.clearForm&&x.push(function(){f.clearForm(t.includeHidden)}),!t.dataType&&t.target){var y=t.success||function(){};x.push(function(r){var a=t.replaceTarget?"replaceWith":"html";e(t.target)[a](r).each(y,arguments)})}else t.success&&x.push(t.success);if(t.success=function(e,r,a){for(var n=t.context||this,i=0,o=x.length;o>i;i++)x[i].apply(n,[e,r,a||f,f])},t.error){var b=t.error;t.error=function(e,r,a){var n=t.context||this;b.apply(n,[e,r,a,f])}}if(t.complete){var T=t.complete;t.complete=function(e,r){var a=t.context||this;T.apply(a,[e,r,f])}}var j=e("input[type=file]:enabled",this).filter(function(){return""!==e(this).val()}),w=j.length>0,S="multipart/form-data",D=f.attr("enctype")==S||f.attr("encoding")==S,k=n.fileapi&&n.formdata;a("fileAPI :"+k);var A,L=(w||D)&&!k;t.iframe!==!1&&(t.iframe||L)?t.closeKeepAlive?e.get(t.closeKeepAlive,function(){A=s(v)}):A=s(v):A=(w||D)&&k?o(v):e.ajax(t),f.removeData("jqxhr").data("jqxhr",A);for(var E=0;E<h.length;E++)h[E]=null;return this.trigger("form-submit-notify",[this,t]),this},e.fn.ajaxForm=function(n){if(n=n||{},n.delegation=n.delegation&&e.isFunction(e.fn.on),!n.delegation&&0===this.length){var i={s:this.selector,c:this.context};return!e.isReady&&i.s?(a("DOM not ready, queuing ajaxForm"),e(function(){e(i.s,i.c).ajaxForm(n)}),this):(a("terminating; zero elements found by selector"+(e.isReady?"":" (DOM not ready)")),this)}return n.delegation?(e(document).off("submit.form-plugin",this.selector,t).off("click.form-plugin",this.selector,r).on("submit.form-plugin",this.selector,n,t).on("click.form-plugin",this.selector,n,r),this):this.ajaxFormUnbind().bind("submit.form-plugin",n,t).bind("click.form-plugin",n,r)},e.fn.ajaxFormUnbind=function(){return this.unbind("submit.form-plugin click.form-plugin")},e.fn.formToArray=function(t,r){var a=[];if(0===this.length)return a;var i,o=this[0],s=this.attr("id"),u=t?o.getElementsByTagName("*"):o.elements;if(u&&!/MSIE [678]/.test(navigator.userAgent)&&(u=e(u).get()),s&&(i=e(':input[form="'+s+'"]').get(),i.length&&(u=(u||[]).concat(i))),!u||!u.length)return a;var c,l,f,m,d,p,h;for(c=0,p=u.length;p>c;c++)if(d=u[c],f=d.name,f&&!d.disabled)if(t&&o.clk&&"image"==d.type)o.clk==d&&(a.push({name:f,value:e(d).val(),type:d.type}),a.push({name:f+".x",value:o.clk_x},{name:f+".y",value:o.clk_y}));else if(m=e.fieldValue(d,!0),m&&m.constructor==Array)for(r&&r.push(d),l=0,h=m.length;h>l;l++)a.push({name:f,value:m[l]});else if(n.fileapi&&"file"==d.type){r&&r.push(d);var v=d.files;if(v.length)for(l=0;l<v.length;l++)a.push({name:f,value:v[l],type:d.type});else a.push({name:f,value:"",type:d.type})}else null!==m&&"undefined"!=typeof m&&(r&&r.push(d),a.push({name:f,value:m,type:d.type,required:d.required}));if(!t&&o.clk){var g=e(o.clk),x=g[0];f=x.name,f&&!x.disabled&&"image"==x.type&&(a.push({name:f,value:g.val()}),a.push({name:f+".x",value:o.clk_x},{name:f+".y",value:o.clk_y}))}return a},e.fn.formSerialize=function(t){return e.param(this.formToArray(t))},e.fn.fieldSerialize=function(t){var r=[];return this.each(function(){var a=this.name;if(a){var n=e.fieldValue(this,t);if(n&&n.constructor==Array)for(var i=0,o=n.length;o>i;i++)r.push({name:a,value:n[i]});else null!==n&&"undefined"!=typeof n&&r.push({name:this.name,value:n})}}),e.param(r)},e.fn.fieldValue=function(t){for(var r=[],a=0,n=this.length;n>a;a++){var i=this[a],o=e.fieldValue(i,t);null===o||"undefined"==typeof o||o.constructor==Array&&!o.length||(o.constructor==Array?e.merge(r,o):r.push(o))}return r},e.fieldValue=function(t,r){var a=t.name,n=t.type,i=t.tagName.toLowerCase();if(void 0===r&&(r=!0),r&&(!a||t.disabled||"reset"==n||"button"==n||("checkbox"==n||"radio"==n)&&!t.checked||("submit"==n||"image"==n)&&t.form&&t.form.clk!=t||"select"==i&&-1==t.selectedIndex))return null;if("select"==i){var o=t.selectedIndex;if(0>o)return null;for(var s=[],u=t.options,c="select-one"==n,l=c?o+1:u.length,f=c?o:0;l>f;f++){var m=u[f];if(m.selected){var d=m.value;if(d||(d=m.attributes&&m.attributes.value&&!m.attributes.value.specified?m.text:m.value),c)return d;s.push(d)}}return s}return e(t).val()},e.fn.clearForm=function(t){return this.each(function(){e("input,select,textarea",this).clearFields(t)})},e.fn.clearFields=e.fn.clearInputs=function(t){var r=/^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;return this.each(function(){var a=this.type,n=this.tagName.toLowerCase();r.test(a)||"textarea"==n?this.value="":"checkbox"==a||"radio"==a?this.checked=!1:"select"==n?this.selectedIndex=-1:"file"==a?/MSIE/.test(navigator.userAgent)?e(this).replaceWith(e(this).clone(!0)):e(this).val(""):t&&(t===!0&&/hidden/.test(a)||"string"==typeof t&&e(this).is(t))&&(this.value="")})},e.fn.resetForm=function(){return this.each(function(){("function"==typeof this.reset||"object"==typeof this.reset&&!this.reset.nodeType)&&this.reset()})},e.fn.enable=function(e){return void 0===e&&(e=!0),this.each(function(){this.disabled=!e})},e.fn.selected=function(t){return void 0===t&&(t=!0),this.each(function(){var r=this.type;if("checkbox"==r||"radio"==r)this.checked=t;else if("option"==this.tagName.toLowerCase()){var a=e(this).parent("select");t&&a[0]&&"select-one"==a[0].type&&a.find("option").selected(!1),this.selected=t}})},e.fn.ajaxSubmit.debug=!1});
/* Sticky-kit v1.1.2 | WTFPL | Leaf Corcoran 2015 | http://leafo.net */
(function(){var c,f;c=this.jQuery||window.jQuery;f=c(window);c.fn.stick_in_parent=function(b){var A,w,B,n,p,J,k,E,t,K,q,L;null==b&&(b={});t=b.sticky_class;B=b.inner_scrolling;E=b.recalc_every;k=b.parent;p=b.offset_top;n=b.spacer;w=b.bottoming;null==p&&(p=0);null==k&&(k=void 0);null==B&&(B=!0);null==t&&(t="is_stuck");A=c(document);null==w&&(w=!0);J=function(a){var b;return window.getComputedStyle?(a=window.getComputedStyle(a[0]),b=parseFloat(a.getPropertyValue("width"))+parseFloat(a.getPropertyValue("margin-left"))+
parseFloat(a.getPropertyValue("margin-right")),"border-box"!==a.getPropertyValue("box-sizing")&&(b+=parseFloat(a.getPropertyValue("border-left-width"))+parseFloat(a.getPropertyValue("border-right-width"))+parseFloat(a.getPropertyValue("padding-left"))+parseFloat(a.getPropertyValue("padding-right"))),b):a.outerWidth(!0)};K=function(a,b,q,C,F,u,r,G){var v,H,m,D,I,d,g,x,y,z,h,l;if(!a.data("sticky_kit")){a.data("sticky_kit",!0);I=A.height();g=a.parent();null!=k&&(g=g.closest(k));if(!g.length)throw"failed to find stick parent";
v=m=!1;(h=null!=n?n&&a.closest(n):c("<div />"))&&h.css("position",a.css("position"));x=function(){var d,f,e;if(!G&&(I=A.height(),d=parseInt(g.css("border-top-width"),10),f=parseInt(g.css("padding-top"),10),b=parseInt(g.css("padding-bottom"),10),q=g.offset().top+d+f,C=g.height(),m&&(v=m=!1,null==n&&(a.insertAfter(h),h.detach()),a.css({position:"",top:"",width:"",bottom:""}).removeClass(t),e=!0),F=a.offset().top-(parseInt(a.css("margin-top"),10)||0)-p,u=a.outerHeight(!0),r=a.css("float"),h&&h.css({width:J(a),
height:u,display:a.css("display"),"vertical-align":a.css("vertical-align"),"float":r}),e))return l()};x();if(u!==C)return D=void 0,d=p,z=E,l=function(){var c,l,e,k;if(!G&&(e=!1,null!=z&&(--z,0>=z&&(z=E,x(),e=!0)),e||A.height()===I||x(),e=f.scrollTop(),null!=D&&(l=e-D),D=e,m?(w&&(k=e+u+d>C+q,v&&!k&&(v=!1,a.css({position:"fixed",bottom:"",top:d}).trigger("sticky_kit:unbottom"))),e<F&&(m=!1,d=p,null==n&&("left"!==r&&"right"!==r||a.insertAfter(h),h.detach()),c={position:"",width:"",top:""},a.css(c).removeClass(t).trigger("sticky_kit:unstick")),
B&&(c=f.height(),u+p>c&&!v&&(d-=l,d=Math.max(c-u,d),d=Math.min(p,d),m&&a.css({top:d+"px"})))):e>F&&(m=!0,c={position:"fixed",top:d},c.width="border-box"===a.css("box-sizing")?a.outerWidth()+"px":a.width()+"px",a.css(c).addClass(t),null==n&&(a.after(h),"left"!==r&&"right"!==r||h.append(a)),a.trigger("sticky_kit:stick")),m&&w&&(null==k&&(k=e+u+d>C+q),!v&&k)))return v=!0,"static"===g.css("position")&&g.css({position:"relative"}),a.css({position:"absolute",bottom:b,top:"auto"}).trigger("sticky_kit:bottom")},
y=function(){x();return l()},H=function(){G=!0;f.off("touchmove",l);f.off("scroll",l);f.off("resize",y);c(document.body).off("sticky_kit:recalc",y);a.off("sticky_kit:detach",H);a.removeData("sticky_kit");a.css({position:"",bottom:"",top:"",width:""});g.position("position","");if(m)return null==n&&("left"!==r&&"right"!==r||a.insertAfter(h),h.remove()),a.removeClass(t)},f.on("touchmove",l),f.on("scroll",l),f.on("resize",y),c(document.body).on("sticky_kit:recalc",y),a.on("sticky_kit:detach",H),setTimeout(l,
0)}};q=0;for(L=this.length;q<L;q++)b=this[q],K(c(b));return this}}).call(this);
/* In-Field Label jQuery Plugin http://fuelyourcoding.com/scripts/infield.html @version 0.1 */
(function($){$.InFieldLabels=function(b,c,d){var f=this;f.$label=$(b);f.label=b;f.$field=$(c);f.field=c;f.$label.data("InFieldLabels",f);f.showing=true;f.init=function(){f.options=$.extend({},$.InFieldLabels.defaultOptions,d);if(f.$field.val()!=""){f.$label.hide();f.showing=false};f.$field.focus(function(){f.fadeOnFocus()}).blur(function(){f.checkForEmpty(true)}).bind('keydown.infieldlabel',function(e){f.hideOnChange(e)}).change(function(e){f.checkForEmpty()}).bind('onPropertyChange',function(){f.checkForEmpty()})};f.fadeOnFocus=function(){if(f.showing){f.setOpacity(f.options.fadeOpacity)}};f.setOpacity=function(a){f.$label.stop().animate({opacity:a},f.options.fadeDuration);f.showing=(a>0.0)};f.checkForEmpty=function(a){if(f.$field.val()==""){f.prepForShow();f.setOpacity(a?1.0:f.options.fadeOpacity)}else{f.setOpacity(0.0)}};f.prepForShow=function(e){if(!f.showing){f.$label.css({opacity:0.0}).show();f.$field.bind('keydown.infieldlabel',function(e){f.hideOnChange(e)})}};f.hideOnChange=function(e){if((e.keyCode==16)||(e.keyCode==9))return;if(f.showing){f.$label.hide();f.showing=false};f.$field.unbind('keydown.infieldlabel')};f.init()};$.InFieldLabels.defaultOptions={fadeOpacity:0.5,fadeDuration:300};$.fn.inFieldLabels=function(c){return this.each(function(){var a=$(this).attr('for');if(!a)return;var b=$("input#"+a+"[type='text'],"+"input#"+a+"[type='password'],"+"input#"+a+"[type='email'],"+"textarea#"+a);if(b.length==0)return;(new $.InFieldLabels(this,b[0],c))})}})(jQuery);;
/*Simple Validation*/
!function(e){e.fn.simpleValidation=function(a,r){var t=e.extend({errorFieldClass:"error",errorMsgTag:"span",errorMsgClass:"errormsg",errorMsg:"Required Field",otherErrorMsg:{email:"Please enter valid email",companyemail:"Please enter company email",alphabet:"Please enter letters only",alphabetwithspace:"Please enter letters & space only",number:"Please enter numbers only",numberwithspace:"Please enter numbers & space only",alphanumeric:"Please don't enter any special character or space",alphanumericwithspace:"Please don't enter any special character",compare:"Please enter the same value again"},beforeSubmit:""},a);return this.each(function(){function a(a,r,s){var o=e(a);s||o.addClass(t.errorFieldClass),o.next("."+t.errorMsgClass).size()<=0&&M.clone().text(r).insertAfter(o)}function s(a){var r=e(a),s=r.next("."+t.errorMsgClass);r.removeClass(t.errorFieldClass),s.size()>0&&s.remove()}function o(r){var o=e(r),i=o.val().trim(),l=(o.attr("name"),o.attr("data-sfv-required")),m=o.attr("data-sfv-regex"),c=o.attr("data-sfv-compare"),d=e(c),M=e("[data-sfv-compare]",n),E=e(M.attr("data-sfv-compare")),x=""!=c&&"undefined"!=typeof c?d.val().trim():"",w=o.attr("data-sfv-regEx-errorMsg"),q=o.attr("data-sfv-require-errorMsg");""!=i?"companyemail"==o.attr("type")||"companyemail"==o.attr("data-sfv-validation")?u.test(i)?p.test(i)?s(o):a(o,w||t.otherErrorMsg.companyemail):a(o,w||t.otherErrorMsg.email):"email"==o.attr("type")||"email"==o.attr("data-sfv-validation")?u.test(i)?s(o):a(o,w||t.otherErrorMsg.email):"alpha"==o.attr("data-sfv-validation")?f.test(i)?s(o):a(o,w||t.otherErrorMsg.alphabet):"alphawithspace"==o.attr("data-sfv-validation")?g.test(i)?s(o):a(o,w||t.otherErrorMsg.alphabetwithspace):"number"==o.attr("data-sfv-validation")?v.test(i)?s(o):a(o,w||t.otherErrorMsg.number):"numberwithspace"==o.attr("data-sfv-validation")?y.test(i)?s(o):a(o,w||t.otherErrorMsg.numberwithspace):"alphanumeric"==o.attr("data-sfv-validation")?h.test(i)?s(o):a(o,w||t.otherErrorMsg.alphanumeric):"alphanumericwithspace"==o.attr("data-sfv-validation")?b.test(i)?s(o):a(o,w||t.otherErrorMsg.alphanumericwithspace):""!=m&&"undefined"!=typeof m?(m=new RegExp("^"+m+"$"),m.test(i)?s(o):a(o,w||t.errorMsg)):""!=c&&"undefined"!=typeof c&&d.size()>0&&""!=x?i!=x?a(d,w||t.otherErrorMsg.compare):s(d):E.attr("id")==o.attr("id")&&i!=M.val()?a(o,w||t.otherErrorMsg.compare):s(o):"yes"==l&&a(o,q||t.errorMsg)}function i(r){var o=e(r),i=o.attr("name"),n=o.attr("data-sfv-require-errorMsg");disSibElem=e("[name='"+i+"']"),disSibLElem=disSibElem.last()[0].nextSibling,disSibElem.is(":checked")?s(disSibLElem):a(disSibLElem,n||t.errorMsg,!0)}var n=e(this),l=n.attr("data-sfv-ajax")||!1,m=e("input[data-sfv-required='yes'][type='checkbox'],input[data-sfv-required='yes'][type='radio']",n),c=e("input[data-sfv-required='yes'],input[data-sfv-validation]:not(input[data-sfv-required='yes']),input[data-sfv-regex]:not(input[data-sfv-required='yes']),input[data-sfv-compare]:not(input[data-sfv-required='yes']),select[data-sfv-required='yes'],textarea[data-sfv-required='yes']",n).not(m),d=e("input[data-sfv-compare]",n),u=/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,p=/^([\w+\-\.]+@(?!gmail.com)(?!hotmail.com)(?!live.com)(?!outlook.com)(?!yahoo.com)(?!ymail.com)(?!rocketmail.com)(?!aol.com)(?!mac.comme.com)(?!icloud.com)(?!inbox.com)(?!sina.com)(?!qq.com)(?!foxmail.com)(?!163.com)(?!126.com)(?!189.cn 263.net)(?!yeah.net)(?!gmx.com)(?!gmx.net)(?!mail.com)(?!mail.ru)(?!rambler.ru)(?!lenta.ru)(?!autorambler.ru)(?!myrambler.ru)(?!ro.ru)(?!yandex.ru)(?!zoho.com)(?!msn.com)(?!webtown.com)(?!rediffmail.com)([\w\-]+\.)+[\w\-]{2,4})?$/,f=/^[A-Za-z]+$/,v=/^[0-9]+$/,h=/^[0-9a-zA-Z]+$/,g=/^[A-Za-z ]+$/,y=/^[0-9 ]+$/,b=/^[0-9a-zA-Z ]+$/,M=e("<"+t.errorMsgTag+"/>",{"class":t.errorMsgClass});d.size()>0&&(c=c.add(e(d.attr("data-sfv-compare")))),n.attr("novalidate",""),n.on("submit",function(a){var s=e(this);return c.add(m).each(function(){var a=e(this).attr("type");"checkbox"==a||"radio"==a?i(this):o(this)}),e("."+t.errorFieldClass+":visible,."+t.errorMsgClass+":visible",e(this)).size()<=0?l?("function"==typeof t.beforeSubmit&&t.beforeSubmit.call(this,s),e.ajax({type:n.attr("method"),url:n.attr("action"),data:s.serialize(),success:function(e){"function"==typeof r&&r.call(this,e,s)}}),!1):!0:void a.preventDefault()}),c.on("focus",function(e){s(e.target)}),c.on("blur",function(e){o(e.target)}),m.on("click",function(e){i(e.target)})})}}(jQuery);;
/*! fancyBox v2.1.5 fancyapps.com | fancyapps.com/fancybox/#license */
(function(r,G,f,v){var J=f("html"),n=f(r),p=f(G),b=f.fancybox=function(){b.open.apply(this,arguments)},I=navigator.userAgent.match(/msie/i),B=null,s=G.createTouch!==v,t=function(a){return a&&a.hasOwnProperty&&a instanceof f},q=function(a){return a&&"string"===f.type(a)},E=function(a){return q(a)&&0<a.indexOf("%")},l=function(a,d){var e=parseInt(a,10)||0;d&&E(a)&&(e*=b.getViewport()[d]/100);return Math.ceil(e)},w=function(a,b){return l(a,b)+"px"};f.extend(b,{version:"2.1.5",defaults:{padding:15,margin:20,
width:800,height:600,minWidth:100,minHeight:100,maxWidth:9999,maxHeight:9999,pixelRatio:1,autoSize:!0,autoHeight:!1,autoWidth:!1,autoResize:!0,autoCenter:!s,fitToView:!0,aspectRatio:!1,topRatio:0.5,leftRatio:0.5,scrolling:"auto",wrapCSS:"",arrows:!0,closeBtn:!0,closeClick:!1,nextClick:!1,mouseWheel:!0,autoPlay:!1,playSpeed:3E3,preload:3,modal:!1,loop:!0,ajax:{dataType:"html",headers:{"X-fancyBox":!0}},iframe:{scrolling:"auto",preload:!0},swf:{wmode:"transparent",allowfullscreen:"true",allowscriptaccess:"always"},
keys:{next:{13:"left",34:"up",39:"left",40:"up"},prev:{8:"right",33:"down",37:"right",38:"down"},close:[27],play:[32],toggle:[70]},direction:{next:"left",prev:"right"},scrollOutside:!0,index:0,type:null,href:null,content:null,title:null,tpl:{wrap:'<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',image:'<img class="fancybox-image" src="{href}" alt="" />',iframe:'<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen'+
(I?' allowtransparency="true"':"")+"></iframe>",error:'<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',closeBtn:'<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a>',next:'<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',prev:'<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'},openEffect:"fade",openSpeed:250,openEasing:"swing",openOpacity:!0,
openMethod:"zoomIn",closeEffect:"fade",closeSpeed:250,closeEasing:"swing",closeOpacity:!0,closeMethod:"zoomOut",nextEffect:"elastic",nextSpeed:250,nextEasing:"swing",nextMethod:"changeIn",prevEffect:"elastic",prevSpeed:250,prevEasing:"swing",prevMethod:"changeOut",helpers:{overlay:!0,title:!0},onCancel:f.noop,beforeLoad:f.noop,afterLoad:f.noop,beforeShow:f.noop,afterShow:f.noop,beforeChange:f.noop,beforeClose:f.noop,afterClose:f.noop},group:{},opts:{},previous:null,coming:null,current:null,isActive:!1,
isOpen:!1,isOpened:!1,wrap:null,skin:null,outer:null,inner:null,player:{timer:null,isActive:!1},ajaxLoad:null,imgPreload:null,transitions:{},helpers:{},open:function(a,d){if(a&&(f.isPlainObject(d)||(d={}),!1!==b.close(!0)))return f.isArray(a)||(a=t(a)?f(a).get():[a]),f.each(a,function(e,c){var k={},g,h,j,m,l;"object"===f.type(c)&&(c.nodeType&&(c=f(c)),t(c)?(k={href:c.data("fancybox-href")||c.attr("href"),title:c.data("fancybox-title")||c.attr("title"),isDom:!0,element:c},f.metadata&&f.extend(!0,k,
c.metadata())):k=c);g=d.href||k.href||(q(c)?c:null);h=d.title!==v?d.title:k.title||"";m=(j=d.content||k.content)?"html":d.type||k.type;!m&&k.isDom&&(m=c.data("fancybox-type"),m||(m=(m=c.prop("class").match(/fancybox\.(\w+)/))?m[1]:null));q(g)&&(m||(b.isImage(g)?m="image":b.isSWF(g)?m="swf":"#"===g.charAt(0)?m="inline":q(c)&&(m="html",j=c)),"ajax"===m&&(l=g.split(/\s+/,2),g=l.shift(),l=l.shift()));j||("inline"===m?g?j=f(q(g)?g.replace(/.*(?=#[^\s]+$)/,""):g):k.isDom&&(j=c):"html"===m?j=g:!m&&(!g&&
k.isDom)&&(m="inline",j=c));f.extend(k,{href:g,type:m,content:j,title:h,selector:l});a[e]=k}),b.opts=f.extend(!0,{},b.defaults,d),d.keys!==v&&(b.opts.keys=d.keys?f.extend({},b.defaults.keys,d.keys):!1),b.group=a,b._start(b.opts.index)},cancel:function(){var a=b.coming;a&&!1!==b.trigger("onCancel")&&(b.hideLoading(),b.ajaxLoad&&b.ajaxLoad.abort(),b.ajaxLoad=null,b.imgPreload&&(b.imgPreload.onload=b.imgPreload.onerror=null),a.wrap&&a.wrap.stop(!0,!0).trigger("onReset").remove(),b.coming=null,b.current||
b._afterZoomOut(a))},close:function(a){b.cancel();!1!==b.trigger("beforeClose")&&(b.unbindEvents(),b.isActive&&(!b.isOpen||!0===a?(f(".fancybox-wrap").stop(!0).trigger("onReset").remove(),b._afterZoomOut()):(b.isOpen=b.isOpened=!1,b.isClosing=!0,f(".fancybox-item, .fancybox-nav").remove(),b.wrap.stop(!0,!0).removeClass("fancybox-opened"),b.transitions[b.current.closeMethod]())))},play:function(a){var d=function(){clearTimeout(b.player.timer)},e=function(){d();b.current&&b.player.isActive&&(b.player.timer=
setTimeout(b.next,b.current.playSpeed))},c=function(){d();p.unbind(".player");b.player.isActive=!1;b.trigger("onPlayEnd")};if(!0===a||!b.player.isActive&&!1!==a){if(b.current&&(b.current.loop||b.current.index<b.group.length-1))b.player.isActive=!0,p.bind({"onCancel.player beforeClose.player":c,"onUpdate.player":e,"beforeLoad.player":d}),e(),b.trigger("onPlayStart")}else c()},next:function(a){var d=b.current;d&&(q(a)||(a=d.direction.next),b.jumpto(d.index+1,a,"next"))},prev:function(a){var d=b.current;
d&&(q(a)||(a=d.direction.prev),b.jumpto(d.index-1,a,"prev"))},jumpto:function(a,d,e){var c=b.current;c&&(a=l(a),b.direction=d||c.direction[a>=c.index?"next":"prev"],b.router=e||"jumpto",c.loop&&(0>a&&(a=c.group.length+a%c.group.length),a%=c.group.length),c.group[a]!==v&&(b.cancel(),b._start(a)))},reposition:function(a,d){var e=b.current,c=e?e.wrap:null,k;c&&(k=b._getPosition(d),a&&"scroll"===a.type?(delete k.position,c.stop(!0,!0).animate(k,200)):(c.css(k),e.pos=f.extend({},e.dim,k)))},update:function(a){var d=
a&&a.type,e=!d||"orientationchange"===d;e&&(clearTimeout(B),B=null);b.isOpen&&!B&&(B=setTimeout(function(){var c=b.current;c&&!b.isClosing&&(b.wrap.removeClass("fancybox-tmp"),(e||"load"===d||"resize"===d&&c.autoResize)&&b._setDimension(),"scroll"===d&&c.canShrink||b.reposition(a),b.trigger("onUpdate"),B=null)},e&&!s?0:300))},toggle:function(a){b.isOpen&&(b.current.fitToView="boolean"===f.type(a)?a:!b.current.fitToView,s&&(b.wrap.removeAttr("style").addClass("fancybox-tmp"),b.trigger("onUpdate")),
b.update())},hideLoading:function(){p.unbind(".loading");f("#fancybox-loading").remove()},showLoading:function(){var a,d;b.hideLoading();a=f('<div id="fancybox-loading"><div></div></div>').click(b.cancel).appendTo("body");p.bind("keydown.loading",function(a){if(27===(a.which||a.keyCode))a.preventDefault(),b.cancel()});b.defaults.fixed||(d=b.getViewport(),a.css({position:"absolute",top:0.5*d.h+d.y,left:0.5*d.w+d.x}))},getViewport:function(){var a=b.current&&b.current.locked||!1,d={x:n.scrollLeft(),
y:n.scrollTop()};a?(d.w=a[0].clientWidth,d.h=a[0].clientHeight):(d.w=s&&r.innerWidth?r.innerWidth:n.width(),d.h=s&&r.innerHeight?r.innerHeight:n.height());return d},unbindEvents:function(){b.wrap&&t(b.wrap)&&b.wrap.unbind(".fb");p.unbind(".fb");n.unbind(".fb")},bindEvents:function(){var a=b.current,d;a&&(n.bind("orientationchange.fb"+(s?"":" resize.fb")+(a.autoCenter&&!a.locked?" scroll.fb":""),b.update),(d=a.keys)&&p.bind("keydown.fb",function(e){var c=e.which||e.keyCode,k=e.target||e.srcElement;
if(27===c&&b.coming)return!1;!e.ctrlKey&&(!e.altKey&&!e.shiftKey&&!e.metaKey&&(!k||!k.type&&!f(k).is("[contenteditable]")))&&f.each(d,function(d,k){if(1<a.group.length&&k[c]!==v)return b[d](k[c]),e.preventDefault(),!1;if(-1<f.inArray(c,k))return b[d](),e.preventDefault(),!1})}),f.fn.mousewheel&&a.mouseWheel&&b.wrap.bind("mousewheel.fb",function(d,c,k,g){for(var h=f(d.target||null),j=!1;h.length&&!j&&!h.is(".fancybox-skin")&&!h.is(".fancybox-wrap");)j=h[0]&&!(h[0].style.overflow&&"hidden"===h[0].style.overflow)&&
(h[0].clientWidth&&h[0].scrollWidth>h[0].clientWidth||h[0].clientHeight&&h[0].scrollHeight>h[0].clientHeight),h=f(h).parent();if(0!==c&&!j&&1<b.group.length&&!a.canShrink){if(0<g||0<k)b.prev(0<g?"down":"left");else if(0>g||0>k)b.next(0>g?"up":"right");d.preventDefault()}}))},trigger:function(a,d){var e,c=d||b.coming||b.current;if(c){f.isFunction(c[a])&&(e=c[a].apply(c,Array.prototype.slice.call(arguments,1)));if(!1===e)return!1;c.helpers&&f.each(c.helpers,function(d,e){if(e&&b.helpers[d]&&f.isFunction(b.helpers[d][a]))b.helpers[d][a](f.extend(!0,
{},b.helpers[d].defaults,e),c)});p.trigger(a)}},isImage:function(a){return q(a)&&a.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i)},isSWF:function(a){return q(a)&&a.match(/\.(swf)((\?|#).*)?$/i)},_start:function(a){var d={},e,c;a=l(a);e=b.group[a]||null;if(!e)return!1;d=f.extend(!0,{},b.opts,e);e=d.margin;c=d.padding;"number"===f.type(e)&&(d.margin=[e,e,e,e]);"number"===f.type(c)&&(d.padding=[c,c,c,c]);d.modal&&f.extend(!0,d,{closeBtn:!1,closeClick:!1,nextClick:!1,arrows:!1,
mouseWheel:!1,keys:null,helpers:{overlay:{closeClick:!1}}});d.autoSize&&(d.autoWidth=d.autoHeight=!0);"auto"===d.width&&(d.autoWidth=!0);"auto"===d.height&&(d.autoHeight=!0);d.group=b.group;d.index=a;b.coming=d;if(!1===b.trigger("beforeLoad"))b.coming=null;else{c=d.type;e=d.href;if(!c)return b.coming=null,b.current&&b.router&&"jumpto"!==b.router?(b.current.index=a,b[b.router](b.direction)):!1;b.isActive=!0;if("image"===c||"swf"===c)d.autoHeight=d.autoWidth=!1,d.scrolling="visible";"image"===c&&(d.aspectRatio=
!0);"iframe"===c&&s&&(d.scrolling="scroll");d.wrap=f(d.tpl.wrap).addClass("fancybox-"+(s?"mobile":"desktop")+" fancybox-type-"+c+" fancybox-tmp "+d.wrapCSS).appendTo(d.parent||"body");f.extend(d,{skin:f(".fancybox-skin",d.wrap),outer:f(".fancybox-outer",d.wrap),inner:f(".fancybox-inner",d.wrap)});f.each(["Top","Right","Bottom","Left"],function(a,b){d.skin.css("padding"+b,w(d.padding[a]))});b.trigger("onReady");if("inline"===c||"html"===c){if(!d.content||!d.content.length)return b._error("content")}else if(!e)return b._error("href");
"image"===c?b._loadImage():"ajax"===c?b._loadAjax():"iframe"===c?b._loadIframe():b._afterLoad()}},_error:function(a){f.extend(b.coming,{type:"html",autoWidth:!0,autoHeight:!0,minWidth:0,minHeight:0,scrolling:"no",hasError:a,content:b.coming.tpl.error});b._afterLoad()},_loadImage:function(){var a=b.imgPreload=new Image;a.onload=function(){this.onload=this.onerror=null;b.coming.width=this.width/b.opts.pixelRatio;b.coming.height=this.height/b.opts.pixelRatio;b._afterLoad()};a.onerror=function(){this.onload=
this.onerror=null;b._error("image")};a.src=b.coming.href;!0!==a.complete&&b.showLoading()},_loadAjax:function(){var a=b.coming;b.showLoading();b.ajaxLoad=f.ajax(f.extend({},a.ajax,{url:a.href,error:function(a,e){b.coming&&"abort"!==e?b._error("ajax",a):b.hideLoading()},success:function(d,e){"success"===e&&(a.content=d,b._afterLoad())}}))},_loadIframe:function(){var a=b.coming,d=f(a.tpl.iframe.replace(/\{rnd\}/g,(new Date).getTime())).attr("scrolling",s?"auto":a.iframe.scrolling).attr("src",a.href);
f(a.wrap).bind("onReset",function(){try{f(this).find("iframe").hide().attr("src","//about:blank").end().empty()}catch(a){}});a.iframe.preload&&(b.showLoading(),d.one("load",function(){f(this).data("ready",1);s||f(this).bind("load.fb",b.update);f(this).parents(".fancybox-wrap").width("100%").removeClass("fancybox-tmp").show();b._afterLoad()}));a.content=d.appendTo(a.inner);a.iframe.preload||b._afterLoad()},_preloadImages:function(){var a=b.group,d=b.current,e=a.length,c=d.preload?Math.min(d.preload,
e-1):0,f,g;for(g=1;g<=c;g+=1)f=a[(d.index+g)%e],"image"===f.type&&f.href&&((new Image).src=f.href)},_afterLoad:function(){var a=b.coming,d=b.current,e,c,k,g,h;b.hideLoading();if(a&&!1!==b.isActive)if(!1===b.trigger("afterLoad",a,d))a.wrap.stop(!0).trigger("onReset").remove(),b.coming=null;else{d&&(b.trigger("beforeChange",d),d.wrap.stop(!0).removeClass("fancybox-opened").find(".fancybox-item, .fancybox-nav").remove());b.unbindEvents();e=a.content;c=a.type;k=a.scrolling;f.extend(b,{wrap:a.wrap,skin:a.skin,
outer:a.outer,inner:a.inner,current:a,previous:d});g=a.href;switch(c){case "inline":case "ajax":case "html":a.selector?e=f("<div>").html(e).find(a.selector):t(e)&&(e.data("fancybox-placeholder")||e.data("fancybox-placeholder",f('<div class="fancybox-placeholder"></div>').insertAfter(e).hide()),e=e.show().detach(),a.wrap.bind("onReset",function(){f(this).find(e).length&&e.hide().replaceAll(e.data("fancybox-placeholder")).data("fancybox-placeholder",!1)}));break;case "image":e=a.tpl.image.replace("{href}",
g);break;case "swf":e='<object id="fancybox-swf" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="movie" value="'+g+'"></param>',h="",f.each(a.swf,function(a,b){e+='<param name="'+a+'" value="'+b+'"></param>';h+=" "+a+'="'+b+'"'}),e+='<embed src="'+g+'" type="application/x-shockwave-flash" width="100%" height="100%"'+h+"></embed></object>"}(!t(e)||!e.parent().is(a.inner))&&a.inner.append(e);b.trigger("beforeShow");a.inner.css("overflow","yes"===k?"scroll":
"no"===k?"hidden":k);b._setDimension();b.reposition();b.isOpen=!1;b.coming=null;b.bindEvents();if(b.isOpened){if(d.prevMethod)b.transitions[d.prevMethod]()}else f(".fancybox-wrap").not(a.wrap).stop(!0).trigger("onReset").remove();b.transitions[b.isOpened?a.nextMethod:a.openMethod]();b._preloadImages()}},_setDimension:function(){var a=b.getViewport(),d=0,e=!1,c=!1,e=b.wrap,k=b.skin,g=b.inner,h=b.current,c=h.width,j=h.height,m=h.minWidth,u=h.minHeight,n=h.maxWidth,p=h.maxHeight,s=h.scrolling,q=h.scrollOutside?
h.scrollbarWidth:0,x=h.margin,y=l(x[1]+x[3]),r=l(x[0]+x[2]),v,z,t,C,A,F,B,D,H;e.add(k).add(g).width("auto").height("auto").removeClass("fancybox-tmp");x=l(k.outerWidth(!0)-k.width());v=l(k.outerHeight(!0)-k.height());z=y+x;t=r+v;C=E(c)?(a.w-z)*l(c)/100:c;A=E(j)?(a.h-t)*l(j)/100:j;if("iframe"===h.type){if(H=h.content,h.autoHeight&&1===H.data("ready"))try{H[0].contentWindow.document.location&&(g.width(C).height(9999),F=H.contents().find("body"),q&&F.css("overflow-x","hidden"),A=F.outerHeight(!0))}catch(G){}}else if(h.autoWidth||
h.autoHeight)g.addClass("fancybox-tmp"),h.autoWidth||g.width(C),h.autoHeight||g.height(A),h.autoWidth&&(C=g.width()),h.autoHeight&&(A=g.height()),g.removeClass("fancybox-tmp");c=l(C);j=l(A);D=C/A;m=l(E(m)?l(m,"w")-z:m);n=l(E(n)?l(n,"w")-z:n);u=l(E(u)?l(u,"h")-t:u);p=l(E(p)?l(p,"h")-t:p);F=n;B=p;h.fitToView&&(n=Math.min(a.w-z,n),p=Math.min(a.h-t,p));z=a.w-y;r=a.h-r;h.aspectRatio?(c>n&&(c=n,j=l(c/D)),j>p&&(j=p,c=l(j*D)),c<m&&(c=m,j=l(c/D)),j<u&&(j=u,c=l(j*D))):(c=Math.max(m,Math.min(c,n)),h.autoHeight&&
"iframe"!==h.type&&(g.width(c),j=g.height()),j=Math.max(u,Math.min(j,p)));if(h.fitToView)if(g.width(c).height(j),e.width(c+x),a=e.width(),y=e.height(),h.aspectRatio)for(;(a>z||y>r)&&(c>m&&j>u)&&!(19<d++);)j=Math.max(u,Math.min(p,j-10)),c=l(j*D),c<m&&(c=m,j=l(c/D)),c>n&&(c=n,j=l(c/D)),g.width(c).height(j),e.width(c+x),a=e.width(),y=e.height();else c=Math.max(m,Math.min(c,c-(a-z))),j=Math.max(u,Math.min(j,j-(y-r)));q&&("auto"===s&&j<A&&c+x+q<z)&&(c+=q);g.width(c).height(j);e.width(c+x);a=e.width();
y=e.height();e=(a>z||y>r)&&c>m&&j>u;c=h.aspectRatio?c<F&&j<B&&c<C&&j<A:(c<F||j<B)&&(c<C||j<A);f.extend(h,{dim:{width:w(a),height:w(y)},origWidth:C,origHeight:A,canShrink:e,canExpand:c,wPadding:x,hPadding:v,wrapSpace:y-k.outerHeight(!0),skinSpace:k.height()-j});!H&&(h.autoHeight&&j>u&&j<p&&!c)&&g.height("auto")},_getPosition:function(a){var d=b.current,e=b.getViewport(),c=d.margin,f=b.wrap.width()+c[1]+c[3],g=b.wrap.height()+c[0]+c[2],c={position:"absolute",top:c[0],left:c[3]};d.autoCenter&&d.fixed&&
!a&&g<=e.h&&f<=e.w?c.position="fixed":d.locked||(c.top+=e.y,c.left+=e.x);c.top=w(Math.max(c.top,c.top+(e.h-g)*d.topRatio));c.left=w(Math.max(c.left,c.left+(e.w-f)*d.leftRatio));return c},_afterZoomIn:function(){var a=b.current;a&&(b.isOpen=b.isOpened=!0,b.wrap.css("overflow","visible").addClass("fancybox-opened"),b.update(),(a.closeClick||a.nextClick&&1<b.group.length)&&b.inner.css("cursor","pointer").bind("click.fb",function(d){!f(d.target).is("a")&&!f(d.target).parent().is("a")&&(d.preventDefault(),
b[a.closeClick?"close":"next"]())}),a.closeBtn&&f(a.tpl.closeBtn).appendTo(b.skin).bind("click.fb",function(a){a.preventDefault();b.close()}),a.arrows&&1<b.group.length&&((a.loop||0<a.index)&&f(a.tpl.prev).appendTo(b.outer).bind("click.fb",b.prev),(a.loop||a.index<b.group.length-1)&&f(a.tpl.next).appendTo(b.outer).bind("click.fb",b.next)),b.trigger("afterShow"),!a.loop&&a.index===a.group.length-1?b.play(!1):b.opts.autoPlay&&!b.player.isActive&&(b.opts.autoPlay=!1,b.play()))},_afterZoomOut:function(a){a=
a||b.current;f(".fancybox-wrap").trigger("onReset").remove();f.extend(b,{group:{},opts:{},router:!1,current:null,isActive:!1,isOpened:!1,isOpen:!1,isClosing:!1,wrap:null,skin:null,outer:null,inner:null});b.trigger("afterClose",a)}});b.transitions={getOrigPosition:function(){var a=b.current,d=a.element,e=a.orig,c={},f=50,g=50,h=a.hPadding,j=a.wPadding,m=b.getViewport();!e&&(a.isDom&&d.is(":visible"))&&(e=d.find("img:first"),e.length||(e=d));t(e)?(c=e.offset(),e.is("img")&&(f=e.outerWidth(),g=e.outerHeight())):
(c.top=m.y+(m.h-g)*a.topRatio,c.left=m.x+(m.w-f)*a.leftRatio);if("fixed"===b.wrap.css("position")||a.locked)c.top-=m.y,c.left-=m.x;return c={top:w(c.top-h*a.topRatio),left:w(c.left-j*a.leftRatio),width:w(f+j),height:w(g+h)}},step:function(a,d){var e,c,f=d.prop;c=b.current;var g=c.wrapSpace,h=c.skinSpace;if("width"===f||"height"===f)e=d.end===d.start?1:(a-d.start)/(d.end-d.start),b.isClosing&&(e=1-e),c="width"===f?c.wPadding:c.hPadding,c=a-c,b.skin[f](l("width"===f?c:c-g*e)),b.inner[f](l("width"===
f?c:c-g*e-h*e))},zoomIn:function(){var a=b.current,d=a.pos,e=a.openEffect,c="elastic"===e,k=f.extend({opacity:1},d);delete k.position;c?(d=this.getOrigPosition(),a.openOpacity&&(d.opacity=0.1)):"fade"===e&&(d.opacity=0.1);b.wrap.css(d).animate(k,{duration:"none"===e?0:a.openSpeed,easing:a.openEasing,step:c?this.step:null,complete:b._afterZoomIn})},zoomOut:function(){var a=b.current,d=a.closeEffect,e="elastic"===d,c={opacity:0.1};e&&(c=this.getOrigPosition(),a.closeOpacity&&(c.opacity=0.1));b.wrap.animate(c,
{duration:"none"===d?0:a.closeSpeed,easing:a.closeEasing,step:e?this.step:null,complete:b._afterZoomOut})},changeIn:function(){var a=b.current,d=a.nextEffect,e=a.pos,c={opacity:1},f=b.direction,g;e.opacity=0.1;"elastic"===d&&(g="down"===f||"up"===f?"top":"left","down"===f||"right"===f?(e[g]=w(l(e[g])-200),c[g]="+=200px"):(e[g]=w(l(e[g])+200),c[g]="-=200px"));"none"===d?b._afterZoomIn():b.wrap.css(e).animate(c,{duration:a.nextSpeed,easing:a.nextEasing,complete:b._afterZoomIn})},changeOut:function(){var a=
b.previous,d=a.prevEffect,e={opacity:0.1},c=b.direction;"elastic"===d&&(e["down"===c||"up"===c?"top":"left"]=("up"===c||"left"===c?"-":"+")+"=200px");a.wrap.animate(e,{duration:"none"===d?0:a.prevSpeed,easing:a.prevEasing,complete:function(){f(this).trigger("onReset").remove()}})}};b.helpers.overlay={defaults:{closeClick:!0,speedOut:200,showEarly:!0,css:{},locked:!s,fixed:!0},overlay:null,fixed:!1,el:f("html"),create:function(a){a=f.extend({},this.defaults,a);this.overlay&&this.close();this.overlay=
f('<div class="fancybox-overlay"></div>').appendTo(b.coming?b.coming.parent:a.parent);this.fixed=!1;a.fixed&&b.defaults.fixed&&(this.overlay.addClass("fancybox-overlay-fixed"),this.fixed=!0)},open:function(a){var d=this;a=f.extend({},this.defaults,a);this.overlay?this.overlay.unbind(".overlay").width("auto").height("auto"):this.create(a);this.fixed||(n.bind("resize.overlay",f.proxy(this.update,this)),this.update());a.closeClick&&this.overlay.bind("click.overlay",function(a){if(f(a.target).hasClass("fancybox-overlay"))return b.isActive?
b.close():d.close(),!1});this.overlay.css(a.css).show()},close:function(){var a,b;n.unbind("resize.overlay");this.el.hasClass("fancybox-lock")&&(f(".fancybox-margin").removeClass("fancybox-margin"),a=n.scrollTop(),b=n.scrollLeft(),this.el.removeClass("fancybox-lock"),n.scrollTop(a).scrollLeft(b));f(".fancybox-overlay").remove().hide();f.extend(this,{overlay:null,fixed:!1})},update:function(){var a="100%",b;this.overlay.width(a).height("100%");I?(b=Math.max(G.documentElement.offsetWidth,G.body.offsetWidth),
p.width()>b&&(a=p.width())):p.width()>n.width()&&(a=p.width());this.overlay.width(a).height(p.height())},onReady:function(a,b){var e=this.overlay;f(".fancybox-overlay").stop(!0,!0);e||this.create(a);a.locked&&(this.fixed&&b.fixed)&&(e||(this.margin=p.height()>n.height()?f("html").css("margin-right").replace("px",""):!1),b.locked=this.overlay.append(b.wrap),b.fixed=!1);!0===a.showEarly&&this.beforeShow.apply(this,arguments)},beforeShow:function(a,b){var e,c;b.locked&&(!1!==this.margin&&(f("*").filter(function(){return"fixed"===
f(this).css("position")&&!f(this).hasClass("fancybox-overlay")&&!f(this).hasClass("fancybox-wrap")}).addClass("fancybox-margin"),this.el.addClass("fancybox-margin")),e=n.scrollTop(),c=n.scrollLeft(),this.el.addClass("fancybox-lock"),n.scrollTop(e).scrollLeft(c));this.open(a)},onUpdate:function(){this.fixed||this.update()},afterClose:function(a){this.overlay&&!b.coming&&this.overlay.fadeOut(a.speedOut,f.proxy(this.close,this))}};b.helpers.title={defaults:{type:"float",position:"bottom"},beforeShow:function(a){var d=
b.current,e=d.title,c=a.type;f.isFunction(e)&&(e=e.call(d.element,d));if(q(e)&&""!==f.trim(e)){d=f('<div class="fancybox-title fancybox-title-'+c+'-wrap">'+e+"</div>");switch(c){case "inside":c=b.skin;break;case "outside":c=b.wrap;break;case "over":c=b.inner;break;default:c=b.skin,d.appendTo("body"),I&&d.width(d.width()),d.wrapInner('<span class="child"></span>'),b.current.margin[2]+=Math.abs(l(d.css("margin-bottom")))}d["top"===a.position?"prependTo":"appendTo"](c)}}};f.fn.fancybox=function(a){var d,
e=f(this),c=this.selector||"",k=function(g){var h=f(this).blur(),j=d,k,l;!g.ctrlKey&&(!g.altKey&&!g.shiftKey&&!g.metaKey)&&!h.is(".fancybox-wrap")&&(k=a.groupAttr||"data-fancybox-group",l=h.attr(k),l||(k="rel",l=h.get(0)[k]),l&&(""!==l&&"nofollow"!==l)&&(h=c.length?f(c):e,h=h.filter("["+k+'="'+l+'"]'),j=h.index(this)),a.index=j,!1!==b.open(h,a)&&g.preventDefault())};a=a||{};d=a.index||0;!c||!1===a.live?e.unbind("click.fb-start").bind("click.fb-start",k):p.undelegate(c,"click.fb-start").delegate(c+
":not('.fancybox-item, .fancybox-nav')","click.fb-start",k);this.filter("[data-fancybox-start=1]").trigger("click");return this};p.ready(function(){var a,d;f.scrollbarWidth===v&&(f.scrollbarWidth=function(){var a=f('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo("body"),b=a.children(),b=b.innerWidth()-b.height(99).innerWidth();a.remove();return b});if(f.support.fixedPosition===v){a=f.support;d=f('<div style="position:fixed;top:20px;"></div>').appendTo("body");var e=20===
d[0].offsetTop||15===d[0].offsetTop;d.remove();a.fixedPosition=e}f.extend(b.defaults,{scrollbarWidth:f.scrollbarWidth(),fixed:f.support.fixedPosition,parent:f("body")});a=f(r).width();J.addClass("fancybox-lock-test");d=f(r).width();J.removeClass("fancybox-lock-test");f("<style type='text/css'>.fancybox-margin{margin-right:"+(d-a)+"px;}</style>").appendTo("head")})})(window,document,jQuery);;
 /*!
 * Buttons helper for fancyBox
 * version: 1.0.5 (Mon, 15 Oct 2012)
 * @requires fancyBox v2.0 or later
 *
 * Usage:
 *     $(".fancybox").fancybox({
 *         helpers : {
 *             buttons: {
 *                 position : 'top'
 *             }
 *         }
 *     });
 *
 */
(function ($) {
	//Shortcut for fancyBox object
	var F = $.fancybox;

	//Add helper object
	F.helpers.buttons = {
		defaults : {
			skipSingle : false, // disables if gallery contains single image
			position   : 'top', // 'top' or 'bottom'
			tpl        : '<div id="fancybox-buttons"><ul><li><a class="btnPrev" title="Previous" href="javascript:;"></a></li><li><a class="btnPlay" title="Start slideshow" href="javascript:;"></a></li><li><a class="btnNext" title="Next" href="javascript:;"></a></li><li><a class="btnToggle" title="Toggle size" href="javascript:;"></a></li><li><a class="btnClose" title="Close" href="javascript:;"></a></li></ul></div>'
		},

		list : null,
		buttons: null,

		beforeLoad: function (opts, obj) {
			//Remove self if gallery do not have at least two items

			if (opts.skipSingle && obj.group.length < 2) {
				obj.helpers.buttons = false;
				obj.closeBtn = true;

				return;
			}

			//Increase top margin to give space for buttons
			obj.margin[ opts.position === 'bottom' ? 2 : 0 ] += 30;
		},

		onPlayStart: function () {
			if (this.buttons) {
				this.buttons.play.attr('title', 'Pause slideshow').addClass('btnPlayOn');
			}
		},

		onPlayEnd: function () {
			if (this.buttons) {
				this.buttons.play.attr('title', 'Start slideshow').removeClass('btnPlayOn');
			}
		},

		afterShow: function (opts, obj) {
			var buttons = this.buttons;

			if (!buttons) {
				this.list = $(opts.tpl).addClass(opts.position).appendTo('body');

				buttons = {
					prev   : this.list.find('.btnPrev').click( F.prev ),
					next   : this.list.find('.btnNext').click( F.next ),
					play   : this.list.find('.btnPlay').click( F.play ),
					toggle : this.list.find('.btnToggle').click( F.toggle ),
					close  : this.list.find('.btnClose').click( F.close )
				}
			}

			//Prev
			if (obj.index > 0 || obj.loop) {
				buttons.prev.removeClass('btnDisabled');
			} else {
				buttons.prev.addClass('btnDisabled');
			}

			//Next / Play
			if (obj.loop || obj.index < obj.group.length - 1) {
				buttons.next.removeClass('btnDisabled');
				buttons.play.removeClass('btnDisabled');

			} else {
				buttons.next.addClass('btnDisabled');
				buttons.play.addClass('btnDisabled');
			}

			this.buttons = buttons;

			this.onUpdate(opts, obj);
		},

		onUpdate: function (opts, obj) {
			var toggle;

			if (!this.buttons) {
				return;
			}

			toggle = this.buttons.toggle.removeClass('btnDisabled btnToggleOn');

			//Size toggle button
			if (obj.canShrink) {
				toggle.addClass('btnToggleOn');

			} else if (!obj.canExpand) {
				toggle.addClass('btnDisabled');
			}
		},

		beforeClose: function () {
			if (this.list) {
				this.list.remove();
			}

			this.list    = null;
			this.buttons = null;
		}
	};

}(jQuery));
;
/*!
 * Media helper for fancyBox
 * version: 1.0.6 (Fri, 14 Jun 2013)
 * @requires fancyBox v2.0 or later
 *
 * Usage:
 *     $(".fancybox").fancybox({
 *         helpers : {
 *             media: true
 *         }
 *     });
 *
 * Set custom URL parameters:
 *     $(".fancybox").fancybox({
 *         helpers : {
 *             media: {
 *                 youtube : {
 *                     params : {
 *                         autoplay : 0
 *                     }
 *                 }
 *             }
 *         }
 *     });
 *
 * Or:
 *     $(".fancybox").fancybox({,
 *         helpers : {
 *             media: true
 *         },
 *         youtube : {
 *             autoplay: 0
 *         }
 *     });
 *
 *  Supports:
 *
 *      Youtube
 *          http://www.youtube.com/watch?v=opj24KnzrWo
 *          http://www.youtube.com/embed/opj24KnzrWo
 *          http://youtu.be/opj24KnzrWo
 *			http://www.youtube-nocookie.com/embed/opj24KnzrWo
 *      Vimeo
 *          http://vimeo.com/40648169
 *          http://vimeo.com/channels/staffpicks/38843628
 *          http://vimeo.com/groups/surrealism/videos/36516384
 *          http://player.vimeo.com/video/45074303
 *      Metacafe
 *          http://www.metacafe.com/watch/7635964/dr_seuss_the_lorax_movie_trailer/
 *          http://www.metacafe.com/watch/7635964/
 *      Dailymotion
 *          http://www.dailymotion.com/video/xoytqh_dr-seuss-the-lorax-premiere_people
 *      Twitvid
 *          http://twitvid.com/QY7MD
 *      Twitpic
 *          http://twitpic.com/7p93st
 *      Instagram
 *          http://instagr.am/p/IejkuUGxQn/
 *          http://instagram.com/p/IejkuUGxQn/
 *      Google maps
 *          http://maps.google.com/maps?q=Eiffel+Tower,+Avenue+Gustave+Eiffel,+Paris,+France&t=h&z=17
 *          http://maps.google.com/?ll=48.857995,2.294297&spn=0.007666,0.021136&t=m&z=16
 *          http://maps.google.com/?ll=48.859463,2.292626&spn=0.000965,0.002642&t=m&z=19&layer=c&cbll=48.859524,2.292532&panoid=YJ0lq28OOy3VT2IqIuVY0g&cbp=12,151.58,,0,-15.56
 */
(function ($) {
	"use strict";

	//Shortcut for fancyBox object
	var F = $.fancybox,
		format = function( url, rez, params ) {
			params = params || '';

			if ( $.type( params ) === "object" ) {
				params = $.param(params, true);
			}

			$.each(rez, function(key, value) {
				url = url.replace( '$' + key, value || '' );
			});

			if (params.length) {
				url += ( url.indexOf('?') > 0 ? '&' : '?' ) + params;
			}

			return url;
		};

	//Add helper object
	F.helpers.media = {
		defaults : {
			youtube : {
				matcher : /(youtube\.com|youtu\.be|youtube-nocookie\.com)\/(watch\?v=|v\/|u\/|embed\/?)?(videoseries\?list=(.*)|[\w-]{11}|\?listType=(.*)&list=(.*)).*/i,
				params  : {
					autoplay    : 1,
					autohide    : 1,
					fs          : 1,
					rel         : 0,
					hd          : 1,
					wmode       : 'opaque',
					enablejsapi : 1
				},
				type : 'iframe',
				url  : '//www.youtube.com/embed/$3'
			},
			vimeo : {
				matcher : /(?:vimeo(?:pro)?.com)\/(?:[^\d]+)?(\d+)(?:.*)/,
				params  : {
					autoplay      : 1,
					hd            : 1,
					show_title    : 1,
					show_byline   : 1,
					show_portrait : 0,
					fullscreen    : 1
				},
				type : 'iframe',
				url  : '//player.vimeo.com/video/$1'
			},
			metacafe : {
				matcher : /metacafe.com\/(?:watch|fplayer)\/([\w\-]{1,10})/,
				params  : {
					autoPlay : 'yes'
				},
				type : 'swf',
				url  : function( rez, params, obj ) {
					obj.swf.flashVars = 'playerVars=' + $.param( params, true );

					return '//www.metacafe.com/fplayer/' + rez[1] + '/.swf';
				}
			},
			dailymotion : {
				matcher : /dailymotion.com\/video\/(.*)\/?(.*)/,
				params  : {
					additionalInfos : 0,
					autoStart : 1
				},
				type : 'swf',
				url  : '//www.dailymotion.com/swf/video/$1'
			},
			twitvid : {
				matcher : /twitvid\.com\/([a-zA-Z0-9_\-\?\=]+)/i,
				params  : {
					autoplay : 0
				},
				type : 'iframe',
				url  : '//www.twitvid.com/embed.php?guid=$1'
			},
			twitpic : {
				matcher : /twitpic\.com\/(?!(?:place|photos|events)\/)([a-zA-Z0-9\?\=\-]+)/i,
				type : 'image',
				url  : '//twitpic.com/show/full/$1/'
			},
			instagram : {
				matcher : /(instagr\.am|instagram\.com)\/p\/([a-zA-Z0-9_\-]+)\/?/i,
				type : 'image',
				url  : '//$1/p/$2/media/?size=l'
			},
			google_maps : {
				matcher : /maps\.google\.([a-z]{2,3}(\.[a-z]{2})?)\/(\?ll=|maps\?)(.*)/i,
				type : 'iframe',
				url  : function( rez ) {
					return '//maps.google.' + rez[1] + '/' + rez[3] + '' + rez[4] + '&output=' + (rez[4].indexOf('layer=c') > 0 ? 'svembed' : 'embed');
				}
			}
		},

		beforeLoad : function(opts, obj) {
			var url   = obj.href || '',
				type  = false,
				what,
				item,
				rez,
				params;

			for (what in opts) {
				if (opts.hasOwnProperty(what)) {
					item = opts[ what ];
					rez  = url.match( item.matcher );

					if (rez) {
						type   = item.type;
						params = $.extend(true, {}, item.params, obj[ what ] || ($.isPlainObject(opts[ what ]) ? opts[ what ].params : null));

						url = $.type( item.url ) === "function" ? item.url.call( this, rez, params, obj ) : format( item.url, rez, params );

						break;
					}
				}
			}

			if (type) {
				obj.href = url;
				obj.type = type;

				obj.autoHeight = false;
			}
		}
	};

}(jQuery));;
 /*!
 * Thumbnail helper for fancyBox
 * version: 1.0.7 (Mon, 01 Oct 2012)
 * @requires fancyBox v2.0 or later
 *
 * Usage:
 *     $(".fancybox").fancybox({
 *         helpers : {
 *             thumbs: {
 *                 width  : 50,
 *                 height : 50
 *             }
 *         }
 *     });
 *
 */
(function ($) {
	//Shortcut for fancyBox object
	var F = $.fancybox;

	//Add helper object
	F.helpers.thumbs = {
		defaults : {
			width    : 50,       // thumbnail width
			height   : 50,       // thumbnail height
			position : 'bottom', // 'top' or 'bottom'
			source   : function ( item ) {  // function to obtain the URL of the thumbnail image
				var href;

				if (item.element) {
					href = $(item.element).find('img').attr('src');
				}

				if (!href && item.type === 'image' && item.href) {
					href = item.href;
				}

				return href;
			}
		},

		wrap  : null,
		list  : null,
		width : 0,

		init: function (opts, obj) {
			var that = this,
				list,
				thumbWidth  = opts.width,
				thumbHeight = opts.height,
				thumbSource = opts.source;

			//Build list structure
			list = '';

			for (var n = 0; n < obj.group.length; n++) {
				list += '<li><a style="width:' + thumbWidth + 'px;height:' + thumbHeight + 'px;" href="javascript:jQuery.fancybox.jumpto(' + n + ');"></a></li>';
			}

			this.wrap = $('<div id="fancybox-thumbs"></div>').addClass(opts.position).appendTo('body');
			this.list = $('<ul>' + list + '</ul>').appendTo(this.wrap);

			//Load each thumbnail
			$.each(obj.group, function (i) {
				var href = thumbSource( obj.group[ i ] );

				if (!href) {
					return;
				}

				$("<img />").load(function () {
					var width  = this.width,
						height = this.height,
						widthRatio, heightRatio, parent;

					if (!that.list || !width || !height) {
						return;
					}

					//Calculate thumbnail width/height and center it
					widthRatio  = width / thumbWidth;
					heightRatio = height / thumbHeight;

					parent = that.list.children().eq(i).find('a');

					if (widthRatio >= 1 && heightRatio >= 1) {
						if (widthRatio > heightRatio) {
							width  = Math.floor(width / heightRatio);
							height = thumbHeight;

						} else {
							width  = thumbWidth;
							height = Math.floor(height / widthRatio);
						}
					}

					$(this).css({
						width  : width,
						height : height,
						top    : Math.floor(thumbHeight / 2 - height / 2),
						left   : Math.floor(thumbWidth / 2 - width / 2)
					});

					parent.width(thumbWidth).height(thumbHeight);

					$(this).hide().appendTo(parent).fadeIn(300);

				}).attr('src', href);
			});

			//Set initial width
			this.width = this.list.children().eq(0).outerWidth(true);

			this.list.width(this.width * (obj.group.length + 1)).css('left', Math.floor($(window).width() * 0.5 - (obj.index * this.width + this.width * 0.5)));
		},

		beforeLoad: function (opts, obj) {
			//Remove self if gallery do not have at least two items
			if (obj.group.length < 2) {
				obj.helpers.thumbs = false;

				return;
			}

			//Increase bottom margin to give space for thumbs
			obj.margin[ opts.position === 'top' ? 0 : 2 ] += ((opts.height) + 15);
		},

		afterShow: function (opts, obj) {
			//Check if exists and create or update list
			if (this.list) {
				this.onUpdate(opts, obj);

			} else {
				this.init(opts, obj);
			}

			//Set active element
			this.list.children().removeClass('active').eq(obj.index).addClass('active');
		},

		//Center list
		onUpdate: function (opts, obj) {
			if (this.list) {
				this.list.stop(true).animate({
					'left': Math.floor($(window).width() * 0.5 - (obj.index * this.width + this.width * 0.5))
				}, 150);
			}
		},

		beforeClose: function () {
			if (this.wrap) {
				this.wrap.remove();
			}

			this.wrap  = null;
			this.list  = null;
			this.width = 0;
		}
	}

}(jQuery));;
/*
 * DC jQuery Social Stream
 * Copyright (c) 2013 Design Chemical
 * http://www.designchemical.com/blog/index.php/premium-jquery-plugins/jquery-social-stream-plugin/
 * Version 1.5.4 (09-08-2013)
 *
 */
 
(function($){SocialStreamObject=function(el,options){this.create(el,options)};$.extend(SocialStreamObject.prototype,{version:"1.5.4",create:function(el,options){this.defaults={feeds:{facebook:{id:"",intro:"Posted",out:"intro,thumb,title,text,user,share",text:"content",comments:3,image_width:4,icon:"facebook.png"},twitter:{id:"",intro:"Tweeted",search:"Tweeted",out:"intro,thumb,text,share",retweets:false,replies:false,images:"",url:"twitter.php",icon:"twitter.png"},google:{id:"",intro:"Shared",out:"intro,thumb,title,text,share",
api_key:"",image_height:75,image_width:75,shares:true,icon:"google.png"},youtube:{id:"",intro:"Uploaded,Favorite,New Video",search:"Search",out:"intro,thumb,title,text,user,share",feed:"uploads,favorites,newsubscriptionvideos",thumb:"default",icon:"youtube.png"},flickr:{id:"",intro:"Uploaded",out:"intro,thumb,title,text,share",lang:"en-us",icon:"flickr.png"},delicious:{id:"",intro:"Bookmarked",out:"intro,thumb,title,text,user,share",icon:"delicious.png"},pinterest:{id:"",intro:"Pinned",out:"intro,thumb,text,user,share",
icon:"pinterest.png"},rss:{id:"",intro:"Posted",out:"intro,title,text,share",text:"contentSnippet",icon:"rss.png"},lastfm:{id:"",intro:"Listened to,Loved,Replied",out:"intro,thumb,title,text,user,share",feed:"recenttracks,lovedtracks,replytracker",icon:"lastfm.png"},dribbble:{id:"",intro:"Posted,Liked",out:"intro,thumb,title,text,user,share",feed:"shots,likes",icon:"dribbble.png"},vimeo:{id:"",intro:"Liked,Video,Appeared In,Video,Album,Channel,Group",out:"intro,thumb,title,text,user,share",feed:"likes,videos,appears_in,all_videos,albums,channels,groups",
thumb:"medium",stats:true,icon:"vimeo.png"},stumbleupon:{id:"",intro:"Shared,Reviewed",out:"intro,thumb,title,text,user,share",feed:"favorites,reviews",icon:"stumbleupon.png"},deviantart:{id:"",intro:"Deviation",out:"intro,thumb,title,text,user,share",icon:"deviantart.png"},tumblr:{id:"",intro:"Posted",out:"intro,title,text,user,share",thumb:100,video:250,icon:"tumblr.png"},instagram:{id:"",intro:"Posted",search:"Search",out:"intro,thumb,text,user,share,meta",accessToken:"",redirectUrl:"",clientId:"",
thumb:"low_resolution",comments:3,likes:8,icon:"instagram.png"}},remove:"",twitterId:"",days:10,limit:50,max:"days",external:true,speed:600,height:550,wall:false,order:"date",filter:true,controls:true,rotate:{direction:"up",delay:8E3},cache:true,container:"dcsns",cstream:"stream",content:"dcsns-content",iconPath:"images/dcsns-dark/",imagePath:"images/dcsns-dark/",debug:false};this.o={},this.timer_on=0,this.id="dcsns-"+$(el).index(),this.timerId="",this.o=$.extend(true,this.defaults,options),opt=this.o,
$load=$('<div class="dcsns-loading">creating stream ...</div>');$(el).addClass(this.o.container).append('<div class="'+this.o.content+'"><ul class="'+this.o.cstream+'"></ul></div>');var $c=$("."+this.o.content,el),$a=$("."+this.o.cstream,el),$l=$("li",$a);if(opt.height>0&&opt.wall==false)$c.css({height:opt.height+"px"});if(this.o.filter==true||this.o.controls==true){var x='<div class="dcsns-toolbar">';if(this.o.filter==true){x+=/*Modified*/'<ul id="dcsns-filter" class="option-set filter">';x+=this.o.wall==true?
'<li data-group="dc-filter" data-filter="*" class="active"><a href="#filter" data-group="dc-filter"  data-filter="*" class="selected link-all">All</a></li>':"";var $f=$(".filter",el);$.each(opt.feeds,function(k,v){x+=v.id!=""?'<li class="f-'+k+'"><a href="#filter" rel="'+k+'" data-group="dc-filter" data-filter=".dcsns-'+k+'"><img src="'+opt.imagePath+opt.feeds[k].icon+'" alt="" /></a></li>':""});x+="</ul>"}if(this.o.controls==true&&opt.wall==false){var play=this.o.rotate.delay<=0?"":'<li><a href="#" class="play"></a></li>';x+='<div class="controls"><ul>'+
play+'<li><a href="#" class="prev"></a></li><li><a href="#" class="next"></a></li></ul>'}x+="</div>";if(opt.wall==false)$(el).append(x);else $(el).before(x)}if(this.o.wall==true){$(".dcsns-toolbar").append($load);this.createwall($a)}else $c.append($load);this.createstream(el,$a,0,opt.days);this.addevents(el,$a);if(this.o.rotate.delay>0)this.rotate(el);$load.remove()},createstream:function(obj,s,f1,f2){$.each(opt.feeds,function(k,v){if(opt.feeds[k].id!=""){var txt=[];$.each(opt.feeds[k].intro.split(","),
function(i,v){v=$.trim(v);txt.push(v)});$.each(opt.feeds[k].id.split(","),function(i,v){v=$.trim(v);if(opt.feeds[k].feed&&v.split("#").length<2)if(k=="youtube"&&v.split("/").length>1)getFeed(k,v,opt.iconPath,opt.feeds[k],obj,opt,f1,f2,"posted","",i);else $.each(opt.feeds[k].feed.split(","),function(i,feed){getFeed(k,v,opt.iconPath,opt.feeds[k],obj,opt,f1,f2,txt[i],feed,i)});else{intro=v.split("#").length<2?opt.feeds[k].intro:opt.feeds[k].search;getFeed(k,v,opt.iconPath,opt.feeds[k],obj,opt,f1,f2,
intro,"",i)}})}})},createwall:function(obj){
	//obj.imagesLoaded(function(){
		obj.isotope({itemSelector:"li.dcsns-li",layoutMode:'masonry',masonry:{gutter:10},getSortData:{postDate:function($elem){return parseInt($($elem).attr("rel"),10)}},sortBy:"postDate"})
	//})
},addevents:function(obj,$a){var self=this,speed=this.o.speed;var $container=$(".stream",obj),filters={};$(".controls",obj).delegate("a","click",function(){var x=$(this).attr("class");switch(x){case "prev":self.pauseTimer();ticker($a,"prev",speed);break;case "next":self.pauseTimer();ticker($a,
"next",speed);break;case "play":self.rotate(obj);$(".controls .play").removeClass("play").addClass("pause");break;case "pause":self.pauseTimer();break}return false});$(".filter",obj).delegate("a","click",function(){if(opt.wall==false){var rel=$(this).attr("rel");if($(this).parent().hasClass("active")){$(".dcsns-"+rel,$a).slideUp().addClass("inactive");$(this).parent().animate({opacity:0.3},400)}else{$(".dcsns-"+rel,$a).slideDown().removeClass("inactive");$(this).parent().animate({opacity:1},400)}$(this).parent().toggleClass("active")}return false});
if(this.o.external)$a.delegate("a","click",function(){if(!$(this).parent().hasClass("section-share"))this.target="_blank"})},rotate:function(a){var self=this,stream=$("."+this.o.cstream,a),speed=this.o.speed,delay=this.o.rotate.delay,r=this.o.rotate.direction=="up"?"prev":"next";this.timer_on=1;$(".controls .play").removeClass("play").addClass("pause");this.timerId=setTimeout(function(){ticker(stream,r,speed);self.rotate(a)},delay)},pauseTimer:function(){clearTimeout(this.timerId);this.timer_on=0;
$(".controls .pause").removeClass("pause").addClass("play")}});$.fn.dcSocialStream=function(options,callback){var d={};this.each(function(){var s=$(this);d=s.data("socialtabs");if(!d){d=new SocialStreamObject(this,options,callback);s.data("socialtabs",d)}});return d};function getFeed(type,id,path,o,obj,opt,f1,f2,intro,feed,fn){var stream=$(".stream",obj),list=[],d="",px=300,c=[],data,href,url,n=opt.limit,txt=[],src;frl="https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num="+n+"&callback=?&q=";
switch(type){case "facebook":var cp=id.split("/");url=url=cp.length>1?"https://graph.facebook.com/"+cp[1]+"/photos?fields=id,link,from,name,picture,images,comments&limit="+n:frl+encodeURIComponent("https://www.facebook.com/feeds/page.php?id="+id+"&format=rss20");break;case "twitter":var curl=o.url.replace(/\&#038;/gi,"&");var cp=id.split("/"),cq=id.split("#"),cu=o.url.split("?"),replies=o.replies==true?"&exclude_replies=false":"&exclude_replies=true";var param="&include_entities=true&include_rts="+
o.retweets+replies;url1=cu.length>1?curl+"&":curl+"?";url=cp.length>1?url1+"url=list&list_id="+cp[1]+"&per_page="+n+param:url1+"url=timeline&screen_name="+id+"&count="+n+param;if(cq.length>1){var rts=o.retweets==false?"+exclude:retweets":"";url=url1+"url=search&q="+encodeURIComponent(cq[1])+"&count="+n}break;case "google":n=n>100?100:n;href="https://plus.google.com/"+id;url="https://www.googleapis.com/plus/v1/people/"+id+"/activities/public";data={key:o.api_key,maxResults:n,prettyprint:false,fields:"items(id,kind,object(attachments(displayName,fullImage,id,image,objectType,url),id,objectType,plusoners,replies,resharers,url),published,title,url,verb)"};
break;case "youtube":var cp=id.split("/"),cq=id.split("#");n=n>50?50:n;href="https://www.youtube.com/";href+=cq.length>1?"results?search_query="+encodeURIComponent(cq[1]):"user/"+id;href=cp.length>1?"https://www.youtube.com/playlist?list="+cp[1]:href;url="https://gdata.youtube.com/feeds/";if(cp.length>1)url+="api/playlists/"+cp[1]+"?v=2&orderby=published";else url+=cq.length>1?"api/videos?alt=rss&orderby=published&max-results="+n+"&racy=include&q="+encodeURIComponent(cq[1]):"base/users/"+id+"/"+feed+
"?alt=rss&v=2&orderby=published&client=ytapi-youtube-profile";url=frl+encodeURIComponent(url);break;case "flickr":var cq=id.split("/"),fd=cq.length>1?"groups_pool":"photos_public";id=cq.length>1?cq[1]:id;href="https://www.flickr.com/photos/"+id;url="http://api.flickr.com/services/feeds/"+fd+".gne?id="+id+"&lang="+o.lang+"&format=json&jsoncallback=?";break;case "delicious":href="https://www.delicious.com/"+id;url="http://feeds.delicious.com/v2/json/"+id;break;case "pinterest":var cp=id.split("/");
url="https://www.pinterest.com/"+id+"/";url+=cp.length>1?"rss":"feed.rss";href="http://www.pinterest.com/"+id;url=frl+encodeURIComponent(url);break;case "rss":href=id;url=frl+encodeURIComponent(id);break;case "lastfm":href="https://www.last.fm/user/"+id;var ver=feed=="lovedtracks"?"2.0":"1.0";url=frl+encodeURIComponent("https://ws.audioscrobbler.com/"+ver+"/user/"+id+"/"+feed+".rss");break;case "dribbble":href="https://www.dribbble.com/"+id;url=feed=="likes"?"http://api.dribbble.com/players/"+id+
"/shots/likes":"http://api.dribbble.com/players/"+id+"/shots";break;case "vimeo":href="https://www.vimeo.com/"+id;url="https://vimeo.com/api/v2/"+id+"/"+feed+".json";break;case "stumbleupon":href="https://www.stumbleupon.com/stumbler/"+id;url=frl+encodeURIComponent("http://rss.stumbleupon.com/user/"+id+"/"+feed);break;case "deviantart":href="https://"+id+".deviantart.com";url=frl+encodeURIComponent("https://backend.deviantart.com/rss.xml?type=deviation&q=by%3A"+id+"+sort%3Atime+meta%3Aall");break;
case "tumblr":href="http://"+id+".tumblr.com";url="http://"+id+".tumblr.com/api/read/json?callback=?";break;case "instagram":href="#";url="https://api.instagram.com/v1";var cp=id.substr(0,1),cq=id.split(cp),url1=encodeURIComponent(cq[1]),qs="",ts=0;switch(cp){case "?":var p=cq[1].split("/");qs="&lat="+p[0]+"&lng="+p[1]+"&distance="+p[2];url+="/media/search";break;case "#":url+="/tags/"+url1+"/media/recent";ts=1;break;case "!":url+="/users/"+url1+"/media/recent";break;case "@":url+="/locations/"+url1+
"/media/recent";break}if(o.accessToken==""&&ts==0)if(location.hash)o.accessToken=location.hash.split("=")[1];else location.href="https://instagram.com/oauth/authorize/?client_id="+o.clientId+"&redirect_uri="+o.redirectUrl+"&response_type=token";url+="?access_token="+o.accessToken+"&client_id="+o.clientId+"&count="+n+qs;break}var dataType=type=="twitter"?"json":"jsonp";jQuery.ajax({url:url,data:data,cache:opt.cache,dataType:dataType,success:function(a){var error="";switch(type){case "facebook":if(cp.length>
1)a=a.data;else if(a.responseStatus==200)a=a.responseData.feed.entries;else error=a.responseDetails;break;case "google":error=a.error?a.error:"";a=a.items;break;case "flickr":a=a.items;break;case "instagram":a=a.data;break;case "twitter":error=a.errors?a.errors:"";if(cq.length>1)a=a.statuses;break;case "youtube":if(a.responseStatus==200){a=a.responseData.feed.entries;if(cp.length>1)var pl=cp[0]}else error=a.responseDetails;break;case "dribbble":a=a.shots;break;case "tumblr":a=a.posts;break;case "delicious":break;
case "vimeo":break;default:if(a.responseStatus==200)a=a.responseData.feed.entries;else error=a.responseDetails;break}if(error=="")$.each(a,function(i,item){if(i<n){var html=[],q=item.link,u='<a href="'+href+'">'+id+"</a>",w="",x='<a href="'+q+'">'+item.title+"</a>",y="",z="",zz="",m="",d=item.publishedDate,sq=q,st=item.title,s="";switch(type){case "facebook":if(cp.length>1){id=item.from.id;var d=new Date;d=d.setFbAlbum(item.created_time);var set=parseQ(item.link);st=cp[0]!=""?cp[0]:item.from.name;
u='<a href="http://www.facebook.com/media/set/?set='+set[1]+'">'+st+"</a>";x="";z='<a href="'+item.link+'"><img src="'+item.images[o.image_width].source+'" alt="" /></a>';if(o.comments>0&&item.comments){i=0;m+='<span class="meta"><span class="comments">comments</span></span>';$.each(item.comments.data,function(i,cmt){if(o.comments>i){m+='<span class="meta item-comments"><a href="http://facebook.com/'+cmt.from.id+'">'+cmt.from.name+"</a>"+cmt.message+"</span>";i++}else return false})}z+=m}else z=item[o.text];
break;case "twitter":d=parseTwitterDate(item.created_at);var un=item.user.screen_name,ua=item.user.profile_image_url_https;href="https://www.twitter.com/"+un;q=href;y='<a href="'+q+'" class="thumb"><img src="'+ua+'" alt="" /></a>';z='<span class="twitter-user"><a href="https://www.twitter.com/'+un+'"><strong>'+item.user.name+" </strong>@"+un+"</a></span>";z+=linkify(item.text);if(o.images!=""&&item.entities.media)$.each(item.entities.media,function(i,media){z+='<a href="'+media.media_url_https+'"><img src="'+
media.media_url_https+":"+o.images+'" alt="" /></a>'});sq=item.id_str;break;case "delicious":var d=new Date;d=d.setRFC3339(item.dt);x='<a href="'+item.u+'">'+item.d+"</a>";q=item.u;z=item.n;sq=item.u;st=item.d;break;case "rss":z=item[o.text];break;case "pinterest":var src=$("img",item.content).attr("src");y=src?'<a href="'+q+'"><img src="'+src+'" alt="" /></a>':"";z=item.contentSnippet;st=z;break;case "youtube":var v=[];v=parseQ(item.link);y='<a href="'+q+'" title="'+item.title+'"><img src="http://img.youtube.com/vi/'+
v["v"]+"/"+o.thumb+'.jpg" alt="" /></a>';z=item.contentSnippet;if(cp.length>1)u='<a href="'+href+'">'+pl+"</a>";break;case "flickr":d=parseTwitterDate(item.published);x=item.title;y='<a href="'+q+'" title="'+item.title+'"><img src="'+item.media.m+'" alt="" /></a>';break;case "lastfm":q=item.content;break;case "dribbble":q=item.url;d=item.created_at;y='<a href="'+q+'"><img src="'+item.image_teaser_url+'" alt="'+item.title+'" /></a>';z='<span class="meta"><span class="views">'+num(item.views_count)+
'</span><span class="likes">'+num(item.likes_count)+'</span><span class="comments">'+num(item.comments_count)+"</span></span>";sq=item.url;break;case "instagram":d=parseInt(item.created_time*1E3,10);x="";y='<a href="'+item.images[o.thumb].url+'"><img src="'+item.images[o.thumb].url+'" alt="" /></a>';z=item.caption!=null?htmlEncode(item.caption.text):"";if(item.comments.count>0&&o.comments>0){i=0;m+='<span class="meta"><span class="comments">'+num(item.comments.count)+" comments</span></span>";$.each(item.comments.data,
function(i,cmt){if(o.comments>i){m+='<span class="meta item-comments"><img src="'+cmt.from.profile_picture+'" />';m+=cmt.from.full_name+" - "+cmt.text+"</span>";i++}else return false})}if(item.likes.count>0&&o.likes>0){i=0;m+='<span class="meta"><span class="likes">'+num(item.likes.count)+" likes</span></span>";m+='<span class="meta item-likes">';$.each(item.likes.data,function(i,lk){if(o.likes>i){m+='<img src="'+lk.profile_picture+'" />';i++}else return false});m+="</span>"}u='<a href="'+q+'">'+
item.user.username+"</a>";st=item.caption!=null?item.caption.text:"";break;case "vimeo":f=feed,at=item.name,tx=item.description,q=item.url;if(f=="channels")y=item.logo!=""?'<a href="'+q+'" class="logo"><img src="'+item.logo+'" alt="" width="'+px+'" /></a>':"";else if(f=="groups")y='<a href="'+q+'"><img src="'+item.thumbnail+'" alt="" /></a>';else{var thumb="thumbnail_"+o.thumb,at=item.title,tx=f!="albums"?item.duration+" secs":item.description;y='<a href="'+item.url+'" title="'+at+'"><img src="'+
item[thumb]+'" alt="" /></a>'}x='<a href="'+q+'">'+at+"</a>";z=tx;if(o.stats==true){var m="";m+=f=="albums"||f=="channels"||f=="groups"?'<span class="videos">'+num(item.total_videos)+"</span>":"";if(f=="channels")m+='<span class="users">'+num(item.total_subscribers)+"</span>";else if(f=="groups")m+='<span class="users">'+num(item.total_members)+"</span>";else if(f!="albums")m+='<span class="likes">'+num(item.stats_number_of_likes)+'</span><span class="views">'+num(item.stats_number_of_plays)+'</span><span class="comments">'+
num(item.stats_number_of_comments)+"</span>";z+='<span class="meta">'+m+"</span>"}var dt=item.upload_date;if(f=="likes")dt=item.liked_on;else if(f=="albums"||f=="channels"||f=="groups")dt=item.created_on;var d=new Date;d=d.setVimeo(dt);sq=q;st=at;break;case "stumbleupon":var src=$("img",item.content).attr("src");y=src!=""&&feed=="favorites"?'<a href="'+q+'"><img src="'+src+'" alt="" /></a>':"";z=item.contentSnippet;break;case "deviantart":var src=$("img",item.content).attr("src");y=src?'<a href="'+
q+'"><img src="'+src+'" alt="" /></a>':"";z=item.contentSnippet;break;case "tumblr":q=item["url-with-slug"];d=item.date;x='<a href="'+q+'">';switch(item.type){case "photo":x=item["photo-caption"];z='<a href="'+q+'"><img src="'+item["photo-url-"+o.thumb]+'" alt="" /></a>';st=x;break;case "video":x+=item["video-caption"];z=o.video!="400"?item["video-player-"+o.video]:item["video-player"];st=x;break;case "regular":x+=item["regular-title"];z=item["regular-body"];st=x;break;case "quote":x+=item["quote-source"];
z=item["quote-text"];st=x;break;case "audio":x=item["id3-artist"]?'<a href="'+q+'">'+item["id3-artist"]+" - "+item["id3-album"]+"</a>":"";x+=item["id3-title"]?'<a href="'+q+'" class="track">'+item["id3-title"]+"</a>":"";z=item["audio-caption"];z+=item["audio-player"];st=item["id3-artist"]+" - "+item["id3-album"]+" - "+item["id3-title"];break;case "conversation":x+=item["conversation-title"];z=item["conversation-text"];st=x;break;case "link":var ltxt=item["link-text"].replace(/:/g,"").replace(/\?/g,
"").replace(/\!/g,"").replace(/\./g,"").replace(/\'/g,"");x='<a href="'+item["link-url"]+'">'+ltxt+"</a>";z=item["link-description"];st=ltxt;break}x+=item.type!="photo"||item.type!="audio"?"</a>":"";st=stripHtml(st);sq=q;break;case "google":var g=item.object.replies?num(item.object.replies.totalItems):0,m=item.object.plusoners?num(item.object.plusoners.totalItems):0,p=item.object.resharers?num(item.object.resharers.totalItems):0,dl;var d=new Date;d=d.setRFC3339(item.published);dl={src:"",imgLink:"",
useLink:"",useTitle:""};var k=item.object.attachments;if(k)if(k.length){for(var l=0;l<k.length;l++){var h=k[l];if(h.image){dl.src=h.image.url;dl.imgLink=h.url;if(h.fullImage){dl.w=h.fullImage.width||0;dl.h=h.fullImage.height||0}}if(h.objectType=="article")dl.useLink=h.url;if(h.displayName)dl.useTitle=h.displayName}if(!dl.useLink)dl.useLink=dl.imgLink;var img_h=o.image_height?o.image_height:75;var img_w=o.image_width?o.image_width:75;if(dl.src.indexOf("resize_h")>=0)dl.src=dl.w>=dl.h?dl.src.replace(/resize_h=\d+/i,
"resize_h="+img_h):dl.src.replace(/resize_h=\d+/i,"resize_w="+img_w)}dl=dl;q=dl.useLink;y=dl.src?(dl.useLink?'<a href="'+dl.useLink+'">':"")+'<img src="'+dl.src+'" />'+(dl.useLink?"</a>":""):"";var t1=px/(dl.w/dl.h)<px/3?' class="clear"':"";x=(dl.useLink?'<a href="'+dl.useLink+'"'+t1+">":"")+(item.title?item.title:dl.useTitle)+(dl.useLink?"</a>":"");if(o.shares)z='<span class="meta"><span class="plusones">+1s '+m+'</span><span class="shares">'+p+'</span><span class="comments">'+g+"</span></span>";
sq=q;st=dl.useTitle;break}icon='<a href="'+href+'"><img src="'+path+o.icon+'" alt="" class="icon" /></a>';if(type=="twitter"){var intent="https://twitter.com/intent/";s='<a href="'+intent+"tweet?in_reply_to="+sq+"&via="+opt.twitterId+'" class="share-reply"></a>';s+='<a href="'+intent+"retweet?tweet_id="+sq+"&via="+opt.twitterId+'" class="share-retweet"></a>';s+='<a href="'+intent+"favorite?tweet_id="+sq+'" class="share-favorite"></a>'}else s=share(st,sq,opt.twitterId);$.each(o.out.split(","),function(i,
v){zz+=v!="intro"?'<span class="section-'+v+'">':"";switch(v){case "intro":/*Modified*/if(type=="twitter") zintro='<span class="ss-date"><a href="https://twitter.com/' + un + "/status/" + item.id_str + '">' + nicetime((new Date(d)).getTime(), 0) + '</a></span><span class="section-' + v + '"><a href="' + q + '">' + decodeURIComponent(intro) + '</a></span>';else zintro='<span class="section-' + v + '"><a href="' + q + '">' + decodeURIComponent(intro) + "</a></span>";break;case "title":zz+=x;break;case "thumb":if(type==
"rss"){var src=item.content.indexOf("img")>=0?$("img",item.content).attr("src"):"";y=src?'<a href="'+q+'" class="thumb"><img src="'+src+'" alt="" /></a>':""}zz+=y;break;case "text":zz+=z;break;case "user":zz+=u;break;case "meta":zz+=m;break;case "share":zz+=s;break}zz+=v!="intro"?"</span>":"";/*Modified*/if(v == "title"){if(type!='twitter'){zz+='<span class="ss-date">'+nicetime(new Date(d).getTime(),0)+'</span>';}}});var df=type=="instagram"?nicetime(d,1):nicetime((new Date(d)).getTime(),1);var ob=df;switch(opt.order){case "random":ob=randomish(6);break;case "none":ob=1;break}var out='<li rel="'+ob+'" class="dcsns-li dcsns-'+
type+" dcsns-feed-"+fn+'">'+w+'<div class="inner">'+zz+'<span class="clear"></span></div>'+zintro+icon+"</li>",str=opt.remove;if(str.indexOf(q)!==-1&&q!="")n=n+1;else if(opt.max=="days")if(df<=f2*86400&&df>=f1*86400)list.push(out);else{if(df>f2*86400)return false}else list.push(out)}});else if(opt.debug==true)list.push('<li class="dcsns-li dcsns-error">Error. '+error+"</li>")},complete:function(){var $newItems=$(list.join(""));if(opt.wall==true)stream.isotope("insert",$newItems);else{stream.append($newItems);
sortstream(stream,"asc")}if(type=="facebook"&&cp.length<2)fbHrefLink(id,$newItems);else if(type=="flickr"&&cq.length>1)flickrHrefLink(cq[1],$newItems)}});return}function linkify(text){text=text.replace(/((https?\:\/\/)|(www\.))(\S+)(\w{2,4})(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/gi,function(url){var full_url=!url.match("^https?://")?"http://"+url:url;return'<a href="'+full_url+'">'+url+"</a>"});text=text.replace(/(^|\s)@(\w+)/g,'$1@<a href="http://www.twitter.com/$2">$2</a>');text=text.replace(/(^|\s)#(\w+)/g,
'$1#<a href="http://twitter.com/search/%23$2">$2</a>');return text}function htmlEncode(v){return $("<div/>").text(v).html()}function stripHtml(v){var $html=$(v);return $html.text()}Date.prototype.setRFC3339=function(dString){var utcOffset,offsetSplitChar;var offsetMultiplier=1;var dateTime=dString.split("T");var date=dateTime[0].split("-");var time=dateTime[1].split(":");var offsetField=time[time.length-1];var offsetString;offsetFieldIdentifier=offsetField.charAt(offsetField.length-1);if(offsetFieldIdentifier==
"Z"){utcOffset=0;time[time.length-1]=offsetField.substr(0,offsetField.length-2)}else{if(offsetField[offsetField.length-1].indexOf("+")!=-1){offsetSplitChar="+";offsetMultiplier=1}else{offsetSplitChar="-";offsetMultiplier=-1}offsetString=offsetField.split(offsetSplitChar);time[time.length-1]==offsetString[0];offsetString=offsetString[1].split(":");utcOffset=offsetString[0]*60+offsetString[1];utcOffset=utcOffset*60*1E3}this.setTime(Date.UTC(date[0],date[1]-1,date[2],time[0],time[1],time[2])+utcOffset*
offsetMultiplier);return this};Date.prototype.setFbAlbum=function(dString){var utcOffset,offsetSplitChar="+",offsetMultiplier=1,dateTime=dString.split("T"),date=dateTime[0].split("-"),time=dateTime[1].split(":"),offsetField=time[time.length-1],offsetString;if(offsetField[offsetField.length-1].indexOf("+")!=-1){offsetSplitChar="-";offsetMultiplier=-1}offsetTime=offsetField.split(offsetSplitChar);utcOffset=parseInt(offsetTime[1]/100,10)*60*1E3;this.setTime(Date.UTC(date[0],date[1]-1,date[2],time[0],
time[1],offsetTime[0])+utcOffset*offsetMultiplier);return this};Date.prototype.setVimeo=function(dString){var utcOffset=0,offsetSplitChar,offsetMultiplier=1;var dateTime=dString.split(" ");var date=dateTime[0].split("-");var time=dateTime[1].split(":");this.setTime(Date.UTC(date[0],date[1]-1,date[2],time[0],time[1],time[2])+utcOffset*offsetMultiplier);return this};function parseTwitterDate(a){var out=navigator.userAgent.indexOf("MSIE")>=0?a.replace(/(\+\S+) (.*)/,"$2 $1"):a;return out}function nicetime(a,
out){ /*modified*/var monthNames=["Jan","Feb","Mar","Apr","May","June","July","Aug","Sept","Oct","Nov","Dec"],fulldate=new Date(a),date=fulldate.getDate(),month_no=fulldate.getMonth(),month=monthNames[month_no],fullyear=fulldate.getFullYear(),year=fullyear.toString().substr(2,2);return date+" "+month+" \'"+year;/*var d=Math.round((+new Date-a)/1E3),fuzzy="",n="mins";if(out==1)return d;else if(out==0){var chunks=new Array;chunks[0]=[60*60*24*365,"year","years"];chunks[1]=[60*60*24*30,"month","months"];chunks[2]=[60*60*24*7,"week","weeks"];chunks[3]=[60*60*24,"day","days"];chunks[4]=[60*60,"hr","hrs"];chunks[5]=[60,"min","mins"];var i=0,j=chunks.length;for(i=0;i<j;i++){s=chunks[i][0];if((xj=Math.floor(d/s))!=0){n=xj==1?chunks[i][1]:chunks[i][2];break}}fuzzy+=xj==1?"1 "+n:xj+" "+n;if(i+1<j){s2=chunks[i+
1][0];if((xj2=Math.floor((d-s*xj)/s2))!=0){n2=xj2==1?chunks[i+1][1]:chunks[i+1][2];fuzzy+=xj2==1?" + 1 "+n2:" + "+xj2+" "+n2}}fuzzy+=" ago";return fuzzy}*/}function num(a){var b=a;if(a>999999)b=Math.floor(a/1E6)+"M";else if(a>9999)b=Math.floor(a/1E3)+"K";else if(a>999)b=Math.floor(a/1E3)+","+a%1E3;return b}function parseQ(url){var v=[],hash,q=url.split("?")[1];if(q!=undefined){q=q.split("&");for(var i=0;i<q.length;i++){hash=q[i].split("=");v.push(hash[1]);v[hash[0]]=hash[1]}}return v}function sortstream(obj,
d){var $l=$("li",obj);$l.sort(function(a,b){var keyA=parseInt($(a).attr("rel"),10),keyB=parseInt($(b).attr("rel"),10);if(d=="asc")return keyA>keyB?1:-1;else return keyA<keyB?1:-1;return 0});$.each($l,function(index,row){obj.append(row)});$(".dcsns-loading").slideUp().remove();return}function randomish(l){var i=0,out="";while(i<l){out+=Math.floor(Math.random()*10+1)+"";i++}return out}function ticker(s,b,speed){var $a=$("li:last",s),$b=$("li:first",s),$gx,bh=$b.outerHeight(true);if($("li",s).not(".inactive").length>
2)if(b=="next"){$gx=$a.clone().hide();$b.before($gx);$a.remove();if($a.hasClass("inactive"))ticker(s,b,speed);else{$(".inner",$gx).css({opacity:0});$gx.slideDown(speed,"linear",function(){$(".inner",this).animate({opacity:1},speed)});return}}else{$gx=$b.clone();if($b.hasClass("inactive")){$a.after($gx);$b.remove();ticker(s,b,speed)}else{$b.animate({marginTop:-bh+"px"},speed,"linear",function(){$a.after($gx);$b.remove()});$(".inner",$b).animate({opacity:0},speed)}}}function fbHrefLink(id,obj){jQuery.ajax({url:"https://graph.facebook.com/"+
id,dataType:"jsonp",success:function(a){$(".icon",obj).each(function(){$(this).parent().attr("href",a.link)});$(".section-user a",obj).each(function(){$(this).attr("href",a.link);$(this).text(a.name)})}})}function flickrHrefLink(id,obj){jQuery.ajax({url:"http://api.flickr.com/services/feeds/groups_pool.gne?id="+id+"&format=json&jsoncallback=?",dataType:"jsonp",success:function(a){$(".icon",obj).each(function(){$(this).parent().attr("href",a.link)})}})}function share(st,sq,twitterId){var s="",sq=encodeURIComponent(sq),
st=encodeURIComponent(st);s='<a href="http://www.facebook.com/sharer.php?u='+sq+"&t="+st+'" class="share-facebook"></a>';s+='<a href="https://twitter.com/share?url='+sq+"&text="+st+"&via="+twitterId+'" class="share-twitter"></a>';s+='<a href="https://plus.google.com/share?url='+sq+'" class="share-google"></a>';s+='<a href="http://www.linkedin.com/shareArticle?mini=true&url='+sq+"&title="+st+'" class="share-linkedin"></a>';return s}})(jQuery);


jQuery(window).load(function(){
	jQuery.getScript("//platform.twitter.com/widgets.js", function(){});
	jQuery('.section-share a').click(function(){
		var u = jQuery(this).attr('href');
		window.open(u,'sharer','toolbar=0,status=0,width=626,height=436');
		return false;
	});
	jQuery('.dcsns-facebook .section-text a').each(function(i){
		var txt = jQuery(this).attr('href');
		var href = decodeURIComponent(txt.replace("http://www.facebook.com/l.php?u=", "")).split('&');
		jQuery(this).attr('href',href[0]);
	});
});;
/**
 * Isotope v1.5.25
 * An exquisite jQuery plugin for magical layouts
 * http://isotope.metafizzy.co
 *
 * Commercial use requires one-time license fee
 * http://metafizzy.co/#licenses
 *
 * Copyright 2012 David DeSandro / Metafizzy
 */
//(function(a,b,c){"use strict";var d=a.document,e=a.Modernizr,f=function(a){return a.charAt(0).toUpperCase()+a.slice(1)},g="Moz Webkit O Ms".split(" "),h=function(a){var b=d.documentElement.style,c;if(typeof b[a]=="string")return a;a=f(a);for(var e=0,h=g.length;e<h;e++){c=g[e]+a;if(typeof b[c]=="string")return c}},i=h("transform"),j=h("transitionProperty"),k={csstransforms:function(){return!!i},csstransforms3d:function(){var a=!!h("perspective");if(a){var c=" -o- -moz- -ms- -webkit- -khtml- ".split(" "),d="@media ("+c.join("transform-3d),(")+"modernizr)",e=b("<style>"+d+"{#modernizr{height:3px}}"+"</style>").appendTo("head"),f=b('<div id="modernizr" />').appendTo("html");a=f.height()===3,f.remove(),e.remove()}return a},csstransitions:function(){return!!j}},l;if(e)for(l in k)e.hasOwnProperty(l)||e.addTest(l,k[l]);else{e=a.Modernizr={_version:"1.6ish: miniModernizr for Isotope"};var m=" ",n;for(l in k)n=k[l](),e[l]=n,m+=" "+(n?"":"no-")+l;b("html").addClass(m)}if(e.csstransforms){var o=e.csstransforms3d?{translate:function(a){return"translate3d("+a[0]+"px, "+a[1]+"px, 0) "},scale:function(a){return"scale3d("+a+", "+a+", 1) "}}:{translate:function(a){return"translate("+a[0]+"px, "+a[1]+"px) "},scale:function(a){return"scale("+a+") "}},p=function(a,c,d){var e=b.data(a,"isoTransform")||{},f={},g,h={},j;f[c]=d,b.extend(e,f);for(g in e)j=e[g],h[g]=o[g](j);var k=h.translate||"",l=h.scale||"",m=k+l;b.data(a,"isoTransform",e),a.style[i]=m};b.cssNumber.scale=!0,b.cssHooks.scale={set:function(a,b){p(a,"scale",b)},get:function(a,c){var d=b.data(a,"isoTransform");return d&&d.scale?d.scale:1}},b.fx.step.scale=function(a){b.cssHooks.scale.set(a.elem,a.now+a.unit)},b.cssNumber.translate=!0,b.cssHooks.translate={set:function(a,b){p(a,"translate",b)},get:function(a,c){var d=b.data(a,"isoTransform");return d&&d.translate?d.translate:[0,0]}}}var q,r;e.csstransitions&&(q={WebkitTransitionProperty:"webkitTransitionEnd",MozTransitionProperty:"transitionend",OTransitionProperty:"oTransitionEnd otransitionend",transitionProperty:"transitionend"}[j],r=h("transitionDuration"));var s=b.event,t=b.event.handle?"handle":"dispatch",u;s.special.smartresize={setup:function(){b(this).bind("resize",s.special.smartresize.handler)},teardown:function(){b(this).unbind("resize",s.special.smartresize.handler)},handler:function(a,b){var c=this,d=arguments;a.type="smartresize",u&&clearTimeout(u),u=setTimeout(function(){s[t].apply(c,d)},b==="execAsap"?0:100)}},b.fn.smartresize=function(a){return a?this.bind("smartresize",a):this.trigger("smartresize",["execAsap"])},b.Isotope=function(a,c,d){this.element=b(c),this._create(a),this._init(d)};var v=["width","height"],w=b(a);b.Isotope.settings={resizable:!0,layoutMode:"masonry",containerClass:"isotope",itemClass:"isotope-item",hiddenClass:"isotope-hidden",hiddenStyle:{opacity:0,scale:.001},visibleStyle:{opacity:1,scale:1},containerStyle:{position:"relative",overflow:"hidden"},animationEngine:"best-available",animationOptions:{queue:!1,duration:800},sortBy:"original-order",sortAscending:!0,resizesContainer:!0,transformsEnabled:!0,itemPositionDataEnabled:!1},b.Isotope.prototype={_create:function(a){this.options=b.extend({},b.Isotope.settings,a),this.styleQueue=[],this.elemCount=0;var c=this.element[0].style;this.originalStyle={};var d=v.slice(0);for(var e in this.options.containerStyle)d.push(e);for(var f=0,g=d.length;f<g;f++)e=d[f],this.originalStyle[e]=c[e]||"";this.element.css(this.options.containerStyle),this._updateAnimationEngine(),this._updateUsingTransforms();var h={"original-order":function(a,b){return b.elemCount++,b.elemCount},random:function(){return Math.random()}};this.options.getSortData=b.extend(this.options.getSortData,h),this.reloadItems(),this.offset={left:parseInt(this.element.css("padding-left")||0,10),top:parseInt(this.element.css("padding-top")||0,10)};var i=this;setTimeout(function(){i.element.addClass(i.options.containerClass)},0),this.options.resizable&&w.bind("smartresize.isotope",function(){i.resize()}),this.element.delegate("."+this.options.hiddenClass,"click",function(){return!1})},_getAtoms:function(a){var b=this.options.itemSelector,c=b?a.filter(b).add(a.find(b)):a,d={position:"absolute"};return c=c.filter(function(a,b){return b.nodeType===1}),this.usingTransforms&&(d.left=0,d.top=0),c.css(d).addClass(this.options.itemClass),this.updateSortData(c,!0),c},_init:function(a){this.$filteredAtoms=this._filter(this.$allAtoms),this._sort(),this.reLayout(a)},option:function(a){if(b.isPlainObject(a)){this.options=b.extend(!0,this.options,a);var c;for(var d in a)c="_update"+f(d),this[c]&&this[c]()}},_updateAnimationEngine:function(){var a=this.options.animationEngine.toLowerCase().replace(/[ _\-]/g,""),b;switch(a){case"css":case"none":b=!1;break;case"jquery":b=!0;break;default:b=!e.csstransitions}this.isUsingJQueryAnimation=b,this._updateUsingTransforms()},_updateTransformsEnabled:function(){this._updateUsingTransforms()},_updateUsingTransforms:function(){var a=this.usingTransforms=this.options.transformsEnabled&&e.csstransforms&&e.csstransitions&&!this.isUsingJQueryAnimation;a||(delete this.options.hiddenStyle.scale,delete this.options.visibleStyle.scale),this.getPositionStyles=a?this._translate:this._positionAbs},_filter:function(a){var b=this.options.filter===""?"*":this.options.filter;if(!b)return a;var c=this.options.hiddenClass,d="."+c,e=a.filter(d),f=e;if(b!=="*"){f=e.filter(b);var g=a.not(d).not(b).addClass(c);this.styleQueue.push({$el:g,style:this.options.hiddenStyle})}return this.styleQueue.push({$el:f,style:this.options.visibleStyle}),f.removeClass(c),a.filter(b)},updateSortData:function(a,c){var d=this,e=this.options.getSortData,f,g;a.each(function(){f=b(this),g={};for(var a in e)!c&&a==="original-order"?g[a]=b.data(this,"isotope-sort-data")[a]:g[a]=e[a](f,d);b.data(this,"isotope-sort-data",g)})},_sort:function(){var a=this.options.sortBy,b=this._getSorter,c=this.options.sortAscending?1:-1,d=function(d,e){var f=b(d,a),g=b(e,a);return f===g&&a!=="original-order"&&(f=b(d,"original-order"),g=b(e,"original-order")),(f>g?1:f<g?-1:0)*c};this.$filteredAtoms.sort(d)},_getSorter:function(a,c){return b.data(a,"isotope-sort-data")[c]},_translate:function(a,b){return{translate:[a,b]}},_positionAbs:function(a,b){return{left:a,top:b}},_pushPosition:function(a,b,c){b=Math.round(b+this.offset.left),c=Math.round(c+this.offset.top);var d=this.getPositionStyles(b,c);this.styleQueue.push({$el:a,style:d}),this.options.itemPositionDataEnabled&&a.data("isotope-item-position",{x:b,y:c})},layout:function(a,b){var c=this.options.layoutMode;this["_"+c+"Layout"](a);if(this.options.resizesContainer){var d=this["_"+c+"GetContainerSize"]();this.styleQueue.push({$el:this.element,style:d})}this._processStyleQueue(a,b),this.isLaidOut=!0},_processStyleQueue:function(a,c){var d=this.isLaidOut?this.isUsingJQueryAnimation?"animate":"css":"css",f=this.options.animationOptions,g=this.options.onLayout,h,i,j,k;i=function(a,b){b.$el[d](b.style,f)};if(this._isInserting&&this.isUsingJQueryAnimation)i=function(a,b){h=b.$el.hasClass("no-transition")?"css":d,b.$el[h](b.style,f)};else if(c||g||f.complete){var l=!1,m=[c,g,f.complete],n=this;j=!0,k=function(){if(l)return;var b;for(var c=0,d=m.length;c<d;c++)b=m[c],typeof b=="function"&&b.call(n.element,a,n);l=!0};if(this.isUsingJQueryAnimation&&d==="animate")f.complete=k,j=!1;else if(e.csstransitions){var o=0,p=this.styleQueue[0],s=p&&p.$el,t;while(!s||!s.length){t=this.styleQueue[o++];if(!t)return;s=t.$el}var u=parseFloat(getComputedStyle(s[0])[r]);u>0&&(i=function(a,b){b.$el[d](b.style,f).one(q,k)},j=!1)}}b.each(this.styleQueue,i),j&&k(),this.styleQueue=[]},resize:function(){this["_"+this.options.layoutMode+"ResizeChanged"]()&&this.reLayout()},reLayout:function(a){this["_"+this.options.layoutMode+"Reset"](),this.layout(this.$filteredAtoms,a)},addItems:function(a,b){var c=this._getAtoms(a);this.$allAtoms=this.$allAtoms.add(c),b&&b(c)},insert:function(a,b){this.element.append(a);var c=this;this.addItems(a,function(a){var d=c._filter(a);c._addHideAppended(d),c._sort(),c.reLayout(),c._revealAppended(d,b)})},appended:function(a,b){var c=this;this.addItems(a,function(a){c._addHideAppended(a),c.layout(a),c._revealAppended(a,b)})},_addHideAppended:function(a){this.$filteredAtoms=this.$filteredAtoms.add(a),a.addClass("no-transition"),this._isInserting=!0,this.styleQueue.push({$el:a,style:this.options.hiddenStyle})},_revealAppended:function(a,b){var c=this;setTimeout(function(){a.removeClass("no-transition"),c.styleQueue.push({$el:a,style:c.options.visibleStyle}),c._isInserting=!1,c._processStyleQueue(a,b)},10)},reloadItems:function(){this.$allAtoms=this._getAtoms(this.element.children())},remove:function(a,b){this.$allAtoms=this.$allAtoms.not(a),this.$filteredAtoms=this.$filteredAtoms.not(a);var c=this,d=function(){a.remove(),b&&b.call(c.element)};a.filter(":not(."+this.options.hiddenClass+")").length?(this.styleQueue.push({$el:a,style:this.options.hiddenStyle}),this._sort(),this.reLayout(d)):d()},shuffle:function(a){this.updateSortData(this.$allAtoms),this.options.sortBy="random",this._sort(),this.reLayout(a)},destroy:function(){var a=this.usingTransforms,b=this.options;this.$allAtoms.removeClass(b.hiddenClass+" "+b.itemClass).each(function(){var b=this.style;b.position="",b.top="",b.left="",b.opacity="",a&&(b[i]="")});var c=this.element[0].style;for(var d in this.originalStyle)c[d]=this.originalStyle[d];this.element.unbind(".isotope").undelegate("."+b.hiddenClass,"click").removeClass(b.containerClass).removeData("isotope"),w.unbind(".isotope")},_getSegments:function(a){var b=this.options.layoutMode,c=a?"rowHeight":"columnWidth",d=a?"height":"width",e=a?"rows":"cols",g=this.element[d](),h,i=this.options[b]&&this.options[b][c]||this.$filteredAtoms["outer"+f(d)](!0)||g;h=Math.floor(g/i),h=Math.max(h,1),this[b][e]=h,this[b][c]=i},_checkIfSegmentsChanged:function(a){var b=this.options.layoutMode,c=a?"rows":"cols",d=this[b][c];return this._getSegments(a),this[b][c]!==d},_masonryReset:function(){this.masonry={},this._getSegments();var a=this.masonry.cols;this.masonry.colYs=[];while(a--)this.masonry.colYs.push(0)},_masonryLayout:function(a){var c=this,d=c.masonry;a.each(function(){var a=b(this),e=Math.ceil(a.outerWidth(!0)/d.columnWidth);e=Math.min(e,d.cols);if(e===1)c._masonryPlaceBrick(a,d.colYs);else{var f=d.cols+1-e,g=[],h,i;for(i=0;i<f;i++)h=d.colYs.slice(i,i+e),g[i]=Math.max.apply(Math,h);c._masonryPlaceBrick(a,g)}})},_masonryPlaceBrick:function(a,b){var c=Math.min.apply(Math,b),d=0;for(var e=0,f=b.length;e<f;e++)if(b[e]===c){d=e;break}var g=this.masonry.columnWidth*d,h=c;this._pushPosition(a,g,h);var i=c+a.outerHeight(!0),j=this.masonry.cols+1-f;for(e=0;e<j;e++)this.masonry.colYs[d+e]=i},_masonryGetContainerSize:function(){var a=Math.max.apply(Math,this.masonry.colYs);return{height:a}},_masonryResizeChanged:function(){return this._checkIfSegmentsChanged()},_fitRowsReset:function(){this.fitRows={x:0,y:0,height:0}},_fitRowsLayout:function(a){var c=this,d=this.element.width(),e=this.fitRows;a.each(function(){var a=b(this),f=a.outerWidth(!0),g=a.outerHeight(!0);e.x!==0&&f+e.x>d&&(e.x=0,e.y=e.height),c._pushPosition(a,e.x,e.y),e.height=Math.max(e.y+g,e.height),e.x+=f})},_fitRowsGetContainerSize:function(){return{height:this.fitRows.height}},_fitRowsResizeChanged:function(){return!0},_cellsByRowReset:function(){this.cellsByRow={index:0},this._getSegments(),this._getSegments(!0)},_cellsByRowLayout:function(a){var c=this,d=this.cellsByRow;a.each(function(){var a=b(this),e=d.index%d.cols,f=Math.floor(d.index/d.cols),g=(e+.5)*d.columnWidth-a.outerWidth(!0)/2,h=(f+.5)*d.rowHeight-a.outerHeight(!0)/2;c._pushPosition(a,g,h),d.index++})},_cellsByRowGetContainerSize:function(){return{height:Math.ceil(this.$filteredAtoms.length/this.cellsByRow.cols)*this.cellsByRow.rowHeight+this.offset.top}},_cellsByRowResizeChanged:function(){return this._checkIfSegmentsChanged()},_straightDownReset:function(){this.straightDown={y:0}},_straightDownLayout:function(a){var c=this;a.each(function(a){var d=b(this);c._pushPosition(d,0,c.straightDown.y),c.straightDown.y+=d.outerHeight(!0)})},_straightDownGetContainerSize:function(){return{height:this.straightDown.y}},_straightDownResizeChanged:function(){return!0},_masonryHorizontalReset:function(){this.masonryHorizontal={},this._getSegments(!0);var a=this.masonryHorizontal.rows;this.masonryHorizontal.rowXs=[];while(a--)this.masonryHorizontal.rowXs.push(0)},_masonryHorizontalLayout:function(a){var c=this,d=c.masonryHorizontal;a.each(function(){var a=b(this),e=Math.ceil(a.outerHeight(!0)/d.rowHeight);e=Math.min(e,d.rows);if(e===1)c._masonryHorizontalPlaceBrick(a,d.rowXs);else{var f=d.rows+1-e,g=[],h,i;for(i=0;i<f;i++)h=d.rowXs.slice(i,i+e),g[i]=Math.max.apply(Math,h);c._masonryHorizontalPlaceBrick(a,g)}})},_masonryHorizontalPlaceBrick:function(a,b){var c=Math.min.apply(Math,b),d=0;for(var e=0,f=b.length;e<f;e++)if(b[e]===c){d=e;break}var g=c,h=this.masonryHorizontal.rowHeight*d;this._pushPosition(a,g,h);var i=c+a.outerWidth(!0),j=this.masonryHorizontal.rows+1-f;for(e=0;e<j;e++)this.masonryHorizontal.rowXs[d+e]=i},_masonryHorizontalGetContainerSize:function(){var a=Math.max.apply(Math,this.masonryHorizontal.rowXs);return{width:a}},_masonryHorizontalResizeChanged:function(){return this._checkIfSegmentsChanged(!0)},_fitColumnsReset:function(){this.fitColumns={x:0,y:0,width:0}},_fitColumnsLayout:function(a){var c=this,d=this.element.height(),e=this.fitColumns;a.each(function(){var a=b(this),f=a.outerWidth(!0),g=a.outerHeight(!0);e.y!==0&&g+e.y>d&&(e.x=e.width,e.y=0),c._pushPosition(a,e.x,e.y),e.width=Math.max(e.x+f,e.width),e.y+=g})},_fitColumnsGetContainerSize:function(){return{width:this.fitColumns.width}},_fitColumnsResizeChanged:function(){return!0},_cellsByColumnReset:function(){this.cellsByColumn={index:0},this._getSegments(),this._getSegments(!0)},_cellsByColumnLayout:function(a){var c=this,d=this.cellsByColumn;a.each(function(){var a=b(this),e=Math.floor(d.index/d.rows),f=d.index%d.rows,g=(e+.5)*d.columnWidth-a.outerWidth(!0)/2,h=(f+.5)*d.rowHeight-a.outerHeight(!0)/2;c._pushPosition(a,g,h),d.index++})},_cellsByColumnGetContainerSize:function(){return{width:Math.ceil(this.$filteredAtoms.length/this.cellsByColumn.rows)*this.cellsByColumn.columnWidth}},_cellsByColumnResizeChanged:function(){return this._checkIfSegmentsChanged(!0)},_straightAcrossReset:function(){this.straightAcross={x:0}},_straightAcrossLayout:function(a){var c=this;a.each(function(a){var d=b(this);c._pushPosition(d,c.straightAcross.x,0),c.straightAcross.x+=d.outerWidth(!0)})},_straightAcrossGetContainerSize:function(){return{width:this.straightAcross.x}},_straightAcrossResizeChanged:function(){return!0}},b.fn.imagesLoaded=function(a){function h(){a.call(c,d)}function i(a){var c=a.target;c.src!==f&&b.inArray(c,g)===-1&&(g.push(c),--e<=0&&(setTimeout(h),d.unbind(".imagesLoaded",i)))}var c=this,d=c.find("img").add(c.filter("img")),e=d.length,f="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==",g=[];return e||h(),d.bind("load.imagesLoaded error.imagesLoaded",i).each(function(){var a=this.src;this.src=f,this.src=a}),c};var x=function(b){a.console&&a.console.error(b)};b.fn.isotope=function(a,c){if(typeof a=="string"){var d=Array.prototype.slice.call(arguments,1);this.each(function(){var c=b.data(this,"isotope");if(!c){x("cannot call methods on isotope prior to initialization; attempted to call method '"+a+"'");return}if(!b.isFunction(c[a])||a.charAt(0)==="_"){x("no such method '"+a+"' for isotope instance");return}c[a].apply(c,d)})}else this.each(function(){var d=b.data(this,"isotope");d?(d.option(a),d._init(c)):b.data(this,"isotope",new b.Isotope(a,this,c))});return this}})(window,jQuery);
jQuery(window).load(function(){
	var filters = {}, $container = jQuery('.stream');
	jQuery('.filter a').click(function(){
      	var $i = jQuery(this), isoFilters = [], prop, selector, $a = $i.parents('.dcsns-toolbar'), $b = $a.next(), $c = jQuery('.stream',$b);
		$i.parent("li").addClass("active").siblings().removeClass("active");
		jQuery('.filter a',$a).removeClass('iso-active');
		$i.addClass('iso-active');
      	filters[ $i.data('group') ] = $i.data('filter');
      	for (prop in filters){
        	isoFilters.push(filters[ prop ])
      	}
      	selector = isoFilters.join('');
      	$c.isotope({filter: selector, sortBy : 'postDate'});
      	return false;
    });
	$container.isotope().isotope('layout');
});;
/* =================================
   Mind Tree Activation Scripts
================================== */
var $jq                 = jQuery.noConflict(),
    isoCont             = "",
    isotopeOpt          = "",
    invInnerDivIso      = "",
    invOuterDivIso      = "", 
    seinvInnerDivIso    = "",
	seinvOuterDivIso    = "", 
    isoVdivs        = [];
$jq(document).ready(function() {
    //Mobile Menu
    $jq('#dl-menu').dlmenu();  
    //DatePicker
    $jq('.datepicker, .dob').datetimepicker({
        timepicker:false,
		format:'d/m/Y',
		maxDate:'2000/12/31',
	    defaultDate:'1990/01/01'
    });
    //Main Menu Animation Begin
    $jq("body").addClass("menu-expanded menu-active-0");
    $jq(".menu-nav-control").click(function() {
        $jq("body").toggleClass("menu-expanded");     
    });
    //Menu Search
    $jq(".search-bar .fulldrop").addClass("close");
    $jq(".search-icon").click(function(e) {
        var dis = $jq(this),
            fd  = dis.parents(".search-bar").find(".fulldrop");
        if(fd.hasClass("opend")) {
            fd.addClass("close").removeClass("opend");
            dis.parents(".search-bar").removeClass("active");
            dis.removeClass("fa-close").addClass("fa-search");
        } else if(fd.hasClass("close")) {
            fd.addClass("opend").removeClass("close");
            dis.parents(".search-bar").addClass("active");
            dis.removeClass("fa-search").addClass("fa-close");
        }
    });
    //Zinnov Accordion
    $jq('#DigitalServices').find('.accordion-toggle').click(function(){
     $jq(this).toggleClass("active").next().slideToggle('fast').siblings(".accordion-content").slideUp('slow');
     $jq(".accordion-toggle").not($jq(this)).removeClass("active");
     $jq(".accordion-content").not($jq(this).next()).slideUp('slow');
    });
    //TTH Page Toggle 
    $jq("#TthWrapper .client-each-block").click(function(){
      var dis = $jq(this),
              disTarget = dis.attr("data-href");
      $jq(disTarget).slideToggle().siblings(".ttheachcontents").slideUp();
      dis.toggleClass("active").parent().siblings().find(".client-each-block").removeClass("active");
    });
    $jq('.tth-popup-close-cta').click(function(){
      $jq('.ttheachcontents').hide();
      $jq(".client-each-block.active").removeClass("active");
    });
    //Imts Script
    $jq("#ImtsForm label").inFieldLabels();
    $jq("#ImtsForm").simpleValidation();
    $jq("#ImtsForm input.required,#ImtsForm select.required").not(".no-placholder-css").focus("focus", function() {
       var dis = $jq(this);
       (dis.prop("tagName") == "SELECT") ? dis.parent().removeClass("placholder-css"): dis.removeClass("placholder-css");
    }).blur(function() {
       var dis = $jq(this),
           target = (dis.prop("tagName") == "SELECT") ? dis.parent() : dis;
       dis.val() == "" ? target.addClass("placholder-css") : target.removeClass("placholder-css");
    });
    $jq("[name='degree']").click(function() {
     var disVal = $jq("[name='degree']:checked").val(),
         bscSpec = $jq("#bsc_spec"),
         othSpec = $jq("#others_spec");
     if(disVal == "be/btech") {
       bscSpec.show();
       othSpec.hide();
     } else if(disVal == "others") {
       othSpec.show();
       bscSpec.hide();
     } else {
       othSpec.add(bscSpec).hide();
     }
    });
    $jq("[type='reset']").click(function() {
      var dis      = $jq(this),
          disForm  = dis.parents("form");
          disForm.find("input").not("input[type='reset'],input[type='checkbox'],input[type='radio'],input[type='submit']").attr("value","").val("").blur();
          disForm.find("textarea").html("").val("").blur();
          disForm.find("select option").removeAttr('selected');
          disForm.find("select").val("").blur();
          disForm.find(".errormsg").remove();
    });
    //Get URL parameter value
    function getParameterByName(name) {
        name        = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex   = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    //Set offset for sticky header in service pages
    function updateServicesubmenutabOffset() {
        var subMenu         = $jq(".field-name-field-services-sub-menu");
        if(subMenu.size() > 0)
            serSMdivOffset  = subMenu.offset();
    }
    //On scroll : sticky headers
    var imtw            = $jq("#IndustryMainTabsWrapper"),
        indTser         = $jq("#industry_services"),
        indTsol         = $jq("#industry_solutions"),
        indTper         = $jq("#industry_perspectives"),
        serviceSM       = $jq(".field-name-field-services-sub-menu"),
        careesLsSmenu   = $jq(".careers-ls-menu-wrapper"),
        indDivOffset    = (imtw.length > 0) ? imtw.offset() : 0;
        clsDivOffset    = (careesLsSmenu.length > 0) ? careesLsSmenu.offset() : 0;
        serSMdivOffset  = (serviceSM.length > 0) ? serviceSM.offset() : 0;
    function stickyHeader() {
        var cScrollv    = $jq(document).scrollTop(),
            header      = $jq("#Header"),
            headerHei   = header.outerHeight(),
            mncontrol   = header.find(".menu-nav-control"),
            body        = $jq("body"),
            classes     = ['menu-expanded','menu-active-0','menu-active-1','carrer-ls-ssmenu'],  
            hidemenuV   = (careesLsSmenu.size() > 0) ? 25 : 215,   
            industryDiv = $jq("#IndustryMainTabsWrapper"),
            serviceDiv  = $jq(".field-name-field-services-sub-menu"),    
            indDivOuter = $jq("#IndustryMainTabsWrapper").parent(),
            serviceSMn  = serviceDiv.next(".container");
        if(cScrollv >= hidemenuV) {
            if(!body.hasClass(classes[2])) {
                body.addClass(classes[2]);                       
            }
            else {
                if(body.hasClass(classes[0]) || body.hasClass(classes[1])) {
                    body.removeClass(classes[0]+" "+classes[1]).addClass(classes[2]);                                               
                }   
            }
            if(careesLsSmenu.size() > 0) {
                mncontrol.fadeOut();
            }
        } else {  
            body.removeClass(classes[2]);
            if(!body.hasClass(classes[0]) || !body.hasClass(classes[1])) 
                body.addClass(classes[0]+" "+classes[1]);
                if(careesLsSmenu.size() > 0) {
                    mncontrol.fadeIn();
                }
        }  
        //Sticky Submenu - career long menu
        if(careesLsSmenu.size() > 0) {
            if(cScrollv >= clsDivOffset.top) {
                if(!body.hasClass(classes[3]))
                    body.addClass(classes[3]);
            } else {
                if(body.hasClass(classes[3]))
                    body.removeClass(classes[3]);
            }
        }
        //Sticky Industry tab
        if(imtw.length > 0) {            
            //hide menu nav control 
            if(careesLsSmenu.size() <= 0) {
                ((cScrollv >= (indDivOffset.top - headerHei)) && (cScrollv <= (indDivOffset.top + indDivOuter.outerHeight() - industryDiv.outerHeight()))) ? mncontrol.fadeOut() : mncontrol.fadeIn();
            }
            //Sticky Industry tab
            if((cScrollv >= indDivOffset.top) && (cScrollv <= (indDivOffset.top + indDivOuter.outerHeight() - industryDiv.outerHeight()))) {
                if(!industryDiv.hasClass("sticky-header"))
                    industryDiv.addClass("sticky-header");
                if(!indDivOuter.hasClass("sticky"))
                    indDivOuter.addClass("sticky");
            } else {
                if(industryDiv.hasClass("sticky-header"))
                    industryDiv.removeClass("sticky-header");
                if(indDivOuter.hasClass("sticky"))
                    indDivOuter.removeClass("sticky");
            }
            if(cScrollv > (indTper.offset().top - industryDiv.outerHeight())) {
                $jq(".smooth_scroll").removeClass("active");
                $jq("[href='#industry_perspectives']").addClass("active");
            } else if(cScrollv > (indTsol.offset().top - industryDiv.outerHeight())) {
                $jq(".smooth_scroll").removeClass("active");
                $jq("[href='#industry_solutions']").addClass("active");    
            } else if(cScrollv > (indTser.offset().top - industryDiv.outerHeight())) {
                $jq(".smooth_scroll").removeClass("active");
                $jq("[href='#industry_services']").addClass("active");
            }
        }
         /*Carrer long scroll auto active*/
        var careerlsDiv = $jq(".careers-ls-menu-wrapper");
        if($jq(".careers-ls-menu").size() > 0) {
            if(cScrollv > ($jq("#CampusCareerScroll").offset().top - careerlsDiv.outerHeight())) {
                $jq(".smooth_scroll").removeClass("active");
                $jq("[href='#CampusCareerScroll']").addClass("active");
            } else if(cScrollv > ($jq("#AwardsWrapper").offset().top - careerlsDiv.outerHeight())) {
                $jq(".smooth_scroll").removeClass("active");
                $jq("[href='#AwardsWrapper']").addClass("active");
            } else if(cScrollv > ($jq("#LeaderShipCareerScroll").offset().top - careerlsDiv.outerHeight())) {
                $jq(".smooth_scroll").removeClass("active");
                $jq("[href='#LeaderShipCareerScroll']").addClass("active");
            } else if(cScrollv > ($jq("#GetReferredMindtree").offset().top - careerlsDiv.outerHeight())) {
                $jq(".smooth_scroll").removeClass("active");
                $jq("[href='#GetReferredMindtree']").addClass("active");
            } else if(cScrollv > ($jq("#CSLViewCareer").offset().top - careerlsDiv.outerHeight())) {
                $jq(".smooth_scroll").removeClass("active");
                $jq("[href='#CSLViewCareer']").addClass("active");
            } else if(cScrollv > ($jq("#AlumniWrapper").offset().top - careerlsDiv.outerHeight())) {
                $jq(".smooth_scroll").removeClass("active");
                $jq("[href='#AlumniWrapper']").addClass("active");
            } else if(cScrollv > ($jq("#MeetMindsCareerScroll").offset().top - careerlsDiv.outerHeight())) {
                $jq(".smooth_scroll").removeClass("active");
                $jq("[href='#MeetMindsCareerScroll']").addClass("active");
            } else if(cScrollv > ($jq("#WhyWorkWithUsCLS").offset().top - careerlsDiv.outerHeight())) {
                $jq(".smooth_scroll").removeClass("active");
                $jq("[href='#WhyWorkWithUsCLS']").addClass("active");
            } 
        }
        //Sticky Service Tab
        if(serviceDiv.size() > 0) {
            if(careesLsSmenu.size() <= 0) {
                ((cScrollv >= (serSMdivOffset.top - headerHei)) && (cScrollv <= (serSMdivOffset.top + serviceSMn.outerHeight() - serviceDiv.outerHeight()))) ? mncontrol.fadeOut() : mncontrol.fadeIn();
            }
            if((cScrollv >= serSMdivOffset.top) && (cScrollv <= (serSMdivOffset.top + serviceSMn.outerHeight()))) {
                if(!serviceDiv.hasClass("sticky-header"))
                    serviceDiv.addClass("sticky-header");
                if(!serviceSMn.hasClass("sticky")) {
                    serviceSMn.css("padding-top",serviceDiv.outerHeight(true)+"px");
                    serviceSMn.addClass("sticky");
                }
            } else {
                if(serviceDiv.hasClass("sticky-header"))
                    serviceDiv.removeClass("sticky-header");
                if(serviceSMn.hasClass("sticky"))
                    serviceSMn.removeAttr("style");
                    serviceSMn.removeClass("sticky");
            }
        }
    }
    //Contact page popup
    $jq(".popup_form_inline").fancybox({
		maxWidth	: 530,
		maxHeight	: 545,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
    $jq(".popup_tc_inline").fancybox({
		maxWidth	: 530,
		maxHeight	: 315,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	//careers popup
	$jq(".popup_careers_inline").fancybox({
         maxWidth : 350,
         maxHeight : 30,
         fitToView : false,
         width  : '70%',
         height  : '70%',
         autoSize : false,
         closeClick : false,
         openEffect : 'none',
         closeEffect : 'none'
    });
    $jq(".popup_iframe").fancybox({
        type : 'iframe',
        autoSize : true,
        padding : 0
    });
    //Podcast Player
    $jq("#podcasts-player").fancybox({
        "width":440
    });
    if($jq(window).width() >= 765) {
        //Image Pop-up Activated
        $jq("a.img-pop").fancybox();
        //Video Pop-up Activated
        $jq("a.video-pop").fancybox({
            helpers : {
                media: true
            }
        });
        //Gallery Pop-up Activated
        $jq(".gallerypopup").fancybox({
            prevEffect  : 'none',
            nextEffect  : 'none',
            helpers : {
                title   : {
                    type: 'outside'
                },
                thumbs  : {
                    width   : 50,
                    height  : 50
                }
            }
        });
        //StickyHeader
        if(!$jq("body").hasClass("front")) {
            stickyHeader();
            $jq(window).scroll(stickyHeader);
        }
    } else {
        $jq("a.img-pop").click(function(e) {
            e.preventDefault();
        })
    }
    // Scroll Reveal 
    if ($jq(window).width() > 1024) {
        var config = {
            after: '0s',
            enter: 'bottom',
            move: '24px',
            over: '0.66s',
            easing: 'ease-in-out',
            viewportFactor: 0.33,
            reset: false,
            init: true
        };
        window.scrollReveal = new scrollReveal({
            reset: false
        });
    } 
    //START : Enterprise Application Services menu
    function scrollToViewport() {
        var target          = $jq("body").find(".sticky");
        if(target.size() > 0) {
            var scrollTo    = $jq(target).offset().top,
                doc         = $jq('html, body'),
                diff        = Math.abs(doc.scrollTop() - scrollTo);
            if(diff > 5) 
                doc.animate({ scrollTop: scrollTo }, 600,function() {
                    updateServicesubmenutabOffset();
                });
        }
    }
    $jq("#ServicesSubMenuTop ul li a,#ServicesSubMenuTop ul li span").click(function(e) {
        if(!$jq(this).hasClass("SubMenuExternal")) {
            e.preventDefault();
            var dis         = $jq(this),
                target      = dis.attr("href");
                target      = (typeof(target) != "undefined") ? target : dis.attr("data-href");
            var ssubmenu    = $jq("[data-role-submenu='"+target+"']"),
                ssubmenuall = $jq(".ServicesSubMenuBottom"),
                subCont     = $jq("#"+target);
                dis.parent().addClass("active").siblings().removeClass("active");
                subCont.show().siblings().hide();
                ssubmenuall.hide();
            if(ssubmenu.size() > 0) {
                ssubmenu.show().siblings(".ServicesSubMenuBottom").hide();
                var starget = ssubmenu.find(".SubMenuTabLinks ul li.active-sub a").attr("href");
                    starget = (typeof(starget) != "undefined") ? starget : ssubmenu.find(".SubMenuTabLinks ul li.active-sub span").attr("data-href");
                subCont.find("#"+starget).show().siblings().hide();
            }
            if(dis.parents(".cls-che").size() > 0) {
                var dispartar = dis.parents("#CampusCareerScroll");
                (target == "CampusOffCampus") ? dispartar.addClass("CampusOffCampusbg") : dispartar.removeClass("CampusOffCampusbg")
            }
            updateServicesubmenutabOffset();
        }
        scrollToViewport();
    });
    $jq(".ServicesSubMenuBottom ul li a,.ServicesSubMenuBottom ul li span").click(function(e) {
        if(!$jq(this).hasClass("SubMenuExternal")) {
            e.preventDefault();
            var dis         = $jq(this),
                target      = dis.attr("href"),
                target      = (typeof(target) != "undefined") ? target : dis.attr("data-href"),
                subCont     = $jq("#"+target);
                dis.parent().addClass("active-sub").siblings().removeClass("active-sub");
                $jq("#"+target).show().siblings().hide();
        }
        updateServicesubmenutabOffset();
        scrollToViewport();
    });
    $jq(".externaltablink").click(function() {
        var dis     = $jq(this),
            target  = dis.attr("data-href");
            $jq("#ServicesSubMenuTop ul li span[data-href='"+target+"'],.ServicesSubMenuBottom ul li span[data-href='"+target+"']").click();
    });
    var active_tab = getParameterByName("tab");
    if(active_tab != "" && typeof(active_tab) != "undefined") {
        $jq("#ServicesSubMenuTop ul li:visible:eq("+(active_tab - 1)+") a,#ServicesSubMenuTop ul li:visible:eq("+(active_tab - 1)+") span").click();
        $jq(".mobile-cdropdown .mobile-cdd-list ul li:not('.hidden'):eq("+(active_tab - 1)+")").addClass("cdd-active").siblings().removeClass("cdd-active");
    } else if($jq("#ServicesSubMenuTop").is(":visible")) {
        $jq("#ServicesSubMenuTop ul li.active a,#ServicesSubMenuTop ul li.active span").click();
    }
    //Active tab through script
    //END : Enterprise Application Services menu
    //Leadership,Board of directors
    var lsDiv = $jq(".view-management-team-listing .view-content,.view-board-of-director-s-team-listing .view-content");
    if(lsDiv.size() > 0) {
        lsDiv.isotope({ itemSelector: '.views-row', layoutMode: 'fitRows' });
    }
    
    //START : Floating filter div
    var floatingDivtarget = "#FilterContaineLHS .sfloatingdiv .region .block-views .content,#EventsFilterContaineLHS .sfloatingdiv .region .block-views .content",
        lshipScroll       = $jq("#block-views-leader-ship-view-block"),
        iarScroll         = $jq(".region-annual-report-2013-14-filter-bar,.region-annual-report-2012-13-filter-bar,.region-annual-report-2014-15-filter-bar,.region-annual-report-2015-16-filter-bar");
    if(lshipScroll.size() > 0) {
        floatingDivtarget = "#LeaderFilterContaineLHS .sfloatingdiv .region .block-views .content";
    } else if(iarScroll.size() > 0) {
        floatingDivtarget = "#LeaderFilterContaineLHS .sfloatingdiv .region .block .content";
    }
    var initialtop = addVal = viewportH = cddFourH = 0;
    function floatingLeftdiv() {
        var leftDiv     = $jq(floatingDivtarget).parents(".floatingdiv"),
            leftPdiv    = $jq(floatingDivtarget).parent(),
            rightDiv    = leftDiv.next(".reg-content"),
            filterDiv   = $jq(floatingDivtarget),
            cddDiv      = filterDiv.parent().prev("div"),
            cddFour     = $jq("#FourFiltersTOP").parents(".row");
        //START : Clear all prev data
            rightDiv.css({"position":"","left":""});
            leftDiv.css({"position":"","left":"","top":"","height":""});
            filterDiv.css({"height":""});
            leftPdiv.css({"height":""});
            initialtop  = addVal = viewportH = cddFourH = 0;
        //END : Clear all prev data
        cddFourH        = (cddFour.size() > 0) ? cddFour.outerHeight() : 0;
        addVal          = $jq("#Header").outerHeight() + $jq("#PageHeader").outerHeight();
        var wHeight     = $jq(window).height(),
            extraRed    = (cddFourH == 0) ? 42 : 20;
            sHeight     = wHeight - cddDiv.outerHeight(true) - extraRed - cddFourH;
            viewportH   = (rightDiv.outerHeight() > (wHeight - addVal));
            ldPosition  = leftDiv.offset(),
            rdPosition  = rightDiv.offset();
        if(viewportH && $jq(window).width() > 1024) {
            rightDiv.css({"position":"relative","left":leftDiv.outerWidth()+"px"});
            leftDiv.css({"position":"fixed","left":ldPosition.left+"px","top":ldPosition.top+"px","height":(wHeight-cddFourH)+"px"});
            filterDiv.css({"height":(sHeight)+"px"});
            leftPdiv.css({"height":(sHeight+10)+"px"});
            //update int value
            initialtop  = leftDiv.position().top;
            onScrollFD();
            $jq(window).scroll(onScrollFD);
        }
    }    
    function onScrollFD() {
        var leftDiv                 = $jq(floatingDivtarget).parents(".floatingdiv"),
            scroll_top              = $jq(this).scrollTop(),
            height_element_parent   = leftDiv.parents(".container").outerHeight(),
            height_element          = leftDiv.outerHeight(),
            position_fixed_max      = height_element_parent - height_element + addVal - cddFourH,
            position_fixed          = (scroll_top < initialtop) ? (initialtop - scroll_top) : ((position_fixed_max > scroll_top) ? (0 + cddFourH) : (position_fixed_max - scroll_top)) ;
        leftDiv.css("top",position_fixed);
        if(cddFourH > 0) {
            var cddFourDiv          = $jq("#FourFiltersTOP").parents(".row");
            if(position_fixed == (0 + cddFourH)) {
                if(!cddFourDiv.hasClass("sticky-fcdd"))
                    cddFourDiv.addClass("sticky-fcdd").next(".row").addClass("sticky-fcdd-cont");
            }
            else {
                if(cddFourDiv.hasClass("sticky-fcdd"))
                    cddFourDiv.removeClass("sticky-fcdd").next(".row").removeClass("sticky-fcdd-cont");
            }

        }
    }
    if(($jq(floatingDivtarget).parents(".floatingdiv").size() > 0) && ($jq(window).width() > 1024)) {
        floatingLeftdiv();
        $jq(window).resize(floatingLeftdiv).load(floatingLeftdiv); 
    }
    //END : Floating filter div
    //For mobile : expand the left filter
    $jq("#FilterContaineLHS .sfd-arrow,#LeaderFilterContaineLHS .sfd-arrow,#EventsFilterContaineLHS .sfd-arrow").click(function() {
        $jq(".floatingdiv").toggleClass("opened");
        var Lscrollbar = $jq(floatingDivtarget).getNiceScroll();
        if($jq(".floatingdiv").hasClass("opened")) {
            Lscrollbar.show();
            $jq(this).html("<<");
        } else {
            Lscrollbar.hide();            
            $jq(this).html(">>");
        }        
    });
    //Set offset for sticky header in Industry page
    var indDivOffset     = $jq("#IndustryMainTabsWrapper").offset();
    function updateIndustrymaintabOffset() {
        var indDiv       = $jq("#IndustryMainTabsWrapper");
        if(indDiv.size() > 0) 
            indDivOffset = indDiv.offset();
    }
    //Set offset for sticky header in career long scroll
    var clsDivOffset     = $jq(".careers-ls-menu-wrapper").offset();
    function updateCareerLSsmaintabOffset() {
        var clsDiv       = $jq(".careers-ls-menu-wrapper");
        if(clsDiv.size() > 0) 
            clsDivOffset = clsDiv.offset();
    }   
    //Industry & service page Add more 
    $jq(".iwt-more").click(function(event) {
        event.preventDefault();
        var target = $jq(this).attr("data-target") ;
        $jq(target).slideToggle("medium",function() {
            $jq(target).is(":visible") ? $jq(".iwt-more").html("Less") : $jq(".iwt-more").html("More");
            $jq('html, body').animate({
                scrollTop: ($jq("#IndustryWelcomeText").offset().top)
            }, 200);
            ($jq(".field-name-field-services-sub-menu").size() > 0) ? updateServicesubmenutabOffset() : updateIndustrymaintabOffset();
        });       
    });
    //Service Page popup
    $jq.fn.spopupactivation = function(e) {        
        var indSolu = $jq(this).parents(".industry-solutions,.industry-solutions-popup").size(),
            popDiv  = (indSolu > 0) ? ".industry-solutions-popup" : ".industry-services-popup",
            serDiv  = (indSolu > 0) ? ".industry-solutions" : ".industry-services",
            isp     = $jq(popDiv),
            is      = $jq(serDiv),
            active  = 1;
        (indSolu > 0) ? $jq("[href='#industry_solutions']").click() : $jq("[href='#industry_services']").click();
        if(isp.is(":visible")) {
            isp.hide();
            is.show();
        } else {
            if($jq(this).hasClass("industry-content-box-cta"))
                active = $jq(this).find("a").attr("data-active");
            else if($jq(this).find(".industry-content-box-cta"))
                active = $jq(this).find(".industry-content-box-cta a").attr("data-active");
            isp.show().find("ul.hcarousel").trigger("slidermove",active);
            is.hide();
        }
    }
    //Trigger Service Page popup on click of content box 
    $jq(".industry-services .industry-content-box,.industry-solutions .industry-content-box").click(function(e) {
        $jq(this).spopupactivation(e);
    });
    //Service Page popup close on click of close button
    $jq(".industry-popup-close-cta").click(function() {
        var servicepopup = $jq(this).parents(".industry-services-popup,.industry-solutions-popup");
        if(servicepopup.size() > 0) {
            servicepopup.hide().next().show();
        }
    })
    //Management
    var lm_classes = ["dark","light"];
    function lm_classSwap() {
        var lm_classes_dummy = lm_classes[1];
        lm_classes[1] = lm_classes[0];
        lm_classes[0] = lm_classes_dummy;
    }
    function leadershipmanagementInt() {
        var row = 0, prevRow = 0, classIndex = 0;
        $jq(".team-image-tiles .teams-each-block").removeClass(lm_classes[0]+" "+lm_classes[1]).each(function() {
            var dis     = $jq(this);
                prevRow    = row;
            if(dis.position().left == 0)
                row+=1;
            if(row != prevRow) {
                lm_classSwap();
                classIndex = 0;
            }
            dis.addClass(lm_classes[classIndex]);
            classIndex = classIndex == 0 ? 1 : 0;
        });
    }
    leadershipmanagementInt();
    $jq(window).resize(leadershipmanagementInt);
    // Leadership Page bx-slider
    $jq('.leadership-carousel').bxSlider({
        minSlides: 1,
        maxSlides: Math.floor($jq(".leadership-carousel").outerWidth() / 260),
        slideWidth: 260,
        slideMargin: 0,
        auto: $jq(window).width() > 767 ? false : true,
        moveSlides: 1,
        hideControlOnEnd: $jq(window).width() < 767 ? false : true,
        infiniteLoop: $jq(window).width() < 767 ? true : false,
        pager: false
    });
    // Leadership Page Slider Shadow Function
    $jq(".team-top-box:odd" ).addClass("odd-even-shadow");
    /*Industry Carousel Script Begin*/
    $jq.fn.hcarousel = function(opts) {
        var hcarousel_total     = $jq(this).length,
            current_hcarousel   = 1;
        $jq(this).each(function() {
            var hcarousel       = $jq(this),
                hcNav           = hcarousel.attr("data-nav"),
                disClass        = hcarousel.attr("class"),
                hclis           = hcarousel.find("> li"),
                totalli         = hclis.length,
                liwidth         = hclis.outerWidth(),
                ulwidth         = totalli * liwidth,
                outerDiv        = $jq("<div/>",{
                                     class : "hcarousel_outer"
                                  }),
                defaultopts     = { "uvactive":"1","callback":"" },
                options         = $jq.extend({}, defaultopts, opts);
                uvactive        = options.uvactive;                
                outerDiv.width(liwidth);
                hcarousel.width(ulwidth);
                hclis.width(liwidth);
                hcarousel.wrap(outerDiv);
            hcarousel.bind("slidermove",function(event,uvactive) {
                var active      = (uvactive > totalli ) ? 0 : (uvactive - 1);
                hcarousel.height(hclis.eq(active).outerHeight());
                hclis.removeClass("active").hide().eq(active).addClass("active").show();
            });
            hcarousel.trigger("slidermove",[uvactive]);
            $jq(hcNav).find("a").click(function(e) {        
                e.preventDefault();
                var chcarousel      = $jq("[data-nav='"+hcNav+"']"),
                    chclis          = chcarousel.find("> li"),
                    cValue          = $jq(this).attr("data-nav"),
                    current_left    = chcarousel.position().left,
                    cactive         = chcarousel.find(" > li.active");
                if(cValue == "next") {
                    var active  = cactive.next("li");
                        active  = (active.size() <= 0) ? cactive.parent("ul").find(" > li:first-child") : active;
                } else if(cValue == "prev") {
                    var active  = cactive.prev("li"); 
                        active  = (active.size() <= 0) ? cactive.parent("ul").find(" > li:last-child") : active;
                }
                hcarousel.height(active.outerHeight());
                cactive.fadeOut(1000,function() {
                    $jq(this).removeClass("active");
                });
                active.fadeIn(1000,function() {
                    $jq(this).addClass("active");
                });
            });
            if((current_hcarousel >= hcarousel_total) && (typeof(options.callback) == "function"))
                options.callback();
            current_hcarousel = current_hcarousel + 1;
        });
    };
    $jq(".hcarousel").hcarousel({
        "callback":function() {
            $jq(".industry-services-popup,.industry-solutions-popup").hide();
        }
    });
    $jq(".smooth_local_scroll").click(function(e) {
        e.preventDefault();
        var target   = $jq(this).attr("href"),
            scrollTo = $jq(target).offset().top,
            doc      = $jq('html, body'),
            diff     = Math.abs(doc.scrollTop() - scrollTo);
        if(diff > 5)
            doc.animate({ scrollTop: scrollTo }, 600);
    });
    $jq(".smooth_scroll").click(function(e) {
        e.preventDefault();
        $jq(".smooth_scroll").removeClass("active");
        $jq(this).addClass("active");
        var target   = $jq(this).attr("href"),
            thisPar  = $jq(this).parents(".careers-ls-menu-wrapper"),
            ouHei    = thisPar.size() > 0 ? thisPar.height() - 5 : $jq(this).outerHeight(),
            scrollTo = $jq(target).offset().top - ouHei,
            doc      = $jq('html, body'),
            diff     = Math.abs(doc.scrollTop() - scrollTo);
        if(diff > 5)
            doc.animate({ scrollTop: scrollTo }, 600);
    });
    //Tabs
    $jq.fn.tabs = function(opts) {
        return $jq(this).each(function() {
            var tabOuter        = $jq(this),
                tabNavs         = tabOuter.find("> .tabnavs"),
                tabNavsLis      = tabNavs.find("> ul > li"),
                tabConts        = tabOuter.find("> .tabcontents"),
                tabContsDivs    = tabConts.find("> div"),
                defaultopts     = { "navpos":"top",
                                    "activetabindex":"0",
                                    "activeclass":"active" },
                options         = $jq.extend({}, defaultopts, opts);
            //Add class for css style
            tabOuter.addClass("tab_nav"+options.navpos); 
            tabNavs.addClass("tabnavs"+options.navpos); 
            tabConts.addClass("tabconts"+options.navpos); 
            //Hide all content divs
            tabContsDivs.hide();
            //Add click event to tab navs
            tabNavsLis.click(function() {
                var dis     = $jq(this),
                    thisid  = dis.attr("id"),
                    target  = tabConts.find("> #"+thisid+"_contents");
                tabContsDivs.hide();
                tabNavsLis.removeClass(options.activeclass);
                dis.addClass(options.activeclass);
                target.show();
            });
            //Default active tab
            tabNavsLis.eq(options.activetabindex).click();
        });
    }
    $jq(".industry-popup-tabs").tabs({
        "navpos":"left",
        "activeclass":"active_tab2"
    });
    /*Industry Script End*/
    //Full Page
    if($jq('body').hasClass("front")) {
        if ($jq(window).width() > 1024) {
            $jq('#fullpage').fullpage({
                'verticalCentered': true,
                'css3': true,
                'navigation': true,
                'navigationPosition': 'right',
                afterRender: function(){
                    //Home page slider for mobile
                    var sliderDiv      = $jq('#HomeMobileBanner > ul');
                    sliderDiv.hide();
                    var sectionHeight  = $jq("#fullpage .section:eq(0)").height();
                    sliderDiv.show();
                    $jq(window).load(function() {
                        sliderDiv.bxSlider({
                          auto: true,
                          mode: 'fade',
                          controls:false,
                          autoControls:false,
                          pause:6000,
                          onSliderLoad:function() {
                            var sliderViewport = $jq("#HomeMobileBanner .bx-wrapper .bx-viewport");                                
                            sliderViewport.height(sectionHeight);
                          }
                        });
                    });
                },
                /*afterRender: function(){
                    idInterval = setInterval(function(){
                            $jq.fn.fullpage.moveSlideRight();
                    }, 5000);
                },
                slidesNavigation: true,
                slidesNavPosition: 'right',*/
                onLeave: function(index, nextIndex, direction) {
                    var body        = $jq("body"),
                        classes     = ['menu-expanded','menu-active-0','menu-active-1'];
                        if(nextIndex > 1) {
                            //body.removeClass(classes[0]+" "+classes[1]).delay(500).addClass(classes[2]);   
                        } else {
                            body.removeClass(classes[2]);
                            body.addClass(classes[0]+" "+classes[1]);
                        }
                        $jq(".section.active").find('[data-scrollreveal]').each(function() {
                        $jq(this).trigger("fpfocused");
                    });
                }
            });
        } else {
            //Home page slider for mobile
            $jq(window).ready(function() {
                $jq('#HomeMobileBanner > ul').bxSlider({
                  auto: true,
                  mode: 'fade',
                  controls:false,
                  autoControls:false
                });
            });
        } 
    }    
    //Activate Social Stream
    var socialstreamDiv = $jq('#MindtreeSocialStream');
    if(socialstreamDiv.size() > 0) {
        socialstreamDiv.dcSocialStream({
            feeds: {
                twitter: {
                    id: '#Mindtree_Ltd',
                    url: '/sites/all/themes/mindtreevone/templates/twitter.php',
                    out: "intro,date,text"
                },
                rss: {
                    id: 'http://feeds.feedburner.com/MindtreeBlogs',
                    out: 'intro,title,text'
                },
                facebook: {
                    id: '435233876513544',
                    out: 'title,text',
                    image_width:5
                },
                youtube: {
                    id: 'TheMindtreeLTD',
                    out: 'title,thumb',
                    thumb: 'hqdefault'
                }
            },
            rotate: {
                delay: 0
            },
            twitterId: 'Mindtree_Ltd',
            control: false,
            filter: true,
            wall: true,
            days: 100,
            order: 'date',
            cache: false,
            max: 'limit',
            iconPath: '/sites/all/themes/mindtreevone/images/social-stream-images/dcsns-dark/',
            imagePath: '/sites/all/themes/mindtreevone/images/social-stream-images/dcsns-dark/',
            debug: false
        });        
    }
    //contact us : Map navigation-head quarters
    $jq(".contact-headquarters-box").click(function() {
        var dis       = $jq(this),
            targetid  = dis.attr("data-id"),
            target    = document.getElementById(targetid);
        dis.addClass("active").parent().siblings().find(".contact-headquarters-box").removeClass("active");
        if(target != "") {
            target.click();
        }
    });
    //Dynamic twitter feed
    if($jq("#TwitterFeed").size() > 0) {
        $jq.ajax({
            url:"/sites/all/libraries/twitterfeed.php",
            success:function(result){
                $jq("#TwitterFeed").html(result);
                //Bxslider for twitter - home page
                $jq("#TwitterFeed").bxSlider({
                    mode: 'fade',
                    pager: false,
                    controls: true,
                    auto: true,
                    speed:600
                });
            }
        });   
    }
    //Investor Isotope Filter
    function isoInvestorFilter() {
        var filterCat   = $jq(".filter-category-select").attr("data-val"),
            filterYear  = $jq(".filter-year-select").attr("data-val"),
            filterQuat  = $jq(".filter-quarter-select").attr("data-val"),
            fyearfn     = { "filter" : function() {
                            var header = $jq(this).find(".view-grouping-header");
                            if(header.size() > 0 )  {
                                var heading = header.html(),
                                    year    = heading.match(/[0-9-]+[0-9]/) || "",
                                    quats   = heading.split(","),
                                    quat    = (quats.length >= 2) ? quats[1] : "";
                                if(year.length > 0) {
                                    if(filterQuat != "" && quat != "")
                                        returnBool = ((filterYear == year[0].trim()) && (filterQuat == quat.trim()));
                                    else
                                        returnBool = (filterYear == year[0].trim());
                                    return returnBool;
                                } else 
                                    return false;
                            } else 
                                return false;
                          } },
            fquatfn     = { "filter" : function() {
                            var header = $jq(this).find(".view-grouping-header");
                            if(header.size() > 0 )  {
                                var heading = header.html(),
                                    quats   = heading.split(","),
                                    quat    = (quats.length >= 2) ? quats[1] : "";
                                return (filterQuat == quat.trim());
                            } else 
                                return false;
                          } },
            fcatfn      = { "filter" : function() {
                            var header = $jq(this).find("h3");
                            if(header.size() > 0 )  {
                                var heading = header.html(),
                                    cat     = heading.replace(/ /g, "_").toLowerCase().trim();
                                return (filterCat == cat);
                            } else 
                                return false;
                          } };
        //$jq(".view-grouping").show();                          
        invInnerDivIso.isotope((filterCat != "" && typeof(filterCat) != "undefined") ? fcatfn : {"filter" : "*"});
        /*$jq(".view-grouping").filter(function() {
            return ($jq(this).find(".view-grouping-content").not(".views-group.isotope-hidden").size() <= 0) ;
        }).hide();*/
        if((filterYear != "" && typeof(filterYear) != "undefined" && filterQuat != "" && typeof(filterQuat) != "undefined"))
            invOuterDivIso.isotope((filterYear != "" && typeof(filterYear) != "undefined") ? fyearfn : {"filter" : "*"});
        else if(filterYear != "" && typeof(filterYear) != "undefined")
            invOuterDivIso.isotope((filterYear != "" && typeof(filterYear) != "undefined") ? fyearfn : {"filter" : "*"});
        else if(filterQuat != "" && typeof(filterQuat) != "undefined")
            invOuterDivIso.isotope((filterQuat != "" && typeof(filterQuat) != "undefined") ? fquatfn : {"filter" : "*"});
        else
            invOuterDivIso.isotope({"filter" : "*"});
        invInnerDivIso.isotope('layout');
        invOuterDivIso.isotope('layout');  
        /*setTimeout(function(){
            $jq(".view-grouping").filter(function() {
                return ($jq(this).find(".view-grouping-content .views-group:visible").size() <= 0);
            }).hide();
            invOuterDivIso.isotope('layout');
            invInnerDivIso.isotope('layout');
        },800);*/   
    }
    /*Stock exchange investors*/
    function isoSEinvestorFilter() {
        var filterYear  = $jq(".filter-year-select").attr("data-val").toLowerCase() || "",
            filterQuat  = $jq(".filter-quarter-select").attr("data-val").toLowerCase() || "";
        if(filterYear != "" && filterQuat != "") {
            seinvInnerDivIso.isotope({"filter" : "."+filterQuat});
            seinvOuterDivIso.isotope({"filter" : "."+filterYear});
        } else if(filterYear != "" && filterQuat == "") {
            seinvInnerDivIso.isotope({"filter" : "*"});
            seinvOuterDivIso.isotope({"filter" : "."+filterYear});
        } else if(filterYear == "" && filterQuat != "") {
            seinvInnerDivIso.isotope({"filter" : "."+filterQuat});
            seinvOuterDivIso.isotope({"filter" : "*"});
        } else if(filterYear == "" && filterQuat == "") {
            seinvInnerDivIso.isotope({"filter" : "*"});
            seinvOuterDivIso.isotope({"filter" : "*"});
        }
        seinvInnerDivIso.isotope('layout');
        seinvOuterDivIso.isotope('layout');  
    }
    //Resize & Move Scroller
    function resize_moveScroller() {
        if(isotopeOpt.scroller && typeof(isotopeOpt.scroller) != "undefined") {
            var scroller = $jq(isotopeOpt.scrollerDiv).getNiceScroll(),
                active   = $jq(isotopeOpt.div).find(isotopeOpt.itemselector+".active:visible");
            scroller.resize();
            if(isotopeOpt.movetoactive && active.size() > 0 && typeof(isotopeOpt.movetoactive) != "undefined") {
                scroller.doScrollPos(0,active.position().top);
            } else {
                scroller.doScrollPos(0,0);
            }
        }
    }
    //Loadmore 
    function loadmore() {
        if(isoVdivs.length > 0) {
            var disoVdivs   = isoVdivs,
                nextDivs    = disoVdivs.slice(0,10);
            isoVdivs        = [];
            isoVdivs        = disoVdivs.slice(10,(disoVdivs.length));
            isoCont.isotope( 'hide', isoVdivs );
            isoCont.isotope( 'reveal', nextDivs );
            if(isoVdivs.length <= 0)
                $jq(isoCont.selector).find(".loadmore").hide();
            isoCont.isotope('layout');
            if(isotopeOpt.scroller && typeof(isotopeOpt.scroller) != "undefined") {
                $jq(isotopeOpt.scrollerDiv).getNiceScroll().resize();
            }
        }
    }   
    //Find Active elementIndex
    function findActiveIndex(isoAlldivs) {
        var aindex = 0;
        $jq(isoAlldivs).map(function(index,dis) {
            if($jq(dis.element).hasClass("active")) {
                aindex = index;
                return false;
            }
        });
        return aindex + 1;
    }
    //Addloadmore
    function addLoadmore(isoAlldivs) {
        $jq(isoCont.selector).find(".loadmore").remove();
        isoVdivs       = [];
        var lastIndex  = findActiveIndex(isoAlldivs),
            sMin       = 20,
            sFrom      = 0,
            sTo        = (lastIndex <= sMin) ? sMin : lastIndex,
            items      = isoAlldivs.slice(sFrom,sTo);
        isoCont.isotope( 'reveal', items );
        if((isoAlldivs.length - 1) > sTo) {
            isoVdivs  = isoAlldivs.slice(sTo,(isoAlldivs.length));
            isoCont.isotope( 'hide', isoVdivs );
            var loadmoreDiv = $jq("<div/>", { "class":"loadmore","html":"Load more...","click":loadmore });
            $jq(isoCont.selector).append(loadmoreDiv);
        }
        //Layout height problem:Temp fix
        setTimeout(function(){
            isoCont.isotope('layout');
            resize_moveScroller();
        }, 1000);
    }
    // Automotive Page bx-slider's
    $jq('.bxslider-chart-one,.bxslider-chart-two,.bxslider-chart-three').bxSlider({
      auto: false,
      mode: 'fade'
    });
    //Social stream filter
    var filters = {}, $container = $jq('.stream');
    $jq.fn.filterss = function() {
        var $i = $jq(this), isoFilters = [], prop, selector, $a = $i.parents('.dcsns-toolbar'), $b = $a.next(), $c = $jq('.stream',$b);
        filters[ $i.data('group') ] = $i.data('filter');
        for (prop in filters){
            isoFilters.push(filters[ prop ])
        }
        selector = isoFilters.join('');
        $c.isotope({filter: selector, sortBy : 'postDate'});
        return false;
    };
    
    //Services ThoughtPosts and Blogs Tabs 
    $jq("#TabBlogThoughPosts #TabBlogPostsTPLinks > li").click(function() {
      var dis = $jq(this),
          disTarget = dis.find(" > span").attr("data-href");
      dis.addClass("active-tab").siblings().removeClass("active-tab")
      $jq("#"+disTarget).show().siblings(".TabBlogThoughPostsContent").hide();
    });
    
    //Services Section Success Story Activation
    $jq('ul#ServicesSSItems').bxSlider({
        auto: true,
        mode: 'horizontal',
        controls: true,
        pager: false,
        minSlides: 2,
        maxSlides: 2,
        moveSlides: 1,
        slideWidth: 365,
        slideMargin: 30
    });
    //Custom Dropdown Filter
    $jq( "body" ).on( "click", ".custom-dropdown", function( e ) {
        var dis = $jq(e.target);
        if(dis.hasClass("custom-dropdown") || dis.hasClass("custom-dd-selected")) {
            $jq(this).toggleClass("active").find(".custom-dd-list").toggle();
        } else {
            $jq(this).removeClass("active").find(".custom-dd-list").hide();
        }
        $jq(this).siblings().removeClass("active").find(".custom-dd-list").hide();
    });
    $jq( "body" ).on( "click", ".custom-dropdown .custom-dd-list ul li", function( e ) {
        var dis     = $jq(this),
            selVal  = dis.html().trim(), 
            dataVal = dis.attr("data-val"),
            cDD     = dis.parents(".custom-dropdown"),
            dataFil = dis.attr("data-filter");
        if(typeof dataVal != "undefined" || typeof dataFil != "undefined") {
            dis.addClass("cdd-active").siblings().removeClass("cdd-active");
            cDD.find(".custom-dd-selected").html(selVal);
            dis.removeClass("active").find(".custom-dd-list").hide();
            cDD.attr("data-val",dataVal);
            if(typeof dataVal != "undefined" && isotopeOpt != "") {
                var data_sortby     = dis.attr("data-sortby") || "",
                    data_sortasc    = dis.attr("data-sortasc") || "";
                if(data_sortasc == "") {
                    nextCdd = cDD.siblings(".custom-dropdown").find("li.cdd-active");
                    data_sortby     = nextCdd.attr("data-sortby");
                    data_sortasc    = nextCdd.attr("data-sortasc");
                }
                isotopesFilter(data_sortby,data_sortasc);
            } else if(typeof dataFil != "undefined") 
                dis.filterss();
            else if(invInnerDivIso !="" && invOuterDivIso != "")
                isoInvestorFilter();
            else if(seinvInnerDivIso !="" && seinvOuterDivIso != "")
                isoSEinvestorFilter();
            if (cDD.hasClass("cls-news-psmc")) {
                if (typeof dataVal != "undefined") {
                    if (dataVal == "cls-press-release") {
                        $jq("#AwardsWrapper .region-careers-press-realse").show().siblings(".region-careers-media-coverage").hide();
                    } else if (dataVal == "cls-media-coverage") {
                        $jq("#AwardsWrapper .region-careers-media-coverage").show().siblings(".region-careers-press-realse").hide();
                    }
                }
            }
        }
    }); 
    $jq(".custom-dropdown").each(function() {
        var dis     = $jq(this),
            dataVal = dis.find(".custom-dd-list ul li.cdd-active").attr("data-val");
        dis.attr("data-val",dataVal);
    });
    //Mobile dropdown - instead of tab
    $jq(".mobile-cdropdown").click(function(e) {
        var dis = $jq(e.target);
        if(dis.hasClass("mobile-cdropdown") || dis.hasClass("mobile-cdd-selected")) {
            $jq(this).toggleClass("active").find(".mobile-cdd-list").toggle();
        } else {
            $jq(this).removeClass("active").find(".mobile-cdd-list").hide();
        }
        $jq(this).siblings().removeClass("active").find(".mobile-cdd-list").hide();
    });
    $jq(".mobile-cdropdown .mobile-cdd-list ul li a,.mobile-cdropdown .mobile-cdd-list ul li span").click(function(e) {
        if(!$jq(this).hasClass("SubMenuExternal")) {    
            e.preventDefault();
            var dis         = $jq(this),
                target      = dis.attr("href"),
                target      = (typeof(target) != "undefined") ? target : dis.attr("data-href"),
                ssubmenu    = $jq("[data-role-subcdd='"+target+"']"),
                ssubmenuall = $jq(".mobile-subcdropdown"),
                subCont     = $jq("#"+target),
                cDD         = dis.parents(".mobile-cdropdown");
                cDD.find(".mobile-cdd-selected").html(dis.html().trim());
                dis.parent().addClass("cdd-active").siblings().removeClass("cdd-active");
                subCont.show().siblings().hide();
                if(!cDD.hasClass("mobile-subcdropdown")) 
                    ssubmenuall.removeClass("visible-xs");
            if(ssubmenu.size() > 0) {
                ssubmenu.removeClass("hidden-xs").addClass("visible-xs").siblings(".mobile-subcdropdown").addClass("hidden-xs");
                var starget = ssubmenu.find(".SubMenuTabLinks ul li.cdd-active a").attr("href"),
                    starget = typeof(starget) != "undefined" ? starget : ssubmenu.find(".SubMenuTabLinks ul li.cdd-active span").attr("data-href");
                subCont.find("#"+starget).show().siblings().hide();
            }
            if(cDD.hasClass("mcdd-map_countries")) {
                var cdd_cities = $jq(".mobile-cdropdown.mcdd-map_cities");
                cdd_cities.find(".mobile-cdd-list ul li").hide();
                var cities     = cdd_cities.find(".mobile-cdd-list ul li."+target);
                cities.show();
                cities.eq("0").find("span").click();
            }
            if(cDD.hasClass("mcdd-map_cities")) {
                $jq(".contact-mobile").hide();
                $jq(".contact-mobile."+target).show();
            }
            if(dis.parents(".cls-che").size() > 0) {
                var dispartar = dis.parents("#CampusCareerScroll");
                (target == "CampusOffCampus") ? dispartar.addClass("CampusOffCampusbg") : dispartar.removeClass("CampusOffCampusbg")
            }
        }
    });
    $jq(window).resize(function (){
      $jq(".mobile-cdropdown:visible .mobile-cdd-list ul li.cdd-active a,.mobile-cdropdown:visible .mobile-cdd-list ul li.cdd-active span").click();
      $jq(".mobile-cdropdown.mobile-subcdropdown:visible .mobile-cdd-list ul li.cdd-active a,.mobile-cdropdown.mobile-subcdropdown:visible .mobile-cdd-list ul li.cdd-active span").click();  
    });
    $jq(".mobile-cdropdown:visible .mobile-cdd-list ul li.cdd-active a,.mobile-cdropdown:visible .mobile-cdd-list ul li.cdd-active span").click();
    $jq(".mobile-cdropdown.mobile-subcdropdown:visible .mobile-cdd-list ul li.cdd-active a,.mobile-cdropdown.mobile-subcdropdown:visible .mobile-cdd-list ul li.cdd-active span").click();
    //Add reading now text
    $jq.fn.addReadingNow = function() {
        var rnDiv       = $jq("<div/>",{"class":"pr-readingnow","html":"Reading Now"}),
            elemDate    = $jq(this).find(".blog-date-category,.blog-category");
        elemDate.hide();
        rnDiv.insertAfter($jq(this).find(".blog-category"));
    }
    //Active left menu
    function activeLeftmenu(almOpt) {
        var bodyClasses = $jq("body").attr("class"),
            aClass      = bodyClasses.match(/page-node-[0-9]+[0-9]|page-node-[0-9]/),
            lmselector  = almOpt.div,
            target      = $jq(lmselector).find(almOpt.itemselector+"."+aClass[0]);            
        if(target.size() > 0) {
            target.addClass("active").addReadingNow();
            var ddaClass = target.attr("class").match(/ [0-9]+[0-9] /);
            if(ddaClass) {
                selDD    = $jq("li[data-val='"+ddaClass[0].trim()+"']");
                if(selDD.size() > 0) {
                    selDD.addClass("cdd-active");
                }
            }
            if(typeof(isotopeOpt.activeDropdown) != "undefined" && leader_ship.size() > 0) {
                if(target.hasClass("ManagementTeam") && !target.hasClass("BoardofDirectors"))  
                    $jq(isotopeOpt.activeDDbox).find("[data-val='ManagementTeam']").addClass("cdd-active").siblings().removeClass("cdd-active");
                else if(target.hasClass("BoardofDirectors") && !target.hasClass("ManagementTeam"))  
                    $jq(isotopeOpt.activeDDbox).find("[data-val='BoardofDirectors']").addClass("cdd-active").siblings().removeClass("cdd-active");
            }
        }        
    }
    //Active dropdown only
    function activeonlyDropdown() {
        var ddpars_classes      = isotopeOpt.activeDDpar_clas;
        if(ddpars_classes != "" && typeof(ddpars_classes) != "undefined") {
            $jq.each(ddpars_classes,function(parameter,ddclass){
                var activeClass = getParameterByName(parameter);
                if(activeClass != "") {
                    var selDD   = $jq(ddclass).find("li[data-val='"+activeClass.trim()+"']");
                    if(selDD.size() > 0) {
                        selDD.addClass("cdd-active");
                    }
                }
            })
        }
    }
    //Add nicescroll bar
    function addScroller(scrollbartarget) {
        var niceScroll    = $jq(scrollbartarget).niceScroll({"cursorcolor":"#666666",
                                "cursorborder":"0px solid #999999",
                                "cursorborderradius":"2px",
                                "cursorwidth":4,
                                "touchbehavior":false,
                                "background":"#f0f0f0",
                                "cursorminheight":40,
                                "disableoutline":false,
                                "horizrailenabled":false,
                                "nativeparentscrolling":true
                            });
        if($jq(scrollbartarget).size() > 0) {
            //Modified default mousewheel
            niceScroll._onmousewheel = niceScroll.onmousewheel;  
            niceScroll.onmousewheel = function(e) {
                var ret = niceScroll._onmousewheel(e);
                if (ret||!niceScroll.scrollrunning) {
                    var py = niceScroll.getScrollTop();
                    var wy = $jq(document).scrollTop();
                    if(niceScroll.page.maxh > 0) {
                        if (py<=0&&niceScroll.lastdeltay>=0) {
                            window.scrollTo(0,wy-20);
                        }
                        else if (py>=niceScroll.page.maxh&&niceScroll.lastdeltay<=0) {            
                            window.scrollTo(0,wy+20);
                        }
                    }
                    return ret;
                }
            }
            niceScroll.bind(niceScroll.doc,"wheel",niceScroll.onmousewheel);
            //Moving scroller div inside target div
            var nsData         = $jq(scrollbartarget).getNiceScroll(),
                nicescrollRail = $jq("#"+nsData[0]["id"]);
            nicescrollRail.appendTo(scrollbartarget);
        }
    }
    //Isotopes
    function addIsotopes(isotopeOpt) {
        var isotopDiv   = isotopeOpt.div,
            staticOpt   = { itemSelector: isotopeOpt.itemselector,
                            layoutMode: isotopeOpt.layout,
                            hiddenStyle: {
                                display: "none"
                            },
                            visibleStyle: {
                                display: ""
                            }
                          },
            finalOpt    = $jq.extend({}, staticOpt, isotopeOpt.isoExtraopt);
        isoCont         = $jq(isotopDiv).isotope(finalOpt);
        if(isotopeOpt.intautoFilter && typeof(isotopeOpt.intautoFilter) != "undefined") {
            $jq(".custom-dropdown").find(".custom-dd-list ul li.cdd-active").click();
        }
        isoCont.isotope( 'once', 'layoutComplete', function(items,filteredItems) {
            if(isotopeOpt.loadmore && typeof(isotopeOpt.loadmore) != "undefined") 
                addLoadmore(filteredItems);
            else
                resize_moveScroller();
        });/*
        isoCont.isotope( 'on', 'layoutComplete', function(items,filteredItems) {
            resize_moveScroller();
        });*/
    }
    //Isotopes filter
    function isotopesFilter(sortByval,sortByord) {
        sortByval = (typeof(isotopeOpt.sortby) != "undefined") ? isotopeOpt.sortby : sortByval;
        sortByord = (typeof(isotopeOpt.sortasc) != "undefined") ? isotopeOpt.sortasc : sortByord;
        switch(isotopeOpt.filterCase) {
            case "CSorTP" :
                var ind_val = $jq(".filter-industries-select").attr("data-val"),
                    ser_val = $jq(".filter-services-select").attr("data-val"),
                    sol_val = $jq(".filter-solutions-select").attr("data-val"),
                    fval    = "",
                    fvals   = [];
                if(ind_val == "" && ser_val == "" && sol_val == "") 
                    fval = "*";
                else {
                    if(ind_val != "" && ser_val != "" && sol_val != "") 
                        fvals.push("."+ind_val+"."+ser_val+"."+sol_val);
                    if(ind_val != "" && ser_val != "")
                        fvals.push("."+ind_val+"."+ser_val);
                    if(ind_val != "" && sol_val != "")
                        fvals.push("."+ind_val+"."+sol_val);                    
                    if(ind_val != "")
                        fvals.push("."+ind_val);
                    fval = fvals.join(",");
                }
                break;
            default :
                var fval    = $jq(".custom-dropdown").map(function() {
                                  var disValue = $jq( this ).attr("data-val");
                                  return (disValue != "" && typeof(disValue) != "undefined" ) ? "."+disValue : "";
                              }).get().join("");
                break;
        }
        var sortby      = "",
            sortOrd     = {};
        if(typeof(sortByval) != "undefined") {
            sortby      = sortByval;
            if(typeof(sortByord) != "undefined") 
                sortOrd = {"sortAscending" : {}};
                sortOrd["sortAscending"][sortByval] = (sortByord === "true");
        } else {
            sortby      = "original-order"
        }
        var filtSortOpt = { filter : fval,sortBy : sortby },
            finalOpt    = $jq.extend({},filtSortOpt,sortOrd),
            newiso      = isoCont.isotope(finalOpt);
            if(typeof(isotopeOpt.updateSortData) != "undefined" && isotopeOpt.updateSortData) {
                newiso  = isoCont.isotope('updateSortData').isotope(),
                newiso  = isoCont.isotope({"sortBy":sortby});
            }
        var newisodata  = newiso.data('isotope'); 
        if(isotopeOpt.loadmore && typeof(isotopeOpt.loadmore) != "undefined") 
            addLoadmore(newisodata.filteredItems);
        else 
            resize_moveScroller();
    }
    //initialize
    /* -- Options --
    div                 => isotopes parent id/class
    itemselector        => isotopes itemselector
    layout              => isotopes layout
    scrollerDiv         => scroller div id/class
    scroller            => boolean (true/false)
    activeLM            => Active left menu - boolean (true/false)
    movetoactive        => boolean (true/false) 
    intautoFilter       => Auto filter on intialize - boolean (true/false)
    loadmore            => boolean (true/false) 
    isoExtraopt         => extra options like sorting,sortascending.. (object)
    sortby              => sorting by eg : weight
    sortasc             => sort ascending - boolean (true/false)
    updateSortData      => update sorting data - boolean (true/false) 
    activeDropdown      => active dropdown - boolean (true/false) 
    activeDDbox         => active dropdown id/class 
    activeonlyDropdown  => active dropdown(only for dropdown not along with left menu) - boolean (true/false)
    activeDDpar_clas    => active dropdown - parameter(URL) & class/id of the dropdown (object)
    */
    var news_press_release      = $jq("#block-views-press-releases-view-block"),
        news_media_coverage     = $jq("#block-views-media-coverage-block"),
        news_all_press_release  = $jq(".all-press-releases"),
        news_all_media_coverage = $jq(".all-media-coverages"),
        all_events_webinars     = $jq("#block-views-events-and-webinars-block"),
        all_events              = $jq("#block-views-events-filter-block"),
        all_webinars            = $jq("#block-views-webinar-filter-block"),
        pers_all_casestudies    = $jq("#block-views-all-case-studies-listing-block"),
        pers_all_thoughtposts   = $jq("#block-views-all-thought-posts-listing-block"),
        pers_case_studies       = $jq("#block-views-case-studies-block"),
        pers_thought_posts      = $jq("#block-views-thought-posts-block"),
        leader_ship             = $jq("#block-views-leader-ship-view-block"),
        alliances               = $jq("#block-views-alliances-filter-bar-block"),
        newsletters             = $jq("#block-views-all-news-letters-block"),
        allbrochures_lis        = $jq("#block-views-all-brochures-listing-block"), 
        allbrochures            = $jq("#block-views-all-brochures-filter-block"),
        alldatasheet            = $jq("#block-views-all-datasheets-block"),
        perspectives_videos     = $jq("#block-views-all-videos-block"),
        investors_ar1           = $jq("#LeaderFilterContaineLHS .region-annual-report-2013-14-filter-bar"),
        investors_ar2           = $jq("#LeaderFilterContaineLHS .region-annual-report-2012-13-filter-bar"),
        investors_ar3           = $jq("#LeaderFilterContaineLHS .region-annual-report-2014-15-filter-bar"),
        investors_ar4           = $jq("#LeaderFilterContaineLHS .region-annual-report-2015-16-filter-bar"),
        perspectives_all_videos = $jq(".view-all-videos-for-lisitng-page");
    if(news_press_release.size() > 0 || news_media_coverage.size() > 0) {        
        var targetDiv = (news_press_release.size() > 0) ? "#block-views-press-releases-view-block" : "#block-views-media-coverage-block";
        isotopeOpt    = {"div":targetDiv+" .view-content","itemselector":".views-row","layout":"fitRows","scrollerDiv":targetDiv+" .content","scroller":true,"activeLM":true,"movetoactive":true,"intautoFilter":true,"loadmore":true};
    } else if(newsletters.size() > 0 || allbrochures_lis.size() > 0 || perspectives_all_videos.size() > 0) {
        var targetDiv = (newsletters.size() > 0) ? "#block-views-all-news-letters-block" : "#block-views-all-brochures-listing-block";
        targetDiv     = (perspectives_all_videos.size() > 0) ? ".view-all-videos-for-lisitng-page" : targetDiv;
        isotopeOpt    = {"div":targetDiv+" .view-content","itemselector":".views-row","layout":"masonry"};        
    } else if(allbrochures.size() > 0) {
        isotopeOpt    = {"div":"#block-views-all-brochures-filter-block .view-content","itemselector":".views-row","layout":"fitRows","scrollerDiv":"#block-views-all-brochures-filter-block .content","scroller":true,"activeLM":true,"movetoactive":true,"loadmore":true};
    }  else if(alldatasheet.size() > 0) {
        isotopeOpt    = {"div":"#block-views-all-datasheets-block .view-content","itemselector":".views-row","layout":"fitRows","scrollerDiv":"#block-views-all-datasheets-block .content","scroller":true,"activeLM":true,"movetoactive":true,"loadmore":true};
    } else if(perspectives_videos.size() > 0) {
        var targetDiv = (perspectives_videos.size() > 0) ? "#block-views-all-videos-block .content" : "";
        isotopeOpt    = {"scrollerDiv":targetDiv,"scroller":true};
    } else if(investors_ar1.size() > 0 || investors_ar2.size() > 0 || investors_ar3.size() > 0 || investors_ar4.size() > 0) {
        var targetDiv = "";
        if(investors_ar1.size() > 0) {
            targetDiv = ".region-annual-report-2013-14-filter-bar .content";
        } else if(investors_ar2.size() > 0) {
            targetDiv = ".region-annual-report-2012-13-filter-bar .content";
        } else if(investors_ar3.size() > 0) {
            targetDiv = ".region-annual-report-2014-15-filter-bar .content";
        } else if(investors_ar4.size() > 0) {
            targetDiv = ".region-annual-report-2015-16-filter-bar .content";
        }
        isotopeOpt    = {"scrollerDiv":targetDiv,"scroller":true};
    } else if(news_all_press_release.size() > 0 || news_all_media_coverage.size() > 0) {        
        var targetDiv = (news_all_press_release.size() > 0) ? ".all-press-releases" : ".all-media-coverages";
        isotopeOpt    = {"div":targetDiv+" .view-content","itemselector":".views-row","layout":"masonry","intautoFilter":true};
    } else if(all_events_webinars.size() > 0) {        
        isotopeOpt    = {"div":"#block-views-events-and-webinars-block .view-content","itemselector":".news-content-box","layout":"masonry","intautoFilter":true,"activeonlyDropdown":true,"activeDDpar_clas":{"evt":".filter-up-select"},
                            "isoExtraopt" : { "getSortData" : { 
                                                "date" : function (elem) {                         
                                                    var dates = $jq(elem).find('.dateforsort').text().trim().split("to"),
                                                        fDate = (dates.length >= 2) ? dates[0] : dates[0];
                                                    return (typeof fDate != "undefined") ? Date.parse(fDate.trim().replace(/-/g, '/')) : "";
                                                } },
                                            }                            
                        };
    } else if(all_events.size() > 0 || all_webinars.size() > 0) { 
        var targetDiv = (all_events.size() > 0) ? "#block-views-events-filter-block" : "#block-views-webinar-filter-block";       
        isotopeOpt    = {"div":targetDiv+" .view-content","itemselector":".views-row","layout":"fitRows","scrollerDiv":targetDiv+" .content","scroller":true,"activeLM":true,"movetoactive":true,"intautoFilter":true,"loadmore":true,
                            "isoExtraopt" : { "getSortData" : { 
                                                "date" : function (elem) {                         
                                                    var dates = $jq(elem).find('.dateforsort').text().trim().split("to"),
                                                        fDate = (dates.length >= 2) ? dates[0] : dates[0];
                                                    return (typeof fDate != "undefined") ? Date.parse(fDate.trim().replace(/-/g, '/')) : "";
                                                } },
                                            }
                        };
    } else if(pers_all_casestudies.size() > 0 || pers_all_thoughtposts.size() > 0) { 
        var targetDiv = (pers_all_casestudies.size() > 0) ? "#block-views-all-case-studies-listing-block" : "#block-views-all-thought-posts-listing-block";
        isotopeOpt    = {"div":targetDiv+" .view-content","itemselector":".views-row","layout":"masonry","filterCase":"CSorTP","sortby":"weight","sortasc":"false","updateSortData":true,"intautoFilter":true,"activeonlyDropdown":true,"activeDDpar_clas":{"ind":".filter-industries-select","ser":".filter-services-select","sol":".filter-solutions-select"},
                            "isoExtraopt" : { "getSortData": {
                                                "weight" : function( itemElem ) {
                                                    var ind_val = $jq(".filter-industries-select").attr("data-val"),
                                                        ser_val = $jq(".filter-services-select").attr("data-val"),
                                                        sol_val = $jq(".filter-solutions-select").attr("data-val"),
                                                        weight  = 0;
                                                    if($jq(itemElem).hasClass(ind_val))
                                                        weight  = weight + 1;
                                                    if($jq(itemElem).hasClass(ser_val))
                                                        weight  = weight + 1;
                                                    if($jq(itemElem).hasClass(sol_val))
                                                        weight  = weight + 1;               
                                                    return weight;
                                                } },
                                                "sortBy" :"weight",
                                                "sortAscending" : {
                                                    weight: false,
                                                }  
                                            }                         
                        }
    } else if(pers_case_studies.size() > 0 || pers_thought_posts.size() > 0) {
        var targetDiv = (pers_case_studies.size() > 0) ? "#block-views-case-studies-block" : "#block-views-thought-posts-block";
        isotopeOpt    = {"div":targetDiv+" .view-content","itemselector":".views-row","layout":"fitRows","scrollerDiv":targetDiv+" .content","scroller":true,"activeLM":true,"movetoactive":true,"loadmore":true,"filterCase":"CSorTP","sortby":"weight","sortasc":"false","updateSortData":true,"intautoFilter":false,
                            "isoExtraopt" : { "getSortData": {
                                                "weight" : function( itemElem ) {
                                                    var ind_val = $jq(".filter-industries-select").attr("data-val"),
                                                        ser_val = $jq(".filter-services-select").attr("data-val"),
                                                        sol_val = $jq(".filter-solutions-select").attr("data-val"),
                                                        weight  = 0;
                                                    if($jq(itemElem).hasClass(ind_val))
                                                        weight  = weight + 1;
                                                    if($jq(itemElem).hasClass(ser_val))
                                                        weight  = weight + 1;
                                                    if($jq(itemElem).hasClass(sol_val))
                                                        weight  = weight + 1;               
                                                    return weight;
                                                } },
                                                "sortBy" :"weight",
                                                "sortAscending" : {
                                                    weight: false,
                                                }  
                                            } 
                        };
    } else if(leader_ship.size() > 0) {
        isotopeOpt    = {"div":"#block-views-leader-ship-view-block .view-content","itemselector":".views-row","layout":"fitRows","scrollerDiv":"#block-views-leader-ship-view-block .content","scroller":true,"activeLM":true,"movetoactive":true,"updateSortData":true,"activeDropdown":true,"activeDDbox":".leader-filter-list","intautoFilter":true,
                            "isoExtraopt" : { "getSortData": {
                                                    "bodsort": function (elem) {                                
                                                        return parseInt($jq(elem).find(".bod-order").text().trim());
                                                    },
                                                    "mtsort": function (elem) {                                
                                                        return parseInt($jq(elem).find(".mt-order").text().trim());
                                                    },
                                                    "allsort": function( elem ) {
                                                        var bodVal  = $jq(elem).find(".bod-order").text().trim(),
                                                            mtVal   = $jq(elem).find(".mt-order").text().trim(),
                                                            weight  = 0;
                                                        if(bodVal == "1" && mtVal == "1") {
                                                            weight  = 1;
                                                        } else if(bodVal != "" && mtVal != "") {
                                                            weight  = 1.1;
                                                        } else if(mtVal != "") {
                                                            weight  = 1.2;
                                                        } else if(bodVal != "") {
                                                            weight  = 1.3;
                                                        } else if(bodVal == "" && mtVal == "") {
                                                            weight  = 1.4;
                                                        }        
                                                        return weight;
                                                    }
                                                },
                                                sortBy : 'allsort' 
                                            } 
                        };
    } else if(alliances.size() > 0) {        
        var targetDiv = (alliances.size() > 0) ? "#block-views-alliances-filter-bar-block" : "";
        isotopeOpt    = {"div":targetDiv+" .view-content","itemselector":".views-row","layout":"fitRows","scrollerDiv":targetDiv+" .content","scroller":true,"activeLM":true,"movetoactive":true};
    } 
    //Initialize Isotopes & loadmore & custom scrollbar
    if(isotopeOpt != "" && typeof(isotopeOpt) != "undefined") {
        if(isotopeOpt.activeLM && typeof(isotopeOpt.activeLM) != "undefined")
            activeLeftmenu(isotopeOpt);
        if(isotopeOpt.activeonlyDropdown && typeof(isotopeOpt.activeonlyDropdown) != "undefined")
            activeonlyDropdown();
        if(isotopeOpt.scroller && typeof(isotopeOpt.scroller) != "undefined")
            addScroller(isotopeOpt.scrollerDiv);
        if(isotopeOpt.div && typeof(isotopeOpt.div) != "undefined")
            addIsotopes(isotopeOpt);
    }
    //Isotopes for investor page
    var investors = $jq(".view-all-content-related-investors");
    if(investors.size() > 0) {
        //Begin : Modified Isotopes protype
        /*Isotope.Item.prototype.reveal = function() {
            itemReveal.apply( this, arguments );
            $jq(this.element).removeClass('isotope-hidden');
        };
        var itemHide = Isotope.Item.prototype.hide;
        Isotope.Item.prototype.hide = function() {
            itemHide.apply( this, arguments );
            $jq(this.element).addClass('isotope-hidden');
        };*/
        //End : Modified Isotopes protype
        invInnerDivIso  =  investors.find('.view-grouping-content').isotope({
                            itemSelector: '.views-group',
                            layoutMode: 'masonry'
                        });
        invOuterDivIso  =  investors.find('.view-content').isotope({
                            itemSelector: '.view-grouping',
                            layoutMode: 'fitRows'
                        });        
    }
    //Isotopes for investor stock page
    var se_investors = $jq(".view-stock-exchange-disclosures-investors-content");
    if(se_investors.size() > 0) {
        seinvInnerDivIso  =  se_investors.find('.sed_group_year  .view-grouping-content').isotope({
                                itemSelector: '.sed_group_month',
                                layoutMode: 'fitRows'
                            });  
        seinvOuterDivIso  =  se_investors.isotope({
                                itemSelector: '.sed_group_year',
                                layoutMode: 'masonry'
                            });    
    }
    //Isotopes for careers meet-our-minds
    var careers_mom = $jq("#block-views-meet-our-minds-block, #MeetMindsDescription .region-careers-meet-our-minds .block-views");
    if(careers_mom.size() > 0) {
        var cm_isoCont = careers_mom.find(".view-content").isotope({
                            itemSelector: '.views-row',
                            layoutMode: 'fitRows'
                        });
        $jq('body').on('click','.view-meet-our-minds .views-row, .view-careers-meet-our-minds-section .views-row',function(e) {
            if(!$jq(e.target).hasClass("close")) {
                $jq(".view-meet-our-minds .views-row, .view-careers-meet-our-minds-section .views-row").removeClass("large").find(".close").remove();
                $jq(this).toggleClass("large");
                $jq("<span/>",{"class":"close","html":"X"}).appendTo($jq(this));
                cm_isoCont.isotope({  layoutMode: 'masonry',
                                      masonry: {
                                        columnWidth: 1
                                      }
                                  });
            }
        });
        $jq('body').on('click','.view-meet-our-minds .views-row .close, .view-careers-meet-our-minds-section .views-row .close',function() {
            $jq(this).parent(".views-row").removeClass("large");
            $jq(this).remove();
            cm_isoCont.isotope({layoutMode: 'fitRows'});
        });
        careers_mom.find(".view-content .views-row:first-child").click()
    }
       //Isotopes for Alumni Minds
    $jq('.view-alumni-meet-our-minds .views-row').addClass("large");
    var careers_alumni_mom = $jq("#block-views-alumni-meet-our-minds-block");
       if(careers_alumni_mom.size() > 0) {
           var cm_isoCont = careers_alumni_mom.find(".view-content").isotope({
                               itemSelector: '.views-row',
                               layoutMode: 'fitRows'
                           });
    $jq('body').on('click','.view-alumni-meet-our-minds .views-row',function(e) {
       if(!$jq(e.target).hasClass("close")) {
           $jq(".view-alumni-meet-our-minds .views-row").removeClass("large").find(".close").remove();
           $jq(this).toggleClass("large");
           $jq("<span/>",{"class":"close","html":"X"}).appendTo($jq(this));
           cm_isoCont.isotope({  layoutMode: 'masonry',
                                 masonry: {
                                   columnWidth: 1
                                 }
                             });
       }
   });
   $jq('body').on('click','.view-alumni-meet-our-minds .views-row .close',function() {
       $jq(this).parent(".views-row").removeClass("large");
       $jq(this).remove();
       cm_isoCont.isotope({layoutMode: 'fitRows'});
   });
   careers_alumni_mom.find(".view-content .views-row:first-child").click()
   }
    //Ajax Upload Contact us
    $jq('#uploadrfp_rfi .file-upload,#uploadcv .file-upload,#uploadcorporate_deck .file-upload').on('change', function() { 
        var dis         = $jq(this),
            disForm     = dis.parents("form"),
            isMulti     = disForm.hasClass("multi-upload"),
            loader      = disForm.find(".urfip_loader"),
            fname       = disForm.find("#fname"),
            submitB     = disForm.find("input.submit"),
            sfile       = disForm.find(".urfip_selectedfile"),
            sfileNw     = $jq("<span/>",{ "class":"urfip_selectedfile" }),
            nfile       = "", 
            firstUpload = (sfile.size() == 1 && sfile.html() == "") ? true : false,
            cur_fname   = $jq(this).val(),
            cur_fnames  = cur_fname.split('.'),
            new_fname   = (cur_fnames[0].length > 12) ? cur_fnames[0].substring(0, 12)+"."+cur_fnames[1] : cur_fname;
        if(disForm.attr("id") == "uploadcv") {
            var firstname = $jq("#careerform #fullname"),
                fval      = firstname.val();
            if(fval == "" || typeof(fval) == "undefined") {
                disForm.find(".u_emsg").remove();
                dis.val("");
                $jq("<span class='u_emsg'>Please fill your name</span>").insertAfter(loader);
                return false;
            } else {
                fname.val(fval);
            }
        }
        disForm.find(".u_emsg").remove();
        if(isMulti) {
            if(firstUpload) {
                sfile.html(new_fname); 
                nfile = sfile;
            } else {
                sfileNw.html(new_fname).insertBefore(loader);
                nfile = sfileNw;
            }
        }
        else {
            sfile.html(new_fname).show();
            nfile = sfile;
        }
        loader.show().html("loading");
        submitB.attr("disabled","disabled");
        disForm.ajaxForm({
            "success":function(data) { 
                data     = data.trim();
                if(data != "" && typeof(data) != "undefined") {
                    data = $jq.parseJSON(data);
                    loader.hide();
                    if(parseInt(data.error) <= 0) {
                        var subInput = (disForm.attr("id") == "uploadcv" || disForm.attr("id") == "uploadcorporate_deck") ? $jq("#careerform input#uploadedcv,#talent-acquisition-agencies input#corporatedeck") : $jq("#contactform:visible input#requestaservice,.contact-us-request-services-two:visible input#requestaservice"),
                            siVal    = subInput.val();
                        (isMulti && (siVal != "" && typeof(siVal) != "undefined")) ? subInput.val(siVal+","+data.success) : subInput.val(data.success);
                        $jq("<span/>",{
                            "html" : '<i class="fa fa-times-circle"></i>',
                            "class" : "remove",
                            "click" : function() {
                                if(isMulti) {
                                    var updated_subInput = (disForm.attr("id") == "uploadcv" ) ? $jq("#careerform input#uploadedcv") : $jq("#contactform:visible input#requestaservice,.contact-us-request-services-two:visible input#requestaservice"),
                                        updated_SI_val   = updated_subInput.val(),
                                        up_files         = updated_SI_val.split(","),
                                        ur_sf            = $jq(this).parent();
                                        if(up_files.length > 1) {
                                            var ind         = disForm.find(".urfip_selectedfile").index(ur_sf),
                                                newUp_files = $jq.grep(up_files, function(value,key) {
                                                                  return key != ind;
                                                              });
                                                updated_subInput.val(newUp_files.join(","));
                                                ur_sf.remove();
                                        } else {
                                            updated_subInput.val("");
                                            ur_sf.html("");
                                        }
                                    if(disForm.find(".urfip_selectedfile").size() < 3) {
                                        dis.removeAttr("disabled");
                                    }
                                } else {
                                    $jq(this).parent().html("");
                                    subInput.val("");
                                    dis.removeAttr("disabled");
                                }
                            }
                        }).appendTo(nfile);
                        if(!isMulti || (sfile.size() + 1) >= 3)
                            dis.attr("disabled","disabled");
                    } else 
                        $jq("<span class='u_emsg'>"+data.errormsg+"</span>").insertAfter(loader); 
                } else {
                    loader.hide();
                    $jq("<span class='u_emsg'>File not uploaded successfully</span>").insertAfter(loader); 
                }
                $jq("#contactform:visible,#careerform:visible").find("input.submit").removeAttr("disabled");
            }
        }).submit();
    });
    //Slider : global page
    $jq('#GlobalSlider').bxSlider({
        auto: true,
        mode: 'fade',
        autoControls: false,
        controls: false,
        pause: 6000
    });
    //Careers Long Scroll Recent Alumni
    var alumniSlider = $jq('.alumni-slider').bxSlider({
      controls: false,
      adaptiveHeight: true,
      pager: false
    });
    $jq('#RecentAlumniPager').bxSlider({
      pager: false,
      minSlides: 1,
      maxSlides: ($jq(window).outerWidth() <= "1023") ? 2 : 4,
      slideWidth: 100,
      slideMargin: 10,
      onSliderLoad:function() {
        $jq("#RecentAlumniPager a").click(function(e) {
            e.preventDefault();
            alumniSlider.goToSlide($jq(this).attr("data-slide-index"));
          $jq(this).addClass("active").siblings("a").removeClass("active");
        })
      }
    });
    $jq("#CSLViewCareer .search-career").click(function() {
      var keywords = $jq("#cls-keywords").val(),
          location = $jq("#cls-location").val(),
          tot_exp  = $jq("#cls-totalexp").val(),
          newHref  = "https://crp.mindtree.com/CRP/SeachResult.aspx?keywords="+keywords+"&FArea=0&Exp="+tot_exp+"&Location="+location+"&Freshness=4&Industry="
      $jq(this).attr("href",newHref);
    });
    //Document click
    $jq(document).click(function(e) {
        var dis = $jq(e.target);
        //close custom dropdown
        if(!dis.hasClass("custom-dropdown") && !dis.parents(".custom-dropdown").hasClass("custom-dropdown") ) {
            $jq(".custom-dropdown").removeClass("active").find(".custom-dd-list").hide();
        }
        //close mobile custom dropdown
        if(!dis.hasClass("mobile-cdropdown") && !dis.parents(".mobile-cdropdown").hasClass("mobile-cdropdown") ) {
            $jq(".mobile-cdropdown").removeClass("active").find(".mobile-cdd-list").hide();
        }
        //Close menu-searchbox
        if(!dis.hasClass(".search-icon") && !dis.parents(".search-bar").hasClass("search-bar") ) {
            var sb = $jq(".search-bar"),
                fd = sb.find(".fulldrop");
            if(!fd.hasClass("close")) {
                fd.addClass("close").removeClass("opend");
                sb.removeClass("active");
                $jq(".search-icon").removeClass("fa-close").addClass("fa-search");
            } 
        }
    });
    $jq(window).load(function() {
        if(isoCont != "") {
            //Relayout after full page load
            isoCont.isotope('layout');      
        }
        /*Alumni Joiners ScrollTO*/
        var scrolltoalumni = getParameterByName("scrollto") || "";
        if(scrolltoalumni != "" && scrolltoalumni == "alumnijoiners") {
            var scrollToAlu = $jq("#block-views-alumni-meet-our-minds-block").offset().top;
            $jq('html, body').animate({ scrollTop: scrollToAlu }, 600);
        }

        var careerscto = getParameterByName("careerscto") || "";
        if(careerscto != "") {
            $jq(".smooth_scroll[href='#"+careerscto+"']").click();
        }
    });
    /*Back top Script start*/
     // hide #back-top first
    $jq("#back-top").hide();
 
    // fade in #back-top
    $jq(window).scroll(function () {
       if ($jq(this).scrollTop() > 100) {
        $jq('#back-top').fadeIn();
       } else {
        $jq('#back-top').fadeOut();
       }
    });

    // scroll body to 0px on click
    $jq('#back-top a').click(function () {
        $jq('body,html').animate({
           scrollTop: 0
        }, 800);
     return false;
    }); 
    /*Back top Script end*/
    /*service page sticky begin*/
    function ServicesFloating() {
		var sfb         = $jq("#ServicesFloatingBlock"),
		    sfb_init    = sfb.offset().top, 
		    sfb_width   = sfb.outerWidth();
		sfb.css({"position":"fixed","width":sfb_width+"px"});
		$jq(window).scroll(function(){
		    var scroll_top              = $jq(this).scrollTop(),
		        wHeight              	= $jq(this).outerHeight(),
		        height_element_parent   = $jq("#LHSMainContent").outerHeight(),
		        height_element          = $jq("#ServicesFloatingBlock").height(),
		        position_fixed_max      = height_element_parent - $jq("#RHSMainContent").outerHeight(),
		        //position_fixed_max      = (height_element > wHeight) ? height_element_parent - (height_element - wHeight)  : height_element_parent,
		        position_fixed          = scroll_top < sfb_init ? sfb_init - scroll_top : position_fixed_max > scroll_top ? 0 : position_fixed_max - scroll_top ;
		    $jq("#ServicesFloatingBlock").css("top",position_fixed);
		});
    }
    /*service page sticky end*/
     /*Events - Schedule a Meeting - AjaxForm*/
    var esmForm = $jq("#schedule_meeting");
    if(esmForm.size() > 0) {
        $jq(".download_report",esmForm).click(function(e) {
            e.preventDefault();
            var dis         = $jq(this),
                disForm     = dis.parents("#schedule_meeting"),
                isValid     = true;
            disForm.find(".error_msg").remove();
            $jq(".required",disForm).each(function() {
                var dis         = $jq(this),
                    disVal      = dis.val(),
                    errorMsg    = $jq("<span/>",{ "class" : "error_msg","html" : "Required Field"})
                if(disVal != "" && typeof(disVal) != "undefined") {
                    dis.next(".error_msg").remove();
                } else {
                    errorMsg.clone().insertAfter(dis);
                    isValid = false;
                }
            });
            if(isValid) {
                var outer = disForm.parents(".bai-retail-page-section");
                /*outer.find(".event_sm_msg").remove();*/
                disForm.addClass("loading");
                dis.attr("disabled","disabled");
                $jq.ajax({
                    type    : "POST",
                    url     : disForm.attr("action"),
                    data    : disForm.serialize(),
                    success : function(result) {
                        disForm.find("input.required").val("");
                        disForm.find("input[name='schedule_date']:eq(0),input[name='schedule_time']:eq(0)").click();
                        /*$jq("<p/>",{
                            "class" : "event_sm_msg",
                            "html"  : result
                        }).insertAfter(outer.find("h3"));*/
                        $jq.fancybox( '<p class="event_sm_msg">'+result+'</p>',{
                            maxWidth : 500  
                        });
                        $jq(".bai-retail-form").slideUp(600);
                        disForm.removeClass("loading");
                        dis.removeAttr("disabled");
                        /*$jq('html, body').animate({ scrollTop: $jq(".event_sm_msg").offset().top-20 }, 600);*/
                    }
                });
            }
        });
    }
    //Clear all - checkbox - annual report
    $jq(".checkbox_clearall").click(function() {
        $jq(this).parents("form").find("input[type='checkbox']").prop("checked","");
    });
    //Select all - checkbox - annual report
    $jq(".selectall_clearall").click(function() {
        $jq(this).parents("form").find("input[type='checkbox']").prop("checked","checked");
    });
    /*Find IE version*/
    function ieVersion() {
        var ua = window.navigator.userAgent;
        if (ua.indexOf("Trident/7.0") > 0)
            return 11;
        else if (ua.indexOf("Trident/6.0") > 0)
            return 10;
        else if (ua.indexOf("Trident/5.0") > 0)
            return 9;
        else
            return 0;  // not IE9, 10 or 11
    }  
    $jq(window).load(function() {
        if(ieVersion() > 0) {
            $jq(".svg_outer svg").attr("class","svg-fluid");
            /*$jq(".svg_outer svg.svg-fluid").outerHeight()*/
            $jq(".svg_outer").height(622);
            $jq(".svg_outer").addClass("svg_outer-fluid");
        }
        /*service page sticky Int*/
        if($jq("#ServicesFloatingBlock").size() > 0 && $jq(window).width() > 1024) {
            if($jq("#LHSMainContent").outerHeight() > $jq("#RHSMainContent").outerHeight())
                ServicesFloating();  
        }
        if($jq(window).width() >= 768) // For hidden in mobiles
            $jq("#floating-image").stick_in_parent();  
    });
});;
