<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Customers_category extends Model
{
    use SoftDeletes;


    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'customers_category';
    
    protected $fillable = [
          'name',
          'description',
         

         
    ];
    

    
    
}
