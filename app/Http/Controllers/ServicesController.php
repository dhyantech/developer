<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Customers_category;
use App\Solutions;
use App\Services;
use App\Customers_category_gallery;
use Redirect;
use Schema;
use Image;
use File;
use DB;
use App\Http\Requests\CreateRequest;
use App\Http\Requests\UpdateRequest;
use App\Http\Requests\UpdatePropertiesGalleryRequest;


class servicesController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	public function index($id)
	{
		$servicesRow = DB::table('services')
            ->leftjoin('services_gallery',
            	function($join)
                         {
                            $join->on('services.id', '=', 'services_gallery.services_id')
                            ->Where(function ($query) {
				                $query->whereNull('services_gallery.deleted_at')
				                      ->orWhere('services_gallery.deleted_at', '=',  '0000-00-00 00:00:00');
				             });

                         }
            	)
            ->select('services.*', 'services_gallery.image_path', 'services_gallery.sortorder', 'services_gallery.id as galId')
            ->where('services.id', '=', $id)
            
            ->get();

        $services = Customers_category::all();

        $strMenue = "";
        foreach($services as $servicesVal){
            $strMenue .= '<li class="first leaf menu-mlid-768"><a href="' . url('customers_category/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
        }

        $solutions = Solutions::all();

        $strSolutionsMenue = "";
        foreach($solutions as $solutionsVal){
            $strSolutionsMenue .= '<li class="first leaf menu-mlid-768"><a href="' . url('solutions/' . $solutionsVal['id']) . '">' . htmlentities($solutionsVal['name']) . '</a></li>';
        }

        $services = Services::all();

        $strServicesMenue['AMC'] = "";
        $strServicesMenue['PJT'] = "";
        $strServicesMenue['DSN'] = "";

        foreach($services as $servicesVal){

            if($servicesVal['type'] == 'AMC'){
                $strServicesMenue['AMC'] .= '<li class="leaf menu-mlid-785"><a href="' . url('services/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
            }

            if($servicesVal['type'] == 'PJT'){
                $strServicesMenue['PJT'] .= '<li class="leaf menu-mlid-785"><a href="' . url('services/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
            }

            if($servicesVal['type'] == 'DSN'){
                $strServicesMenue['DSN'] .= '<li class="leaf menu-mlid-785"><a href="' . url('services/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
            }
            
        }

       return view('services', compact('strMenue', 'servicesRow', 'strSolutionsMenue', 'strServicesMenue'));
	}

	

	

}