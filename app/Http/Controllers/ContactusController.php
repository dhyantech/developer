<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Solutions;
use App\Services;
use App\Customers_category;
use Redirect;
use Schema;

use App\Http\Requests\CreateRequest;
use App\Http\Requests\UpdateRequest;



class ContactusController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	public function index()
	{
        $customersCategory = Customers_category::all();

       $customersCategory = Customers_category::all();

        $strMenue = "";
        foreach($customersCategory as $customersCategoryVal){
            $strMenue .= '<li class="first leaf menu-mlid-768"><a href="' . url('customers_category/' . $customersCategoryVal['id']) . '">' . htmlentities($customersCategoryVal['name']) . '</a></li>';
        }

        $solutions = Solutions::all();

        $strSolutionsMenue = "";
        foreach($solutions as $solutionsVal){
            $strSolutionsMenue .= '<li class="first leaf menu-mlid-768"><a href="' . url('solutions/' . $solutionsVal['id']) . '">' . htmlentities($solutionsVal['name']) . '</a></li>';
        }

        $services = Services::all();

        $strServicesMenue['AMC'] = "";
        $strServicesMenue['PJT'] = "";
        $strServicesMenue['DSN'] = "";

        foreach($services as $servicesVal){

            if($servicesVal['type'] == 'AMC'){
                $strServicesMenue['AMC'] .= '<li class="leaf menu-mlid-785"><a href="' . url('services/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
            }

            if($servicesVal['type'] == 'PJT'){
                $strServicesMenue['PJT'] .= '<li class="leaf menu-mlid-785"><a href="' . url('services/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
            }

            if($servicesVal['type'] == 'DSN'){
                $strServicesMenue['DSN'] .= '<li class="leaf menu-mlid-785"><a href="' . url('services/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
            }
            
        }

		return view('contactus', compact('strMenue', 'strSolutionsMenue', 'strServicesMenue'));
	}

	

	

}