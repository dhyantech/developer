<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Solutions;
use App\Solutions_gallery;
use Redirect;
use Schema;
use Image;
use File;
use DB;
use App\Http\Requests\CreateRequest;
use App\Http\Requests\UpdateRequest;
use App\Http\Requests\UpdatePropertiesGalleryRequest;


class SolutionsController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	public function index($id)
	{
		$customersCategoryRow = DB::table('solutions')
            ->leftjoin('solutions_gallery',
            	function($join)
                         {
                            $join->on('solutions.id', '=', 'solutions_gallery.solutions_id')
                            ->Where(function ($query) {
				                $query->whereNull('solutions_gallery.deleted_at')
				                      ->orWhere('solutions_gallery.deleted_at', '=',  '0000-00-00 00:00:00');
				             });

                         }
            	)
            ->select('solutions.*', 'solutions_gallery.image_path', 'solutions_gallery.sortorder', 'solutions_gallery.id as galId')
            ->where('solutions.id', '=', $id)
            
            ->get();

        $services = Customers_category::all();

        $strMenue = "";
        foreach($services as $servicesVal){
            $strMenue .= '<li class="first leaf menu-mlid-768"><a href="' . url('customers_category/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
        }

        $solutions = Solutions::all();

        $strSolutionsMenue = "";
        foreach($solutions as $solutionsVal){
            $strSolutionsMenue .= '<li class="first leaf menu-mlid-768"><a href="' . url('solutions/' . $solutionsVal['id']) . '">' . htmlentities($solutionsVal['name']) . '</a></li>';
        }

        $services = Services::all();

        $strServicesMenue['AMC'] = "";
        $strServicesMenue['PJT'] = "";
        $strServicesMenue['DSN'] = "";

        foreach($services as $servicesVal){

            if($servicesVal['type'] == 'AMC'){
                $strServicesMenue['AMC'] .= '<li class="leaf menu-mlid-785"><a href="' . url('services/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
            }

            if($servicesVal['type'] == 'PJT'){
                $strServicesMenue['PJT'] .= '<li class="leaf menu-mlid-785"><a href="' . url('services/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
            }

            if($servicesVal['type'] == 'DSN'){
                $strServicesMenue['DSN'] .= '<li class="leaf menu-mlid-785"><a href="' . url('services/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
            }
            
        }

       return view('solutions', compact('strMenue', 'customersCategoryRow', 'strSolutionsMenue', 'strServicesMenue'));
	}

	

	

}