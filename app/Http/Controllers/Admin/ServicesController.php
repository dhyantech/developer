<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services;
use App\Services_gallery;
use Redirect;
use Schema;
use Image;
use File;
use DB;
use App\Http\Requests\CreateRequest;
use App\Http\Requests\UpdateRequest;
use App\Http\Requests\UpdatePropertiesGalleryRequest;


class ServicesController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	 public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
		$services = Services::all();
		return view('admin.services.index', compact('services'));
	}

	

	public function add()
	{
		return view('admin.services.add');
	}

	public function store(CreateRequest $request)
	{
	    
	    
		$insertData = Services::create($request->all());
		
		

		return redirect()->intended('admin/services');
	}

	public function edit($id)
	{
		$services = Services::find($id);
	   
	    return view('admin.services.edit', compact('services'));
	}

	public function gallery($id)
	{
		$services = DB::table('services')
            ->leftjoin('services_gallery',
            	function($join)
                         {
                            $join->on('services.id', '=', 'services_gallery.services_id')
                            ->Where(function ($query) {
				                $query->whereNull('services_gallery.deleted_at')
				                      ->orWhere('services_gallery.deleted_at', '=',  '0000-00-00 00:00:00');
				            });

                         }
            	)
            ->select('services.*', 'services_gallery.image_path', 'services_gallery.sortorder', 'services_gallery.id as galId')
            ->where('services.id', '=', $id)
            
            ->get();

       return view('admin.services.addGallery', compact('services'));
	}


	/**
	 * Update the specified services in storage.
     * @param UpdatePropertiesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateRequest $request)
	{
		$services = Services::findOrFail($id);

        
		

	   	$allRequest = $request->all();
	   	
	   	
	   	
	   	$services->update($allRequest);

		return redirect()->intended('admin/services');
	}


	/*
		Gallery store
	*/
	/**
	 * Update the specified services in storage.
     * @param UpdatePropertiesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function galleryStore($id, UpdateRequest $request)
	{
		$services = Services::findOrFail($id);
		$folderPathFull = 'uploads/services/' . str_replace(' ', '_', $services->name) . '_' . $services->id;
		$folderPathFullThumb = 'uploads/services/' . str_replace(' ', '_', $services->name) . '_' . $services->id . "/thumb";

		$folderPath = str_replace(' ', '_', $services->name) . '_' . $services->id . '/thumb';

		if (!file_exists(public_path($folderPathFull))) {
			mkdir(public_path($folderPathFull));
			mkdir(public_path($folderPathFullThumb));


			//$services->gallery = $folderPath;
			//$services->save();
		}
		
        
		if($request->hasFile('file') && $request->file('file')->isValid()){

			$objPropertyGalleryNewRow = new Services_gallery;
			$objPropertyGalleryNewRow->services_id = $services->id;
			$objPropertyGalleryNewRow->save();

	    	$displayPicture = $request->file('file');
	    	
	    	
	    	$fileName = str_replace( "." . $displayPicture->getClientOriginalExtension(), '', str_replace(' ', '_',$displayPicture->getClientOriginalName())) . "_CCG_" . $objPropertyGalleryNewRow->id . "_.". $displayPicture->getClientOriginalExtension();
	    	Image::make($displayPicture)->resize(200, 200)->save(public_path($folderPathFullThumb . '/' . $fileName));
	    	Image::make($displayPicture)->resize(1365, 456)->save(public_path($folderPathFull . '/' . $fileName));

	    	$objPropertyGalleryNewRow->image_path = $folderPath . '/'. $fileName;
	    	$objPropertyGalleryNewRow->save();
	   }

	   //return '';
	}

	/**
	 * Remove the specified services from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Services::destroy($id);

		return redirect()->intended('admin/services');
	}

	/**
	 * Remove the specified services from storage.
	 *
	 * @param  int  $id
	 */
	public function destroyGal($propId, $galId)
	{
		Services_gallery::destroy($galId);

		return redirect()->intended('admin/services/'. $propId . '/gallery');
	}

	

}