<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Aboutus;
use App\Aboutus_gallery;
use Redirect;
use Schema;
use Image;
use File;
use DB;
use App\Http\Requests\CreateRequest;
use App\Http\Requests\UpdateRequest;
use App\Http\Requests\UpdatePropertiesGalleryRequest;


class AboutUsController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	 public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
		$aboutUs = Aboutus::all();
		return view('admin.aboutus.index', compact('aboutUs'));
	}

	

	public function add()
	{
		return view('admin.aboutus.add');
	}

	public function store(CreateRequest $request)
	{
	    
	    
		$insertData = Aboutus::create($request->all());
		
		

		return redirect()->intended('admin/aboutUs');
	}

	public function edit($id)
	{
		$aboutUs = Aboutus::find($id);
	   
	    return view('admin.aboutus.edit', compact('aboutUs'));
	}

	public function gallery($id)
	{
		$aboutUs = DB::table('aboutus')
            ->leftjoin('aboutus_gallery',
            	function($join)
                         {
                            $join->on('aboutus.id', '=', 'aboutus_gallery.aboutus_id')
                            ->Where(function ($query) {
				                $query->whereNull('aboutus_gallery.deleted_at')
				                      ->orWhere('aboutus_gallery.deleted_at', '=',  '0000-00-00 00:00:00');
				            });

                         }
            	)
            ->select('aboutus.*', 'aboutus_gallery.image_path', 'aboutus_gallery.sortorder', 'aboutus_gallery.id as galId')
            ->where('aboutus.id', '=', $id)
            
            ->get();

       return view('admin.aboutus.addGallery', compact('aboutUs'));
	}


	/**
	 * Update the specified aboutUs in storage.
     * @param UpdatePropertiesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateRequest $request)
	{
		$aboutUs = Aboutus::findOrFail($id);

        
		

	   	$allRequest = $request->all();
	   	
	   	
	   	
	   	$aboutUs->update($allRequest);

		return redirect()->intended('admin/aboutUs');
	}


	/*
		Gallery store
	*/
	/**
	 * Update the specified aboutUs in storage.
     * @param UpdatePropertiesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function galleryStore($id, UpdateRequest $request)
	{
		$aboutUs = Aboutus::findOrFail($id);
		$folderPathFull = 'uploads/aboutus/' . str_replace(' ', '_', $aboutUs->name) . '_' . $aboutUs->id;
		$folderPathFullThumb = 'uploads/aboutus/' . str_replace(' ', '_', $aboutUs->name) . '_' . $aboutUs->id . "/thumb";

		$folderPath = str_replace(' ', '_', $aboutUs->name) . '_' . $aboutUs->id . '/thumb';

		if (!file_exists(public_path($folderPathFull))) {
			mkdir(public_path($folderPathFull));
			mkdir(public_path($folderPathFullThumb));


			//$aboutUs->gallery = $folderPath;
			//$aboutUs->save();
		}
		
        
		if($request->hasFile('file') && $request->file('file')->isValid()){

			$objPropertyGalleryNewRow = new aboutus_gallery;
			$objPropertyGalleryNewRow->aboutUs_id = $aboutUs->id;
			$objPropertyGalleryNewRow->save();

	    	$displayPicture = $request->file('file');
	    	
	    	
	    	$fileName = str_replace( "." . $displayPicture->getClientOriginalExtension(), '', str_replace(' ', '_',$displayPicture->getClientOriginalName())) . "_CCG_" . $objPropertyGalleryNewRow->id . "_.". $displayPicture->getClientOriginalExtension();
	    	Image::make($displayPicture)->resize(200, 200)->save(public_path($folderPathFullThumb . '/' . $fileName));
	    	Image::make($displayPicture)->resize(1365, 456)->save(public_path($folderPathFull . '/' . $fileName));

	    	$objPropertyGalleryNewRow->image_path = $folderPath . '/'. $fileName;
	    	$objPropertyGalleryNewRow->save();
	   }

	   //return '';
	}

	/**
	 * Remove the specified aboutUs from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Aboutus::destroy($id);

		return redirect()->intended('admin/aboutUs');
	}

	/**
	 * Remove the specified aboutUs from storage.
	 *
	 * @param  int  $id
	 */
	public function destroyGal($propId, $galId)
	{
		aboutus_gallery::destroy($galId);

		return redirect()->intended('admin/aboutUs/'. $propId . '/gallery');
	}

	

}