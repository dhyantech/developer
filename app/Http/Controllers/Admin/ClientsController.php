<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Clients;
use Redirect;
use Schema;
use Image;
use File;
use DB;
use App\Http\Requests\CreateRequest;
use App\Http\Requests\UpdateRequest;



class ClientsController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	 public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
		$clients = Clients::all();
		return view('admin.clients.index', compact('clients'));
	}

	public function add()
	{
		return view('admin.clients.add');
	}

	public function store(CreateRequest $request)
	{
	    
	    
		$insertData = Clients::create($request->all());
		
		if($request->hasFile('client_image') && $insertData->id > 0 && $request->file('client_image')->isValid()){

			$uploadPath = 'uploads/clients/';
			if (!file_exists(public_path($uploadPath))) {
				mkdir(public_path($uploadPath));

			}

	    	$displayPicture = $request->file('client_image');
	    	
	    	$fileName = str_replace( "." . $displayPicture->getClientOriginalExtension(), '', str_replace(' ', '_',$displayPicture->getClientOriginalName())) . "_CT_" . $insertData->id . "_.". $displayPicture->getClientOriginalExtension();
	    	$fileNameThumb = str_replace( "." . $displayPicture->getClientOriginalExtension(), '', str_replace(' ', '_',$displayPicture->getClientOriginalName())) . "_CT_TMB_" . $insertData->id . "_." . $displayPicture->getClientOriginalExtension();

	    	Image::make($displayPicture)->resize(210, 112)->save(public_path($uploadPath . $fileNameThumb));
	    	Image::make($displayPicture)->save(public_path($uploadPath . $fileName));

	    	$insertData->client_path = $fileNameThumb;
	    	$insertData->save();
	   }

		return redirect()->intended('admin/clients');
	}

	public function edit($id)
	{
		$clients = Clients::find($id);
	   
	    return view('admin.clients.edit', compact('clients'));
	}

	


	/**
	 * Update the specified clients in storage.
     * @param UpdatePropertiesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateRequest $request)
	{
		$clients = Clients::findOrFail($id);

        
		if($request->hasFile('client_image') && $clients->id > 0 && $request->file('client_image')->isValid()){

			$uploadPath = 'uploads/clients/';
	    	$displayPicture = $request->file('client_image');
	    	
	    	$fileName = str_replace( "." . $displayPicture->getClientOriginalExtension(), '', str_replace(' ', '_',$displayPicture->getClientOriginalName())) . "_CT_" . $clients->id . "_.". $displayPicture->getClientOriginalExtension();
	    	$fileNameThumb = str_replace( "." . $displayPicture->getClientOriginalExtension(), '', str_replace(' ', '_',$displayPicture->getClientOriginalName())) . "_CT_TMB_" . $clients->id . "_." . $displayPicture->getClientOriginalExtension();

	    	Image::make($displayPicture)->resize(210, 112)->save(public_path($uploadPath . $fileNameThumb));
	    	Image::make($displayPicture)->save(public_path($uploadPath . $fileName));

	    	$clients->client_path = $fileNameThumb;
	    	$clients->save();
	   }

	   	$allRequest = $request->all();
	   	
	   	
	   	
	   	$clients->update($allRequest);

		return redirect()->intended('admin/clients');
	}


	

	/**
	 * Remove the specified clients from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Clients::destroy($id);

		return redirect()->intended('admin/clients');
	}

	

	

}