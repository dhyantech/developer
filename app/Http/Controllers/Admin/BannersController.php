<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Banner;
use Redirect;
use Schema;
use Image;
use File;
use DB;
use App\Http\Requests\CreateRequest;
use App\Http\Requests\UpdateRequest;



class BannersController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	 public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
		$banners = Banner::all();
		return view('admin.banners.index', compact('banners'));
	}

	public function add()
	{
		return view('admin.banners.add');
	}

	public function store(CreateRequest $request)
	{
	    
	    
		$insertData = Banner::create($request->all());
		
		if($request->hasFile('banner_image') && $insertData->id > 0 && $request->file('banner_image')->isValid()){

			$uploadPath = 'uploads/banners/';
			$uploadThumbPath = 'uploads/banners/thumb/';
			if (!file_exists(public_path($uploadPath))) {
				mkdir(public_path($uploadPath));
				mkdir(public_path($uploadThumbPath));

			}

	    	$displayPicture = $request->file('banner_image');
	    	
	    	$fileName = str_replace( "." . $displayPicture->getClientOriginalExtension(), '', str_replace(' ', '_',$displayPicture->getClientOriginalName())) . "_BR_" . $insertData->id . "_.". $displayPicture->getClientOriginalExtension();
	    	

	    	Image::make($displayPicture)->resize(200, 200)->save(public_path($uploadThumbPath . $fileName));
	    	Image::make($displayPicture)->resize(1600, 800)->save(public_path($uploadPath . $fileName));

	    	$insertData->banner_path = $fileName;
	    	$insertData->save();
	   }

		return redirect()->intended('admin/banners');
	}

	public function edit($id)
	{
		$banners = Banner::find($id);
	   
	    return view('admin.banners.edit', compact('banners'));
	}

	


	/**
	 * Update the specified banners in storage.
     * @param UpdatePropertiesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateRequest $request)
	{
		$banners = Banner::findOrFail($id);

        
		if($request->hasFile('banner_image') && $banners->id > 0 && $request->file('banner_image')->isValid()){

			$uploadPath = 'uploads/banners/';
			$uploadThumbPath = 'uploads/banners/thumb/';
	    	$displayPicture = $request->file('banner_image');
	    	
	    	$fileName = str_replace( "." . $displayPicture->getClientOriginalExtension(), '', str_replace(' ', '_',$displayPicture->getClientOriginalName())) . "_BR_" . $banners->id . "_.". $displayPicture->getClientOriginalExtension();
	    	Image::make($displayPicture)->resize(200, 200)->save(public_path($uploadThumbPath . $fileName));
	    	Image::make($displayPicture)->resize(1600, 800)->save(public_path($uploadPath . $fileName));

	    	$banners->banner_path = $fileName;
	    	$banners->save();
	   }

	   	$allRequest = $request->all();
	   	
	   	
	   	
	   	$banners->update($allRequest);

		return redirect()->intended('admin/banners');
	}


	

	/**
	 * Remove the specified banners from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Banner::destroy($id);

		return redirect()->intended('admin/banners');
	}

	

	

}