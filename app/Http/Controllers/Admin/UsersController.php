<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

use Redirect;
use Schema;
use Image;
use File;
use DB;
use App\Http\Requests\CreateUsersRequest;
use App\Http\Requests\UpdateUsersRequest;



class UsersController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	 public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
		$users = User::all();
		return view('admin.users.index', compact('users'));
	}

	public function add()
	{
		return view('admin.users.add');
	}

	public function store(CreateUsersRequest $request)
	{
	    
	    $request->password = bcrypt($request->password);

		$insertData = User::create($request->all());
		
		return redirect()->intended('admin/users');
	}

	public function edit($id)
	{
		$users = User::find($id);
	   
	    return view('admin.users.edit', compact('users'));
	}

	


	/**
	 * Update the specified users in storage.
     * @param UpdateUsersRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateUsersRequest $request)
	{
		$users = User::findOrFail($id);

        
		

	   	$allRequest = $request->all();
	   	
	   	if(empty($allRequest['password'])){
	   		
	   		unset($allRequest['password']);
	   	}
	   	
	   	$users->update($allRequest);

		return redirect()->intended('admin/users');
	}


	

	/**
	 * Remove the specified users from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		User::destroy($id);

		return redirect()->intended('admin/users');
	}

	

	

}