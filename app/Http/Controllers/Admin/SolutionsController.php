<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Solutions;
use App\Solutions_gallery;
use Redirect;
use Schema;
use Image;
use File;
use DB;
use App\Http\Requests\CreateRequest;
use App\Http\Requests\UpdateRequest;
use App\Http\Requests\UpdatePropertiesGalleryRequest;


class solutionsController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	 public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
		$solutions = Solutions::all();
		return view('admin.solutions.index', compact('solutions'));
	}

	

	public function add()
	{
		return view('admin.solutions.add');
	}

	public function store(CreateRequest $request)
	{
	    
	    
		$insertData = Solutions::create($request->all());
		
		

		return redirect()->intended('admin/solutions');
	}

	public function edit($id)
	{
		$solutions = Solutions::find($id);
	   
	    return view('admin.solutions.edit', compact('solutions'));
	}

	public function gallery($id)
	{
		$solutions = DB::table('solutions')
            ->leftjoin('solutions_gallery',
            	function($join)
                         {
                            $join->on('solutions.id', '=', 'solutions_gallery.solutions_id')
                            ->Where(function ($query) {
				                $query->whereNull('solutions_gallery.deleted_at')
				                      ->orWhere('solutions_gallery.deleted_at', '=',  '0000-00-00 00:00:00');
				            });

                         }
            	)
            ->select('solutions.*', 'solutions_gallery.image_path', 'solutions_gallery.sortorder', 'solutions_gallery.id as galId')
            ->where('solutions.id', '=', $id)
            
            ->get();

       return view('admin.solutions.addGallery', compact('solutions'));
	}


	/**
	 * Update the specified solutions in storage.
     * @param UpdatePropertiesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateRequest $request)
	{
		$solutions = Solutions::findOrFail($id);

        
		

	   	$allRequest = $request->all();
	   	
	   	
	   	
	   	$solutions->update($allRequest);

		return redirect()->intended('admin/solutions');
	}


	/*
		Gallery store
	*/
	/**
	 * Update the specified solutions in storage.
     * @param UpdatePropertiesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function galleryStore($id, UpdateRequest $request)
	{
		$solutions = Solutions::findOrFail($id);
		$folderPathFull = 'uploads/solutions/' . str_replace(' ', '_', $solutions->name) . '_' . $solutions->id;
		$folderPathFullThumb = 'uploads/solutions/' . str_replace(' ', '_', $solutions->name) . '_' . $solutions->id . "/thumb";

		$folderPath = str_replace(' ', '_', $solutions->name) . '_' . $solutions->id . '/thumb';

		if (!file_exists(public_path($folderPathFull))) {
			mkdir(public_path($folderPathFull));
			mkdir(public_path($folderPathFullThumb));


			//$solutions->gallery = $folderPath;
			//$solutions->save();
		}
		
        
		if($request->hasFile('file') && $request->file('file')->isValid()){

			$objPropertyGalleryNewRow = new Solutions_gallery;
			$objPropertyGalleryNewRow->solutions_id = $solutions->id;
			$objPropertyGalleryNewRow->save();

	    	$displayPicture = $request->file('file');
	    	
	    	
	    	$fileName = str_replace( "." . $displayPicture->getClientOriginalExtension(), '', str_replace(' ', '_',$displayPicture->getClientOriginalName())) . "_CCG_" . $objPropertyGalleryNewRow->id . "_.". $displayPicture->getClientOriginalExtension();
	    	Image::make($displayPicture)->resize(200, 200)->save(public_path($folderPathFullThumb . '/' . $fileName));
	    	Image::make($displayPicture)->resize(1365, 456)->save(public_path($folderPathFull . '/' . $fileName));

	    	$objPropertyGalleryNewRow->image_path = $folderPath . '/'. $fileName;
	    	$objPropertyGalleryNewRow->save();
	   }

	   //return '';
	}

	/**
	 * Remove the specified solutions from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Solutions::destroy($id);

		return redirect()->intended('admin/solutions');
	}

	/**
	 * Remove the specified solutions from storage.
	 *
	 * @param  int  $id
	 */
	public function destroyGal($propId, $galId)
	{
		Solutions_gallery::destroy($galId);

		return redirect()->intended('admin/solutions/'. $propId . '/gallery');
	}

	

}