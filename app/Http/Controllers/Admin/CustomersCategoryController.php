<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Customers_category;
use App\Customers_category_gallery;
use Redirect;
use Schema;
use Image;
use File;
use DB;
use App\Http\Requests\CreateRequest;
use App\Http\Requests\UpdateRequest;
use App\Http\Requests\UpdatePropertiesGalleryRequest;


class customersCategoryController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	 public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
		$customersCategory = Customers_category::all();
		return view('admin.customers_category.index', compact('customersCategory'));
	}

	

	public function add()
	{
		return view('admin.customers_category.add');
	}

	public function store(CreateRequest $request)
	{
	    
	    
		$insertData = Customers_category::create($request->all());
		
		

		return redirect()->intended('admin/customers_category');
	}

	public function edit($id)
	{
		$customersCategory = Customers_category::find($id);
	   
	    return view('admin.customers_category.edit', compact('customersCategory'));
	}

	public function gallery($id)
	{
		$customersCategory = DB::table('customers_category')
            ->leftjoin('customers_category_gallery',
            	function($join)
                         {
                            $join->on('customers_category.id', '=', 'customers_category_gallery.customers_category_id')
                            ->Where(function ($query) {
				                $query->whereNull('customers_category_gallery.deleted_at')
				                      ->orWhere('customers_category_gallery.deleted_at', '=',  '0000-00-00 00:00:00');
				            });

                         }
            	)
            ->select('customers_category.*', 'customers_category_gallery.image_path', 'customers_category_gallery.sortorder', 'customers_category_gallery.id as galId')
            ->where('customers_category.id', '=', $id)
            
            ->get();

       return view('admin.customers_category.addGallery', compact('customersCategory'));
	}


	/**
	 * Update the specified customersCategory in storage.
     * @param UpdatePropertiesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateRequest $request)
	{
		$customersCategory = Customers_category::findOrFail($id);

        
		

	   	$allRequest = $request->all();
	   	
	   	
	   	
	   	$customersCategory->update($allRequest);

		return redirect()->intended('admin/customers_category');
	}


	/*
		Gallery store
	*/
	/**
	 * Update the specified customersCategory in storage.
     * @param UpdatePropertiesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function galleryStore($id, UpdateRequest $request)
	{
		$customersCategory = Customers_category::findOrFail($id);
		$folderPathFull = 'uploads/customers_category/' . str_replace(' ', '_', $customersCategory->name) . '_' . $customersCategory->id;
		$folderPathFullThumb = 'uploads/customers_category/' . str_replace(' ', '_', $customersCategory->name) . '_' . $customersCategory->id . "/thumb";

		$folderPath = str_replace(' ', '_', $customersCategory->name) . '_' . $customersCategory->id . '/thumb';

		if (!file_exists(public_path($folderPathFull))) {
			mkdir(public_path($folderPathFull));
			mkdir(public_path($folderPathFullThumb));


			//$customersCategory->gallery = $folderPath;
			//$customersCategory->save();
		}
		
        
		if($request->hasFile('file') && $request->file('file')->isValid()){

			$objPropertyGalleryNewRow = new Customers_category_gallery;
			$objPropertyGalleryNewRow->customers_category_id = $customersCategory->id;
			$objPropertyGalleryNewRow->save();

	    	$displayPicture = $request->file('file');
	    	
	    	
	    	$fileName = str_replace( "." . $displayPicture->getClientOriginalExtension(), '', str_replace(' ', '_',$displayPicture->getClientOriginalName())) . "_CCG_" . $objPropertyGalleryNewRow->id . "_.". $displayPicture->getClientOriginalExtension();
	    	Image::make($displayPicture)->resize(200, 200)->save(public_path($folderPathFullThumb . '/' . $fileName));
	    	Image::make($displayPicture)->resize(1365, 456)->save(public_path($folderPathFull . '/' . $fileName));

	    	$objPropertyGalleryNewRow->image_path = $folderPath . '/'. $fileName;
	    	$objPropertyGalleryNewRow->save();
	   }

	   //return '';
	}

	/**
	 * Remove the specified customersCategory from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Customers_category::destroy($id);

		return redirect()->intended('admin/customers_category');
	}

	/**
	 * Remove the specified customersCategory from storage.
	 *
	 * @param  int  $id
	 */
	public function destroyGal($propId, $galId)
	{
		Customers_category_gallery::destroy($galId);

		return redirect()->intended('admin/customers_category/'. $propId . '/gallery');
	}

	

}