<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Banner;
use App\Customers_category;
use App\Solutions;
use App\Services;
use App\Clients;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::all();
        $clients = Clients::all();
        $customersCategory = Customers_category::all();

        $strMenue = "";
        foreach($customersCategory as $customersCategoryVal){
            $strMenue .= '<li class="first leaf menu-mlid-768"><a href="' . url('customers_category/' . $customersCategoryVal['id']) . '">' . htmlentities($customersCategoryVal['name']) . '</a></li>';
        }

        $solutions = Solutions::all();

        $strSolutionsMenue = "";
        foreach($solutions as $solutionsVal){
            $strSolutionsMenue .= '<li class="first leaf menu-mlid-768"><a href="' . url('solutions/' . $solutionsVal['id']) . '">' . htmlentities($solutionsVal['name']) . '</a></li>';
        }

        $services = Services::all();

        $strServicesMenue['AMC'] = "";
        $strServicesMenue['PJT'] = "";
        $strServicesMenue['DSN'] = "";

        foreach($services as $servicesVal){

            if($servicesVal['type'] == 'AMC'){
                $strServicesMenue['AMC'] .= '<li class="leaf menu-mlid-785"><a href="' . url('services/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
            }

            if($servicesVal['type'] == 'PJT'){
                $strServicesMenue['PJT'] .= '<li class="leaf menu-mlid-785"><a href="' . url('services/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
            }

            if($servicesVal['type'] == 'DSN'){
                $strServicesMenue['DSN'] .= '<li class="leaf menu-mlid-785"><a href="' . url('services/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
            }
            
        }

        return view('home', compact('banners', 'clients', 'strMenue', 'strSolutionsMenue', 'strServicesMenue'));

    }
}
