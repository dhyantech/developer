<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Customers_category;
use App\Solutions;
use App\Customers_category_gallery;
use Redirect;
use App\Services;
use Schema;
use Image;
use File;
use DB;
use App\Http\Requests\CreateRequest;
use App\Http\Requests\UpdateRequest;
use App\Http\Requests\UpdatePropertiesGalleryRequest;


class customersCategoryController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	public function index($id)
	{
		$customersCategoryRow = DB::table('customers_category')
            ->leftjoin('customers_category_gallery',
            	function($join)
                         {
                            $join->on('customers_category.id', '=', 'customers_category_gallery.customers_category_id')
                            ->Where(function ($query) {
				                $query->whereNull('customers_category_gallery.deleted_at')
				                      ->orWhere('customers_category_gallery.deleted_at', '=',  '0000-00-00 00:00:00');
				             });

                         }
            	)
            ->select('customers_category.*', 'customers_category_gallery.image_path', 'customers_category_gallery.sortorder', 'customers_category_gallery.id as galId')
            ->where('customers_category.id', '=', $id)
            
            ->get();

        $customersCategory = Customers_category::all();

        $strMenue = "";
        foreach($customersCategory as $customersCategoryVal){
            $strMenue .= '<li class="first leaf menu-mlid-768"><a href="' . url('customers_category/' . $customersCategoryVal['id']) . '">' . htmlentities($customersCategoryVal['name']) . '</a></li>';
        }

        $solutions = Solutions::all();

        $strSolutionsMenue = "";
        foreach($solutions as $solutionsVal){
            $strSolutionsMenue .= '<li class="first leaf menu-mlid-768"><a href="' . url('solutions/' . $solutionsVal['id']) . '">' . htmlentities($solutionsVal['name']) . '</a></li>';
        }

        $services = Services::all();

        $strServicesMenue['AMC'] = "";
        $strServicesMenue['PJT'] = "";
        $strServicesMenue['DSN'] = "";

        foreach($services as $servicesVal){

            if($servicesVal['type'] == 'AMC'){
                $strServicesMenue['AMC'] .= '<li class="leaf menu-mlid-785"><a href="' . url('services/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
            }

            if($servicesVal['type'] == 'PJT'){
                $strServicesMenue['PJT'] .= '<li class="leaf menu-mlid-785"><a href="' . url('services/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
            }

            if($servicesVal['type'] == 'DSN'){
                $strServicesMenue['DSN'] .= '<li class="leaf menu-mlid-785"><a href="' . url('services/' . $servicesVal['id']) . '">' . htmlentities($servicesVal['name']) . '</a></li>';
            }
            
        }

       return view('customers_category', compact('strMenue', 'customersCategoryRow', 'strSolutionsMenue', 'strServicesMenue'));
	}

	

	

}