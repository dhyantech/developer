<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/




Route::group(['middleware' => ['web']], function () {

    
	Route::auth();

	//Front-end
	Route::get('/', 'HomeController@index');
	Route::get('/customers_category/{id}', 'CustomersCategoryController@index');
	Route::get('/solutions/{id}', 'SolutionsController@index');
	Route::get('/contactus', 'ContactusController@index');
	Route::get('/aboutus', 'AboutUsController@index');
	Route::get('/services/{id}', 'ServicesController@index');

    //Admin
	Route::get('admin', 'Admin\DashboardController@index');

	//Users
	Route::get('admin/users', 'Admin\UsersController@index');
	Route::get('admin/users/add', 'Admin\UsersController@add');
	Route::get('admin/users/{id}/edit', 'Admin\UsersController@edit');
	Route::get('admin/users/{id}/delete', 'Admin\UsersController@destroy');

	Route::post('admin/users/store', 'Admin\UsersController@store');
	Route::post('admin/users/update/{id}', 'Admin\UsersController@update');

	//Banners
	Route::get('admin/banners', 'Admin\BannersController@index');
	Route::get('admin/banners/add', 'Admin\BannersController@add');
	Route::get('admin/banners/{id}/edit', 'Admin\BannersController@edit');
	Route::get('admin/banners/{id}/delete', 'Admin\BannersController@destroy');

	Route::post('admin/banners/store', 'Admin\BannersController@store');
	Route::post('admin/banners/update/{id}', 'Admin\BannersController@update');

	//Clients
	Route::get('admin/clients', 'Admin\ClientsController@index');
	Route::get('admin/clients/add', 'Admin\ClientsController@add');
	Route::get('admin/clients/{id}/edit', 'Admin\ClientsController@edit');
	Route::get('admin/clients/{id}/delete', 'Admin\ClientsController@destroy');

	Route::post('admin/clients/store', 'Admin\ClientsController@store');
	Route::post('admin/clients/update/{id}', 'Admin\ClientsController@update');

	//Customer Category
	Route::get('admin/customers_category', 'Admin\CustomersCategoryController@index');
	Route::get('admin/customers_category/add', 'Admin\CustomersCategoryController@add');
	Route::get('admin/customers_category/{id}/edit', 'Admin\CustomersCategoryController@edit');
	Route::get('admin/customers_category/{id}/delete', 'Admin\CustomersCategoryController@destroy');

	Route::get('admin/customers_category/{id}/gallery', 'Admin\CustomersCategoryController@gallery');
	Route::get('admin/customers_category/{propId}/deleteGallery/{galId}', 'Admin\CustomersCategoryController@destroyGal');

	Route::post('admin/customers_category/gallery_store/{id}', 'Admin\CustomersCategoryController@galleryStore');
	Route::post('admin/customers_category/store', 'Admin\CustomersCategoryController@store');
	Route::post('admin/customers_category/update/{id}', 'Admin\CustomersCategoryController@update');

	//Solutions
	Route::get('admin/solutions', 'Admin\SolutionsController@index');
	Route::get('admin/solutions/add', 'Admin\SolutionsController@add');
	Route::get('admin/solutions/{id}/edit', 'Admin\SolutionsController@edit');
	Route::get('admin/solutions/{id}/delete', 'Admin\SolutionsController@destroy');

	Route::get('admin/solutions/{id}/gallery', 'Admin\SolutionsController@gallery');
	Route::get('admin/solutions/{propId}/deleteGallery/{galId}', 'Admin\SolutionsController@destroyGal');

	Route::post('admin/solutions/gallery_store/{id}', 'Admin\SolutionsController@galleryStore');
	Route::post('admin/solutions/store', 'Admin\SolutionsController@store');
	Route::post('admin/solutions/update/{id}', 'Admin\SolutionsController@update');

	//Services
	Route::get('admin/services', 'Admin\ServicesController@index');
	Route::get('admin/services/add', 'Admin\ServicesController@add');
	Route::get('admin/services/{id}/edit', 'Admin\ServicesController@edit');
	Route::get('admin/services/{id}/delete', 'Admin\ServicesController@destroy');

	Route::get('admin/services/{id}/gallery', 'Admin\ServicesController@gallery');
	Route::get('admin/services/{propId}/deleteGallery/{galId}', 'Admin\ServicesController@destroyGal');

	Route::post('admin/services/gallery_store/{id}', 'Admin\ServicesController@galleryStore');
	Route::post('admin/services/store', 'Admin\ServicesController@store');
	Route::post('admin/services/update/{id}', 'Admin\ServicesController@update');

	//About Us
	Route::get('admin/aboutUs', 'Admin\AboutUsController@index');
	Route::get('admin/aboutUs/add', 'Admin\AboutUsController@add');
	Route::get('admin/aboutUs/{id}/edit', 'Admin\AboutUsController@edit');
	Route::get('admin/aboutUs/{id}/delete', 'Admin\AboutUsController@destroy');

	Route::get('admin/aboutUs/{id}/gallery', 'Admin\AboutUsController@gallery');
	Route::get('admin/aboutUs/{propId}/deleteGallery/{galId}', 'Admin\AboutUsController@destroyGal');

	Route::post('admin/aboutUs/gallery_store/{id}', 'Admin\AboutUsController@galleryStore');
	Route::post('admin/aboutUs/store', 'Admin\AboutUsController@store');
	Route::post('admin/aboutUs/update/{id}', 'Admin\AboutUsController@update');
	


	
	
	
});




